#!/bin/bash

echo "Setting up PDBe Graph API environment"
echo "Usage: ./setup.sh"
echo "Arguments:"
echo "1. Environment dev/prod_hh/prod_hx (optional), considered as local setup if no args passed"

environment=$1

if [[ $environment == '' ]]; then
    environment="local"
    echo "No args passed, assuming local setup for development purposes"
fi

echo "Setting up ${environment} environment"

CURR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "Creating Python environment"
# if [[ $environment == 'local' ]]; then
#     python3 -m venv $CURR_DIR/venv
# else
#     python3.6 -m venv $CURR_DIR/venv
# fi

rm -rf $CURR_DIR/venv
python3.9 -m venv $CURR_DIR/venv

if [[ $environment == 'dev' ]]; then
    git checkout development
fi

python3.9.9 -m venv $CURR_DIR/venv

source $CURR_DIR/venv/bin/activate

echo "Upgrade pip and setuptools"
pip install --upgrade setuptools pip

if [[ $environment == 'local' ]]; then
    pip install mod_wsgi
fi

echo "Install project specific Python modules"
pip install -r $CURR_DIR/requirements.txt

if [[ $environment == 'int' ]]; then
    pip install -r $CURR_DIR/requirements.dev.txt
fi

if [[ $environment != 'local' ]]; then
    echo "Cloning pdbe-graph-api-config to get config"
    git clone git@gitlab.ebi.ac.uk:pdbe-kb/pdbe-graph-api-config.git
    cp pdbe-graph-api-config/${environment}/.env .
    cp pdbe-graph-api-config/${environment}/apidoc.json apidoc.json
else
    mv local_apidoc.json apidoc.json
fi

deactivate 

echo "Creating API documentation"
npm install
./node_modules/apidoc/bin/apidoc -i pdbegraphapi/ -o pdbe_doc -t doc_template
