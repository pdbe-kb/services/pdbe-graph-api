#!/usr/bin/env python

import bottle

from pdbegraphapi.amino_acid_codes import amino_acid_codes
from pdbegraphapi.neo4j_model import run_query


from pdbegraphapi import request_handler
from pdbegraphapi.config import rePDBid, reSlashOrNot

print('[RESIDUE] Starting')

app = bottle.Bottle()

app.install(request_handler)

print('[RESIDUE] Loaded')

"""
@api {get} residue_mapping/:pdbId/:entityId/:residueNumber Get annotations for a PDB Residue
@apiName GetPDBResidueAnnotations
@apiGroup Residue
@apiDescription Get mappings (as assigned by the SIFTS process) for a PDB Residue to UniProt, Pfam, InterPro, CATH, SCOP, IntEnz, GO, Ensembl and HMMER accessions (and vice versa).
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID
@apiParam {String} entityId=1 PDB Entity ID
@apiParam {String} residueNumber=30 PDB Residue Number

@apiUse GenericResidueTerms

@apiExample {json=./examples/success/mappings_for_residue.json} apiSuccessExample Example success response JSON
"""
@app.get('/' +rePDBid +'/<entity_id>/<residue_number>'+reSlashOrNot)
def get_mappings_for_residue_api(pdbid, entity_id, residue_number):

    entry_id = pdbid.strip("'")
    entity_id = entity_id.strip("'")
    residue_number = residue_number.strip("'")

    api_result = {
        entry_id: [{
            "entity_id": int(entity_id),
            "chains": []
        }]
    }

    unp_result, unp_status = get_mappings_for_residue_uniprot(
        entry_id, entity_id, residue_number)
    pfam_result, pfam_status = get_mappings_for_residue_pfam(
        entry_id, entity_id, residue_number)
    interpro_result, interpro_status = get_mappings_for_residue_interpro(
        entry_id, entity_id, residue_number)
    cath_result, cath_status = get_mappings_for_residue_cath(
        entry_id, entity_id, residue_number)
    scop_result, scop_status = get_mappings_for_residue_scop(
        entry_id, entity_id, residue_number)
    binding_sites_result, binding_sites_status = get_mappings_for_residue_binding_site(
        entry_id, entity_id, residue_number, False)

    if(unp_status != 200 and pfam_status != 200 and interpro_status != 200 and cath_status != 200 and scop_status != 200 and binding_sites_status != 200):
        response_status = unp_status
    else:
        response_status = 200

    if response_status == 404:
        bottle.response.status = 404
        return api_result

    chains, response_status = get_basic_residue_details(entry_id, entity_id, residue_number)

    modified_chains = []
    for chain in chains:

        modified_residues = []

        for residue in chain["residues"]:

            residue["features"] = {}
            residue["features"]["UniProt"] = unp_result
            residue["features"]["Pfam"] = pfam_result
            residue["features"]["InterPro"] = interpro_result
            residue["features"]["CATH"] = cath_result
            residue["features"]["SCOP"] = scop_result
            residue["features"]["binding_sites"] = binding_sites_result
            residue["features"]["FunPDBe"], funpdbe_status = get_funpdbe_annotation(entry_id, entity_id, chain["auth_asym_id"], residue_number)

            modified_residues.append(residue)

        chain["residues"] = modified_residues
        modified_chains.append(chain)

    api_result[entry_id][0]["chains"] = sorted(modified_chains, key=lambda x: x["auth_asym_id"])

    bottle.response.status = 200
    return api_result


"""
@api {get} residue_mapping/:pdbId/:entityId/:residueStart/:residueEnd Get annotations for a PDB Residue range
@apiName GetPDBResidueRangeAnnotations
@apiGroup Residue
@apiDescription Get mappings (as assigned by the SIFTS process) for a PDB Residue range to UniProt, Pfam, InterPro, CATH, SCOP, IntEnz, GO, Ensembl and HMMER accessions (and vice versa).
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID
@apiParam {String} entityId=1 PDB Entity ID
@apiParam {String} residueStart=30 Start PDB Residue Number
@apiParam {String} residueEnd=40 End PDB Residue Number

@apiUse GenericResidueTerms

@apiExample {json=./examples/success/mappings_for_residue_range.json} apiSuccessExample Example success response JSON
"""
@app.get('/' +rePDBid +'/<entity_id>/<residue_start>/<residue_end>')
def get_mappings_for_residue_range(pdbid, entity_id, residue_start, residue_end):

    entry_id = pdbid.strip("'")
    entity_id = entity_id.strip("'")
    residue_start = residue_start.strip("'")
    residue_end = residue_end.strip("'")

    api_result = {
        entry_id: [{
            "entity_id": int(entity_id),
            "chains": []
        }]
    }

    dict_chains = {}
    response_status_list = []

    for residue_number in range(int(residue_start), int(residue_end) + 1):

        residue_number = str(residue_number)

        unp_result, unp_status = get_mappings_for_residue_uniprot(
            entry_id, entity_id, residue_number)
        pfam_result, pfam_status = get_mappings_for_residue_pfam(
            entry_id, entity_id, residue_number)
        interpro_result, interpro_status = get_mappings_for_residue_interpro(
            entry_id, entity_id, residue_number)
        cath_result, cath_status = get_mappings_for_residue_cath(
            entry_id, entity_id, residue_number)
        scop_result, scop_status = get_mappings_for_residue_scop(
            entry_id, entity_id, residue_number)
        binding_sites_result, binding_sites_status = get_mappings_for_residue_binding_site(
            entry_id, entity_id, residue_number, False)

        if(unp_status != 200 and pfam_status != 200 and interpro_status != 200 and cath_status != 200 and scop_status != 200 and binding_sites_status != 200):
            response_status = unp_status
        else:
            response_status = 200

        response_status_list.append(response_status)

        chains, response_status = get_basic_residue_details(
            entry_id, entity_id, residue_number)

        for chain in chains:

            auth_asym_id = chain["auth_asym_id"]
            struct_asym_id = chain["struct_asym_id"]
            chain_key = (auth_asym_id, struct_asym_id)

            if dict_chains.get(chain_key) is None:
                dict_chains[chain_key] = {
                    "auth_asym_id": auth_asym_id,
                    "struct_asym_id": struct_asym_id,
                    "residues": []
                }

            for residue in chain["residues"]:

                residue["features"] = {}
                residue["features"]["UniProt"] = unp_result
                residue["features"]["Pfam"] = pfam_result
                residue["features"]["InterPro"] = interpro_result
                residue["features"]["CATH"] = cath_result
                residue["features"]["SCOP"] = scop_result
                residue["features"]["binding_sites"] = binding_sites_result
                residue["features"]["FunPDBe"], funpdbe_status = get_funpdbe_annotation(entry_id, entity_id, chain["auth_asym_id"], residue_number)

                dict_chains[chain_key]["residues"].append(residue)

    # check if atleast a single residue got a feature, if all residues got failed response, mark response as 404
    if 200 not in response_status_list:
        bottle.response.status = 404

        return {
            entry_id: [{
            "entity_id": int(entity_id),
            "chains": []
            }]
        }

    for key in dict_chains.keys():
        api_result[entry_id][0]["chains"].append(dict_chains[key])

    api_result[entry_id][0]["chains"] = sorted(api_result[entry_id][0]["chains"], key=lambda x: x["auth_asym_id"])

    bottle.response.status = 200
    return api_result


def get_mappings_for_residue_uniprot(entry_id, entity_id, residue_number):

    query = """
    MATCH (entry:Entry {ID:$entryId})-[:HAS_ENTITY]->(entity:Entity {ID:$entityId})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue {ID:$residueNumber})-[:MAP_TO_UNIPROT_RESIDUE]->
    (unp_res:UNPResidue)<-[:HAS_UNP_RESIDUE]-(unp:UniProt)
    RETURN unp.ACCESSION as unp_accession, unp.NAME as unp_name, unp_res.ID as unp_res, pdb_res.CHEM_COMP_ID as pdb_three_letter_code, unp_res.ONE_LETTER_CODE as unp_one_letter_code
    """

    result = run_query(query, parameters={
        'entryId': str(entry_id), 'entityId': str(entity_id), 'residueNumber': residue_number
    })

    if(len(result) == 0):
        return {}, 404

    final_result = {}

    for unp in result:
        
        temp_one_letter_code = unp['pdb_three_letter_code']

        if amino_acid_codes.get(temp_one_letter_code) is not None:
            pdb_one_letter_code = amino_acid_codes[unp['pdb_three_letter_code']][0]
        else:
            pdb_one_letter_code = temp_one_letter_code

        final_result[unp['unp_accession']] = {
            "identifier": unp['unp_name'],
            "name": unp['unp_name'],
            "unp_residue_number": int(unp["unp_res"]),
            "unp_one_letter_code": unp['unp_one_letter_code'],
            "pdb_one_letter_code": pdb_one_letter_code
        }

    return final_result, 200


def get_mappings_for_residue_pfam(entry_id, entity_id, residue_number):

    query = """
    MATCH (entry:Entry {ID:$entryId})-[:HAS_ENTITY]->(entity:Entity {ID:$entityId})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue {ID:$residueNumber})-[:MAP_TO_UNIPROT_RESIDUE]->
    (unp_res:UNPResidue)-[:IS_IN_PFAM]->(pfam:Pfam)
    RETURN pfam.PFAM_ACCESSION as pfam_accession, pfam.NAME as pfam_name, pfam.DESCRIPTION as pfam_desc
    """

    result = run_query(query, parameters={
        'entryId': str(entry_id), 'entityId': str(entity_id), 'residueNumber': residue_number
    })

    if(len(result) == 0):
        return {}, 404

    final_result = {}

    for pfam in result:

        final_result[pfam['pfam_accession']] = {
            "identifier": pfam['pfam_name'],
            "name": pfam['pfam_name'],
            "description": pfam['pfam_desc']
        }

    return final_result, 200


def get_mappings_for_residue_interpro(entry_id, entity_id, residue_number):

    query = """
    MATCH (entry:Entry {ID:$entryId})-[:HAS_ENTITY]->(entity:Entity {ID:$entityId})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue {ID:$residueNumber})-[:IS_IN_INTERPRO]->(interpro:Interpro)
    RETURN interpro.INTERPRO_ACCESSION as interpro_accession, interpro.NAME as interpro_name
    """

    result = run_query(query, parameters={
        'entryId': str(entry_id), 'entityId': str(entity_id), 'residueNumber': residue_number
    })

    if(len(result) == 0):
        return {}, 404

    final_result = {}

    for interpro in result:
        final_result[interpro['interpro_accession']] = {
            "identifier": interpro['interpro_name'],
            "name": interpro['interpro_name']
        }

    return final_result, 200


def get_mappings_for_residue_cath(entry_id, entity_id, residue_number):

    query = """
    MATCH (entry:Entry {ID:$entryId})-[:HAS_ENTITY]->(entity:Entity {ID:$entityId})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue {ID:$residueNumber})-[:IS_IN_CATH_DOMAIN]->(cath:CATH)
    RETURN cath.ARCH as arch, cath.CATHCODE as cathcode, cath.CLASS as class, cath.DOMAIN as domain, cath.HOMOL as homol, cath.TOPOL as topol, cath.NAME as name
    """

    result = run_query(query, parameters={
        'entryId': str(entry_id), 'entityId': str(entity_id), 'residueNumber': residue_number
    })

    if(len(result) == 0):
        return {}, 404

    final_result = {}

    for cath in result:
        final_result[cath['cathcode']] = {
            "homology": cath['homol'],
            "topology": cath['topol'],
            "architecture": cath['arch'],
            "identifier": cath['topol'],
            "class": cath['class'],
            "name": cath['name']
        }

    return final_result, 200


def get_mappings_for_residue_scop(entry_id, entity_id, residue_number):

    query = """
    MATCH (entry:Entry {ID:$entryId})-[:HAS_ENTITY]->(entity:Entity {ID:$entityId})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue {ID:$residueNumber})-[relation:IS_IN_SCOP_DOMAIN]->(scop:SCOP)
    OPTIONAL MATCH (superfamily:SCOP {SUNID: relation.SUPERFAMILY_ID})
    OPTIONAL MATCH (fold:SCOP {SUNID: relation.FOLD_ID})
    OPTIONAL MATCH (class:SCOP {SUNID: relation.CLASS_ID})
    RETURN  distinct scop.SUNID as sunid, scop.DESCRIPTION as desc, superfamily.SUNID as super_sunid, superfamily.DESCRIPTION as super_desc, 
    fold.SUNID as fold_sunid, fold.DESCRIPTION as fold_desc, class.SUNID as class_sunid, class.DESCRIPTION as class_desc, scop.SCCS as sccs, 
    relation.SCOP_ID as scop_id, relation.AUTH_ASYM_ID as chain_id
    """

    result = run_query(query, parameters={
        'entryId': str(entry_id), 'entityId': str(entity_id), 'residueNumber': str(residue_number)
    })

    if(len(result) == 0):
        return {}, 404

    final_result = {}
    sunid_mapping = {}

    for scop in result:

        if sunid_mapping.get(scop['sunid']) is None:
            sunid_mapping[scop['sunid']] = []

        sunid_mapping[scop['sunid']].append(
            (scop['scop_id'], scop['chain_id']))

        final_result[scop['sunid']] = {
            "superfamily": {
                "sunid": scop['super_sunid'],
                "description": scop['super_desc']
            },
            "sccs": scop['sccs'],
            "fold": {
                "sunid": scop['fold_sunid'],
                "description": scop['fold_desc']
            },
            "identifier": scop['desc'],
            "class": {
                "sunid": scop['class_sunid'],
                "description": scop['class_desc']
            },
            "description": scop['desc'],
            "mappings": []
        }

    for sunid in sunid_mapping.keys():
        for mapping in sunid_mapping[sunid]:
            scop_id, chain_id = mapping

            final_result[sunid]['mappings'].append({
                "scop_id": scop_id,
                "chain_id": chain_id,
                "struct_asym_id": chain_id
            })

    return final_result, 200


def get_mappings_for_residue_binding_site(entry_id, entity_id, residue_number, site_residues):

    query = None
    if(site_residues is True):
        query = """
        MATCH (entry:Entry {ID:$entry_id})-[:HAS_ENTITY]->(entity:Entity {ID:$entity_id})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue {ID:$residue_number})-[res_relation:IS_IN_BINDING_SITE]->
        (site:BindingSite)
        WITH site, pdb_res
        OPTIONAL MATCH (lig_chem_comp:ChemicalComponent)<-[:IS_A]-(ligand_entity:Entity)-[ligand_entity_relation:IS_AN_INSTANCE_OF]->(ligand_chain:BoundLigand)-[ligand_relation:IS_IN_BINDING_SITE]->(site)
        OPTIONAL MATCH (site)-[bound_relation:BOUNDED_BY]->(boundligand_chain:BoundLigand)<-[boundligand_entity_relation:IS_AN_INSTANCE_OF]-(boundligand_entity:Entity)-[:IS_A]->(boundlig_chem_comp:ChemicalComponent)
        OPTIONAL MATCH (site)<-[res_all_relation:IS_IN_BINDING_SITE]-(pdb_res_all:PDBResidue)<-[:HAS_PDB_RESIDUE]-(pdb_res_all_entity:Entity) WHERE pdb_res_all.ID <> $residue_number
        RETURN site.ID, site.DETAILS, site.EVIDENCE_CODE, pdb_res_all.ID, pdb_res_all.CHEM_COMP_ID, res_all_relation.AUTH_ASYM_ID, res_all_relation.STRUCT_ASYM_ID, 
        res_all_relation.AUTH_SEQ_ID, pdb_res_all_entity.ID, res_all_relation.SYMMETRY_SYMBOL, boundlig_chem_comp.ID, boundlig_chem_comp.NAME, boundlig_chem_comp.FORMULA, boundligand_chain.AUTH_ASYM_ID, boundligand_chain.STRUCT_ASYM_ID, 
        boundligand_chain.AUTH_SEQ_ID, boundligand_chain.PDB_INS_CODE, boundligand_entity.ID, boundligand_chain.ID, lig_chem_comp.ID, lig_chem_comp.NAME, lig_chem_comp.FORMULA, ligand_chain.AUTH_ASYM_ID, ligand_chain.AUTH_SEQ_ID, ligand_chain.PDB_INS_CODE, 
        ligand_chain.STRUCT_ASYM_ID, ligand_relation.SYMMETRY_SYMBOL, ligand_entity.ID, ligand_chain.ID, pdb_res.ID, pdb_res.CHEM_COMP_ID ORDER BY site.ID
        """
    else:
        query = """
        MATCH (entry:Entry {ID:$entry_id})-[:HAS_ENTITY]->(entity:Entity {ID:$entity_id})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue {ID:$residue_number})-[res_relation:IS_IN_BINDING_SITE]->(site:BindingSite)
        WITH site, pdb_res
        MATCH (site)-[bound_relation:BOUNDED_BY]->(boundChain:BoundLigand)<-[entity_chain_rel:IS_AN_INSTANCE_OF]-(entity:Entity)-[entity_lig_rel:IS_A]->(ligand_chem_comp:ChemicalComponent)
        RETURN site.ID, site.DETAILS, site.EVIDENCE_CODE, ligand_chem_comp.ID, ligand_chem_comp.NAME, ligand_chem_comp.FORMULA, boundChain.AUTH_ASYM_ID, boundChain.STRUCT_ASYM_ID, 
        boundChain.AUTH_SEQ_ID, boundChain.PDB_INS_CODE, entity.ID, boundChain.ID, pdb_res.ID, pdb_res.CHEM_COMP_ID ORDER BY site.ID
        """

    mappings = run_query(query, parameters={
        'entry_id': str(entry_id), 'entity_id': str(entity_id), 'residue_number': str(residue_number)
    })

    if(len(mappings) == 0):
        return {}, 404

    site_dict = {}
    site_ligand_dict = {}
    site_boundligand_dict = {}
    site_pdb_res_dict = {}
    residue_chem_comp_id = None
    final_result = []

    for mapping in mappings:

        if(site_residues is True):
            (site_id, site_name, site_evidence, pdb_res_id, pdb_res_chem_comp_id, pdb_res_auth_asym_id, pdb_res_struct_asym_id, pdb_res_auth_seq_id, pdb_res_entity_id, pdb_res_symmetry_symbol, bound_ligand,
             bound_ligand_name, bound_ligand_formula, bound_auth_asym_id, bound_struct_asym_id, bound_auth_seq_id, bound_pdb_ins_code, bound_entity_id, bound_residue_id, ligand, ligand_name, ligand_formula,
             ligand_auth_asym_id, ligand_auth_seq_id, ligand_pdb_ins_code, ligand_struct_asym_id, ligand_sym_symbol, ligand_entity_id, ligand_residue_id, pdb_res, pdb_res_chem_comp_id) = mapping
        else:
            (site_id, site_name, site_evidence, bound_ligand, bound_ligand_name, bound_ligand_formula, bound_auth_asym_id, bound_struct_asym_id,
             bound_auth_seq_id, bound_pdb_ins_code, bound_entity_id, bound_residue_id, pdb_res, pdb_res_chem_comp_id) = mapping

        if(residue_chem_comp_id is None):
            residue_chem_comp_id = pdb_res_chem_comp_id

        if(site_dict.get(site_id) is None):
            site_dict[site_id] = (site_name, site_evidence)

        if(bound_ligand is not None):
            if(site_boundligand_dict.get(site_id) is None):
                site_boundligand_dict[site_id] = [(bound_ligand, bound_ligand_name, bound_ligand_formula, bound_auth_asym_id, bound_struct_asym_id,
                                                   bound_auth_seq_id, bound_pdb_ins_code, bound_entity_id, bound_residue_id)]
            else:
                site_boundligand_dict[site_id].append((bound_ligand, bound_ligand_name, bound_ligand_formula, bound_auth_asym_id, bound_struct_asym_id,
                                                       bound_auth_seq_id, bound_pdb_ins_code, bound_entity_id, bound_residue_id))
        if(site_residues is True):
            if(ligand is not None):
                if(site_ligand_dict.get(site_id) is None):
                    site_ligand_dict[site_id] = [(ligand, ligand_name, ligand_formula, ligand_auth_asym_id, ligand_auth_seq_id, ligand_pdb_ins_code,
                                                  ligand_struct_asym_id, ligand_sym_symbol, ligand_entity_id, ligand_residue_id)]
                else:
                    site_ligand_dict[site_id].append((ligand, ligand_name, ligand_formula, ligand_auth_asym_id,
                                                      ligand_auth_seq_id, ligand_pdb_ins_code, ligand_struct_asym_id, ligand_sym_symbol, ligand_entity_id, ligand_residue_id))

        if(site_residues is True):
            if(pdb_res_id is not None):
                if(site_pdb_res_dict.get(site_id) is None):
                    site_pdb_res_dict[site_id] = [(pdb_res_id, pdb_res_chem_comp_id, pdb_res_auth_asym_id,
                                                   pdb_res_struct_asym_id, pdb_res_auth_seq_id, pdb_res_entity_id, pdb_res_symmetry_symbol)]
                else:
                    site_pdb_res_dict[site_id].append((pdb_res_id, pdb_res_chem_comp_id, pdb_res_auth_asym_id,
                                                       pdb_res_struct_asym_id, pdb_res_auth_seq_id, pdb_res_entity_id, pdb_res_symmetry_symbol))

    for key in site_dict.keys():
        (site_name, evidence) = site_dict[key]
        if(site_residues is True):
            temp = {
                "site_id": key,
                "evidence_code": evidence,
                "details": site_name,
                "site_residues": [],
                "ligand_residues": []
            }
        else:
            temp = {
                "site_id": key,
                "evidence_code": evidence,
                "details": site_name,
                "ligand_residues": []
            }

        if(site_boundligand_dict.get(key) is not None):
            for result in list(set(site_boundligand_dict[key])):
                (bound_ligand, bound_ligand_name, bound_ligand_formula, bound_auth_asym_id,
                 bound_struct_asym_id, bound_auth_seq_id, bound_pdb_ins_code, bound_entity_id, bound_residue_id) = result
                if(bound_residue_id is not None):
                    temp["ligand_residues"].append({
                        "entity_id": int(bound_entity_id),
                        "residue_number": int(bound_residue_id),
                        "author_insertion_code": bound_pdb_ins_code,
                        "chain_id": bound_auth_asym_id,
                        "author_residue_number": int(bound_auth_seq_id),
                        "chem_comp_id": bound_ligand,
                        "struct_asym_id": bound_struct_asym_id
                    })

        if(site_residues is True):
            if(site_ligand_dict.get(key) is not None):
                for result in list(set(site_ligand_dict[key])):
                    (ligand, ligand_name, ligand_formula, ligand_auth_asym_id, ligand_auth_seq_id, ligand_pdb_ins_code,
                     ligand_struct_asym_id, ligand_sym_symbol, ligand_entity_id, ligand_residue_id) = result

                    temp["site_residues"].append({
                        "entity_id": int(ligand_entity_id),
                        "residue_number": int(ligand_residue_id),
                        "author_insertion_code": ligand_pdb_ins_code,
                        "chain_id": ligand_auth_asym_id,
                        "author_residue_number": int(ligand_auth_seq_id),
                        "chem_comp_id": ligand,
                        "struct_asym_id": ligand_struct_asym_id,
                        "symmetry_symbol": ligand_sym_symbol
                    })

        if(site_residues is True):
            if(site_pdb_res_dict.get(key) is not None):
                for result in list(set(site_pdb_res_dict[key])):
                    (pdb_res_id, pdb_res_chem_comp_id, pdb_res_auth_asym_id, pdb_res_struct_asym_id,
                     pdb_res_auth_seq_id, pdb_res_entity_id, pdb_res_symmetry_symbol) = result

                    temp["site_residues"].append({
                        "entity_id": int(pdb_res_entity_id),
                        "residue_number": int(pdb_res_id),
                        "author_insertion_code": "null",
                        "chain_id": pdb_res_auth_asym_id,
                        "author_residue_number": int(pdb_res_auth_seq_id),
                        "chem_comp_id": pdb_res_chem_comp_id,
                        "struct_asym_id": pdb_res_struct_asym_id,
                        "symmetry_symbol": pdb_res_symmetry_symbol
                    })

        final_result.append(temp)

    return final_result, 200


def get_mappings_for_residue_binding_site_all(entry_id, entity_id, residue_number, site_residues):

    query = None
    if(site_residues is True):
        query = """
        MATCH (entry:Entry {ID:$entry_id})-[:HAS_ENTITY]->(entity:Entity {ID:$entity_id})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue {ID:$residue})-[res_relation:IS_IN_BINDING_SITE]->
        (site:BindingSite)
        WITH site, pdb_res
        OPTIONAL MATCH (ligand:Ligand)-[:IS_AN_INSTANCE_OF]->(ligand_entity:Entity)-[ligand_entity_relation:CONTAINS_CHAIN]->(ligand_chain:Chain)-[ligand_relation:IS_IN_BINDING_SITE]->(site)
        OPTIONAL MATCH (site)-[bound_relation:BOUNDED_BY]->(boundligand_chain:Chain)<-[boundligand_entity_relation:CONTAINS_CHAIN]-(boundligand_entity:Entity)-[:IS_AN_INSTANCE_OF]->(boundligand:Ligand)
        OPTIONAL MATCH (site)<-[res_all_relation:IS_IN_BINDING_SITE]-(pdb_res_all:PDBResidue)<-[:HAS_PDB_RESIDUE]-(pdb_res_all_entity:Entity) WHERE pdb_res_all.ID <> $residue
        RETURN site.ID, site.DETAILS, site.EVIDENCE_CODE, pdb_res_all.ID, pdb_res_all.CHEM_COMP_ID, res_all_relation.AUTH_ASYM_ID, res_all_relation.STRUCT_ASYM_ID, 
        res_all_relation.AUTH_SEQ_ID, pdb_res_all_entity.ID, res_all_relation.SYMMETRY_SYMBOL, boundligand.ID, boundligand.NAME, boundligand.FORMULA, boundligand_chain.AUTH_ASYM_ID, boundligand_chain.STRUCT_ASYM_ID, 
        boundligand_entity_relation.AUTH_SEQ_ID, boundligand_entity.ID, boundligand_entity_relation.RES_ID, ligand.ID, ligand.NAME, ligand.FORMULA, ligand_chain.AUTH_ASYM_ID, ligand_chain.AUTH_SEQ_ID, 
        ligand_chain.STRUCT_ASYM_ID, ligand_relation.SYMMETRY_SYMBOL, ligand_entity.ID, ligand_chain.RES_ID, pdb_res.ID, pdb_res.CHEM_COMP_ID ORDER BY site.ID
        """
    else:
        query = """
        MATCH (entry:Entry {ID:$entry_id})-[:HAS_ENTITY]->(entity:Entity {ID:$entity_id})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue {ID:$residue})-[res_relation:IS_IN_BINDING_SITE]->
        (site:BindingSite)
        WITH site, pdb_res
        MATCH (site)-[bound_relation:BOUNDED_BY]->(boundChain:Chain)<-[entity_chain_rel:CONTAINS_CHAIN]-(entity:Entity)-[entity_lig_rel:IS_AN_INSTANCE_OF]->(ligand:Ligand)
        RETURN site.ID, site.DETAILS, site.EVIDENCE_CODE, ligand.ID, ligand.NAME, ligand.FORMULA, boundChain.AUTH_ASYM_ID, boundChain.STRUCT_ASYM_ID, 
        entity_chain_rel.AUTH_SEQ_ID, entity.ID, entity_chain_rel.RES_ID, pdb_res.ID, pdb_res.CHEM_COMP_ID ORDER BY site.ID
        """

    mappings = run_query(query, parameters={
        'entry_id': str(entry_id), 'entity_id': str(entity_id), 'residue': str(residue_number)
    })

    if(len(mappings) == 0):
        return {}, 404

    site_dict = {}
    site_ligand_dict = {}
    site_boundligand_dict = {}
    site_pdb_res_dict = {}
    residue_chem_comp_id = None
    final_result = []

    for mapping in mappings:

        if(site_residues is True):
            (site_id, site_name, site_evidence, pdb_res_id, pdb_res_chem_comp_id, pdb_res_auth_asym_id, pdb_res_struct_asym_id, pdb_res_auth_seq_id, pdb_res_entity_id, pdb_res_symmetry_symbol, bound_ligand,
             bound_ligand_name, bound_ligand_formula, bound_auth_asym_id, bound_struct_asym_id, bound_auth_seq_id, bound_entity_id, bound_residue_id, ligand, ligand_name, ligand_formula,
             ligand_auth_asym_id, ligand_auth_seq_id, ligand_struct_asym_id, ligand_sym_symbol, ligand_entity_id, ligand_residue_id, pdb_res, pdb_res_chem_comp_id) = mapping
        else:
            (site_id, site_name, site_evidence, bound_ligand, bound_ligand_name, bound_ligand_formula, bound_auth_asym_id, bound_struct_asym_id,
             bound_auth_seq_id, bound_entity_id, bound_residue_id, pdb_res, pdb_res_chem_comp_id) = mapping

        if(residue_chem_comp_id is None):
            residue_chem_comp_id = pdb_res_chem_comp_id

        if(site_dict.get(site_id) is None):
            site_dict[site_id] = (site_name, site_evidence)

        if(bound_ligand is not None):
            if(site_boundligand_dict.get(site_id) is None):
                site_boundligand_dict[site_id] = [(bound_ligand, bound_ligand_name, bound_ligand_formula, bound_auth_asym_id, bound_struct_asym_id,
                                                   bound_auth_seq_id, bound_entity_id, bound_residue_id)]
            else:
                site_boundligand_dict[site_id].append((bound_ligand, bound_ligand_name, bound_ligand_formula, bound_auth_asym_id, bound_struct_asym_id,
                                                       bound_auth_seq_id, bound_entity_id, bound_residue_id))
        if(site_residues is True):
            if(ligand is not None):
                if(site_ligand_dict.get(site_id) is None):
                    site_ligand_dict[site_id] = [(ligand, ligand_name, ligand_formula, ligand_auth_asym_id, ligand_auth_seq_id,
                                                  ligand_struct_asym_id, ligand_sym_symbol, ligand_entity_id, ligand_residue_id)]
                else:
                    site_ligand_dict[site_id].append((ligand, ligand_name, ligand_formula, ligand_auth_asym_id,
                                                      ligand_auth_seq_id, ligand_struct_asym_id, ligand_sym_symbol, ligand_entity_id, ligand_residue_id))

        if(site_residues is True):
            if(pdb_res_id is not None):
                if(site_pdb_res_dict.get(site_id) is None):
                    site_pdb_res_dict[site_id] = [(pdb_res_id, pdb_res_chem_comp_id, pdb_res_auth_asym_id,
                                                   pdb_res_struct_asym_id, pdb_res_auth_seq_id, pdb_res_entity_id, pdb_res_symmetry_symbol)]
                else:
                    site_pdb_res_dict[site_id].append((pdb_res_id, pdb_res_chem_comp_id, pdb_res_auth_asym_id,
                                                       pdb_res_struct_asym_id, pdb_res_auth_seq_id, pdb_res_entity_id, pdb_res_symmetry_symbol))

    for key in site_dict.keys():
        (site_name, evidence) = site_dict[key]
        if(site_residues is True):
            temp = {
                "site_id": key,
                "evidence_code": evidence,
                "details": site_name,
                "site_residues": [],
                "ligand_residues": []
            }
        else:
            temp = {
                "site_id": key,
                "evidence_code": evidence,
                "details": site_name,
                "ligand_residues": []
            }

        if(site_boundligand_dict.get(key) is not None):
            for result in list(set(site_boundligand_dict[key])):
                (bound_ligand, bound_ligand_name, bound_ligand_formula, bound_auth_asym_id,
                 bound_struct_asym_id, bound_auth_seq_id, bound_entity_id, bound_residue_id) = result
                if(bound_residue_id is not None):
                    temp["ligand_residues"].append({
                        "entity_id": int(bound_entity_id),
                        "residue_number": int(bound_residue_id),
                        "author_insertion_code": "null",
                        "chain_id": bound_auth_asym_id,
                        "author_residue_number": int(bound_auth_seq_id),
                        "chem_comp_id": bound_ligand,
                        "struct_asym_id": bound_struct_asym_id
                    })

        if(site_residues is True):
            if(site_ligand_dict.get(key) is not None):
                for result in list(set(site_ligand_dict[key])):
                    (ligand, ligand_name, ligand_formula, ligand_auth_asym_id, ligand_auth_seq_id,
                     ligand_struct_asym_id, ligand_sym_symbol, ligand_entity_id, ligand_residue_id) = result

                    temp["site_residues"].append({
                        "entity_id": int(ligand_entity_id),
                        "residue_number": int(ligand_residue_id),
                        "author_insertion_code": "null",
                        "chain_id": ligand_auth_asym_id,
                        "author_residue_number": int(ligand_auth_seq_id),
                        "chem_comp_id": ligand,
                        "struct_asym_id": ligand_struct_asym_id,
                        "symmetry_symbol": ligand_sym_symbol
                    })

        if(site_residues is True):
            if(site_pdb_res_dict.get(key) is not None):
                for result in list(set(site_pdb_res_dict[key])):
                    (pdb_res_id, pdb_res_chem_comp_id, pdb_res_auth_asym_id, pdb_res_struct_asym_id,
                     pdb_res_auth_seq_id, pdb_res_entity_id, pdb_res_symmetry_symbol) = result

                    temp["site_residues"].append({
                        "entity_id": int(pdb_res_entity_id),
                        "residue_number": int(pdb_res_id),
                        "author_insertion_code": "null",
                        "chain_id": pdb_res_auth_asym_id,
                        "author_residue_number": int(pdb_res_auth_seq_id),
                        "chem_comp_id": pdb_res_chem_comp_id,
                        "struct_asym_id": pdb_res_struct_asym_id,
                        "symmetry_symbol": pdb_res_symmetry_symbol
                    })

        final_result.append(temp)

    return final_result, 200


def get_basic_residue_details(entry_id, entity_id, residue_number):

    query = """
    MATCH (entry:Entry {ID:$entry_id})-[:HAS_ENTITY]->(entity:Entity {ID:$entity_id})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue {ID:$residue_number})-[chain_rel:IS_IN_CHAIN]->(chain:Chain)
    RETURN chain.AUTH_ASYM_ID, chain.STRUCT_ASYM_ID, chain_rel.AUTH_SEQ_ID, chain_rel.PDB_INS_CODE, chain_rel.OBSERVED
    """

    chains = []
    dict_chains = {}

    mappings = run_query(query, parameters={
        'entry_id': str(entry_id), 'entity_id': str(entity_id), 'residue_number': str(residue_number)
    })

    if(len(mappings) == 0):
        return {}, 404

    for mapping in mappings:
        (auth_asym_id, struct_asym_id, auth_seq_id, pdb_ins_code, observed) = mapping
        chain_key = (auth_asym_id, struct_asym_id)

        if dict_chains.get(chain_key) is None:
            dict_chains[chain_key] = {
                "auth_asym_id": auth_asym_id,
                "struct_asym_id": struct_asym_id,
                "residues": []
            }
        dict_chains[chain_key]["residues"].append({
            "residue_number": int(residue_number),
            "author_residue_number": int(auth_seq_id) if auth_seq_id is not None else None,
            "author_insertion_code": "" if pdb_ins_code is None else pdb_ins_code,
            "observed": observed
        })

    for chain_key in dict_chains.keys():
        chains.append(dict_chains[chain_key])

    return chains, 200


"""
@api {get} residue_mapping/sequence_conservation/:pdbId/:entityId/:residueNumber Get sequence conservations for a PDB Residue
@apiName GetPDBResidueSequenceConservation
@apiGroup Residue
@apiDescription Get sequence conservations for a PDB Residue
@apiVersion 2.0.0

@apiParam {String} pdbId=3wov PDB Entry ID
@apiParam {String} entityId=1 PDB Entity ID
@apiParam {String} residueNumber=89 PDB Residue Number
"""
@app.get('/sequence_conservation/' +rePDBid +'/<entity_id>/<residue_number>'+reSlashOrNot)
def get_sequence_conservation_api(pdbid, entity_id, residue_number):

    entry_id = pdbid.strip("'")
    entity_id = entity_id.strip("'")
    residue_number = residue_number.strip("'")

    api_result = {
        entry_id: [{
            "entity_id": int(entity_id),
            "chains": []
        }]
    }

    chains, response_status = get_basic_residue_details(entry_id, entity_id, residue_number)

    api_result[entry_id][0]["chains"] = chains

    for chain in chains:

        for residue in chain["residues"]:

            seq_conservation, response_status = get_sequence_conservation(entry_id, entity_id, residue["residue_number"])
            seq_conservation = seq_conservation.get("residue_conservation")

            if seq_conservation is not None:
                residue["residue_conservation"] = seq_conservation
            else:
                residue["residue_conservation"] = {}

    bottle.response.status = 200
    return api_result


def get_sequence_conservation(entry_id, entity_id, residue_number):

    query = """
    MATCH (entry:Entry {ID:$entryId})-[:HAS_ENTITY]->(entity:Entity {ID:$entityId})-[:HAS_PDB_RESIDUE]->(pdb_res:PDB_Residue {ID:$residueNumber})-[hr:HAS_SEQID]->(seq:PDB_Sequence) 
    RETURN hr.CONSERVED_SCORE, hr.P_SCORE_A, hr.P_SCORE_C, hr.P_SCORE_D, hr.P_SCORE_E, hr.P_SCORE_F, hr.P_SCORE_G, hr.P_SCORE_H, hr.P_SCORE_I, hr.P_SCORE_K, 
    hr.P_SCORE_L, hr.P_SCORE_M, hr.P_SCORE_N, hr.P_SCORE_P, hr.P_SCORE_Q, hr.P_SCORE_R, hr.P_SCORE_S, hr.P_SCORE_T, hr.P_SCORE_V, hr.P_SCORE_W, hr.P_SCORE_Y
    """

    result = run_query(query, parameters={
        'entryId': str(entry_id), 'entityId': str(entity_id), 'residueNumber': str(residue_number)
    })

    if(len(result) == 0):
        return {}, 404

    final_result = {}

    for score in result:

        final_result["residue_conservation"] = {
            "conservation_score": score['hr.CONSERVED_SCORE'],
            "proba_A": score['hr.P_SCORE_A'],
            "proba_C": score['hr.P_SCORE_C'],
            "proba_D": score['hr.P_SCORE_D'],
            "proba_E": score['hr.P_SCORE_E'],
            "proba_F": score['hr.P_SCORE_F'],
            "proba_G": score['hr.P_SCORE_G'],
            "proba_H": score['hr.P_SCORE_H'],
            "proba_I": score['hr.P_SCORE_I'],
            "proba_K": score['hr.P_SCORE_K'],
            "proba_L": score['hr.P_SCORE_L'],
            "proba_M": score['hr.P_SCORE_M'],
            "proba_N": score['hr.P_SCORE_N'],
            "proba_P": score['hr.P_SCORE_P'],
            "proba_Q": score['hr.P_SCORE_Q'],
            "proba_R": score['hr.P_SCORE_R'],
            "proba_S": score['hr.P_SCORE_S'],
            "proba_T": score['hr.P_SCORE_T'],
            "proba_V": score['hr.P_SCORE_V'],
            "proba_W": score['hr.P_SCORE_W'],
            "proba_Y": score['hr.P_SCORE_Y']
        }

    return final_result, 200


"""
@api {get} residue_mapping/funpdbe_annotation/:pdbId/:entityId/:residueNumber Get FunPDBe annotations for a PDB Residue
@apiName GetPDBResidueFunPDBeAnnotation
@apiGroup Residue
@apiDescription Get FunPDBe annotations for a PDB Residue
@apiVersion 2.0.0

@apiParam {String} pdbId=1a08 PDB Entry ID
@apiParam {String} entityId=1 PDB Entity ID
@apiParam {String} residueNumber=34 PDB Residue Number
"""
@app.get('/funpdbe_annotation/' +rePDBid +'/<entity_id>/<residue_number>'+reSlashOrNot)
def get_funpdbe_annotation_api(pdbid, entity_id, residue_number):

    entry_id = pdbid.strip("'")
    entity_id = entity_id.strip("'")
    residue_number = residue_number.strip("'")

    api_result = {
        entry_id: [{
            "entity_id": int(entity_id),
            "chains": []
        }]
    }

    chains, response_status = get_basic_residue_details(entry_id, entity_id, residue_number)

    api_result[entry_id][0]["chains"] = sorted(chains, key=lambda x: x["auth_asym_id"])

    funpdbe_annotation = None

    for chain in chains:

        for residue in chain["residues"]:
            
            funpdbe_annotation, response_status = get_funpdbe_annotation(entry_id, entity_id, chain["auth_asym_id"], residue["residue_number"])
    
            if funpdbe_annotation is not None:
                residue["FunPDBe"] = funpdbe_annotation
            else:
                residue["FunPDBe"] = {}

    bottle.response.status = 200
    return api_result


def get_funpdbe_annotation(entry_id, entity_id, chain_id, residue_number):

    query = """
    MATCH (entry:Entry {ID:$entry_id})-[:HAS_ENTITY]->(entity:Entity {ID:$entity_id})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue {ID:$residue_number})-[:IS_IN_CHAIN]->(chain:Chain {AUTH_ASYM_ID:$chain_id}),
    (pdb_res)<-[res_rel:FUNPDBE_ANNOTATION_FOR]-(funpdbe_group:FunPDBeResidueGroup)-[:FUNPDBE_RESIDUE_GROUP_OF]->(funpdbe_entry:FunPDBeEntry)-[:HAS_EVIDENCE_CODE]->(evidence_code:EvidenceCodeOntology)
    WHERE res_rel.CHAIN_LABEL = chain.AUTH_ASYM_ID
    RETURN entity.ID, chain.AUTH_ASYM_ID, pdb_res.ID, funpdbe_entry.DATA_RESOURCE, funpdbe_group.LABEL, funpdbe_entry.RESOURCE_ENTRY_URL, COLLECT(evidence_code.ECO_CODE), 
    res_rel.RAW_SCORE, res_rel.CONFIDENCE_SCORE, res_rel.CONFIDENCE_CLASSIFICATION
    """

    result = run_query(query, parameters={
        'entry_id': str(entry_id), 'entity_id': str(entity_id), 'chain_id': str(chain_id), 'residue_number': str(residue_number)
    })

    if(len(result) == 0):
        return {}, 404

    final_result = []

    for row in result:
        (entity_id, auth_asym_id, pdb_res_id, origin, label, resource_url, evidence_codes, raw_score, conf_score, conf_class) = row

        final_result.append({
            "origin": origin,
            "label": label,
            "url": resource_url,
            "raw_score": None if raw_score is None or raw_score == "" else float(raw_score),
            "confidence_score": None if conf_score is None or conf_score == "" else float(conf_score),
            "confidence_classification": conf_class, 
            "evidence_codes": [x for x in evidence_codes]
        })
        

    return final_result, 200


@app.get('/sequence/' +rePDBid +'/<pdb_chain_id>' +reSlashOrNot)
def get_sequence_api(pdbid, pdb_chain_id):

    entry_id = pdbid.strip("'")
    chain_id = pdb_chain_id.strip("'")

    api_result = {
        entry_id: {
            "sequence": None
        }
    }

    sequence, response_status = get_sequence(entry_id, chain_id)

    api_result[entry_id]["sequence"] = sequence

    bottle.response.status = response_status
    return api_result


"""
Returns amino acid sequence for the given chain of the PDB entry
"""
def get_sequence(entry_id, chain_id):

    query = """
    MATCH (entry:Entry {ID:$entry_id})-[:HAS_ENTITY]->(entity:Entity)-[:CONTAINS_CHAIN]->(chain:Chain {STRUCT_ASYM_ID:$chain_id})<-[:IS_IN_CHAIN]-(pdb_res:PDBResidue)
    RETURN pdb_res.CHEM_COMP_ID ORDER BY toInteger(pdb_res.ID)
    """

    result = run_query(query, parameters={
        'entry_id': str(entry_id), 'chain_id': str(chain_id)
    })

    if(len(result) == 0):
        return {}, 404

    sequence = ''

    for row in result:
        sequence += amino_acid_codes[row[0]][0]

    return sequence, 200