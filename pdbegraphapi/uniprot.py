#!/usr/bin/env python

import bottle
from decimal import Decimal

import re
from pdbegraphapi.amino_acid_codes import amino_acid_codes, amino_acid_codes_one_to_three
from pdbegraphapi import request_handler
from operator import itemgetter, attrgetter
from pdbegraphapi.common_params import seq_conservation_aa_colors
from collections import OrderedDict
from itertools import repeat
from pdbegraphapi import util_common
from pdbegraphapi.neo4j_model import run_query
from pdbegraphapi.residue import get_mappings_for_residue_pfam, get_mappings_for_residue_cath, get_mappings_for_residue_interpro, get_mappings_for_residue_scop
from pdbegraphapi.pdb import get_complex, get_subcomplex
from concurrent.futures import ThreadPoolExecutor
from pdbegraphapi.config import rePDBid, reSlashOrNot, reResidue, rePDBComplex, reIdentity
from pdbegraphapi.util_common import split_nonconsecutive_residues_with_code, split_nonconsecutive_two_residues_with_code, split_nonconsecutive_two_residues, count_residues, get_amino_one_to_three
from pdbegraphapi.util_common import get_amino_three_to_one
from protvista_adapter.adapter import convert_to_protvista
from protvista_adapter.common_params import funpdbe_resource_dict
from pdbegraphapi.neo4j_model import run_query

print('[UNIPROT] Starting')

BONDLENGTH_QUANTIZE_DECIMAL = Decimal("0.001")
POOL_MAX_WORKERS = 10
ALLOWED_OVERLAP_PERCENT_BEST_STRUCTURE = 30

app = bottle.Bottle()

# Installs the request handler plugin
app.install(request_handler)

print('[UNIPROT] Loaded')

"""
@api {get} uniprot/:accession Get all PDB structures for a UniProt accession
@apiName GetPDBforUNP
@apiGroup UniProt
@apiDescription Get all PDB structures for a UniProt accession.
@apiVersion 2.0.0

@apiParam {String} accession=P07550 UniProt accession

@apiUse GenericResidueTerms

@apiExample {json=./examples/success/unipdb.json} apiSuccessExample Example success response JSON
"""
@app.get('/<uniprot_accession>'+reSlashOrNot)
def get_unipdb_api(uniprot_accession):

    uniprot_accession = uniprot_accession.strip("'")

    api_result = {
        uniprot_accession: {}
    }

    response, response_status = get_unipdb(uniprot_accession)

    if response_status == 404:
        bottle.response.status = 404
        return api_result

    api_result[uniprot_accession] = response

    bottle.response.status = 200
    return api_result


def get_unipdb(uniprot_accession):

    query = """
    MATCH (unp:UniProt {ACCESSION:$accession})-[:HAS_UNP_RESIDUE]->(unp_res:UNPResidue)<-[relation:MAP_TO_UNIPROT_RESIDUE]-(pdb_res:PDBResidue)
    <-[:HAS_PDB_RESIDUE]-(entity:Entity)<-[:HAS_ENTITY]-(entry:Entry)
    WITH entry.ID AS entryId, entity.ID AS entityId, relation.CHAINS AS chains, toInteger(pdb_res.ID) AS pdbRes, toInteger(unp_res.ID) AS unpRes, pdb_res.CHEM_COMP_ID AS pdbResCode, unp_res.ONE_LETTER_CODE AS unpResCode 
    RETURN entryId, entityId, chains, pdbRes, unpRes, pdbResCode, unpResCode ORDER BY entryId, pdbRes
    """

    mappings = run_query(query, parameters={
        'accession': str(uniprot_accession)
    })

    if(len(mappings) == 0):
        return {}, 404

    final_result = {
        'Pfam': get_mappings_for_unp_residue_pfam(uniprot_accession),
        'mappings': []
    }

    entry_entity_dict = {}

    for mapping in mappings:

        (entry, entity, chains, pdb_res, unp_res, pdb_res_code, unp_res_code) = mapping
        pdb_res_one_code = get_amino_three_to_one(pdb_res_code)

        # PDBE-3217: If * it means a modified/mutated residue. In this case get the exact one letter code from Chemical_Component
        if pdb_res_one_code == "*":
            query = """
            MATCH (chem_comp:ChemicalComponent {ID:$chem_comp_id})
            RETURN chem_comp.ONE_LETTER_CODE
            """

            chem_mapping = run_query(query, parameters={
                "chem_comp_id": pdb_res_code
            })

            if chem_mapping:
                pdb_res_one_code = chem_mapping[0][0]
            else:
                pdb_res_one_code = pdb_res_code

        data = (chains, pdb_res, unp_res, pdb_res_one_code, unp_res_code)


        # create array of mappings in dict for an (entry, entity) pair if not present
        if entry_entity_dict.get((entry, entity)) is None:
            entry_entity_dict[(entry, entity)] = [data]
        else:
            entry_entity_dict[(entry, entity)].append(data)

    final_map = {}

    for key in entry_entity_dict.keys():
        final_map[key] = []
        pdb_seq = ''
        unp_seq = ''
        prev_pdb_res = None
        prev_unp_res = None
        pdb_start = None
        unp_start = None
        incr = 0
        start = True

        while incr < len(entry_entity_dict[key]):
            (chains, pdb_res, unp_res, pdb_res_code,
             unp_res_code) = entry_entity_dict[key][incr]

            if start or prev_pdb_res == pdb_res - 1:
                if start:
                    if prev_pdb_res != None:
                        pdb_start = prev_pdb_res
                        unp_start = prev_unp_res
                    else:
                        pdb_start = pdb_res
                        unp_start = unp_res
                pdb_seq += pdb_res_code
                unp_seq += unp_res_code
                start = False
            else:
                # check for a new segment
                # pdb_res and unp_end will be ending residue numbers
                final_map[key].append(
                    (chains, pdb_seq, unp_seq, pdb_start, prev_pdb_res, unp_start, prev_unp_res))
                pdb_seq = pdb_res_code
                unp_seq = unp_res_code
                start = True

            # the very last residue
            if incr == len(entry_entity_dict[key]) - 1:
                # pdb_res and unp_end will be ending residue numbers
                final_map[key].append(
                    (chains, pdb_seq, unp_seq, pdb_start, pdb_res, unp_start, unp_res))

            prev_pdb_res = pdb_res
            prev_unp_res = unp_res
            incr += 1

    for key in final_map.keys():
        entry_id, entity_id = key

        mappings = []

        for mapping in final_map[key]:
            (chains, pdb_seq, unp_seq, pdb_start,
             pdb_end, unp_start, unp_end) = mapping
            mappings.append({
                'chains': chains,
                'pdb_sequence': pdb_seq,
                'unp_sequence': unp_seq,
                'pdb_start': pdb_start,
                'pdb_end': pdb_end,
                'unp_start': unp_start,
                'unp_end': unp_end
            })

        final_result['mappings'].append({
            'entry_id': entry_id,
            'entity_id': int(entity_id),
            'segments': mappings
        })

    return final_result, 200


def get_mappings_for_unp_residue_pfam(uniprot_accession):

    query = """
    MATCH (unp:UniProt {ACCESSION:$accession})-[:HAS_UNP_RESIDUE]->(unp_res:UNPResidue)-[:IS_IN_PFAM]->(pfam:Pfam)
    WITH pfam.PFAM_ACCESSION AS pfamAccession, pfam.NAME AS pfamName, pfam.DESCRIPTION AS pfamDesc, toInteger(unp_res.ID) AS unpRes
    RETURN pfamAccession, pfamName, pfamDesc, min(unpRes) AS unpStart, max(unpRes) AS unpEnd
    """

    result = run_query(query, parameters={
        'accession': str(uniprot_accession)
    })

    final_result = {}

    for pfam in result:

        (accession, name, desc, unp_start, unp_end) = pfam
        final_result = {
            "identifier": name,
            "name": name,
            "description": desc,
            "unp_start": unp_start,
            "unp_end": unp_end
        }

    return final_result

"""
@api {get} uniprot/:accession/:unpResidue Get SIFTS mappings for a UniProt residue
@apiName GetAnnotationsUNPResidue
@apiGroup UniProt
@apiDescription Get mappings (as assigned by the SIFTS process) for a UniProt Residue to sequence and structural domains.
@apiVersion 2.0.0

@apiParam {String} accession=P07550 UniProt accession
@apiParam {String} unpResidue=30 UniProt residue

@apiExample {json=./examples/success/unipdb_residue.json} apiSuccessExample Example success response JSON
"""
@app.get('/<uniprot_accession>/'+reResidue+reSlashOrNot)
def get_unipdb_residue_api(uniprot_accession, unp_res):
    """
    
    """

    uniprot_accession = uniprot_accession.strip("'")
    unp_res = unp_res.strip("'")

    api_result = {
        uniprot_accession: {}
    }

    unipdb_result, response_status = get_unipdb_residue(
        uniprot_accession, unp_res)

    if response_status == 404:
        bottle.response.status = 404
        return api_result

    api_result[uniprot_accession] = unipdb_result

    bottle.response.status = 200
    return api_result


def get_unipdb_residue(uniprot_accession, unp_res):

    query = """
    MATCH (unp:UniProt {ACCESSION:$accession})-[:HAS_UNP_RESIDUE]->(unp_res:UNPResidue {ID:$unpResidue})<-[:MAP_TO_UNIPROT_RESIDUE]-(pdb_res:PDBResidue)<-[:HAS_PDB_RESIDUE]-(entity:Entity)<-[:HAS_ENTITY]-(entry:Entry)
    RETURN entry.ID AS entryId, entity.ID AS entityId, pdb_res.ID AS pdbRes
    """

    mappings = run_query(query, parameters={
        'accession': str(uniprot_accession), 'unpResidue': str(unp_res)
    })

    if(len(mappings) == 0):
        return {}, 404

    final_result = {}

    pfam_dict = {}
    interpro_dict = {}
    cath_dict = {}
    scop_dict = {}

    for mapping in mappings:

        (entry_id, entity_id, pdb_res) = mapping
        temp_map, resp_status = get_mappings_for_residue_pfam(
            entry_id, entity_id, pdb_res)

        for pfam in temp_map.keys():
            if pfam_dict.get(pfam) is None:
                pfam_dict[pfam] = temp_map[pfam]

        temp_map, resp_status = get_mappings_for_residue_interpro(
            entry_id, entity_id, pdb_res)

        for interpro in temp_map.keys():
            if interpro_dict.get(interpro) is None:
                interpro_dict[interpro] = temp_map[interpro]

        temp_map, resp_status = get_mappings_for_residue_cath(
            entry_id, entity_id, pdb_res)

        for cath in temp_map.keys():
            if cath_dict.get(cath) is None:
                cath_dict[cath] = temp_map[cath]

        temp_map, resp_status = get_mappings_for_residue_scop(
            entry_id, entity_id, pdb_res)

        for scop in temp_map.keys():
            if scop_dict.get(scop) is None:
                scop_dict[scop] = temp_map[scop]

    final_result['Pfam'] = pfam_dict
    final_result['InterPro'] = interpro_dict
    final_result['CATH'] = cath_dict
    final_result['SCOP'] = scop_dict

    return final_result, 200


"""
@api {get} uniprot/complex/:accession Get list of complexes in which the protein interacts
@apiName GetUniProtComplexes
@apiGroup UniProt
@apiDescription Get list of complexes in which the protein interacts along with other participants and subcomplexes if any.
@apiVersion 2.0.0

@apiParam {String} accession=P07550 UniProt accession

@apiUse ComplexTerms

@apiExample {json=./examples/success/uniprot_complex.json} apiSuccessExample Example success response JSON
"""
@app.get('/complex/<uniprot_accession>'+reSlashOrNot)
def get_uniprot_complex_api(uniprot_accession):

    uniprot_accession = uniprot_accession.strip("'")

    api_result = {
        uniprot_accession: []
    }
    result, status = get_uniprot_complex(uniprot_accession)

    if status == 404:
        bottle.response.status = 404
        return api_result

    for pdb_complex in result:
        complex_result, complex_result_status = get_complex(pdb_complex)
        subcomplex_result, subcomplex_result_status = get_subcomplex(pdb_complex)

        api_result[uniprot_accession].append({
            pdb_complex: {
            "participants": complex_result,
            "subcomplexes": subcomplex_result
            }
        })
    

    bottle.response.status = 200
    return api_result



def get_uniprot_complex(accession):

    query = """
    MATCH (unp:UniProt {ACCESSION:$accession})-[:IS_PART_OF_PDB_COMPLEX]->(complex:PDBComplex)
    RETURN complex.COMPLEX_ID ORDER BY complex.COMPLEX_ID
    """

    result = []

    mappings = run_query(query, parameters={
        "accession": str(accession)
    })

    if not mappings:
        return [], 404

    return [x[0] for x in mappings], 200

        

@app.get('/summary_stats/<uniprot_accession>'+reSlashOrNot)
def get_uniprot_summary_stats_api(uniprot_accession):

    uniprot_accession = uniprot_accession.strip("'")

    api_result = {
        uniprot_accession: []
    }

    result = status = None

    if uniprot_accession.startswith("PRO_"):
        _, result = get_uniprot_summary_stats(uniprot_accession)
    else:
        result, status = get_uniprot_summary_stats(uniprot_accession)

    if status == 404 or not result:
        bottle.response.status = 404
        return {}    

    api_result[uniprot_accession] = result

    bottle.response.status = 200
    return api_result


def get_uniprot_summary_stats(accession):

    is_processed_protein = True if accession.startswith("PRO_") else False

    final_result = {
        "pdbs": 0,
        "ligands": 0,
        "interaction_partners": 0,
        "annotations": 0,
        "similar_proteins": 0
    }
    
    fails = 0

    # check if a valid UniProt in DB, else return {} and 404
    check_query = """
    MATCH (u:UniProt {ACCESSION:$accession})
    RETURN u.ACCESSION
    """

    temp_result = run_query(check_query, parameters={
        "accession": str(accession)
    })

    if not temp_result:
        return None, 404

    try:
        pdb_entries, pdb_entries_status = get_uniprot_pdb_details(accession)

        del pdb_entries_status

        if pdb_entries:
            final_result["pdbs"] =len(set(map(lambda x: x[0], pdb_entries.keys())))

    except Exception as err:
        fails += 1

    try:
        ligands_mapping, ligands_mapping_status = get_uniprot_ligands(accession)

        del ligands_mapping_status

        if ligands_mapping:
            final_result["ligands"] = len(ligands_mapping)

    except Exception as err:
        fails += 1

    try:
        interaction_partners_result = get_uniprot_generic_interface_residues_api(accession)
        
        if interaction_partners_result:
            final_result["interaction_partners"] = len(interaction_partners_result[accession]["data"])

    except Exception as err:
        fails += 1

    try:
        if exists_annotations(accession):
            final_result["annotations"] = 1
        # PDBE-3296: Consider variation and sequence conservation for the count
        elif get_sequence_conservation_uniprot_api(accession):
            final_result["annotations"] = 1
        elif get_uniprot_variation_api(accession):
            final_result["annotations"] = 1
        else:
            final_result["annotations"] = 0

    except Exception as err:
        fails += 1

    try:
        similar_proteins_result = get_similar_proteins(accession)
        
        if similar_proteins_result:
            final_result["similar_proteins"] = len(similar_proteins_result)

        similar_proteins_result = get_similar_proteins_identical_uniprot_api(accession, "90")
        
        if similar_proteins_result:
            final_result["similar_proteins"] += len(similar_proteins_result[accession])

    except Exception as err:
        fails += 1
    
    # return None if none of them have response, 404 cases
    if fails == 5 or (final_result["pdbs"] == 0 and final_result["ligands"] == 0 and final_result["interaction_partners"] == 0 and final_result["annotations"] == 0 and final_result["similar_proteins"] == 0):
        
        if is_processed_protein:
            return accession, None

        return None, 404

    if is_processed_protein:
        return accession, final_result

    return final_result, 200


"""
@api {get} uniprot/best_structures/:accession/:unpStart/:unpEnd Get Best Structures for a UniProt residue range
@apiName GetBestStructuresUNPRange
@apiGroup UniProt
@apiDescription Get the list of PDB structures mapping to a UniProt residue range sorted by coverage of the protein and, if the same, resolution.
@apiVersion 2.0.0

@apiParam {String} accession=P07550 UniProt accession
@apiParam {String} unpStart=30 UniProt residue start
@apiParam {String} unpEnd=40 UniProt residue end

@apiSuccess {Integer} end mmcif-style end residue.
@apiSuccess {String} chain_id PDB chain id.
@apiSuccess {String} pdb_id The PDB id.
@apiSuccess {Integer} start mmcif-style start residue.
@apiSuccess {Integer} unp_end Index of last residue in UniProt sequence mapped to this PDB entity.
@apiSuccess {Integer} unp_start Index of first residue in UniProt sequence mapped to this PDB entity.
@apiSuccess {Float} coverage The ratio of coverage of the UniProt protein [0-1].
@apiSuccess {Float} resolution The higher limit of resolution of crystallographic data as reported by depositors. Null if not available.
@apiSuccess {String} experimental_method The experimental method.
@apiSuccess {Integer} tax_id The taxonomy id.

@apiExample {json=./examples/success/best_structures_residue_range.json} apiSuccessExample Example success response JSON
"""
@app.get('/best_structures/<accession>/<unp_start>/<unp_end>'+reSlashOrNot)
def get_best_structures_residue_range_api(accession, unp_start, unp_end):

    accession = accession.strip("'")
    unp_start_in = unp_start.strip("'")
    unp_end_in = unp_end.strip("'")

    query = """
    MATCH(uniprot:UniProt {ACCESSION:$accession})-[:HAS_UNP_RESIDUE]->(unp_res:UNPResidue)<-[:MAP_TO_UNIPROT_RESIDUE]-(pdb_res:PDBResidue)<-[:HAS_PDB_RESIDUE]-(entity:Entity)-[:HAS_TAXONOMY]->(tax:Taxonomy), (entity)-[:CONTAINS_CHAIN]->(chain:Chain), (entity)<-[:HAS_ENTITY]-(entry:Entry)-[:EXPERIMENT]->(method:Method) WHERE toInteger(unp_res.ID) IN RANGE($unp_start, $unp_end)
    WITH entry.ID AS entry_id, entity.ID AS entity_id, chain.AUTH_ASYM_ID AS chain_id, collect(DISTINCT pdb_res.ID) AS pdb_residues, collect(DISTINCT unp_res.ID) AS unp_residues, MIN(toInteger(pdb_res.ID)) AS pdb_start, MAX(toInteger(pdb_res.ID)) as pdb_end, MIN(toInteger(unp_res.ID)) as unp_start, MAX(toInteger(unp_res.ID)) as unp_end, tax.TAX_ID AS taxonomy_id, method.METHOD AS experiment, toFloat(entry.RESOLUTION) AS resolution, toFloat(entry.R_FACTOR) as r_factor
    RETURN entry_id, entity_id, chain_id, SIZE(pdb_residues)/(unp_end-unp_start) AS coverage, pdb_start, pdb_end, unp_start, unp_end, taxonomy_id, resolution, r_factor, experiment ORDER BY round(3 * toFloat(coverage))/3 DESC, toFloat(resolution) ASC
    """

    mappings = run_query(query, {"accession":accession,
                              "unp_start":int(unp_start_in), "unp_end":int(unp_end_in)})

    if(len(mappings) == 0):
        bottle.response.status = 404
        return {}

    api_result = {
        accession: []
    }

    for mapping in mappings:
        (entry_id, entity_id, chain_id, coverage, pdb_start, pdb_end, unp_start,
         unp_end, taxonomy_id, resolution, r_factor, experiment) = mapping

        # skip entry if unp_start and unp_end not lies within range
        if int(unp_start_in) < int(unp_start) or int(unp_end_in) > int(unp_end):
            continue

        api_result[accession].append({
            "end": pdb_end,
            "entity_id": int(entity_id),
            "chain_id": chain_id,
            "pdb_id": entry_id,
            "start": pdb_start,
            "unp_end": unp_end,
            "coverage": None if coverage is None else float("%.3f" % float(coverage)),
            "unp_start": unp_start,
            "resolution": None if resolution is None else float("%.1f" % float(resolution)),
            "experimental_method": experiment,
            "tax_id": int(taxonomy_id)
        })

    bottle.response.status = 200
    return api_result

@app.get('/best_structures/<uniprot_accession>'+reSlashOrNot)
def get_best_structures_observed_api(uniprot_accession):

    uniprot_accession = uniprot_accession.strip("'")

    api_result = {
        uniprot_accession: []
    }
    result, status = get_best_structures_observed(uniprot_accession)

    if status == 404:
        bottle.response.status = 404
        return api_result    

    api_result[uniprot_accession] = result

    bottle.response.status = 200
    return api_result


def get_best_structures_observed(accession):

    is_processed_protein = True if accession.startswith('PRO_') else False

    query = """
    MATCH (unp:UniProt)<-[unp_rel:HAS_UNIPROT_OBS_SEGMENT]-(entity:Entity)<-[:HAS_ENTITY]-(entry:Entry)
    WHERE unp.ACCESSION=$accession
    OPTIONAL MATCH (entry)-[:EXPERIMENT]->(method:Method)
    OPTIONAL MATCH (unp)-[:HAS_TAXONOMY]->(tax:Taxonomy)
    OPTIONAL MATCH (entity)-[:IS_PART_OF_ASSEMBLY]->(assembly:Assembly {PREFERED:'True'})
    RETURN toInteger(unp.LENGTH), toInteger(assembly.ID), entry.ID, toInteger(tax.TAX_ID), method.METHOD, toInteger(entity.ID), entry.RESOLUTION, unp_rel.OBSERVED, unp_rel.STRUCT_ASYM_ID, toInteger(unp_rel.UNP_START) AS unp_start, toInteger(unp_rel.UNP_END), toInteger(unp_rel.PDB_START), toInteger(unp_rel.PDB_END)
    ORDER BY toInteger(unp_rel.RANKING_SCORES[7]) DESC, unp_start
    """

    if is_processed_protein:
        query = """
        MATCH (:UniProt {ACCESSION:$accession})<-[rel:IS_A_POLYPROTEIN_OF]-(unp:UniProt)
        WITH unp, toInteger(rel.UNP_START) AS unpStart, toInteger(rel.UNP_END) AS unpEnd
        MATCH (unp)<-[unp_rel:HAS_UNIPROT_OBS_SEGMENT]-(entity:Entity)<-[:HAS_ENTITY]-(entry:Entry)
        WHERE toInteger(unp_rel.UNP_START) >= unpStart AND toInteger(unp_rel.UNP_END) <= unpEnd
        OPTIONAL MATCH (entry)-[:EXPERIMENT]->(method:Method)
        OPTIONAL MATCH (unp)-[:HAS_TAXONOMY]->(tax:Taxonomy)
        OPTIONAL MATCH (entity)-[:IS_PART_OF_ASSEMBLY]->(assembly:Assembly {PREFERED:'True'})
        RETURN toInteger(unp.LENGTH), toInteger(assembly.ID), entry.ID, toInteger(tax.TAX_ID), method.METHOD, toInteger(entity.ID), entry.RESOLUTION, unp_rel.OBSERVED, unp_rel.STRUCT_ASYM_ID, toInteger(unp_rel.UNP_START) AS unp_start, toInteger(unp_rel.UNP_END), toInteger(unp_rel.PDB_START), toInteger(unp_rel.PDB_END)
        ORDER BY toInteger(unp_rel.RANKING_SCORES[7]) DESC, unp_start
        """

    mappings = run_query(query, parameters={
        "accession": accession
    })

    if not mappings:
        return [], 404

    # get polyprotein and length in case of a processed protein
    polyprotein_accession = polyprotein_length = None

    if is_processed_protein:
        query = """
        MATCH (:UniProt {ACCESSION:$accession})<-[rel:IS_A_POLYPROTEIN_OF]-(unp:UniProt)
        RETURN unp.ACCESSION, unp.LENGTH
        """

        results = run_query(query, parameters={
            "accession": accession
        })

        if not results:
            return [], 404

        (polyprotein_accession, polyprotein_length) = results[0]

    dict_results = OrderedDict()
    result = []

    for mapping in mappings:
        (unp_length, assembly_id, entry_id, tax_id, experiment, entity_id, resolution, observed, chain_id, unp_start, unp_end, pdb_start, pdb_end) = mapping
        dict_key = (entry_id, entity_id, chain_id, tax_id, experiment)

        if dict_results.get(dict_key) is None:
        
            dict_results[dict_key] = {
                "experimental_method": experiment,
                "tax_id": tax_id,
                "resolution": None if resolution is None else float("%.2f" % float(resolution)),
                "pdb_id": entry_id,
                "chain_id": chain_id,
                "entity_id": entity_id,
                "preferred_assembly_id": assembly_id,
                "observed_regions": [],
                "pdb_segment": [(pdb_start, pdb_end)],
                "unp_segment": [(unp_start, unp_end)],
                "obs_segment": []
            }

            if is_processed_protein:
                dict_results[dict_key].update({
                    "uniprot_id": polyprotein_accession,
                    "uniprot_length": int(polyprotein_length)
                })

        else:
            dict_results[dict_key]["pdb_segment"].append((pdb_start, pdb_end))
            dict_results[dict_key]["unp_segment"].append((unp_start, unp_end))

        if observed == 'Y':
            dict_results[dict_key]["observed_regions"].append({
                "unp_start": unp_start,
                "unp_end": unp_end
            })
            dict_results[dict_key]["obs_segment"].append((unp_start, unp_end))

    for dict_key in dict_results.keys():
        
        dict_results[dict_key]["pdb_segment"] = sorted(dict_results[dict_key]["pdb_segment"])
        dict_results[dict_key]["unp_segment"] = sorted(dict_results[dict_key]["unp_segment"])

        dict_results[dict_key]["start"] = dict_results[dict_key]["pdb_segment"][0][0]
        dict_results[dict_key]["end"] = dict_results[dict_key]["pdb_segment"][-1][1]
        dict_results[dict_key]["unp_start"] = dict_results[dict_key]["unp_segment"][0][0]
        dict_results[dict_key]["unp_end"] = dict_results[dict_key]["unp_segment"][-1][1]

        coverage = count_residues(dict_results[dict_key]["obs_segment"]) / unp_length
        dict_results[dict_key]["coverage"] = float("%.2f" % float(coverage))
        
        del dict_results[dict_key]["pdb_segment"], dict_results[dict_key]["unp_segment"], dict_results[dict_key]["obs_segment"]

        result.append(dict_results[dict_key])

    # get the best coverage structures on top
    # result = sorted(result, key=itemgetter("coverage"), reverse=True)

    return result, 200


"""
@api {get} uniprot/best_non_overlapping_structures/:accession Get non-overlapping structures for a UniProt accession
@apiName GetUNPNonOverlappingStructures
@apiGroup UniProt
@apiDescription This call provides details on non-overlapping PDB chains with the highest number of observed residues for the UniProt accession. 
@apiVersion 2.0.0

@apiParam {String} accession=P07550 UniProt Accession

@apiSuccess {String} chain_id PDB chain id.
@apiSuccess {Integer} entity_id Entity id (molecule number in mmcif-speak).
@apiSuccess {Integer} start Starting PDB residue number. mmcif-style residue index (within entity or struct_asym_id).
@apiSuccess {Integer} end Ending PDB residue number. mmcif-style residue index (within entity or struct_asym_id).
@apiSuccess {Integer} unp_start Starting UniProt residue number.
@apiSuccess {Integer} unp_end Ending UniProt residue number.
@apiSuccess {String} pdb_id Four letter PDB code.
"""
@app.get('/best_non_overlapping_structures/<uniprot_accession>'+reSlashOrNot)
def get_uniprot_best_non_overlapping_structures_api(uniprot_accession):

    uniprot_accession = uniprot_accession.strip("'")
    
    api_result = {
        uniprot_accession: []
    }

    result, resp_status = get_uniprot_best_non_overlapping_structures(uniprot_accession, 'N')

    api_result[uniprot_accession] = result

    bottle.response.status = resp_status
    return api_result


def get_uniprot_best_non_overlapping_structures(uniprot_accession, return_flag):

    result, resp_status = get_best_structures_observed(uniprot_accession)
    # init all to None
    smallest_range = considered_range_set = final_result = None
    
    # filter only non-overlapping best structures, JIRA PDBE-2063
    for structure in result:
        unp_start = structure["unp_start"]
        unp_end = structure["unp_end"] + 1 # fix for PDBE-2273
        current_range = set(range(unp_start, unp_end))

        # first range is always the smallest range, also the first structure
        if not final_result:
            smallest_range = current_range
            considered_range_set = current_range
            final_result = [structure]

        else:

            # check if current range is smallest
            if len(current_range) < len(smallest_range) and len(current_range) != 0:
                smallest_range = current_range
            
            # check if current range overlaps with considered ranges (with allowed overlap), if not append it
            # number of overlap residues = ALLOWED_OVERLAP_PERCENT_BEST_STRUCTURE% of the smallest range
            allowed_overlap = len(smallest_range) * ALLOWED_OVERLAP_PERCENT_BEST_STRUCTURE/100

            if len(list(current_range & considered_range_set)) <= allowed_overlap:
                considered_range_set = considered_range_set.union(current_range)
                final_result.append(structure)

    if not final_result:
        if return_flag == 'N':
            return [], 404
        else:
            return {uniprot_accession: []}, 404

    if return_flag == 'N':
        return final_result, 200
    else:
        return { uniprot_accession: final_result }, 200
        

@app.get('/ligands/<uniprot_accession>'+reSlashOrNot)
def get_uniprot_ligands_api(uniprot_accession):

    uniprot_accession = uniprot_accession.strip("'")
    api_result = {
        uniprot_accession: []
    }

    ligand_mappings, ligand_ligand_mapping_status = get_uniprot_ligands(uniprot_accession)

    if not ligand_mappings:
        bottle.response.status = 404
        return {}

    api_result[uniprot_accession] = ligand_mappings

    bottle.response.status = 200
    return api_result


def get_uniprot_ligands(uniprot_accession):

    # JIRA PDBE-2120 - Get list of ligands interacting with protein. Also find other roles of those ligands like co-factor, enzyme, drug etc. Using OPTIONAL MATCH for them
    # JIRA PDBE-2371 - Using OPTIONAL MATCH on EC
    # JIRA PDBE-2388 - Added sorting
    # JIRA PDBE-2427 - Added INTERACTS_WITH_ARP relationship check to get direct interaction flag between entities
    ligand_query = """
    MATCH (unp:UniProt {ACCESSION:$uniprot_accession})<-[:HAS_UNIPROT]-(src_entity:Entity)<-[:HAS_ENTITY]-(entry:Entry)-[:HAS_ENTITY]->(dest_entity:Entity)-[:IS_A]->(chem_comp:ChemicalComponent)
        WHERE dest_entity.POLYMER_TYPE IN ['B','S']
    OPTIONAL MATCH (src_entity)-[:HAS_EC]->(ec:EC)
    OPTIONAL MATCH (chem_comp)-[:ACTS_AS_COFACTOR]->(cofactor:COFactorClass)
    OPTIONAL MATCH (chem_comp)-[:CONTAINS_FRAGMENT]->(fragment:Fragment)
    OPTIONAL MATCH (chem_comp)-[:CONTAINS_SCAFFOLD]->(scaffold:Scaffold)
    OPTIONAL MATCH (src_entity)-[r:INTERACTS_WITH_ARP]-(dest_entity)
    OPTIONAL MATCH (unp)-[t:IS_A_TARGET]->(chem_comp)-[:HAS_DRUGBANK]->(dBank:DrugBank)
    OPTIONAL MATCH (unp)-[rx:HAS_REACTION]->(rxn:Reaction)-[:CONTAINS]->(chembl:ChEBI)<-[:HAS_CHEBI]-(chem_comp)
    WITH rxn.ID as rxn_id , chembl.ID as chembl_id, t, dBank,chem_comp.ID AS chem_comp_list, chem_comp.NAME AS entity_desc, COLLECT(DISTINCT r.DIRECT) AS direct_interaction, COLLECT(DISTINCT SPLIT(dest_entity.UNIQID, '_')[0]) AS entities, cofactor.COFACTOR_ID AS cofactor_id, COLLECT(DISTINCT fragment.UNIQID) AS fragments, scaffold.UNIQID AS scaffold_id
    RETURN rxn_id, chembl_id, t, dBank, chem_comp_list, entity_desc, direct_interaction, entities, cofactor_id, fragments, scaffold_id ORDER BY cofactor_id, scaffold_id, LENGTH(entities) DESC
    """

    if uniprot_accession.startswith('PRO_'):
        ligand_query = """
        MATCH (u:UniProt {ACCESSION:$uniprot_accession})-[:HAS_UNP_RESIDUE]->(ur:UNPResidue)
        WITH toInteger(ur.ID) AS residue, SPLIT(ur.UNIQID, '_')[0] AS accession ORDER BY residue
        WITH COLLECT(residue) AS residues, accession
        MATCH (unp:UniProt {ACCESSION:accession})<-[rel:HAS_UNIPROT_SEGMENT]-(src_entity:Entity) WHERE toInteger(rel.UNP_START) >= residues[0] AND toInteger(rel.UNP_END) <= residues[-1]
        WITH DISTINCT src_entity
        MATCH (src_entity)<-[:HAS_ENTITY]-(entry:Entry)-[:HAS_ENTITY]->(dest_entity:Entity)-[:IS_A]->(chem_comp:ChemicalComponent) WHERE dest_entity.POLYMER_TYPE IN ['B','S']
            OPTIONAL MATCH (src_entity)-[:HAS_EC]->(ec:EC)
            OPTIONAL MATCH (chem_comp)-[:ACTS_AS_COFACTOR]->(cofactor:COFactorClass)
            OPTIONAL MATCH (chem_comp)-[:CONTAINS_FRAGMENT]->(fragment:Fragment)
            OPTIONAL MATCH (chem_comp)-[:CONTAINS_SCAFFOLD]->(scaffold:Scaffold)
            OPTIONAL MATCH (src_entity)-[r:INTERACTS_WITH_ARP]-(dest_entity)
            OPTIONAL MATCH (unp)-[t:IS_A_TARGET]->(chem_comp)-[:HAS_DRUGBANK]->(dBank:DrugBank)
            OPTIONAL MATCH (unp)-[rx:HAS_REACTION]->(rxn:Reaction)-[:CONTAINS]->(chembl:ChEBI)<-[:HAS_CHEBI]-(chem_comp)
            WITH rxn.ID as rxn_id , chembl.ID as chembl_id, t, dBank, chem_comp.ID AS chem_comp_list, chem_comp.NAME AS entity_desc, COLLECT(DISTINCT r.DIRECT) AS direct_interaction, COLLECT(DISTINCT SPLIT(dest_entity.UNIQID, '_')[0]) AS entities, cofactor.COFACTOR_ID AS cofactor_id, COLLECT(DISTINCT fragment.UNIQID) AS fragments, scaffold.UNIQID AS scaffold_id
            RETURN rxn_id, chembl_id, t, dBank, chem_comp_list, entity_desc, direct_interaction, entities, cofactor_id, fragments, scaffold_id ORDER BY cofactor_id, scaffold_id, LENGTH(entities) DESC
        """

    ligand_mappings = run_query(ligand_query, parameters={
        "uniprot_accession": str(uniprot_accession)
    })

    if not ligand_mappings:
        return {}, 404

    final_result = []

    for ligand_mapping in ligand_mappings:
        (rxn_id, chembl_id, target, drugbank_id, chem_comp_id, description, direct_interactions, entities, cofactor_id, fragments, scaffold_id) = ligand_mapping

        ligand_dict = {}
        ligand_dict[chem_comp_id] = {
            "name": description,
            "pdbs": entities,
            "acts_as": [],
            "scaffold_id": scaffold_id,
            "fragments": fragments,
            "directly_interacts": True if direct_interactions and direct_interactions[0] == 'Y' else False
        }

        if cofactor_id:
            ligand_dict[chem_comp_id]["acts_as"].append("cofactor-like")
        if drugbank_id:
            ligand_dict[chem_comp_id]["acts_as"].append("drug-like")
        if rxn_id and chembl_id : 
            ligand_dict[chem_comp_id]["acts_as"].append("reactant-like")

        final_result.append(ligand_dict)

    return final_result, 200


@app.get('/interaction_partners/<uniprot_accession>'+reSlashOrNot)
def get_uniprot_interaction_partners_api(uniprot_accession):

    uniprot_accession = uniprot_accession.strip("'")

    api_result = {
        uniprot_accession: []
    }
    
    partners_mapping = get_uniprot_generic_interface_residues_api(uniprot_accession)
    
    if not partners_mapping:
        bottle.response.status = 404
        return {}

    temp_list = []
    antibody_entities = list()

    for partner in partners_mapping[uniprot_accession]["data"]:
        
        accession = partner["accession"]
        name = partner["name"]
        search_result = re.search("^(.*) \(([A-Z0-9-]+)\)$", accession)
        annotation_type = partner["additionalData"]["type"]

        if search_result:
            accession = search_result.group(2)
            name = search_result.group(1)

        pdb_entities = set()

        for residue in partner["residues"]:
            for pdb_entry in residue["interactingPDBEntries"]:
                pdb_entities.add("{}_{}".format(pdb_entry["pdbId"], pdb_entry["entityId"]))

        temp_fragment = {
            "accession": accession,
            "pdbs": list(pdb_entities),
            "name": name,
            "is_self": False,
            "acts_as": []
        }

        # handle PRD
        if annotation_type == "PRD":
            temp_fragment["acts_as"].append(f'{partner["additionalData"]["prdType"]} {partner["additionalData"]["prdClass"]}')
        # handle antibody
        elif annotation_type == "AB":
            temp_fragment["acts_as"].append('antibody')
            antibody_entities.extend(list(pdb_entities))
        # set a flag for UNP
        elif annotation_type == "UNP":
            temp_fragment["type"] = "UNP"

        # added for PDBE-3246
        if accession == uniprot_accession:
            temp_fragment.update({
                "is_self": True
            })

        temp_list.append(temp_fragment)
    
    # sort the list based on highest number of PDB entities
    temp_list = sorted(temp_list, key=lambda x: len(x["pdbs"]), reverse=True)
    
    # PDBE-3246: make self interaction partners comes first
    temp_list = sorted(temp_list, key=lambda x: x["is_self"], reverse=True)

    for record in temp_list:
        anno_type = record.get("type")
        acts_as = record["acts_as"]
        
        # check if a UNP is an antibody
        if anno_type and anno_type == "UNP":
            if len(set(record["pdbs"]).intersection(antibody_entities)) != 0:
                acts_as.append("antibody")

        api_result[uniprot_accession].append({
            record["accession"]: {
                "pdbs": record["pdbs"],
                "name": record["name"],
                "is_self": record["is_self"],
                "acts_as": acts_as
            }
        })

    bottle.response.status = 200
    return api_result



"""
@api {get} uniprot/secondary_structures/:accession Get secondary structure mappings for a UniProt accession
@apiName GetUNPSecStruct
@apiGroup UniProt
@apiDescription This call provides details on mapped secondary structures for a UniProt accession.
@apiVersion 2.0.0

@apiParam {String} accession=P07550 UniProt Accession

@apiUse GenericFields

@apiExample {json=./examples/success/uniprot_generic_secondary_structures.json} apiSuccessExample Example success response JSON
"""
@app.get('/secondary_structures/<uniprot_accession>'+reSlashOrNot)
def get_uniprot_generic_secondary_structures_api(uniprot_accession):

    uniprot_accession = uniprot_accession.strip("'")

    api_result = get_generic_structure(uniprot_accession, "SECONDARY STRUCTURES")

    is_processed_protein = True if uniprot_accession.startswith("PRO_") else False
    
    if is_processed_protein:
        processed_protein_start = get_processed_protein_start(uniprot_accession)
        api_result[uniprot_accession].update({
            "processed_protein_start": int(processed_protein_start)
        })

    if api_result is None:
        bottle.response.status = 404
        return {}

    best_non_overlapping_structures, resp_status = get_uniprot_best_non_overlapping_structures(uniprot_accession, 'N')

    if not best_non_overlapping_structures:
        bottle.response.status = 200
        return {}

    all_results = []

    for best_structure in best_non_overlapping_structures:

        secondary_structures_query = """
        MATCH (entry:Entry)-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[chain_rel:IS_IN_CHAIN]->(chain:Chain), (pdb_res)-[:MAP_TO_UNIPROT_RESIDUE]->(unp_res:UNPResidue)<-[:HAS_UNP_RESIDUE]-(unp:UniProt)
        WHERE entry.ID=$entry_id AND entity.ID=$entity_id AND chain.STRUCT_ASYM_ID=$chain_id AND unp.ACCESSION=$accession AND chain_rel.OBSERVED='Y' AND (chain_rel.IS_IN_HELIX='Y' OR exists(chain_rel.SHEETS))
        RETURN entry.ID AS entry, toInteger(entity.ID) AS entity_id, toInteger(unp_res.ID) AS unp_res_id, unp_res.ONE_LETTER_CODE AS unp_res_code, toInteger(pdb_res.ID) AS pdb_res_id, chain.STRUCT_ASYM_ID,
        CASE chain_rel.HELIX_SEGMENT
        WHEN "0"
            THEN null
            ELSE SUBSTRING(chain_rel.HELIX_SEGMENT, 1, LENGTH(chain_rel.HELIX_SEGMENT) - 2)
        END AS helix,
        CASE chain_rel.SHEETS
        WHEN null
        THEN null
        ELSE SUBSTRING(chain_rel.SHEETS, 1, LENGTH(chain_rel.SHEETS) - 2)
        END AS sheet
        ORDER BY toInteger(unp_res_id)
        """

        temp_mapping = run_query(secondary_structures_query, parameters={
            "entry_id": str(best_structure.get("pdb_id")),
            "entity_id": str(best_structure.get("entity_id")),
            "chain_id": str(best_structure.get("chain_id")),
            "accession": str(uniprot_accession)
        })

        all_results.extend(temp_mapping)

    helix_list = []
    sheet_list = []
    idp_list = []

    for mapping in all_results:
        (entry_id, entity_id, res_id, res_code, pdb_res_id, struct_asym_id, helix, sheet) = mapping

        if is_processed_protein:
            res_id -= processed_protein_start - 1

        if helix is not None:

            helix_list.append((("Helix", helix, "Helix"), entry_id, entity_id, struct_asym_id, res_id, pdb_res_id, res_code))

        if sheet is not None:
            sheet_key = (sheet, res_id, res_code, pdb_res_id)

            sheet_list.append((("Strand", sheet, "Strand"), entry_id, entity_id, struct_asym_id, res_id, pdb_res_id, res_code))

    helix_fragments = util_common.generate_data_fragment(helix_list)

    if helix_fragments:
        for helix_fragment in helix_fragments:
            api_result[uniprot_accession]["data"].append(helix_fragment)

    sheet_fragments = util_common.generate_data_fragment(sheet_list)
    
    if sheet_fragments:
        for sheet_fragment in sheet_fragments:
            api_result[uniprot_accession]["data"].append(sheet_fragment)

    for best_structure in best_non_overlapping_structures:
        
        idp_query = """
        MATCH (unp:UniProt)-[:HAS_UNP_RESIDUE]->(unp_res:UNPResidue)
        WHERE unp.ACCESSION=$uniprot_accession AND unp_res.IDP_REGION='Y'
        WITH unp_res
        MATCH (entry:Entry)-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[rel:MAP_TO_UNIPROT_RESIDUE]->(unp_res)
        WHERE entry.ID=$entry_id AND entity.ID=$entity_id
        RETURN entry.ID, toInteger(entity.ID), rel.CHAINS, toInteger(pdb_res.ID), toInteger(unp_res.ID), unp_res.ONE_LETTER_CODE ORDER BY toInteger(unp_res.ID)
        """

        temp_mappings = run_query(idp_query, parameters={
            "uniprot_accession": uniprot_accession,
            "entry_id": best_structure.get("pdb_id"),
            "entity_id": str(best_structure.get("entity_id"))
        })
        
        for mapping in temp_mappings:
            (entry_id, entity_id, chains, pdb_res_id, unp_res_id, unp_res_code) = mapping
            chains = tuple(chains)

            if is_processed_protein:
                unp_res_id -= processed_protein_start - 1

            idp_list.append((("MobiDB", "MobiDB", "MobiDB"), entry_id, entity_id, chains, unp_res_id, pdb_res_id, unp_res_code))
    
    idp_fragments = util_common.generate_data_fragment(idp_list)

    if idp_fragments:
        for idp_fragment in idp_fragments:
            api_result[uniprot_accession]["data"].append(idp_fragment)

    # PDBE-4657 - Add KnotProt annotations
    # PDBE-4660 - Add WEBnma annotations
    # PDBE-4661 - Add dynamine annotations
    other_flexibility_mappings = {}

    for best_structure in best_non_overlapping_structures:
        other_flexibility_query = """
        MATCH
            (entry:Entry)-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->(pdbRes:PDBResidue)<-[resRel:FUNPDBE_ANNOTATION_FOR]-
            (funGroup:FunPDBeResidueGroup)-[:FUNPDBE_RESIDUE_GROUP_OF]->(funEntry:FunPDBeEntry)
        WHERE
            entry.ID=$entry_id AND entity.ID=$entity_id AND funEntry.DATA_RESOURCE IN ['WEBnma', 'KnotProt', 'dynamine'] AND
            entity.BEST_CHAIN_ID=resRel.CHAIN_LABEL
        WITH
            funEntry.DATA_RESOURCE AS dataResource, resRel.RAW_SCORE AS rawScore, resRel.CONFIDENCE_SCORE AS confScore,
            resRel.CONFIDENCE_CLASSIFICATION AS confClass, pdbRes, entity.BEST_CHAIN_ID AS bestChain, funGroup.LABEL AS label
        MATCH (pdbRes)-[chainRel:IS_IN_CHAIN]->(chain:Chain) WHERE chain.STRUCT_ASYM_ID=bestChain
        MATCH (pdbRes)-[:MAP_TO_UNIPROT_RESIDUE]->(unpRes:UNPResidue)<-[:HAS_UNP_RESIDUE]-(unp:UniProt) WHERE unp.ACCESSION=$accession
        RETURN
            dataResource, chain.AUTH_ASYM_ID AS authAsymId, toInteger(unpRes.ID) AS unpResNum, unpRes.ONE_LETTER_CODE AS unpCode,
            toInteger(chainRel.AUTH_SEQ_ID) AS pdbResNum, pdbRes.CHEM_COMP_ID AS pdbCode, toFloat(rawScore) AS rawScore,
            toFloat(confScore) AS confScore, confClass, label
        ORDER BY unpResNum
        """

        temp_mappings = run_query(other_flexibility_query, parameters={
            "accession": uniprot_accession,
            "entry_id": best_structure.get("pdb_id"),
            "entity_id": str(best_structure.get("entity_id")),
        })

        for item in temp_mappings:
            (data_resource, chain_id, unp_index, unp_code, pdb_index, pdb_code, raw_score, conf_score, conf_class, label) = item

            if not other_flexibility_mappings.get(data_resource):
                other_flexibility_mappings[data_resource] = {
                    "name": data_resource,
                    "accession": data_resource,
                    "dataType": data_resource,
                    "residues": []
                }

            other_flexibility_mappings[data_resource]["residues"].append({
                "startIndex": unp_index,
                "endIndex": unp_index,
                "pdbStartIndex": pdb_index,
                "pdbEndIndex": pdb_index,
                "startCode": get_amino_one_to_three(unp_code),
                "endCode": get_amino_one_to_three(unp_code),
                "indexType": "UNIPROT",
                "pdbEntries": [{
                    "pdbId": best_structure.get("pdb_id"),
                    "entityId": best_structure.get("entity_id"),
                    "chainIds": [chain_id]
                }],
                "additionalData": {
                    "rawScore": raw_score,
                    "confidenceScore": conf_score,
                    "confidenceClassification": conf_class,
                    "groupLabel": label,
                }
            })

    for value in other_flexibility_mappings.values():
        api_result[uniprot_accession]["data"].append(value)

    bottle.response.status = 200
    return api_result


@app.get('/protvista/secondary_structures/<uniprot_accession>'+reSlashOrNot)
def get_uniprot_protvista_secondary_structures_api(uniprot_accession):

    generic_response = get_uniprot_generic_secondary_structures_api(uniprot_accession)

    if not generic_response:
        bottle.response.status = 404
        return {}

    bottle.response.status = 200
    return convert_to_protvista("protein", "secstructures", generic_response)


"""
@api {get} uniprot/domains/:accession Get sequence and structural domains for a UniProt accession
@apiName GetUNPDomains
@apiGroup UniProt
@apiDescription This call provides details on sequence and structural domains for a UniProt accession.
@apiVersion 2.0.0

@apiParam {String} accession=P07550 UniProt Accession

@apiUse GenericFields

@apiExample {json=./examples/success/uniprot_generic_domains.json} apiSuccessExample Example success response JSON
"""
@app.get('/domains/<uniprot_accession>'+reSlashOrNot)
def get_uniprot_generic_domains_api(uniprot_accession):

    uniprot_accession = uniprot_accession.strip("'")

    api_result = get_generic_structure(uniprot_accession, "DOMAINS")
    
    is_processed_protein = True if uniprot_accession.startswith("PRO_") else False

    if is_processed_protein:
        processed_protein_start = get_processed_protein_start(uniprot_accession) - 1
        api_result[uniprot_accession].update({
            "processed_protein_start": int(processed_protein_start)
        })

    if api_result is None:
        bottle.response.status = 404
        return {}
    
    pfam_query = """
    MATCH (unp:UniProt {ACCESSION: $uniprot_accession})-[:HAS_UNP_RESIDUE]->(unp_res:UNPResidue)-[:IS_IN_PFAM]->(pfam:Pfam)
    WITH DISTINCT pfam.PFAM_ACCESSION AS accession, pfam.NAME AS pfam_name, unp_res.ID AS unp_res_id, unp_res.ONE_LETTER_CODE AS amino_acid_code ORDER BY toInteger(unp_res_id)
    RETURN accession, pfam_name, COLLECT(unp_res_id) AS unp_residues, COLLECT(amino_acid_code) AS amino_acid_codes
    """

    pfam_mappings = run_query(pfam_query, parameters={
        "uniprot_accession": str(uniprot_accession)
    })

    for pfam_mapping in pfam_mappings:
        (accession, pfam_name, unp_residues, amino_acid_codes) = pfam_mapping

        pfam_fragment = {
            "name": pfam_name,
            "accession": accession,
            "dataType": "Pfam",
            "residues": []
        }

        # handle cases when there is only one residue
        if len(unp_residues) == 1:
            pfam_fragment["residues"].append({
                "startIndex": int(unp_residues[0]) if not is_processed_protein else (int(unp_residues[0]) - processed_protein_start),
                "endIndex": int(unp_residues[0]) if not is_processed_protein else (int(unp_residues[0]) - processed_protein_start),
                "startCode": amino_acid_codes[0],
                "endCode": amino_acid_codes[0],
                "indexType": "UNIPROT"
            })
            
            api_result[uniprot_accession]["data"].append(pfam_fragment)

            continue
    
        for residues in util_common.split_nonconsecutive_residues_with_code([unp_residues, amino_acid_codes]):
            unp_start = int(residues[0][0]) if not is_processed_protein else (int(residues[0][0]) - processed_protein_start)
            unp_end = int(residues[-1][0]) if not is_processed_protein else (int(residues[-1][0]) - processed_protein_start)
            unp_start_code = util_common.get_amino_one_to_three(residues[0][1])
            unp_end_code = util_common.get_amino_one_to_three(residues[-1][1])

            pfam_fragment["residues"].append({
                "startIndex": unp_start,
                "endIndex": unp_end,
                "startCode": unp_start_code,
                "endCode": unp_end_code,
                "indexType": "UNIPROT"
            })

        if pfam_fragment["residues"]:
            api_result[uniprot_accession]["data"].append(pfam_fragment)

    best_non_overlapping_structures, resp_status = get_uniprot_best_non_overlapping_structures(uniprot_accession, 'N')
    
    if best_non_overlapping_structures:
        best_pdb_ids_predicates = []

        for best_structure in best_non_overlapping_structures:
            best_pdb_ids_predicates.append("(entry.ID='{}' AND entity.ID='{}')".format(best_structure.get("pdb_id"), best_structure.get("entity_id")))

        best_pdb_ids_predicate_string = " OR ".join(best_pdb_ids_predicates)
        
        cath_query = """
        MATCH (entry:Entry)-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[:MAP_TO_UNIPROT_RESIDUE]->(unp_res:UNPResidue)<-[:HAS_UNP_RESIDUE]-(unp:UniProt), (pdb_res)-[rel:IS_IN_CATH_DOMAIN]->(cath:CATH)
        WHERE ({}) AND unp.ACCESSION=$uniprot_accession
        WITH DISTINCT entry.ID AS entry_id, entity.ID AS entity_id, toInteger(unp_res.ID) AS unp_res_id, unp_res.ONE_LETTER_CODE AS amino_acid_code, toInteger(pdb_res.ID) AS pdb_res_id, cath.DOMAIN AS domain_id, cath.CATHCODE AS cath_code, cath.HOMOL AS cath_homol, rel.AUTH_ASYM_ID AS auth_asym_id ORDER BY toInteger(unp_res_id)
        RETURN domain_id, cath_code, cath_homol, entry_id, entity_id, auth_asym_id, COLLECT(unp_res_id) AS unp_residues, COLLECT(amino_acid_code) AS amino_acid_codes, COLLECT(pdb_res_id) AS pdb_res_ids
        """.format(best_pdb_ids_predicate_string)
        
        scop_query = """
        MATCH (entry:Entry)-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[:MAP_TO_UNIPROT_RESIDUE]->(unp_res:UNPResidue)<-[:HAS_UNP_RESIDUE]-(unp:UniProt), (pdb_res)-[rel:IS_IN_SCOP_DOMAIN]->(scop:SCOP)
        WHERE ({}) AND unp.ACCESSION=$uniprot_accession
        WITH DISTINCT entry.ID AS entry_id, entity.ID AS entity_id, toInteger(unp_res.ID) AS unp_res_id, unp_res.ONE_LETTER_CODE AS amino_acid_code, toInteger(pdb_res.ID) AS pdb_res_id, rel.SCOP_ID AS scop_id, scop.SUNID AS sunid, scop.DESCRIPTION AS scop_desc, rel.AUTH_ASYM_ID AS auth_asym_id ORDER BY toInteger(unp_res_id)
        RETURN scop_id, sunid, scop_desc, entry_id, entity_id, auth_asym_id, COLLECT(unp_res_id) AS unp_residues, COLLECT(amino_acid_code) AS amino_acid_codes, COLLECT(pdb_res_id) AS pdb_res_ids
        """.format(best_pdb_ids_predicate_string)
        
        cath_b_query = """
        MATCH (entry:Entry)-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[:MAP_TO_UNIPROT_RESIDUE]->(unp_res:UNPResidue)<-[:HAS_UNP_RESIDUE]-(unp:UniProt), (pdb_res)-[rel:IS_IN_CATH_B_DOMAIN]->(cath_b:CATHB)
        WHERE ({}) AND unp.ACCESSION=$uniprot_accession
        WITH DISTINCT entry.ID AS entry_id, entity.ID AS entity_id, toInteger(unp_res.ID) AS unp_res_id, unp_res.ONE_LETTER_CODE AS amino_acid_code, toInteger(pdb_res.ID) AS pdb_res_id, cath_b.DOMAIN AS domain_id, cath_b.CATHCODE AS cath_b_code, cath_b.HOMOL AS cath_b_homol, rel.AUTH_ASYM_ID AS auth_asym_id ORDER BY toInteger(unp_res_id)
        RETURN domain_id, cath_b_code, cath_b_homol, entry_id, entity_id, auth_asym_id, COLLECT(unp_res_id) AS unp_residues, COLLECT(amino_acid_code) AS amino_acid_codes, COLLECT(pdb_res_id) AS pdb_res_ids
        """.format(best_pdb_ids_predicate_string)

        # PDBE-2694: Remove superfamilies
        interpro_query = """
        MATCH (entry:Entry)-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[:MAP_TO_UNIPROT_RESIDUE]->(unp_res:UNPResidue)<-[:HAS_UNP_RESIDUE]-(unp:UniProt), (pdb_res)-[rel:IS_IN_INTERPRO]->(interpro:Interpro)
        WHERE ({}) AND unp.ACCESSION=$uniprot_accession AND interpro.ENTRY_TYPE IN ['F','D']
        WITH DISTINCT entry.ID AS entry_id, entity.ID AS entity_id, toInteger(unp_res.ID) AS unp_res_id, unp_res.ONE_LETTER_CODE AS amino_acid_code, toInteger(pdb_res.ID) AS pdb_res_id, 
            interpro.INTERPRO_ACCESSION AS interpro_acc, interpro.NAME AS interpro_name, interpro.ENTRY_TYPE AS entry_type, rel.AUTH_ASYM_ID AS auth_asym_id ORDER BY toInteger(unp_res_id)
        WITH interpro_acc, interpro_name, entry_type, entry_id, entity_id, auth_asym_id, COLLECT(unp_res_id) AS unp_residues, COLLECT(amino_acid_code) AS amino_acid_codes, COLLECT(pdb_res_id) AS pdb_res_ids
        RETURN interpro_acc, interpro_name, entry_type, entry_id, entity_id, auth_asym_id, unp_residues, amino_acid_codes, pdb_res_ids ORDER BY entry_type DESC
        """.format(best_pdb_ids_predicate_string)
        
        dict_cath = dict()
        dict_scop = dict()
        dict_cath_b = dict()
        dict_interpro = dict()

        # PDBE-2556: Add CATH-B only if different from CATH
        cath_temp_list = []

        cath_mappings = run_query(cath_query, parameters={
            "uniprot_accession": str(uniprot_accession)
        })
        
        for cath_mapping in cath_mappings:
            (domain_id, cath_code, cath_homol, entry_id, entity_id, auth_asym_id, unp_residues, amino_acid_codes, pdb_residues) = cath_mapping

            cath_fragment = {
                "name": cath_homol,
                "accession": domain_id,
                "dataType": "CATH",
                "residues": []
            }

            # handle cases when there is only one residue
            if len(unp_residues) == 1:
                cath_fragment["residues"].append({
                    "startIndex": int(unp_residues[0]) if not is_processed_protein else (int(unp_residues[0]) - processed_protein_start),
                    "endIndex": int(unp_residues[0]) if not is_processed_protein else (int(unp_residues[0]) - processed_protein_start),
                    "pdbStartIndex": int(pdb_residues[0]),
                    "pdbEndIndex": int(pdb_residues[0]),
                    "startCode": amino_acid_codes[0],
                    "endCode": amino_acid_codes[0],
                    "indexType": "UNIPROT",
                    "pdbEntries": [{
                        "pdbId": entry_id,
                        "entityId": int(entity_id),
                        "chainIds": [auth_asym_id]
                    }]
                })

                cath_temp_list.append((int(unp_residues[0]), int(unp_residues[0]), domain_id))
                
                if cath_fragment["residues"]:
                    api_result[uniprot_accession]["data"].append(cath_fragment)

                continue

            for residues in util_common.split_nonconsecutive_two_residues_with_code([unp_residues, pdb_residues, amino_acid_codes]):
                unp_start = int(residues[0][0]) if not is_processed_protein else (int(residues[0][0]) - processed_protein_start)
                unp_end = int(residues[-1][0]) if not is_processed_protein else (int(residues[-1][0]) - processed_protein_start)
                pdb_start = int(residues[0][1])
                pdb_end = int(residues[-1][1])
                unp_start_code = util_common.get_amino_one_to_three(residues[0][2])
                unp_end_code = util_common.get_amino_one_to_three(residues[-1][2])

                cath_fragment["residues"].append({
                    "startIndex": unp_start,
                    "endIndex": unp_end,
                    "pdbStartIndex": pdb_start,
                    "pdbEndIndex": pdb_end,
                    "startCode": unp_start_code,
                    "endCode": unp_end_code,
                    "indexType": "UNIPROT",
                    "pdbEntries": [{
                        "pdbId": entry_id,
                        "entityId": int(entity_id),
                        "chainIds": [auth_asym_id]
                    }]
                })

                cath_temp_list.append((unp_start, unp_end, domain_id))

            if cath_fragment["residues"]:
                api_result[uniprot_accession]["data"].append(cath_fragment)
          
        cath_b_mappings = run_query(cath_b_query, parameters={
            "uniprot_accession": str(uniprot_accession)
        })

        for cath_b_mapping in cath_b_mappings:
            (domain_id, cath_b_code, cath_b_homol, entry_id, entity_id, auth_asym_id, unp_residues, amino_acid_codes, pdb_residues) = cath_b_mapping

            cath_b_fragment = {
                "name": cath_b_homol,
                "accession": domain_id,
                "dataType": "CATH-B",
                "residues": []
            }

            # handle cases when there is only one residue
            if len(unp_residues) == 1:
                
                unp_start = unp_end = int(unp_residues[0]) if not is_processed_protein else (int(unp_residues[0]) - processed_protein_start)

                # PDBE-2556: Add CATH-B only if different from CATH
                if ((unp_start, unp_end, domain_id)) in cath_temp_list:
                    continue

                cath_b_fragment["residues"].append({
                    "startIndex": unp_start,
                    "endIndex": unp_end,
                    "pdbStartIndex": int(pdb_residues[0]),
                    "pdbEndIndex": int(pdb_residues[0]),
                    "startCode": amino_acid_codes[0],
                    "endCode": amino_acid_codes[0],
                    "indexType": "UNIPROT",
                    "pdbEntries": [{
                        "pdbId": entry_id,
                        "entityId": int(entity_id),
                        "chainIds": [auth_asym_id]
                    }]
                })
                
                if cath_b_fragment["residues"]:
                    api_result[uniprot_accession]["data"].append(cath_b_fragment)
                
                continue

            for residues in util_common.split_nonconsecutive_two_residues_with_code([unp_residues, pdb_residues, amino_acid_codes]):

                unp_start = int(residues[0][0]) if not is_processed_protein else (int(residues[0][0]) - processed_protein_start)
                unp_end = int(residues[-1][0]) if not is_processed_protein else (int(residues[-1][0]) - processed_protein_start)
                pdb_start = int(residues[0][1])
                pdb_end = int(residues[-1][1])
                unp_start_code = util_common.get_amino_one_to_three(residues[0][2])
                unp_end_code = util_common.get_amino_one_to_three(residues[-1][2])

                # PDBE-2556: Add CATH-B only if different from CATH
                if ((unp_start, unp_end, domain_id)) in cath_temp_list:
                    continue

                cath_b_fragment["residues"].append({
                    "startIndex": unp_start,
                    "endIndex": unp_end,
                    "pdbStartIndex": pdb_start,
                    "pdbEndIndex": pdb_end,
                    "startCode": unp_start_code,
                    "endCode": unp_end_code,
                    "indexType": "UNIPROT",
                    "pdbEntries": [{
                        "pdbId": entry_id,
                        "entityId": int(entity_id),
                        "chainIds": [auth_asym_id]
                    }]
                })

            if cath_b_fragment["residues"]:
                api_result[uniprot_accession]["data"].append(cath_b_fragment)

        scop_mappings = run_query(scop_query, parameters={
            "uniprot_accession": str(uniprot_accession)
        })

        for scop_mapping in scop_mappings:
            (scop_id, sunid, scop_desc, entry_id, entity_id, auth_asym_id, unp_residues, amino_acid_codes, pdb_residues) = scop_mapping

            scop_fragment = {
                "name": scop_desc,
                "accession": scop_id,
                "dataType": "SCOP",
                "residues": []
            }

            # handle cases when there is only one residue
            if len(unp_residues) == 1:
                
                scop_fragment["residues"].append({
                    "startIndex": int(unp_residues[0]) if not is_processed_protein else (int(unp_residues[0]) - processed_protein_start),
                    "endIndex": int(unp_residues[0]) if not is_processed_protein else (int(unp_residues[0]) - processed_protein_start),
                    "pdbStartIndex": int(pdb_residues[0]),
                    "pdbEndIndex": int(pdb_residues[0]),
                    "startCode": amino_acid_codes[0],
                    "endCode": amino_acid_codes[0],
                    "indexType": "UNIPROT",
                    "pdbEntries": [{
                        "pdbId": entry_id,
                        "entityId": int(entity_id),
                        "chainIds": [auth_asym_id]
                    }]
                })
                
                if scop_fragment["residues"]:
                    api_result[uniprot_accession]["data"].append(scop_fragment)
                
                continue

            for residues in util_common.split_nonconsecutive_two_residues_with_code([unp_residues, pdb_residues, amino_acid_codes]):

                unp_start = int(residues[0][0]) if not is_processed_protein else (int(residues[0][0]) - processed_protein_start)
                unp_end = int(residues[-1][0]) if not is_processed_protein else (int(residues[-1][0]) - processed_protein_start)
                pdb_start = int(residues[0][1])
                pdb_end = int(residues[-1][1])
                unp_start_code = util_common.get_amino_one_to_three(residues[0][2])
                unp_end_code = util_common.get_amino_one_to_three(residues[-1][2])

                scop_fragment["residues"].append({
                    "startIndex": unp_start,
                    "endIndex": unp_end,
                    "pdbStartIndex": pdb_start,
                    "pdbEndIndex": pdb_end,
                    "startCode": unp_start_code,
                    "endCode": unp_end_code,
                    "indexType": "UNIPROT",
                    "pdbEntries": [{
                        "pdbId": entry_id,
                        "entityId": int(entity_id),
                        "chainIds": [auth_asym_id]
                    }]
                })

            if scop_fragment["residues"]:
                api_result[uniprot_accession]["data"].append(scop_fragment)

        interpro_mappings = run_query(interpro_query, parameters={
            "uniprot_accession": str(uniprot_accession)
        })

        dict_temp_residues = {}
        
        for interpro_mapping in interpro_mappings:
            (interpro_acc, interpro_name, entry_type, entry_id, entity_id, auth_asym_id, unp_residues, amino_acid_codes, pdb_residues) = interpro_mapping

            # PDBE-2694: annotations of type family will have priority over domains, consider domains if no family found for the location
            # This is why query is sorted based on descending order of ENTRY_TYPE, so that family always comes before domain
            temp_key = (entry_id, entity_id, auth_asym_id)
            if dict_temp_residues.get(temp_key) is None:
                
                dict_temp_residues[temp_key] = unp_residues
            else:
                list_to_check = dict_temp_residues[temp_key]

                if any(elem in unp_residues for elem in list_to_check):
                    continue
                else:
                    dict_temp_residues[temp_key].extend(unp_residues)

            interpro_fragment = {
                "name": interpro_name,
                "accession": interpro_acc,
                "dataType": "InterPro",
                "residues": []
            }

            # handle cases when there is only one residue
            if len(unp_residues) == 1:
                
                interpro_fragment["residues"].append({
                    "startIndex": int(unp_residues[0]) if not is_processed_protein else (int(unp_residues[0]) - processed_protein_start),
                    "endIndex": int(unp_residues[0]) if not is_processed_protein else (int(unp_residues[0]) - processed_protein_start),
                    "pdbStartIndex": int(pdb_residues[0]),
                    "pdbEndIndex": int(pdb_residues[0]),
                    "startCode": amino_acid_codes[0],
                    "endCode": amino_acid_codes[0],
                    "indexType": "UNIPROT",
                    "pdbEntries": [{
                        "pdbId": entry_id,
                        "entityId": int(entity_id),
                        "chainIds": [auth_asym_id]
                    }]
                })
                
                if interpro_fragment["residues"]:
                    api_result[uniprot_accession]["data"].append(interpro_fragment)
                
                continue

            for residues in util_common.split_nonconsecutive_two_residues_with_code([unp_residues, pdb_residues, amino_acid_codes]):

                unp_start = int(residues[0][0]) if not is_processed_protein else (int(residues[0][0]) - processed_protein_start)
                unp_end = int(residues[-1][0]) if not is_processed_protein else (int(residues[-1][0]) - processed_protein_start)
                pdb_start = int(residues[0][1])
                pdb_end = int(residues[-1][1])
                unp_start_code = util_common.get_amino_one_to_three(residues[0][2])
                unp_end_code = util_common.get_amino_one_to_three(residues[-1][2])

                interpro_fragment["residues"].append({
                    "startIndex": unp_start,
                    "endIndex": unp_end,
                    "pdbStartIndex": pdb_start,
                    "pdbEndIndex": pdb_end,
                    "startCode": unp_start_code,
                    "endCode": unp_end_code,
                    "indexType": "UNIPROT",
                    "pdbEntries": [{
                        "pdbId": entry_id,
                        "entityId": int(entity_id),
                        "chainIds": [auth_asym_id]
                    }]
                })

            if interpro_fragment["residues"]:
                api_result[uniprot_accession]["data"].append(interpro_fragment)

    # segregate entries based on domain, domainType, startIndex and endIndex
    domains_dict = dict()
    domains_residue_dict = dict()
    
    for domain in api_result[uniprot_accession]["data"]:
        dict_key = (domain["accession"], domain["dataType"])

        if domains_dict.get(dict_key) is None:
            domains_dict[dict_key] = dict()

            for key in domain.keys():
                if key == "residues":
                    domains_dict[dict_key]["residues"] = []
                    continue
                domains_dict[dict_key][key] = domain[key]

        for residue in domain["residues"]:
            residue_dict_key = (domain["accession"], domain["dataType"], residue["startIndex"], residue["endIndex"])

            if domains_residue_dict.get(residue_dict_key) is None:
                # some domains don't have PDB data. for eg. Pfam
                pdb_data_available = residue.get("pdbStartIndex") is not None
                
                domains_residue_dict[residue_dict_key] = {
                    "startIndex": residue["startIndex"],
                    "endIndex": residue["endIndex"],
                    "startCode": residue["startCode"],
                    "endCode": residue["endCode"],
                    "indexType": residue["indexType"]
                }
                if pdb_data_available:
                    domains_residue_dict[residue_dict_key]["pdbStartIndex"] = residue["pdbStartIndex"]
                    domains_residue_dict[residue_dict_key]["pdbEndIndex"] = residue["pdbEndIndex"]
                    domains_residue_dict[residue_dict_key]["pdbEntries"] = residue["pdbEntries"]
            else:
                pdb_entries = residue.get("pdbEntries") if residue.get("pdbEntries") else []
                for x in pdb_entries:
                    domains_residue_dict[residue_dict_key]["pdbEntries"].append(x)

    # clear everything from result list
    api_result[uniprot_accession]["data"].clear()

    for key, value in domains_residue_dict.items():
        (accession, data_type, start_index, end_index) = key
        domains_dict[(accession, data_type)]["residues"].append(value)

    for key, value in domains_dict.items():
        api_result[uniprot_accession]["data"].append(value)

    bottle.response.status = 200
    return api_result


@app.get('/protvista/domains/<uniprot_accession>'+reSlashOrNot)
def get_uniprot_protvista_domains_api(uniprot_accession):

    uniprot_accession = uniprot_accession.strip("'")
    generic_response = get_uniprot_generic_domains_api(uniprot_accession)

    if not generic_response:
        bottle.response.status = 404
        return {}
    
    if not generic_response[uniprot_accession]['data']:
        bottle.response.status = 404
        return {}

    bottle.response.status = 200
    return convert_to_protvista("protein", "domains", generic_response)


"""
@api {get} uniprot/unipdb/:accession Get PDB structure mappings for a UniProt accession
@apiName GetUNPUniPDB
@apiGroup UniProt
@apiDescription This call provides details on mapped PDB structures for a UniProt accession.
@apiVersion 2.0.0

@apiParam {String} accession=P07550 UniProt Accession

@apiUse GenericFields

@apiExample {json=./examples/success/uniprot_generic_unipdb.json} apiSuccessExample Example success response JSON
"""
@app.get('/unipdb/<uniprot_accession>'+reSlashOrNot)
def get_uniprot_generic_unipdb_api(uniprot_accession):

    uniprot_accession = uniprot_accession.strip("'")
    is_polyprotein = False

    if uniprot_accession.startswith('PRO_'):
        is_polyprotein = True

    api_result = get_generic_structure(uniprot_accession, "UNIPDB")

    if api_result is None:
        bottle.response.status = 404
        return {}

    # get structures mapped to the protein
    query = """
    MATCH (unp:UniProt {ACCESSION:$uniprot_accession})<-[:HAS_UNIPROT]-(entity:Entity)
    WITH unp.ACCESSION AS accession, COLLECT(DISTINCT SPLIT(entity.UNIQID, '_')[0]) AS pdb_entries
    RETURN pdb_entries
    """

    if is_polyprotein:
        query = """
        MATCH (u:UniProt {ACCESSION:$uniprot_accession})-[:HAS_UNP_RESIDUE]->(ur:UNPResidue) WHERE NOT ur.MAPPED_PDB_ENTITIES IS null
        WITH [x IN ur.MAPPED_PDB_ENTITIES | SPLIT(x, '_')[0]] AS entries
        RETURN apoc.coll.toSet(apoc.coll.flatten(COLLECT(entries))) AS pdb_entries
        """

    mappings = run_query(query, parameters={
        "uniprot_accession": str(uniprot_accession)
    })

    if not mappings:
        bottle.response.status = 404
        return {}

    pdb_entries = []

    for mapping in mappings:
        pdb_entries = mapping[0]

    # get entry and other basic details
    query = """
    MATCH (entry:Entry) WHERE entry.ID IN $pdb_entries
    OPTIONAL MATCH (entry)-[:HAS_ENTITY]->(boundLigand:Entity {POLYMER_TYPE:'B'})
    OPTIONAL MATCH (entry)-[:HAS_ENTITY]->(nonPolyEntity:Entity) WHERE nonPolyEntity.POLYMER_TYPE IN ['D','R','D/R']
    OPTIONAL MATCH (entry)-[:HAS_ENTITY]->(allEntity:Entity) WHERE allEntity.TYPE='p'
    OPTIONAL MATCH (entry)-[:EXPERIMENT]->(method:Method)
    WITH entry.ID AS entry_id, entry.TITLE AS title, entry.RESOLUTION AS resolution, SIZE(COLLECT(DISTINCT boundLigand.ID)) AS count_ligands,
    COLLECT(DISTINCT nonPolyEntity.POLYMER_TYPE) AS non_poly_types, SIZE(COLLECT(DISTINCT allEntity.ID)) AS entity_count, method.METHOD AS experiment
    RETURN entry_id, title, resolution, count_ligands, non_poly_types, entity_count, experiment ORDER BY toFloat(resolution)
    """

    mappings = run_query(query, parameters={
        "pdb_entries": pdb_entries
    })

    dict_entry = dict()

    for mapping in mappings:
        (entry_id, title, resolution, count_ligands, non_poly_types, entity_count, experiment) = mapping
        
        dict_entry[entry_id] = {
            "title": title,
            "resolution": 0.0 if resolution is None else resolution,
            "ligand_count": count_ligands,
            "non_poly_types": non_poly_types,
            "entity_count": entity_count,
            "experiment": experiment
        }

    # get uniprot sequence to extract residue one letter code
    [unp_sequence, unp_sequence_length] = get_uniprot_sequence(uniprot_accession)[0]
    processed_protein_start = None

    # get observed residues and best chain details
    query = """
    MATCH (unp:UniProt)<-[unp_rel:HAS_UNIPROT_OBS_SEGMENT]-(entity:Entity)<-[:HAS_ENTITY]-(entry:Entry)
    WHERE entry.ID IN $pdb_entries AND unp.ACCESSION=$uniprot_accession AND unp_rel.STRUCT_ASYM_ID=entity.BEST_CHAIN_ID
    RETURN 
        entry.ID, entity.ID, entity.BEST_CHAIN_ID, unp_rel.OBSERVED, toInteger(unp_rel.UNP_START) AS unp_start, toInteger(unp_rel.UNP_END) AS unp_end, toInteger(unp_rel.PDB_START) AS pdb_start, 
        toInteger(unp_rel.PDB_END) AS pdb_end, toInteger(unp_rel.RANKING_SCORES[7]) AS ranking_score
        ORDER BY unp_start
    """
    
    if is_polyprotein:
        query = """
        MATCH (u:UniProt {ACCESSION:$uniprot_accession})-[:HAS_UNP_RESIDUE]->(ur:UNPResidue)
        WITH toInteger(ur.ID) AS residue, SPLIT(ur.UNIQID, '_')[0] AS accession ORDER BY residue
        WITH COLLECT(residue) AS residues, accession
        MATCH (unp:UniProt)<-[unp_rel:HAS_UNIPROT_OBS_SEGMENT]-(entity:Entity)<-[:HAS_ENTITY]-(entry:Entry)
            WHERE entry.ID IN $pdb_entries AND unp.ACCESSION=accession AND unp_rel.STRUCT_ASYM_ID=entity.BEST_CHAIN_ID AND toInteger(unp_rel.UNP_START) >= residues[0] AND toInteger(unp_rel.UNP_END) <= residues[-1]
        RETURN entry.ID, entity.ID, entity.BEST_CHAIN_ID, unp_rel.OBSERVED, toInteger(unp_rel.UNP_START) AS unp_start, toInteger(unp_rel.UNP_END) AS unp_end, toInteger(unp_rel.PDB_START) AS pdb_start, toInteger(unp_rel.PDB_END) AS pdb_end, toInteger(unp_rel.RANKING_SCORES[7]) AS ranking_score
            ORDER BY unp_start
        """

        processed_protein_start = get_processed_protein_start(uniprot_accession)
        api_result[uniprot_accession].update({
            "processed_protein_start": int(processed_protein_start)
        })
    
    mappings = run_query(query, parameters={
        "pdb_entries": pdb_entries,
        "uniprot_accession": str(uniprot_accession)
    })

    dict_temp_entry = {}

    for mapping in mappings:
        (entry_id, entity_id, best_chain, observed, unp_start, unp_end, pdb_start, pdb_end, ranking_score) = mapping
        ranking_score = ranking_score if ranking_score else 0
        
        
        if is_polyprotein:
            unp_start -= processed_protein_start - 1
            unp_end -= processed_protein_start - 1

        if best_chain is None:
            continue

        dict_key = (entry_id, entity_id, best_chain)

        if dict_temp_entry.get(dict_key) is None:
            dict_temp_entry[dict_key] = {
                "resolution": dict_entry.get(entry_id)["resolution"],
                "best_chain": best_chain,
                "ligand_count": dict_entry.get(entry_id)["ligand_count"],
                "non_poly_types": dict_entry.get(entry_id)["non_poly_types"],
                "entity_count": dict_entry.get(entry_id)["entity_count"],
                "experiment": dict_entry.get(entry_id)["experiment"],
                "title": dict_entry.get(entry_id)["title"],
                "fragments": [],
                "residue_count": 0,
                "obs_segments": [],
                "ranking_score": ranking_score
            }

        start_code = unp_sequence[unp_start - 1]
        end_code = unp_sequence[unp_end - 1]

        temp_fragment = {
            "start": unp_start,
            "end": unp_end,
            "start_code": util_common.get_amino_one_to_three(start_code),
            "end_code": util_common.get_amino_one_to_three(end_code)
        }

        if observed == 'N':

            temp_fragment["observed"] = "N"
            # PDBE-2673: set a flag if an unobserved region exists
            dict_temp_entry[dict_key]["unobserved_regions_present"] = True

        dict_temp_entry[dict_key]["fragments"].append(temp_fragment)

        # keep count of observed residues
        if observed == 'Y':
            dict_temp_entry[dict_key]["obs_segments"].append((unp_start, unp_end))
            temp_fragment["observed"] = "Y"
            
    for dict_key, dict_items in dict_temp_entry.items():
        (entry_id, entity_id, chain_id) = dict_key
        dict_temp_entry[dict_key]["residue_count"] = util_common.count_residues(dict_temp_entry[dict_key]["obs_segments"])
        
        del dict_temp_entry[dict_key]["obs_segments"]
        
        if dict_items.get("unobserved_regions_present") is None:
            dict_items["unobserved_regions_present"] = False

        entry_fragment = {
            "name": entry_id,
            "accession": entry_id,
            "dataType": "bestChain",
            "entityId": int(entity_id),
            "bestChainId": chain_id,
            "residues": [],
            "additionalData": {
                "resolution": None if dict_items.get("resolution") is None else round(float(dict_items.get("resolution")), 2),
                "ligandCount": dict_items["ligand_count"],
                "entityCount": dict_items["entity_count"],
                "experiment": dict_items["experiment"],
                "title": dict_items["title"],
                "residueCount": dict_items["residue_count"],
                "nonPolyTypes": dict_items["non_poly_types"],
                "unobservedRegionsPresent": dict_items["unobserved_regions_present"],
                "rankingScore": dict_items["ranking_score"]
            }
        }
        
        for fragment in dict_items["fragments"]:
            entry_fragment["residues"].append({
                "startIndex": fragment["start"],
                "endIndex": fragment["end"],
                "startCode": fragment["start_code"],
                "endCode": fragment["end_code"],
                "indexType": "UNIPROT",
                "observed": fragment["observed"]
            })

        # get residue modifications
        res_modifications = get_residue_modifications(entry_id, entity_id, uniprot_accession)

        if res_modifications:
            for res_modification in res_modifications:
                (pdb_res_id, chem_comp_id, unp_res_id, unp_res_code) = res_modification

                residue_fragment = {
                    "startIndex": int(unp_res_id),
                    "endIndex": int(unp_res_id),
                    "startCode": util_common.get_amino_one_to_three(unp_res_code),
                    "endCode": util_common.get_amino_one_to_three(unp_res_code),
                    "indexType": "UNIPROT",
                    "pdbCode": chem_comp_id,
                    "modification": True
                }

                entry_fragment["residues"].append(residue_fragment)

        # get residue mutations
        res_mutations = get_residue_mutations(entry_id, entity_id, uniprot_accession)
        
        if res_mutations:
            for res_mutation in res_mutations:
                items = res_mutation.split("|")

                if len(items) != 7:
                    continue

                [mutation_type, pdb_res_id, unp_res_id, pdb_res_code, unp_res_code, auth_asym_id, struct_asym_id] = items
                
                if struct_asym_id != best_chain:
                    continue
                
                residue_fragment = {
                    "startIndex": int(unp_res_id),
                    "endIndex": int(unp_res_id),
                    "startCode": util_common.get_amino_one_to_three(unp_res_code),
                    "endCode": util_common.get_amino_one_to_three(unp_res_code),
                    "indexType": "UNIPROT",
                    "pdbCode": pdb_res_code
                }

                # check if it's a non standard amino acid
                if util_common.get_amino_three_to_one(pdb_res_code) == "*":
                    residue_fragment.update({
                        "modification": True
                    })
                else:
                    residue_fragment.update({
                        "mutation": True,
                        "mutationType": mutation_type
                    })
                
                entry_fragment["residues"].append(residue_fragment)

        api_result[uniprot_accession]["data"].append(entry_fragment)    

    # api_result[uniprot_accession]["data"] = sorted(api_result[uniprot_accession]["data"], key=lambda x: x["additionalData"]["resolution"])
    # api_result[uniprot_accession]["data"] = sorted(api_result[uniprot_accession]["data"], key=lambda x: x["additionalData"]["residueCount"], reverse=True)
    api_result[uniprot_accession]["data"] = sorted(api_result[uniprot_accession]["data"], key=lambda x: x["additionalData"]["rankingScore"], reverse=True)

    
    bottle.response.status = 200
    return api_result


@app.get('/protvista/unipdb/<uniprot_accession>'+reSlashOrNot)
def get_uniprot_protvista_unipdb_api(uniprot_accession):

    generic_response = get_uniprot_generic_unipdb_api(uniprot_accession)

    if not generic_response:
        bottle.response.status = 404
        return {}

    bottle.response.status = 200
    return convert_to_protvista("protein", "unipdb", generic_response)


"""
@api {get} uniprot/interface_residues/:accession Get interface residues for a UniProt accession
@apiName GetUNPInterfaceResidues
@apiGroup UniProt
@apiDescription This call provides details on interface residues for a UniProt accession.
@apiVersion 2.0.0

@apiParam {String} accession=P07550 UniProt Accession

@apiUse GenericFields

@apiExample {json=./examples/success/uniprot_generic_interface_residues.json} apiSuccessExample Example success response JSON
"""
@app.get('/interface_residues/<uniprot_accession>'+reSlashOrNot)
def get_uniprot_generic_interface_residues_api(uniprot_accession):

    uniprot_accession = uniprot_accession.strip("'")
    api_result = get_generic_structure(uniprot_accession, "INTERACTION INTERFACES")
    is_processed_protein = True if uniprot_accession.startswith("PRO_") else False

    if is_processed_protein:
        processed_protein_start = get_processed_protein_start(uniprot_accession)
        api_result[uniprot_accession].update({
            "processed_protein_start": int(processed_protein_start)
        })

    if api_result is None:
        bottle.response.status = 404
        return {}
    
    query = """
    // Get all interaction partners for the uniprot residue
    MATCH (unp:UniProt)-[:HAS_UNP_RESIDUE]->(unp_res:UNPResidue)-[rel:INTERACTS_WITH]->(partner) WHERE unp.ACCESSION=$uniprot_accession

    WITH DISTINCT toInteger(unp_res.ID) AS unp_res_id, unp_res.ONE_LETTER_CODE AS amino_acid_code, partner, rel, rel.ALL_ENTRIES AS all_entries, rel.INTERACTING_POLYMER_ENTITIES[0] AS representative_entity
    WITH
        CASE labels(partner)[0]
            WHEN 'UniProt'
                THEN partner.ACCESSION +"::" +partner.DESCR
            WHEN 'PRD'
                THEN partner.ID +'::' +partner.NAME +'::' +partner.TYPE +'::' +partner.CLASS
            WHEN 'Antibody'
                THEN 'Ab::' + rel.NAME +'::' + [(e:Entry)-[:HAS_ENTITY]->(en:Entity) WHERE e.ID=SPLIT(representative_entity, '_')[0] AND en.ID=SPLIT(representative_entity, '_')[1] | en.DESCRIPTION][0]
            WHEN 'Entity'
            THEN CASE partner.POLYMER_TYPE
                    WHEN 'D'
                        THEN 'DNA'
                    WHEN 'R'
                        THEN 'RNA'
                    WHEN 'D/R'
                        THEN 'DNA/RNA'
                    WHEN 'P'
                        THEN 'Other'
                END
        END AS partner, unp_res_id, amino_acid_code, COLLECT(rel.INTERACTING_POLYMER_ENTITIES) AS polymer_entities, COLLECT(rel.INTERACTING_POLYMER_CHAINS) AS polymer_chains, COLLECT(COALESCE(all_entries,[])) AS all_entries

    RETURN partner, unp_res_id, amino_acid_code, apoc.coll.flatten(polymer_entities), apoc.coll.flatten(polymer_chains), apoc.coll.flatten(all_entries) ORDER BY partner
    """

    mappings = run_query(query, parameters={
        "uniprot_accession": str(uniprot_accession)
    })

    if not mappings:
        bottle.response.status = 404
        return {}

    dict_accession = {}

    for mapping in mappings:
        (dest_partner, unp_res_id, amino_acid_code, pdb_entities, auth_asym_ids, all_pdb_entries) = mapping

        if not dest_partner:
            continue

        is_prd = is_antibody = is_unp = is_other = False

        if is_processed_protein:
            unp_res_id -= processed_protein_start - 1
        
        # take care of UniProt, PRD, antibody and non-UniProt mappings
        if '::' in dest_partner:
            splits = dest_partner.split('::')

            # UniProt
            if len(splits) == 2 and splits[0] != 'Ab':
                (accession, partner_desc) = splits
                is_unp = True
            # PRD
            elif len(splits) == 4:
                is_prd = True
                (accession, partner_desc, prd_type, prd_class) = splits
            # antibody
            elif splits[0] == 'Ab':
                is_antibody = True
                accession = splits[1]
                partner_desc = splits[2]
        else:
            accession = partner_desc = dest_partner
            is_other = True

        if dict_accession.get(accession) is None:
            dict_accession[accession] = {
                "name": partner_desc,
                "accession": accession,
                "residues": [],
                "allPDBEntries": set(),
                "additionalData": dict()
            }

        if is_prd:
            dict_accession[accession]["additionalData"] = {
                "prdType": prd_type,
                "prdClass": prd_class
            }

        interacting_entries = []

        for a, b in zip(pdb_entities, auth_asym_ids):
            [entry_id, entity_id] = a.split('_')
            interacting_entries.append({
                "pdbId": entry_id,
                "entityId": int(entity_id),
                "chainIds": b
            })

            dict_accession[accession]["allPDBEntries"].add(entry_id)

        # add pdb entries in case of PRD or antibody
        if is_prd or is_antibody:
            for a in pdb_entities:
                [entry_id, entity_id] = a.split('_')
                interacting_entries.append({
                    "pdbId": entry_id,
                    "entityId": int(entity_id)
                })

                dict_accession[accession]["allPDBEntries"].add(entry_id)

        residue_fragment = {
            "startIndex": unp_res_id,
            "endIndex": unp_res_id,
            "startCode": util_common.get_amino_one_to_three(amino_acid_code),
            "endCode": util_common.get_amino_one_to_three(amino_acid_code),
            "indexType": "UNIPROT",
            "interactingPDBEntries": interacting_entries,
            "allPDBEntries": all_pdb_entries
        }

        dict_accession[accession]["residues"].append(residue_fragment)

        # set type
        annotation_type = "UNP"
        
        if is_prd:
            annotation_type = "PRD"
        elif is_antibody:
            annotation_type = "AB"
        elif is_other:
            annotation_type = "OTHER"
        
        dict_accession[accession]["additionalData"].update({
            "type": annotation_type
        })
        
    for accession, values in dict_accession.items():

        # sort the residues on residue number
        values["residues"] = sorted(values["residues"], key=lambda x: x["startIndex"])

        # keep list of all pdb_entries in which protein A interacts with protein B
        values["allPDBEntries"] = list(values["allPDBEntries"])

        api_result[uniprot_accession]["data"].append(values)

     # sort the list of partners based on number of residues (descending), so that partners with more interacting residues comes first
    api_result[uniprot_accession]["data"] = sorted(api_result[uniprot_accession]["data"], key=lambda x: len(x["residues"]), reverse=True)

    bottle.response.status = 200
    return api_result



@app.get('/protvista/interface_residues/<uniprot_accession>'+reSlashOrNot)
def get_uniprot_protvista_interface_residues_api(uniprot_accession):

    generic_response = get_uniprot_generic_interface_residues_api(uniprot_accession)

    if not generic_response:
        bottle.response.status = 404
        return {}

    bottle.response.status = 200
    return convert_to_protvista("protein", "interface", generic_response)


"""
@api {get} uniprot/ligand_sites/:accession Get ligand binding residues for a UniProt accession
@apiName GetUNPLigandSites
@apiGroup UniProt
@apiDescription This call provides details on ligand binding residues for a UniProt accession.
@apiVersion 2.0.0

@apiParam {String} accession=P07550 UniProt Accession

@apiUse GenericFields

@apiExample {json=./examples/success/uniprot_generic_ligand_sites.json} apiSuccessExample Example success response JSON
"""
@app.get('/ligand_sites/<uniprot_accession>'+reSlashOrNot)
def get_uniprot_generic_ligand_sites_api(uniprot_accession):

    uniprot_accession = uniprot_accession.strip("'")
    api_result = get_generic_structure(uniprot_accession, "LIGAND BINDING SITES")
    is_processed_protein = True if uniprot_accession.startswith("PRO_") else False

    if is_processed_protein:
        processed_protein_start = get_processed_protein_start(uniprot_accession)
        api_result[uniprot_accession].update({
            "processed_protein_start": int(processed_protein_start)
        })

    if api_result is None:
        bottle.response.status = 404
        return {}
        
    query = """
    MATCH (unp:UniProt {ACCESSION:$uniprot_accession})-[:HAS_UNP_RESIDUE]->(unp_res:UNPResidue)
    WITH unp_res, unp
    MATCH (unp_res)-[rel:BOUNDED_BY]->(chem_comp:ChemicalComponent) WHERE chem_comp.ID <> 'HOH'
    OPTIONAL MATCH (chem_comp)-[:ACTS_AS_COFACTOR]->(cofactor:COFactorClass)
    OPTIONAL MATCH (chem_comp)-[:CONTAINS_SCAFFOLD]->(scaffold:Scaffold)
    OPTIONAL MATCH (chem_comp)<-[:IS_A_TARGET]-(target_unp:UniProt)
    OPTIONAL MATCH (unp)-[t:IS_A_TARGET]->(chem_comp)-[:HAS_DRUGBANK]->(dBank:DrugBank)
    OPTIONAL MATCH (unp)-[rx:HAS_REACTION]->(rxn:Reaction)-[:CONTAINS]->(chembl:ChEBI)<-[:HAS_CHEBI]-(chem_comp)
    RETURN DISTINCT chem_comp.ID AS chem_comp_id, chem_comp.NAME AS chem_desc, toInteger(chem_comp.NUMBER_ATOMS_NH) AS num_atoms, COALESCE(cofactor.COFACTOR_ID,"") AS cofactor_id, COALESCE(scaffold.UNIQID,"") AS scaffold_id, 
        COLLECT(target_unp.ACCESSION) AS target_unp_accs, toInteger(unp_res.ID) AS unp_res_id, unp_res.ONE_LETTER_CODE AS amino_acid_code, rel.INTERACTING_POLYMER_ENTITIES AS polymer_entities, rel.INTERACTING_POLYMER_CHAINS AS polymer_chains, 
        rel.ALL_ENTRIES AS all_entries, COALESCE(rxn.ID,"") AS rxn_id , COALESCE(chembl.ID,"") AS chembl_id, t, COALESCE(dBank.ID,"") as drugbank_id
    """

    mappings = run_query(query, parameters={
        "uniprot_accession": str(uniprot_accession)
    })

    if not mappings:
        bottle.response.status = 404
        return {}

    dict_chem_comp = {}
    dict_chem_comp_residue = {}

    for mapping in mappings:
        (chem_comp, chem_desc, num_atoms, cofactor_id, scaffold_id, target_unp_accs, unp_res_id, amino_acid_code, polymer_entities, polymer_chains, all_entries, rxn_id, chembl_id, target, drugbank_id) = mapping
        residue_key = (chem_comp, unp_res_id, amino_acid_code)
        
        if is_processed_protein:
            unp_res_id -= processed_protein_start - 1

        if dict_chem_comp.get(chem_comp) is None:
            dict_chem_comp[chem_comp] = {
                "name": chem_desc,
                "accession": chem_comp,
                "residues": [],
                "additionalData": {
                    "scaffoldId": scaffold_id,
                    "coFactorId": cofactor_id,
                    "reactionId": rxn_id,
                    "chemblId" : chembl_id,
                    "drugBankId" : drugbank_id,
                    "numAtoms": int(num_atoms),
                    "targetUniProts": target_unp_accs,
                    "pdbEntries": set()
                }
            }
        
        if dict_chem_comp_residue.get(residue_key) is None:
            dict_chem_comp_residue[residue_key] = {
                "startIndex": int(unp_res_id),
                "endIndex": int(unp_res_id),
                "startCode": util_common.get_amino_one_to_three(amino_acid_code),
                "endCode": util_common.get_amino_one_to_three(amino_acid_code),
                "indexType": "UNIPROT",
                "interactingPDBEntries": [],
                "allPDBEntries": all_entries
            }

        for incr in range(0, len(polymer_entities)):
            [pdb_entry, pdb_entity] = polymer_entities[incr].split("_")
            dict_chem_comp_residue[residue_key]["interactingPDBEntries"].append({
                "pdbId": pdb_entry,
                "entityId": int(pdb_entity),
                "chainIds": polymer_chains[incr]
            })
            dict_chem_comp[chem_comp]["additionalData"]["pdbEntries"].add(pdb_entry)

    for residue_key in dict_chem_comp_residue.keys():
        
        (chem_comp, unp_res_id, amino_acid_code) = residue_key
        dict_chem_comp[chem_comp]["residues"].append(dict_chem_comp_residue[residue_key])

    for chem_comp in dict_chem_comp.keys():
        dict_chem_comp[chem_comp]["additionalData"]["pdbEntries"] = list(dict_chem_comp[chem_comp]["additionalData"]["pdbEntries"])
        api_result[uniprot_accession]["data"].append(dict_chem_comp[chem_comp])

        # sort residues based on residue number
        dict_chem_comp[chem_comp]["residues"] = sorted(dict_chem_comp[chem_comp]["residues"], key=lambda x: x["startIndex"])

    # JIRA PDBE-2388, PDBE-3001
    # sort the list based on number of interacting residues so that highest number (not so important) goes to bottom
    api_result[uniprot_accession]["data"] = sorted(api_result[uniprot_accession]["data"], key = lambda x: len(x["residues"]))
    
    # sort the list based on cofactor, scaffold_id, target accession (denoting a drug)
    api_result[uniprot_accession]["data"] = sorted(api_result[uniprot_accession]["data"], key = lambda x: (x["additionalData"]["coFactorId"], x["additionalData"]["scaffoldId"], x["additionalData"]["targetUniProts"]), reverse=True)


    bottle.response.status = 200
    return api_result


@app.get('/protvista/ligand_sites/<uniprot_accession>'+reSlashOrNot)
def get_uniprot_protvista_ligand_sites_api(uniprot_accession):

    generic_response = get_uniprot_generic_ligand_sites_api(uniprot_accession)

    if not generic_response:
        bottle.response.status = 404
        return {}

    bottle.response.status = 200
    return convert_to_protvista("protein", "ligandsites", generic_response)


# JIRA PDBE-2323 - Added code to get amino acid one letter code
def get_uniprot_pdb_details(uniprot_accession):
    
    dict_entry = {}

    # get structures mapped to the protein
    query = """
    MATCH (unp:UniProt {ACCESSION:$uniprot_accession})<-[:HAS_UNIPROT]-(entity:Entity)
    WITH unp.ACCESSION AS accession, COLLECT(DISTINCT SPLIT(entity.UNIQID, '_')[0]) AS pdb_entries
    RETURN pdb_entries
    """

    if uniprot_accession.startswith("PRO_"):
        query = """
        MATCH (:UniProt {ACCESSION:$uniprot_accession})<-[rel:IS_A_POLYPROTEIN_OF]-(u:UniProt)
        WITH u, toInteger(rel.UNP_START) AS unpStart, toInteger(rel.UNP_END) AS unpEnd
        MATCH (u)<-[unp_rel:HAS_UNIPROT_OBS_SEGMENT]-(entity:Entity)
            WHERE toInteger(unp_rel.UNP_START) >= unpStart AND toInteger(unp_rel.UNP_END) <= unpEnd
        WITH u.ACCESSION AS accession, COLLECT(DISTINCT SPLIT(entity.UNIQID, '_')[0]) AS pdb_entries
        RETURN pdb_entries
        """

    mappings = run_query(query, parameters={
        "uniprot_accession": str(uniprot_accession)
    })
    
    if not mappings:
        return {}, 404

    pdb_entries = []
    for mapping in mappings:
        pdb_entries = mapping[0]

    
    # get entry and other basic details
    query = """
    MATCH (entry:Entry) WHERE entry.ID IN $pdb_entries
    OPTIONAL MATCH (entry)-[:HAS_ENTITY]->(boundLigand:Entity {POLYMER_TYPE:'B'})
    OPTIONAL MATCH (entry)-[:HAS_ENTITY]->(nonPolyEntity:Entity) WHERE nonPolyEntity.POLYMER_TYPE IN ['D','R','D/R']
    OPTIONAL MATCH (entry)-[:HAS_ENTITY]->(allEntity:Entity) WHERE allEntity.TYPE='p'
    OPTIONAL MATCH (entry)-[:EXPERIMENT]->(method:Method)
    WITH entry.ID AS entry_id, entry.TITLE AS title, entry.RESOLUTION AS resolution, SIZE(COLLECT(DISTINCT boundLigand.ID)) AS count_ligands,
    COLLECT(DISTINCT nonPolyEntity.POLYMER_TYPE) AS non_poly_types, SIZE(COLLECT(DISTINCT allEntity.ID)) AS entity_count, method.METHOD AS experiment
    RETURN entry_id, title, resolution, count_ligands, non_poly_types, entity_count, experiment ORDER BY toFloat(resolution)
    """

    mappings = run_query(query, parameters={
        "pdb_entries": pdb_entries
    })
    
    for mapping in mappings:
        (entry_id, title, resolution, count_ligands, non_poly_types, entity_count, experiment) = mapping
        
        dict_entry[entry_id] = {
            "title": title,
            "resolution": 0.0 if resolution is None else resolution,
            "ligand_count": count_ligands,
            "non_poly_types": non_poly_types,
            "entity_count": entity_count,
            "experiment": experiment
        }

    # get uniprot sequence to extract residue one letter code
    unp_sequence = get_uniprot_sequence(uniprot_accession)[0][0]

    if uniprot_accession.startswith("PRO_"):
        unp_sequence = get_uniprot_sequence(get_polyprotein(uniprot_accession))[0][0]

    # get observed residues and best chain details
    query = """
    MATCH (unp:UniProt)<-[unp_rel:HAS_UNIPROT_OBS_SEGMENT]-(entity:Entity)<-[:HAS_ENTITY]-(entry:Entry)
    WHERE entry.ID IN $pdb_entries AND unp.ACCESSION=$uniprot_accession AND unp_rel.STRUCT_ASYM_ID=entity.BEST_CHAIN_ID
    RETURN entry.ID, entity.ID, entity.BEST_CHAIN_ID, unp_rel.OBSERVED, toInteger(unp_rel.UNP_START) AS unp_start, toInteger(unp_rel.UNP_END) ORDER BY unp_start
    """
    
    if uniprot_accession.startswith("PRO_"):
        query = """
        MATCH (:UniProt {ACCESSION:$uniprot_accession})<-[rel:IS_A_POLYPROTEIN_OF]-(u:UniProt)
        WITH u, toInteger(rel.UNP_START) AS unpStart, toInteger(rel.UNP_END) AS unpEnd
        MATCH (u)<-[unp_rel:HAS_UNIPROT_OBS_SEGMENT]-(entity:Entity)<-[:HAS_ENTITY]-(entry:Entry)
            WHERE entry.ID IN $pdb_entries AND toInteger(unp_rel.UNP_START) >= unpStart AND toInteger(unp_rel.UNP_END) <= unpEnd AND unp_rel.STRUCT_ASYM_ID=entity.BEST_CHAIN_ID
        RETURN entry.ID, entity.ID, entity.BEST_CHAIN_ID, unp_rel.OBSERVED, toInteger(unp_rel.UNP_START) AS unp_start, toInteger(unp_rel.UNP_END) ORDER BY unp_start
        """

    mappings = run_query(query, parameters={
        "pdb_entries": pdb_entries,
        "uniprot_accession": str(uniprot_accession)
    })

    dict_temp_entry = {}

    for mapping in mappings:
        (entry_id, entity_id, best_chain, observed, unp_start, unp_end) = mapping

        if best_chain is None:
            continue

        dict_key = (entry_id, entity_id, best_chain)

        if dict_temp_entry.get(dict_key) is None:
            dict_temp_entry[dict_key] = {
                "resolution": dict_entry.get(entry_id)["resolution"],
                "best_chain": best_chain,
                "ligand_count": dict_entry.get(entry_id)["ligand_count"],
                "non_poly_types": dict_entry.get(entry_id)["non_poly_types"],
                "entity_count": dict_entry.get(entry_id)["entity_count"],
                "experiment": dict_entry.get(entry_id)["experiment"],
                "title": dict_entry.get(entry_id)["title"],
                "fragments": [],
                "residue_count": 0,
                "obs_segments": []
            }

        start_code = unp_sequence[unp_start - 1]
        end_code = unp_sequence[unp_end - 1]

        temp_fragment = {
            "start": unp_start,
            "end": unp_end,
            "start_code": start_code,
            "end_code": end_code,
            "tooltipContent": """Type: PDB mapped to UniProt<br>Range: {}{}-{}{}<br>PDB entry: <a target="_blank" href="http://www.ebi.ac.uk/pdbe/entry/pdb/{}/protein/{}">{} Best Chain {}</a>""".format(amino_acid_codes_one_to_three[start_code], unp_start, amino_acid_codes_one_to_three[end_code], unp_end, entry_id, entity_id, entry_id, best_chain)

        }

        if observed == 'N':
            temp_fragment["color"] = "rgb(211, 211, 211)"

            # PDBE-2673: set a flag if an unobserved region exists
            dict_temp_entry[dict_key]["unobserved_regions_present"] = True

        dict_temp_entry[dict_key]["fragments"].append(temp_fragment)

        # keep count of observed residues
        if observed == 'Y':
            dict_temp_entry[dict_key]["obs_segments"].append((unp_start, unp_end))
            

    for dict_key in dict_temp_entry.keys():
        dict_temp_entry[dict_key]["residue_count"] = count_residues(dict_temp_entry[dict_key]["obs_segments"])
        
        del dict_temp_entry[dict_key]["obs_segments"]

    return dict_temp_entry, 200

"""
@api {get} uniprot/annotations/:accession Get annotations for a UniProt accession
@apiName GetUNPAnnotations
@apiGroup UniProt
@apiDescription This call provides details on annotations for a UniProt accession.
@apiVersion 2.0.0

@apiParam {String} accession=P07550 UniProt Accession

@apiUse GenericFields

@apiExample {json=./examples/success/uniprot_generic_annotations.json} apiSuccessExample Example success response JSON
"""
@app.get('/annotations/<uniprot_accession>'+reSlashOrNot)
def get_uniprot_generic_annotations_api(uniprot_accession):

    uniprot_accession = uniprot_accession.strip("'")
    api_result = get_generic_structure(uniprot_accession, "ANNOTATIONS")
    is_processed_protein = True if uniprot_accession.startswith("PRO_") else False

    if is_processed_protein:
        processed_protein_start = get_processed_protein_start(uniprot_accession)
        api_result[uniprot_accession].update({
            "processed_protein_start": int(processed_protein_start)
        })

    if api_result is None:
        bottle.response.status = 404
        return {}

    # get the top structure for the uniprot accession
    best_structure = get_uniprot_best_entity(uniprot_accession)

    if not best_structure:
        bottle.response.status = 404
        return {}

    [best_entry, best_entity] = best_structure.split("_")

    query = """
    MATCH (unp:UniProt {ACCESSION:$uniprot_accession})-[:HAS_UNP_RESIDUE]->(unp_res:UNPResidue)<-[:MAP_TO_UNIPROT_RESIDUE]-(pdb_res:PDBResidue)
        <-[res_rel:FUNPDBE_ANNOTATION_FOR]-(fun_group:FunPDBeResidueGroup)-[:FUNPDBE_RESIDUE_GROUP_OF]->(fun_entry:FunPDBeEntry), (pdb_res)<-[:HAS_PDB_RESIDUE]-(entity:Entity)
    WHERE
        (NOT fun_entry.DATA_RESOURCE IN ["FoldX","Missense3D","KnotProt","Kincore","WEBnma","Frustratometer","EMVS","SKEMPI","dynamine","FireProt DB","nextprot","PDBTM"]) AND
        (NOT (fun_entry.DATA_RESOURCE="p2rank" AND res_rel.CONFIDENCE_CLASSIFICATION <> "high" )) AND 
        (NOT (fun_entry.DATA_RESOURCE="3Dcomplex" AND fun_group.LABEL IN ["Interior_residue", "Interface_residue", "Surface_residue"] )) AND
        entity.BEST_CHAIN_ID=res_rel.CHAIN_LABEL
    WITH SPLIT(pdb_res.UNIQID, '_')[0] AS entry_id, SPLIT(pdb_res.UNIQID, '_')[1] AS entity_id, res_rel.CHAIN_LABEL AS auth_asym_id, fun_entry.DATA_RESOURCE AS data_resource, 
        fun_entry.RESOURCE_ENTRY_URL AS resource_url, unp_res.ID AS unp_res_id, unp_res.ONE_LETTER_CODE AS amino_acid_code, pdb_res.ID AS pdb_res_id, fun_group.LABEL AS group_label,
        res_rel.RAW_SCORE AS raw_score, res_rel.CONFIDENCE_CLASSIFICATION AS conf_class, res_rel.CONFIDENCE_SCORE AS conf_score
    WITH DISTINCT data_resource, group_label, raw_score, conf_class, conf_score, toInteger(unp_res_id) AS unp_res_id, entry_id, toInteger(entity_id) AS entity_id, auth_asym_id, amino_acid_code, resource_url
        ORDER BY unp_res_id
    RETURN data_resource, group_label, raw_score, conf_class, conf_score, resource_url, unp_res_id, amino_acid_code, entry_id, entity_id, COLLECT(auth_asym_id)
    """
    
    annotations_mappings = run_query(query, parameters={
        "uniprot_accession": str(uniprot_accession)
    })
    
    if not annotations_mappings:
        bottle.response.status = 404
        return {}

    annotation_dict = dict()
    annotation_residue_dict = dict()

    for annotations_mapping in annotations_mappings:
        (data_resource, group_label, raw_score, conf_class, conf_score, resource_url, unp_res_id, amino_acid_code, entry_id, entity_id, auth_asym_ids) = annotations_mapping

        # PDBE-3714, PDBE-3715: Set data resource to specific group label in case of depth and dynamine
        if data_resource in ["depth", "dynamine"]:

            # skip structures which are not best
            if not (best_entry == entry_id and int(best_entity) == entity_id):
                continue
                
            # PDBE-2832 - Skip dynamine sidechain
            if data_resource == "dynamine" and group_label == 'sidechain':
                continue            

            data_resource = group_label
        
        # PDBE-4662: Split POPScomp_PDBML based on group labels
        elif data_resource == "POPScomp_PDBML":
            data_resource = "Total SASA"

            if group_label.startswith("hydrophobic"):
                data_resource = "Hydrophobic SASA"
            elif group_label.startswith("hydrophilic"):
                data_resource = "Hydrophilic SASA"

        elif data_resource == "3Dcomplex":
            if group_label == "ASA_BiologicalUnit":
                data_resource = "ASA in assembly"
            elif group_label == "ASA_alone":
                data_resource = "ASA alone"

        if is_processed_protein:
            unp_res_id -= processed_protein_start - 1

        dict_key = (data_resource, unp_res_id)

        if annotation_residue_dict.get(data_resource) is None:
            annotation_residue_dict[data_resource] = []

        if annotation_dict.get(dict_key) is None:
            annotation_dict[dict_key] = {
                "amino_acid_code": amino_acid_code,
                "entries": []
            }

        annotation_residue_dict[data_resource].append(unp_res_id)
        annotation_dict[dict_key]["entries"].append((entry_id, entity_id, auth_asym_ids, resource_url, raw_score, conf_class, conf_score, group_label))

    resource_master_dict = dict()

    for data_resource, residues in annotation_residue_dict.items():
        for residue in sorted(list(set(residues))):
            entries = []
            
            for entry_id, entity_id, auth_asym_ids, resource_url, raw_score, conf_class, conf_score, group_label in annotation_dict[(data_resource, residue)]["entries"]:
                entries.append((entry_id, entity_id, tuple(auth_asym_ids), resource_url, raw_score, conf_class, conf_score, group_label))

            entries = tuple(entries)
            
            if resource_master_dict.get((data_resource, entries)) is None:
                resource_master_dict[(data_resource, entries)] = {
                    "residues": [],
                    "amino_acid_codes": []
                }

            resource_master_dict[(data_resource, entries)]["residues"].append(residue)
            resource_master_dict[(data_resource, entries)]["amino_acid_codes"].append(annotation_dict[(data_resource, residue)]["amino_acid_code"])
    
    del annotation_dict, annotation_residue_dict

    return_resource_dict = dict()

    for key in resource_master_dict.keys():

        (data_resource, entries) = key

        if return_resource_dict.get(data_resource) is None:
            return_resource_dict[data_resource] = {
                "name": data_resource,
                "accession": data_resource,
                "dataType": data_resource,
                "residues": []
            }

        for residue in util_common.split_nonconsecutive_residues_with_code([resource_master_dict[key]["residues"], resource_master_dict[key]["amino_acid_codes"]]):
            start_res_id = residue[0][0]
            end_res_id = residue[-1][0]
            start_res_code = residue[0][1]
            end_res_code = residue[-1][1]

            residue_fragment = {
                "startIndex": start_res_id,
                "endIndex": end_res_id,
                "startCode": util_common.get_amino_one_to_three(start_res_code),
                "endCode": util_common.get_amino_one_to_three(end_res_code),
                "indexType": "UNIPROT",
                "pdbEntries": []
            }

            for entry in entries:
                residue_fragment["pdbEntries"].append({
                    "pdbId": entry[0],
                    "entityId": entry[1],
                    "chainIds": entry[2],
                    "additionalData": {
                        "resourceUrl": entry[3],
                        "rawScore": float(entry[4]) if entry[4] else None,
                        "confLevel": entry[5],
                        "confScore": float(entry[6]) if entry[6] else None,
                        "groupLabel": entry[7],
                    }
                })

            return_resource_dict[data_resource]["residues"].append(residue_fragment)

    for resource, items in return_resource_dict.items():
        api_result[uniprot_accession]["data"].append(items)
        
    bottle.response.status = 200
    return api_result


@app.get('/protvista/annotations/<uniprot_accession>'+reSlashOrNot)
def get_uniprot_protvista_annotations_api(uniprot_accession):

    generic_response = get_uniprot_generic_annotations_api(uniprot_accession)

    if not generic_response:
        bottle.response.status = 404
        return {}

    bottle.response.status = 200
    return convert_to_protvista("protein", "annotations", generic_response)


"""
@api {get} uniprot/sequence_conservation/:accession/:residueNumber Get sequence conservations for a UniProt Residue
@apiName GetUNPResidueSequenceConservation
@apiGroup UniProt
@apiDescription Get sequence conservations for a UniProt Residue
@apiVersion 2.0.0

@apiParam {String} accession=P07550 UniProt accession
@apiParam {String} residueNumber=10 UniProt Residue Number

@apiExample {json=./examples/success/sequence_conservation_uniprot_residue.json} apiSuccessExample Example success response JSON
"""
@app.get('/sequence_conservation/<uniprot_accession>/<residue_number>'+reSlashOrNot)
def get_sequence_conservation_uniprot_residue_api(uniprot_accession, residue_number):

    uniprot_accession = uniprot_accession.strip("'")
    residue_number = residue_number.strip("'")
    query = """
    MATCH
        (uni:UniProt {ACCESSION:$uniprot_accession})-[:HAS_UNP_RESIDUE]->(unpRes:UNPResidue {ID:$residue_number})-[hr:HAS_SEQID]->(seq:UniProtSequence)
    WHERE
        NOT hr.CONSERVED_SCORE IN ['m', 'i']
    WITH
        seq.UNIQID AS seqId, toInteger(unpRes.ID) AS residueId, hr ORDER BY residueId
    RETURN
        seqId, COLLECT(residueId) AS residues, COLLECT(toFloat(hr.CONSERVED_SCORE)) AS scores, COLLECT(toFloat(hr.P_SCORE_A)) AS scoreA, COLLECT(toFloat(hr.P_SCORE_C)) AS scoreC,
        COLLECT(toFloat(hr.P_SCORE_D)) AS scoreD, COLLECT(toFloat(hr.P_SCORE_E)) AS scoreE, COLLECT(toFloat(hr.P_SCORE_F)) AS scoreF, COLLECT(toFloat(hr.P_SCORE_G)) AS scoreG,
        COLLECT(toFloat(hr.P_SCORE_H)) AS scoreH, COLLECT(toFloat(hr.P_SCORE_I)) AS scoreI, COLLECT(toFloat(hr.P_SCORE_K)) AS scoreK, COLLECT(toFloat(hr.P_SCORE_L)) AS scoreL,
        COLLECT(toFloat(hr.P_SCORE_M)) AS scoreM, COLLECT(toFloat(hr.P_SCORE_N)) AS scoreN, COLLECT(toFloat(hr.P_SCORE_P)) AS scoreP, COLLECT(toFloat(hr.P_SCORE_Q)) AS scoreQ,
        COLLECT(toFloat(hr.P_SCORE_R)) AS scoreR, COLLECT(toFloat(hr.P_SCORE_S)) AS scoreS, COLLECT(toFloat(hr.P_SCORE_T)) AS scoreT, COLLECT(toFloat(hr.P_SCORE_V)) AS scoreV,
        COLLECT(toFloat(hr.P_SCORE_W)) AS scoreW, COLLECT(toFloat(hr.P_SCORE_Y)) AS scoreY
    """
    
    result = run_query(query, parameters={
        'uniprot_accession': uniprot_accession, 'residue_number': residue_number
    })

    if not result:
        bottle.response.status = 404
        return {}

    final_result = {
        "identifier": uniprot_accession,
        "main_track_color": "#808080",
        "sub_track_color": "#d3d3d3",
        "data": {}
    }

    result = result[0]

    final_result["length"] = len(result["residues"])
    final_result["seq_id"] = result["seqId"]
    residues = result["residues"]

    # PDBE-4249 - Offset for processed proteins
    if uniprot_accession.startswith("PRO_"):
        residues = list(map(lambda x: (x - residues[0] + 1), residues))

    final_result["data"] = {
        "index": residues,
        "conservation_score": result["scores"],
        "probability_A": result["scoreA"],
        "probability_C": result["scoreC"],
        "probability_D": result["scoreD"],
        "probability_E": result["scoreE"],
        "probability_F": result["scoreF"],
        "probability_G": result["scoreG"],
        "probability_H": result["scoreH"],
        "probability_I": result["scoreI"],
        "probability_K": result["scoreK"],
        "probability_L": result["scoreL"],
        "probability_M": result["scoreM"],
        "probability_N": result["scoreN"],
        "probability_P": result["scoreP"],
        "probability_Q": result["scoreQ"],
        "probability_R": result["scoreR"],
        "probability_S": result["scoreS"],
        "probability_T": result["scoreT"],
        "probability_V": result["scoreV"],
        "probability_W": result["scoreW"],
        "probability_Y": result["scoreY"],
    }

    bottle.response.status = 200
    return final_result



"""
@api {get} uniprot/sequence_conservation/:accession Get sequence conservations for a UniProt accession
@apiName GetUniProtSequenceConservation
@apiGroup UniProt
@apiDescription Get sequence conservations for a UniProt accession
@apiVersion 2.0.0

@apiParam {String} accession=P07550 UniProt accession

@apiExample {json=./examples/success/sequence_conservation_uniprot.json} apiSuccessExample Example success response JSON
"""
@app.get('/sequence_conservation/<uniprot_accession>'+reSlashOrNot)
def get_sequence_conservation_uniprot_api(uniprot_accession):

    uniprot_accession = uniprot_accession.strip("'")
    query = """
    MATCH
        (uni:UniProt {ACCESSION:$uniprot_accession})-[:HAS_UNP_RESIDUE]->(unpRes:UNPResidue)-[hr:HAS_SEQID]->(seq:UniProtSequence)
    WHERE
        NOT hr.CONSERVED_SCORE IN ['m', 'i']
    WITH
        seq.UNIQID AS seqId, toInteger(unpRes.ID) AS residueId, hr ORDER BY residueId
    RETURN
        seqId, COLLECT(residueId) AS residues, COLLECT(toFloat(hr.CONSERVED_SCORE)) AS scores, COLLECT(toFloat(hr.P_SCORE_A)) AS scoreA, COLLECT(toFloat(hr.P_SCORE_C)) AS scoreC,
        COLLECT(toFloat(hr.P_SCORE_D)) AS scoreD, COLLECT(toFloat(hr.P_SCORE_E)) AS scoreE, COLLECT(toFloat(hr.P_SCORE_F)) AS scoreF, COLLECT(toFloat(hr.P_SCORE_G)) AS scoreG,
        COLLECT(toFloat(hr.P_SCORE_H)) AS scoreH, COLLECT(toFloat(hr.P_SCORE_I)) AS scoreI, COLLECT(toFloat(hr.P_SCORE_K)) AS scoreK, COLLECT(toFloat(hr.P_SCORE_L)) AS scoreL,
        COLLECT(toFloat(hr.P_SCORE_M)) AS scoreM, COLLECT(toFloat(hr.P_SCORE_N)) AS scoreN, COLLECT(toFloat(hr.P_SCORE_P)) AS scoreP, COLLECT(toFloat(hr.P_SCORE_Q)) AS scoreQ,
        COLLECT(toFloat(hr.P_SCORE_R)) AS scoreR, COLLECT(toFloat(hr.P_SCORE_S)) AS scoreS, COLLECT(toFloat(hr.P_SCORE_T)) AS scoreT, COLLECT(toFloat(hr.P_SCORE_V)) AS scoreV,
        COLLECT(toFloat(hr.P_SCORE_W)) AS scoreW, COLLECT(toFloat(hr.P_SCORE_Y)) AS scoreY
    """
    
    result = run_query(query, parameters={
        'uniprot_accession': uniprot_accession
    })

    if not result:
        bottle.response.status = 404
        return {}

    final_result = {
        "identifier": uniprot_accession,
        "main_track_color": "#808080",
        "sub_track_color": "#d3d3d3",
        "data": {}
    }

    result = result[0]

    final_result["length"] = len(result["residues"])
    final_result["seq_id"] = result["seqId"]
    residues = result["residues"]

    # PDBE-4249 - Offset for processed proteins
    if uniprot_accession.startswith("PRO_"):
        residues = list(map(lambda x: (x - residues[0] + 1), residues))

    final_result["data"] = {
        "index": residues,
        "conservation_score": result["scores"],
        "probability_A": result["scoreA"],
        "probability_C": result["scoreC"],
        "probability_D": result["scoreD"],
        "probability_E": result["scoreE"],
        "probability_F": result["scoreF"],
        "probability_G": result["scoreG"],
        "probability_H": result["scoreH"],
        "probability_I": result["scoreI"],
        "probability_K": result["scoreK"],
        "probability_L": result["scoreL"],
        "probability_M": result["scoreM"],
        "probability_N": result["scoreN"],
        "probability_P": result["scoreP"],
        "probability_Q": result["scoreQ"],
        "probability_R": result["scoreR"],
        "probability_S": result["scoreS"],
        "probability_T": result["scoreT"],
        "probability_V": result["scoreV"],
        "probability_W": result["scoreW"],
        "probability_Y": result["scoreY"],
    }

    bottle.response.status = 200
    return final_result



@app.get('/similar_proteins/<uniprot_accession>'+reSlashOrNot)
def get_similar_proteins_uniprot_api(uniprot_accession):

    uniprot_accession = uniprot_accession.strip("'")

    similar_proteins = get_similar_proteins(uniprot_accession)

    if not similar_proteins:
        bottle.response.status = 404
        return {}

    with ThreadPoolExecutor(max_workers = POOL_MAX_WORKERS) as executor:
        results = executor.map(get_uniprot_best_non_overlapping_structures, similar_proteins, repeat('Y'))

    dict_results = {}
    final_result = {
        uniprot_accession: []
    }


    for result in results:
        result = result[0]
        
        for accession in result.keys():
            
            if dict_results.get(accession) is None:
                dict_results[accession] = {
                    "uniprot_id": accession,
                    "name": similar_proteins[accession]["name"],
                    "description": similar_proteins[accession]["description"],
                    "species": similar_proteins[accession]["species"],
                    "taxid": similar_proteins[accession]["tax_id"],
                    "unp_length": similar_proteins[accession]["unp_length"],
                    "representative_pdbs": [],
                    "temp_observed_regions": [],
                    "mapped_segment": []
                }

            for representative_pdb in result[accession]:

                t_obs_regions = []

                for observed_region in representative_pdb["observed_regions"]:
                    dict_results[accession]["temp_observed_regions"].append((observed_region["unp_start"], observed_region["unp_end"]))
                    t_obs_regions.append({
                        "unp_start": observed_region["unp_start"],
                        "unp_end": observed_region["unp_end"]
                    })

                dict_results[accession]["representative_pdbs"].append({
                    "pdb_id": representative_pdb["pdb_id"],
                    "best_chain": representative_pdb["chain_id"],
                    "entity_id": representative_pdb["entity_id"],
                    "unp_start": representative_pdb["unp_start"],
                    "unp_end": representative_pdb["unp_end"],
                    "observed_regions": t_obs_regions
                })

    for accession in dict_results.keys():

        residue_count = count_residues(dict_results[accession]["temp_observed_regions"])
        coverage = residue_count / dict_results[accession]["unp_length"]

        dict_results[accession]["coverage"] =  float("%.2f" % float(coverage))

        del dict_results[accession]["temp_observed_regions"]

        # if there are no representative structures, check if there is a UniRef90 mapping. If so, get them
        if not dict_results[accession]["representative_pdbs"]:
            
            unf_mappings = get_unf_mappings(accession)

            if unf_mappings is not None:
                dict_results[accession]["mapped_segment"].append(unf_mappings)

        dict_results[accession]["representative_pdbs"] = sorted(dict_results[accession]["representative_pdbs"], key=itemgetter("unp_start"))
        final_result[uniprot_accession].append(dict_results[accession])
    
    # sort the list based on highest coverage
    final_result[uniprot_accession] = sorted(final_result[uniprot_accession], key=itemgetter("coverage"), reverse=True)

    bottle.response.status = 200
    return final_result


def get_similar_proteins(uniprot_accession):
    
    query = """
    MATCH (unp:UniProt)-[:IS_IN_UNIREF90_CLUSTER]->(unf:UniRefReference)<-[:IS_IN_UNIREF90_CLUSTER]-(similar_protein:UniProt)
    WHERE unp.ACCESSION=$uniprot_accession AND similar_protein.ACCESSION <> unp.ACCESSION
    WITH similar_protein
    MATCH (similar_protein)-[:HAS_TAXONOMY]->(tax:Taxonomy)
    RETURN similar_protein.ACCESSION, toInteger(similar_protein.LENGTH), similar_protein.NAME, similar_protein.DESCR, toInteger(tax.TAX_ID), tax.NCBI_SCIENTIFIC
    """

    mappings = run_query(query, parameters={
        "uniprot_accession": uniprot_accession
    })

    dict_results = {}

    for mapping in mappings:
        (accession, unp_length, name, description, tax_id, species) = mapping

        dict_results[accession] = {
            "name": name,
            "description": description,
            "tax_id": tax_id,
            "species": species,
            "unp_length": unp_length
        }

    return dict_results


"""
@api {get} uniprot/similar_proteins/:accession/:identity Get similar proteins for a UniProt accession for a given sequence identity
@apiName GetUniProtSimilarIdenticalProteins
@apiGroup UniProt
@apiDescription Get similar proteins for a UniProt accession for a given sequence identity
@apiVersion 2.0.0

@apiParam {String} accession=P07550 UniProt accession
@apiParam {String} identity=85 Sequence identity

@apiExample {json=./examples/success/similar_proteins_identical_uniprot.json} apiSuccessExample Example success response JSON
"""
@app.get('/similar_proteins/<uniprot_accession>/' +reIdentity +reSlashOrNot)
def get_similar_proteins_identical_uniprot_api(uniprot_accession, seq_identity):

    uniprot_accession = uniprot_accession.strip("'")
    seq_identity = seq_identity.strip("'")

    query = """
    MATCH (uniprot:UniProt {ACCESSION:$uniprot_accession})<-[:HAS_UNIPROT]-(entity:Entity)
    WITH uniprot.ACCESSION AS srcAccession, entity

    // Get all sequence clusters based on the identity
    MATCH (entity)-[:IS_PART_OF_SEQUENCE_CLUSTER]->(cluster:SeqCluster) WHERE toInteger(cluster.IDENTITY) >= $identity
    WITH DISTINCT srcAccession, cluster

    // Get all cluster member entities and mapped uniprots
    MATCH (cluster)<-[:IS_PART_OF_SEQUENCE_CLUSTER]-(similarEntity)-[rel:HAS_UNIPROT_SEGMENT]->(similarUniProt)-[:HAS_TAXONOMY]->(tax:Taxonomy)
    WHERE similarUniProt.ACCESSION <> srcAccession AND NOT similarUniProt.ACCESSION CONTAINS '-'

    WITH DISTINCT toInteger(cluster.IDENTITY) AS identity, similarUniProt.ACCESSION AS accession, similarUniProt.NAME AS name,
        similarUniProt.DESCR AS description, toInteger(similarUniProt.LENGTH) AS length, toInteger(tax.TAX_ID) AS taxId,
        tax.NCBI_SCIENTIFIC AS species, SPLIT(similarEntity.UNIQID, '_')[0] AS entryId, toInteger(SPLIT(similarEntity.UNIQID, '_')[1]) AS entityId,
        similarEntity.BEST_CHAIN_ID AS bestChainId, toInteger(rel.UNP_START) AS unpStart, toInteger(rel.UNP_END) AS unpEnd
    ORDER BY unpStart

    RETURN {
        uniprot_id: accession,
        name: name,
        description: description,
        species: species,
        taxid: taxId,
        unp_length: length,
        sequence_identity: identity,
        mapped_segment: COLLECT({
            pdb_id: entryId,
            entity_id: entityId,
            best_chain: bestChainId,
            unp_start: unpStart,
            unp_end: unpEnd
        })
    }
    """

    if uniprot_accession.startswith("PRO_"):
        query = """
        MATCH (processed:UniProt {ACCESSION:$uniprot_accession})<-[rel:IS_A_POLYPROTEIN_OF]-(poly:UniProt)
        WITH poly, toInteger(rel.UNP_START) AS unpStart, toInteger(rel.UNP_END) AS unpEnd
            
        MATCH (poly)<-[unpRel:HAS_UNIPROT_SEGMENT]-(entity:Entity)
            WHERE toInteger(unpRel.UNP_START) >= unpStart AND toInteger(unpRel.UNP_END) <= unpEnd

        WITH poly.ACCESSION AS srcAccession, entity

        // Get all sequence clusters based on the identity
        MATCH (entity)-[:IS_PART_OF_SEQUENCE_CLUSTER]->(cluster:SeqCluster) WHERE toInteger(cluster.IDENTITY) = $identity
        WITH DISTINCT srcAccession, cluster

        // Get all cluster member entities and mapped uniprots
        MATCH (cluster)<-[:IS_PART_OF_SEQUENCE_CLUSTER]-(similarEntity)-[rel:HAS_UNIPROT_SEGMENT]->(similarUniProt)-[:HAS_TAXONOMY]->(tax:Taxonomy)
        WHERE similarUniProt.ACCESSION <> srcAccession AND NOT similarUniProt.ACCESSION CONTAINS '-'

        WITH DISTINCT toInteger(cluster.IDENTITY) AS identity, similarUniProt.ACCESSION AS accession, similarUniProt.NAME AS name,
        similarUniProt.DESCR AS description, toInteger(similarUniProt.LENGTH) AS length, toInteger(tax.TAX_ID) AS taxId,
        tax.NCBI_SCIENTIFIC AS species, SPLIT(similarEntity.UNIQID, '_')[0] AS entryId, toInteger(SPLIT(similarEntity.UNIQID, '_')[1]) AS entityId,
        similarEntity.BEST_CHAIN_ID AS bestChainId, toInteger(rel.UNP_START) AS unpStart, toInteger(rel.UNP_END) AS unpEnd
        ORDER BY unpStart

        RETURN {
        uniprot_id: accession,
        name: name,
        description: description,
        species: species,
        taxid: taxId,
        unp_length: length,
        sequence_identity: identity,
        mapped_segment: COLLECT({
                pdb_id: entryId,
                entity_id: entityId,
                best_chain: bestChainId,
                unp_start: unpStart,
                unp_end: unpEnd
            })
        }
        """

    similar_proteins = run_query(query, parameters={
        "uniprot_accession": uniprot_accession,
        "identity": int(seq_identity)
    })

    if not similar_proteins:
        bottle.response.status = 404
        return {}

    result = [item for sublist in similar_proteins for item in sublist]
    result = sorted(result, key=lambda x: x["sequence_identity"], reverse=True)

    # consider the longest non overlapping segments
    for protein in result:
        considered_range_set = final_segment = None

        # sort the list so that long segments comes first
        for segment in sorted(protein["mapped_segment"], key=lambda x:len(range(x["unp_start"], x["unp_end"])), reverse=True):
            current_range = set(range(segment["unp_start"], segment["unp_end"] + 1))

            # always consider first segment as large
            if not final_segment:
                considered_range_set = current_range
                final_segment = [segment]
            else:
                # if a new range
                if not current_range & considered_range_set:
                    considered_range_set = considered_range_set.union(current_range)
                    final_segment.append(segment)

        protein["mapped_segment"] = sorted(final_segment, key=lambda x:x["unp_start"])

    bottle.response.status = 200
    return {
        uniprot_accession : result
    }

"""
@api {get} uniprot/superposition/:accession Get superposition details for a UniProt accession
@apiName GetUniProtSuperposition
@apiGroup UniProt
@apiDescription Get superposition details for a UniProt accession
@apiVersion 2.0.0

@apiParam {String} accession=P0DTD1 UniProt accession

@apiUse GenericFields

@apiExample {json=./examples/success/uniprot_superposition.json} apiSuccessExample Example success response JSON
"""
@app.get('/superposition/<uniprot_accession>' +reSlashOrNot)
def get_superposition_api(uniprot_accession):

    uniprot_accession = uniprot_accession.strip("'")

    query = """
    MATCH (u:UniProt {ACCESSION:$uniprot_accession})-[:HAS_STRUCTURAL_CLUSTER]->(cluster:StructuralCluster)
    MATCH (cluster)-[:SEGMENT_START]->(startRes:UNPResidue), (cluster)-[:SEGMENT_END]->(endRes:UNPResidue)
    WITH cluster.ID AS clusterId, toInteger(startRes.ID) AS startResId , toInteger(endRes.ID) AS endResId,
        [(cluster)<-[:IS_PART_OF]-(chain:Chain) | chain] AS chains,
        [(cluster)-[:HAS_REPRESENTATIVE]->(repChain:Chain) | repChain.UNIQID][0] AS repChain
    RETURN startResId, endResId, COLLECT({
    	cluster: clusterId,
        chains: chains,
        repChain: repChain
    }) AS clusters ORDER BY startResId
    """

    if uniprot_accession.startswith("PRO_"):
            query = """
            MATCH (:UniProt {ACCESSION:$uniprot_accession})<-[rel:IS_A_POLYPROTEIN_OF]-(u:UniProt)
            WITH u, toInteger(rel.UNP_START) AS unpStart, toInteger(rel.UNP_END) AS unpEnd
            MATCH (u)-[:HAS_STRUCTURAL_CLUSTER]->(cluster:StructuralCluster)
            MATCH (cluster)-[:SEGMENT_START]->(startRes:UNPResidue), (cluster)-[:SEGMENT_END]->(endRes:UNPResidue)
                WHERE unpStart >= toInteger(startRes.ID) AND unpEnd <= toInteger(endRes.ID) OR 
                toInteger(startRes.ID) >= unpStart AND toInteger(endRes.ID) <= unpEnd
            WITH cluster.ID AS clusterId, toInteger(startRes.ID) AS startResId , toInteger(endRes.ID) AS endResId,
                    [(cluster)<-[:IS_PART_OF]-(chain:Chain) | chain] AS chains,
                    [(cluster)-[:HAS_REPRESENTATIVE]->(repChain:Chain) | repChain.UNIQID][0] AS repChain
            RETURN startResId, endResId, COLLECT({
                    cluster: clusterId,
                    chains: chains,
                    repChain: repChain
                }) AS clusters ORDER BY startResId
            """
        
    superpositions = run_query(query, parameters={
        "uniprot_accession": uniprot_accession
    })

    if not superpositions:
        bottle.response.status = 404
        return {}

    api_result = {
        uniprot_accession: []
    }
    final_dict = {}

    for element in superpositions:
        (start_residue, end_residue, clusters) = element
        segment_fragment = {
            "segment_start": start_residue,
            "segment_end": end_residue,
            "clusters": []
        }

        cluster_list = []
        for cluster in clusters:
            cluster_id = cluster["cluster"]
            member_chains = cluster["chains"]
            repr_chain = cluster["repChain"]

            member_list = []
            for member_chain in member_chains:
                [pdb_id, entity_id, struct_asym_id] = member_chain["UNIQID"].split("_")
                member_list.append({
                    "pdb_id": pdb_id,
                    "auth_asym_id": member_chain["AUTH_ASYM_ID"],
                    "struct_asym_id": struct_asym_id,
                    "entity_id": int(entity_id),
                    "is_representative": True if member_chain["UNIQID"] == repr_chain else False
                })

            # put representative chain on top
            member_list = sorted(member_list, key=lambda x: x["is_representative"], reverse=True)
            cluster_list.append(member_list)

        segment_fragment["clusters"] = cluster_list

        api_result[uniprot_accession].append(segment_fragment)

    bottle.response.status = 200
    return api_result


@app.get('/variation/<uniprot_accession>'+reSlashOrNot)
def get_uniprot_variation_api(uniprot_accession):

    uniprot_accession = uniprot_accession.strip("'")

    unp_basic_details = get_uniprot_basic_details(uniprot_accession)

    if not unp_basic_details:
        bottle.response.status = 404
        return {}

    seq_mapping, seq_resp_status = get_uniprot_sequence(uniprot_accession)

    (sequence, seq_length) = seq_mapping

    api_result = {
        uniprot_accession: {
            "accession": uniprot_accession,
            "entryName": unp_basic_details[0][0],
            "proteinName": unp_basic_details[0][1],
            "features": [],
            "sequence": sequence,
            "length": seq_length
        }
    }

    feature_dict = {}
    xref_details = get_variation_xref_details(uniprot_accession)

    for xref in xref_details:
        (var_id, alt_sequence, begin, end, wild_type, polyphen_prediction, polyphen_score, sift_prediction, sift_score, somatic_status, cytogenetic_band, consequence_type, 
            genomic_location, source_types, clinical_significance, variant_desc, xref_name, xref_id, xref_url, xref_alt_url) = xref

        if (polyphen_score == "0.0" and sift_score is None) or (polyphen_score is None and sift_score == "0.0"):
            continue

        if feature_dict.get(var_id) is None:
            feature_dict[var_id] = {
                "type": "VARIANT",
                "alternativeSequence": alt_sequence,
                "begin": begin,
                "end": end,
                "xrefs": [],
                "evidences": [],
                "association": [],
                "wildType": wild_type,
                "polyphenPrediction": polyphen_prediction,
                "polyphenScore": None if polyphen_score is None else float(polyphen_score),
                "siftPrediction": sift_prediction,
                "siftScore": None if sift_score is None else float(sift_score),
                "somaticStatus": None if somatic_status is None else float(somatic_status),
                "cytogeneticBand": cytogenetic_band,
                "consequenceType": consequence_type,
                "genomicLocation": genomic_location,
                "sourceType": source_types,
                "tempAssociation": {}
            }

        if clinical_significance is not None:
            feature_dict[var_id].update({
                "clinicalSignificances": clinical_significance
            })

        if variant_desc is not None:
            feature_dict[var_id].update({
                "description": variant_desc
            })

        xref_fragment = {
            "name": xref_name,
            "id": xref_id
        }

        if xref_url is not None:
            xref_fragment.update({
                "url": xref_url
            })
        
        if xref_alt_url is not None:
            xref_fragment.update({
                "alternativeUrl": xref_alt_url
            })

        feature_dict[var_id]["xrefs"].append(xref_fragment)

    evidence_details = get_variation_evidence_details(uniprot_accession)
    
    for evidence in evidence_details:
        (var_id, alt_sequence, begin, end, wild_type, polyphen_prediction, polyphen_score, sift_prediction, sift_score, somatic_status, cytogenetic_band, consequence_type, 
            genomic_location, source_types, clinical_significance, evidence_code, evidence_name, evidence_id, evidence_url, evidence_alt_url) = evidence

        if feature_dict.get(var_id) is None:
            feature_dict[var_id] = {
                "type": "VARIANT",
                "alternativeSequence": alt_sequence,
                "begin": begin,
                "end": end,
                "evidences": [],
                "association": [],
                "wildType": wild_type,
                "polyphenPrediction": polyphen_prediction,
                "polyphenScore": None if polyphen_score is None else float(polyphen_score),
                "siftPrediction": sift_prediction,
                "siftScore": None if sift_score is None else float(sift_score),
                "somaticStatus": None if somatic_status is None else float(somatic_status),
                "cytogeneticBand": cytogenetic_band,
                "consequenceType": consequence_type,
                "genomicLocation": genomic_location,
                "sourceType": source_types,
                "tempAssociation": {}
            }

        if clinical_significance is not None:
            feature_dict[var_id].update({
                "clinicalSignificances": clinical_significance
            })

        evidence_fragment = {
            "code": evidence_code,
            "source": {
                "name": evidence_name,
                "id": evidence_id
            }
        }

        if evidence_url is not None:
            evidence_fragment["source"].update({
                "url": evidence_url
            })
        
        if evidence_alt_url is not None:
            evidence_fragment["source"].update({
                "alternativeUrl": evidence_alt_url
            })

        feature_dict[var_id]["evidences"].append(evidence_fragment)
    
    association_details = get_variation_association_details(uniprot_accession)
    
    association_dict = {}

    for association in association_details:
        (var_id, alt_sequence, begin, end, wild_type, polyphen_prediction, polyphen_score, sift_prediction, sift_score, somatic_status, cytogenetic_band, consequence_type, 
            genomic_location, source_types, clinical_significance, asso_id, asso_name, asso_desc, asso_disease, evidence_code, evidence_name, evidence_id, evidence_desc, evidence_url, evidence_alt_url) = association

        if feature_dict.get(var_id) is None:
            feature_dict[var_id] = {
                "type": "VARIANT",
                "alternativeSequence": alt_sequence,
                "begin": begin,
                "end": end,
                "association": [],
                "wildType": wild_type,
                "polyphenPrediction": polyphen_prediction,
                "polyphenScore": None if polyphen_score is None else float(polyphen_score),
                "siftPrediction": sift_prediction,
                "siftScore": None if sift_score is None else float(sift_score),
                "somaticStatus": None if somatic_status is None else float(somatic_status),
                "cytogeneticBand": cytogenetic_band,
                "consequenceType": consequence_type,
                "genomicLocation": genomic_location,
                "sourceType": source_types,
                "tempAssociation": {}
            }

        if clinical_significance is not None:
            feature_dict[var_id].update({
                "clinicalSignificances": clinical_significance
            })

        if feature_dict[var_id]["tempAssociation"].get(asso_id) is None:
            feature_dict[var_id]["tempAssociation"][asso_id] = {
                "name": asso_name,
                "disease": True if asso_disease == "True" else False,
                "evidences": []
            }

            if asso_desc is not None:
                feature_dict[var_id]["tempAssociation"][asso_id].update({
                    "description": asso_desc
                })

        evidence_fragment = {
            "code": evidence_code,
            "source": {
                "name": evidence_name,
                "id": evidence_id
            }
        }

        if evidence_url is not None:
            evidence_fragment.update({
                "url": evidence_url
            })

        if evidence_alt_url is not None:
            evidence_fragment.update({
                "alternativeUrl": evidence_alt_url
            })
        
        feature_dict[var_id]["tempAssociation"][asso_id]["evidences"].append(evidence_fragment)

    for feature, items in feature_dict.items():

        items["association"] = [x for x in items["tempAssociation"].values()]

        if items.get("xrefs") is not None and not items["xrefs"]:
            del items["xrefs"]
        if items.get("evidences") is not None and not items["evidences"]:
            del items["evidences"]
        if items.get("association") is not None and not items["association"]:
            del items["association"]

        del items["tempAssociation"]

        api_result[uniprot_accession]["features"].append(items)

    # PDBE-3712: Get Missense3D variation details
    # PDBE-3956: Get FoldX variation details
    # PDBE-4780: Added FireProt DB variant details
    # PDBE-4723: Added Frustratometer variant details
    # PDBE-5093: Added nextprot variant details
    query = """
    MATCH (unp:UniProt {ACCESSION: $uniprot_accession})-[:HAS_UNP_RESIDUE]->(unpRes:UNPResidue)<-[:MAP_TO_UNIPROT_RESIDUE]-(pdbRes:PDBResidue)<-[resRel:FUNPDBE_ANNOTATION_FOR]-(funGroup:FunPDBeResidueGroup)-[:FUNPDBE_RESIDUE_GROUP_OF]->(funEntry:FunPDBeEntry)
    WHERE
        funEntry.DATA_RESOURCE IN ['Missense3D', 'FoldX', 'SKEMPI', 'FireProt DB', 'Frustratometer', 'nextprot'] AND resRel.AA_VARIANT <> ''
    WITH toInteger(unpRes.ID) AS unpRes, pdbRes.CHEM_COMP_ID AS pdbResCode, resRel.AA_VARIANT AS variant, resRel.AA_VARIANT_CAUSES AS causes, SPLIT(funEntry.SQL_ID, '_')[0] AS pdbId, funEntry.RESOURCE_ENTRY_URL AS entryUrl, funGroup.LABEL AS label, funEntry.DATA_RESOURCE AS resource, resRel.RAW_SCORE AS rawScore ORDER BY unpRes
    RETURN DISTINCT unpRes, pdbResCode, variant, causes, pdbId, entryUrl, label, resource, rawScore
    """

    details = run_query(query, parameters = {
        "uniprot_accession": uniprot_accession
    })

    for row in details:
        (unp_res_id, pdb_res_code, variant, variant_causes, pdb_id, entry_url, label, resource, raw_score) = row
        clinical_significances = "fireprotdb"

        if resource == "Missense3D":
            clinical_significances = "missense"
        elif resource == "FoldX":
            clinical_significances = "foldx"
        elif resource == "SKEMPI":
            clinical_significances = "skempi"
        elif resource == "Frustratometer":
            clinical_significances = "frustratometer"
        elif resource == "nextprot":
            clinical_significances = "nextprot"

        result_data = {
            "type": "VARIANT",
            "alternativeSequence": get_amino_three_to_one(variant),
            "begin": str(unp_res_id),
            "end": str(unp_res_id),
            "association": [],
            "wildType": get_amino_three_to_one(pdb_res_code),
            "polyphenPrediction": "",
            "polyphenScore": None,
            "clinicalSignificances": clinical_significances,
            "siftScore": None,
            "consequenceType": variant_causes,
            "genomicLocation": f"""{resource}_{pdb_id}_{get_amino_three_to_one(pdb_res_code)}>{get_amino_three_to_one(variant)}""",
            "sourceType": "prediction",
            "category": label,
            "resourceUrl": entry_url,
            "rawScore": raw_score,
        }

        api_result[uniprot_accession]["features"].append(result_data)

    if not api_result[uniprot_accession]["features"]:
        bottle.response.status = 404
        return {}
    
    bottle.response.status = 200
    return api_result


@app.get('/protvista/variation/<uniprot_accession>'+reSlashOrNot)
def get_protvista_variation_api(uniprot_accession):

    generic_response = get_uniprot_variation_api(uniprot_accession)

    if not generic_response:
        bottle.response.status = 404
        return {}

    bottle.response.status = 200
    return convert_to_protvista("protein", "variation", generic_response)


def get_uniprot_basic_details(accession):

    query = """
    MATCH (unp:UniProt {ACCESSION:$accession})
    RETURN unp.NAME, unp.DESCR, unp.LENGTH
    """

    return run_query(query, parameters={
        "accession": str(accession)
    })


def get_variation_xref_details(accession):

    query = """
    MATCH (unp:UniProt {ACCESSION:$accession})<-[:IS_VARIANT_IN]-(unp_variant:UNPVariant)<-[:IS_XREF_FOR]-(unp_xref:UNPVariantXref)
    RETURN unp_variant.ID, COALESCE(unp_variant.ALTERNATIVE_SEQUENCE, 'undefined'), unp_variant.BEGIN, unp_variant.END, unp_variant.WILD_TYPE, unp_variant.POLYPHEN_PREDICTION, unp_variant.POLYPHEN_SCORE, unp_variant.SIFT_PREDICTION,
        unp_variant.SIFT_SCORE, unp_variant.SOMATIC_STATUS, unp_variant.CYTOGENETIC_BAND, unp_variant.CONSEQUENCE_TYPE, unp_variant.GENOMIC_LOCATION, unp_variant.SOURCE_TYPES, unp_variant.CLINICAL_SIGNIFICANCES, 
        unp_variant.DESCRIPTION, unp_xref.NAME, unp_xref.ID, unp_xref.URL, unp_xref.ALTERNATIVE_URL
        ORDER BY toInteger(unp_variant.BEGIN)
    """
    
    return run_query(query, parameters={
        "accession": str(accession)
    })


def get_variation_evidence_details(accession):

    query = """
    MATCH (unp:UniProt {ACCESSION:$accession})<-[:IS_VARIANT_IN]-(unp_variant:UNPVariant)<-[:IS_EVIDENCE_FOR]-(unp_evidence:UNPVariantEvidence)
    RETURN unp_variant.ID, COALESCE(unp_variant.ALTERNATIVE_SEQUENCE, 'undefined'), unp_variant.BEGIN, unp_variant.END, unp_variant.WILD_TYPE, unp_variant.POLYPHEN_PREDICTION, unp_variant.POLYPHEN_SCORE, unp_variant.SIFT_PREDICTION,
        unp_variant.SIFT_SCORE, unp_variant.SOMATIC_STATUS, unp_variant.CYTOGENETIC_BAND, unp_variant.CONSEQUENCE_TYPE, unp_variant.GENOMIC_LOCATION, unp_variant.SOURCE_TYPES, unp_variant.CLINICAL_SIGNIFICANCES, 
        unp_evidence.CODE, unp_evidence.NAME, unp_evidence.ID, unp_evidence.URL, unp_evidence.ALTERNATIVE_URL
    """
    
    return run_query(query, parameters={
        "accession": str(accession)
    })


def get_variation_association_details(accession):

    query = """
    MATCH (unp:UniProt {ACCESSION:$accession})<-[:IS_VARIANT_IN]-(unp_variant:UNPVariant)<-[:IS_ASSOCIATION_FOR]-(unp_asso:UNPAssociation)<-[:IS_EVIDENCE_FOR]-(unp_evidence:UNPVariantEvidence)
    RETURN unp_variant.ID, COALESCE(unp_variant.ALTERNATIVE_SEQUENCE, 'undefined'), unp_variant.BEGIN, unp_variant.END, unp_variant.WILD_TYPE, unp_variant.POLYPHEN_PREDICTION, unp_variant.POLYPHEN_SCORE, unp_variant.SIFT_PREDICTION,
        unp_variant.SIFT_SCORE, unp_variant.SOMATIC_STATUS, unp_variant.CYTOGENETIC_BAND, unp_variant.CONSEQUENCE_TYPE, unp_variant.GENOMIC_LOCATION, unp_variant.SOURCE_TYPES, unp_variant.CLINICAL_SIGNIFICANCES, 
        unp_asso.ID, unp_asso.NAME, unp_asso.DESCRIPTION, unp_asso.DISEASE, unp_evidence.CODE, unp_evidence.NAME, unp_evidence.ID, unp_evidence.DESCRIPTION, unp_evidence.URL, unp_evidence.ALTERNATIVE_URL
        ORDER BY toInteger(unp_variant.BEGIN)
    """

    return run_query(query, parameters={
        "accession": str(accession)
    })

def get_unf_mappings(uniprot_accession):

    query = """
    MATCH (unf:UniProt)<-[unf_rel:HAS_UNIREF90_SEGMENT]-(entity:Entity)<-[:HAS_ENTITY]-(entry:Entry)
    WHERE unf.ACCESSION=$uniprot_accession AND unf_rel.STRUCT_ASYM_ID=entity.BEST_CHAIN_ID
    WITH SPLIT(entity.UNIQID, '_') AS temp_list, unf_rel.STRUCT_ASYM_ID AS chain_id, toInteger(unf_rel.UNP_START) AS unp_start, toInteger(unf_rel.UNP_END) AS unp_end ORDER BY toFloat(entry.RESOLUTION)
    RETURN temp_list[0], toInteger(temp_list[1]), chain_id, unp_start, unp_end ORDER BY unp_start LIMIT 1
    """

    mappings = run_query(query, parameters={
        "uniprot_accession": uniprot_accession
    })
    
    if mappings:
        (entry_id, entity_id, chain_id, unp_start, unp_end) = mappings[0]

        return {
            "pdb_id": entry_id,
            "entity_id": entity_id,
            "best_chain": chain_id,
            "unp_start": unp_start,
            "unp_end": unp_end
        }
    else:
        return None


def get_uniprot_sequence(uniprot_accession):

    query = """
    MATCH (unp:UniProt {ACCESSION:$uniprot_accession})-[:HAS_UNP_RESIDUE]->(unp_res:UNPResidue)
    WITH unp_res.ONE_LETTER_CODE AS one_letter_code ORDER BY toInt(unp_res.ID)
    WITH COLLECT(one_letter_code) AS codes
    WITH REDUCE (s = HEAD(codes), n IN TAIL(codes) | s + n) AS sequence
    RETURN sequence, LENGTH(sequence) AS seq_length
    """

    mapping = run_query(query, parameters={
        "uniprot_accession": str(uniprot_accession)
    })

    (sequence, seq_length) = mapping[0]

    if not sequence:
        return {}, 404

    return mapping[0], 200


def get_generic_structure(uniprot_accession, data_type):

    uniprot_accession = uniprot_accession.strip("'")

    seq_mapping, seq_resp_status = get_uniprot_sequence(uniprot_accession)

    if not seq_mapping:
        return None

    (sequence, seq_length) = seq_mapping
    
    return {
        uniprot_accession: {
            "sequence": sequence,
            "length": seq_length,
            "dataType": data_type,
            "data": []
        }
    }

def get_residue_modifications(pdb_id, entity_id, accession):

    query = """
    MATCH (entry:Entry {ID:$pdb_id})-[:HAS_ENTITY]->(entity:Entity {ID:$entity_id})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[rel:MAP_TO_UNIPROT_RESIDUE]->(unp_res:UNPResidue)<-[:HAS_UNP_RESIDUE]-(unp:UniProt {ACCESSION:$accession})
    MATCH (chem_comp:ChemicalComponent) WHERE chem_comp.ID=pdb_res.CHEM_COMP_ID AND chem_comp.TYPE IN ['P','D','R'] AND chem_comp.MON_NSTD_PARENT_CHEM_COMP_ID IS NOT null
    RETURN pdb_res.ID, pdb_res.CHEM_COMP_ID, unp_res.ID, unp_res.ONE_LETTER_CODE ORDER BY toInteger(pdb_res.ID)
    """

    result_list = []
    mappings = run_query(query, parameters={
        "pdb_id": str(pdb_id),
        "entity_id": str(entity_id),
        "accession": str(accession)
    })

    for mapping in mappings:
        
        result_list.append(mapping)

    return result_list


def get_residue_mutations(pdb_id, entity_id, accession):

    query = """
    MATCH (entry:Entry {ID:$pdb_id})-[:HAS_ENTITY]->(entity:Entity {ID:$entity_id})-[:HAS_RESIDUE_CONFLICT]->(conflict:ResidueConflict)
    RETURN conflict.ID, conflict.DETAILS
    """

    mappings = run_query(query, parameters={
        "pdb_id": str(pdb_id),
        "entity_id": str(entity_id)
    })

    for mapping in mappings:
        (conflict_id, details) = mapping
        t_accession = conflict_id.split("_")[0]

        # skip records if accession is not same
        if accession != t_accession:
            continue
        
        return details


def get_mapped_entries_for_chem(list_pdb_entity, chem_comp):

    query = """
    UNWIND $list_pdb_entity AS pdb_entity
    WITH SPLIT(pdb_entity, '_') AS splits
    MATCH (entry:Entry)-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->(:PDBResidue)-[:HAS_ARP_CONTACT]-(b:BoundLigand) WHERE b.CHEM_COMP_ID=$chem_comp AND entry.ID=splits[0] AND entity.ID=splits[1]
    RETURN COLLECT(DISTINCT entry.ID)
    """

    results = run_query(query, parameters={
        "list_pdb_entity": list_pdb_entity,
        "chem_comp": str(chem_comp)
    })

    return results


def get_polyprotein(processed_protein):

    query = """
    MATCH (u:UniProt {ACCESSION:$processed_protein})-[:IS_A_POLYPROTEIN_OF]-(p:UniProt)
    RETURN p.ACCESSION
    """

    results = run_query(query, parameters={
        "processed_protein": processed_protein
    })

    return results[0][0]


def get_processed_protein_start(processed_protein):
    query = """
    MATCH (u:UniProt {ACCESSION:$processed_protein})-[:HAS_UNP_RESIDUE]->(ur:UNPResidue)
    WITH toInteger(ur.ID) AS resId
    RETURN resId ORDER BY resId LIMIT 1
    """

    results = run_query(query, parameters={
        "processed_protein": processed_protein
    })

    return results[0][0]


"""
@api {get} uniprot/processed_proteins/:accession Get processed protein details for a UniProt accession
@apiName GetUniProtProcessedProteins
@apiGroup UniProt
@apiDescription Get processed protein details for a UniProt accession
@apiVersion 2.0.0

@apiParam {String} accession=P0DTD1 UniProt accession

@apiUse GenericFields

@apiExample {json=./examples/success/uniprot_processed_proteins.json} apiSuccessExample Example success response JSON
"""
@app.get('/processed_proteins/<uniprot_accession>' +reSlashOrNot)
def get_uniprot_processed_proteins_api(uniprot_accession):

    uniprot_accession = uniprot_accession.strip("'")

    query = """
    MATCH (unp:UniProt {ACCESSION:$uniprot_accession})-[rel:IS_A_POLYPROTEIN_OF]->(processed_protein:UniProt)
    WITH processed_protein.ACCESSION AS accession, processed_protein.NAME AS name,
        toInteger(rel.UNP_START) AS unpStart, toInteger(rel.UNP_END) AS unpEnd
    RETURN accession, name, unpStart, unpEnd, toInteger(unpEnd - unpStart + 1) AS unpLength ORDER BY unpStart
    """

    processed_proteins = run_query(query, parameters={
        "uniprot_accession": str(uniprot_accession)
    })
    
    if not processed_proteins:
        bottle.response.status = 404
        return {}

    # PDBE-4026: return 404 in case if there is a single processed protein
    if len(processed_proteins) == 1:
        bottle.response.status = 404
        return {}

    api_result = get_generic_structure(uniprot_accession, "PROCESSED PROTEINS")
    
    unp_basic_details = get_uniprot_basic_details(uniprot_accession)
    poly_unp_length = int(unp_basic_details[0][2])

    processed_dict = {}
    for processed_protein in processed_proteins:
        (accession, protein_name, unp_start, unp_end, unp_length) = processed_protein

        # ignore processed protein if covers full length of polyprotein
        if poly_unp_length == unp_length:
            continue
        
        processed_dict[accession] = {
            "id": accession,
            "name": protein_name,
            "unp_length": poly_unp_length,
            "unp_start": unp_start,
            "unp_start_code": get_amino_one_to_three(api_result[uniprot_accession]["sequence"][unp_start - 1]),
            "unp_end": unp_end,
            "unp_end_code": get_amino_one_to_three(api_result[uniprot_accession]["sequence"][unp_end - 1]),
            "summary_counts": {
                "pdbs": 0,
                "ligands": 0,
                "interactions": 0
            },
            "representative_pdb": {}
        }

    # get representative structures
    with ThreadPoolExecutor(max_workers = POOL_MAX_WORKERS) as executor:
        repr_results = executor.map(get_uniprot_best_non_overlapping_structures, processed_dict.keys(), repeat('Y'))

        for result in repr_results:
            (result, _) = result
            pro_id = list(result)[0]
            representatives = result[pro_id]

            if representatives:
                representative = representatives[0]
                processed_dict[pro_id]["representative_pdb"] = {
                    "pdb_id": representative["pdb_id"],
                    "best_chain": representative["chain_id"],
                    "entity_id": representative["entity_id"],
                    "unp_start": representative["unp_start"],
                    "unp_end": representative["unp_end"]
                }
            else:
                processed_dict[pro_id]["representative_pdb"] = None

           
    # get summary
    with ThreadPoolExecutor(max_workers = POOL_MAX_WORKERS) as executor:
        summary_results = executor.map(get_uniprot_summary_stats, processed_dict.keys())

        for result in summary_results:
            (accession, summary) = result
            number_of_pdbs = 0 if summary is None else summary["pdbs"]
            processed_dict[accession]["summary_counts"].update({
                "pdbs": number_of_pdbs,
                "ligands": 0 if summary is None or number_of_pdbs == 0 else summary["ligands"],
                "interactions": 0 if summary is None or number_of_pdbs == 0 else summary["interaction_partners"]
            })

            api_result[uniprot_accession]["data"].append(processed_dict[accession])

    if not api_result[uniprot_accession]["data"]:
        bottle.response.status = 404
        return {}

    bottle.response.status = 200
    return api_result


@app.get('/protvista/processed_proteins/<uniprot_accession>'+reSlashOrNot)
def get_uniprot_protvista_processed_proteins_api(uniprot_accession):

    generic_response = get_uniprot_processed_proteins_api(uniprot_accession)

    if not generic_response:
        bottle.response.status = 404
        return {}

    bottle.response.status = 200
    return convert_to_protvista("protein", "processed_protein", generic_response)


@app.get('/superposition_segment/<uniprot_accession>/<residue_start>/<residue_end>' +reSlashOrNot)
def get_uniprot_superposition_segment_api(uniprot_accession, residue_start, residue_end):

    uniprot_accession = uniprot_accession.strip("'")
    residue_start = int(residue_start.strip("'"))
    residue_end = int(residue_end.strip("'"))
    results = get_superposition_api(uniprot_accession)

    if not results:
        bottle.response.status = 404
        return {}

    incr = 1
    for result in results[uniprot_accession]:
        segment_start = result["segment_start"]
        segment_end = result["segment_end"]

        if residue_start >= segment_start and residue_end <= segment_end:
            bottle.response.status = 200
            return {
                uniprot_accession: {
                    "segment_id": int(incr)
                }
            }

        incr += 1

    # no segment matching the residues found
    bottle.response.status = 404
    return {}


@app.get('/annotation_partners/<uniprot_accession>' +reSlashOrNot)
def get_uniprot_annotation_partners_api(uniprot_accession):

    uniprot_accession = uniprot_accession.strip("'")

    query = """
    MATCH (u:UniProt {ACCESSION:$uniprot_accession})-[:HAS_UNP_RESIDUE]->(ur:UNPResidue)<-[:MAP_TO_UNIPROT_RESIDUE]-(pdbRes:PDBResidue)<-[:FUNPDBE_ANNOTATION_FOR]-(resGroup:FunPDBeResidueGroup)-[:FUNPDBE_RESIDUE_GROUP_OF]->(funEntry:FunPDBeEntry)
    RETURN COLLECT(DISTINCT funEntry.DATA_RESOURCE) AS partners
    """
    


    results = run_query(query, parameters={
        "uniprot_accession": uniprot_accession
    })
    
    if len(results[0]) == 0 or not results[0][0]:
        bottle.response.status = 404
        return {}
    
    partner_results = {
        uniprot_accession: {
            "externalResources": []
        }
    }
    for partner in results[0][0]:
        partner_data = funpdbe_resource_dict.get(partner)
        if partner_data:
            partner_link = partner_data["link"]

            # only add partners with https 
            if partner_link.startswith("https"):
                partner_results[uniprot_accession]["externalResources"].append({
                    "resourceName": partner,
                    "url": partner_link
                })

    bottle.response.status = 200
    return partner_results


def get_uniprot_best_entity(uniprot_accession):

    query = """
    MATCH (unp:UniProt {ACCESSION:$uniprot_accession})<-[rel:HAS_UNIPROT_OBS_SEGMENT]-(entity:Entity)
    WITH toInteger(rel.RANKING_SCORES[7]) AS rankingScore, entity.UNIQID AS entity ORDER BY rankingScore DESC
    RETURN entity LIMIT 1
    """

    if uniprot_accession.startswith("PRO_"):
       query = """
        MATCH (unp:UniProt {ACCESSION:$uniprot_accession})<-[:IS_A_POLYPROTEIN_OF]-(u:UniProt)<-[rel:HAS_UNIPROT_OBS_SEGMENT]-(entity:Entity)
        WITH toInteger(rel.RANKING_SCORES[7]) AS rankingScore, entity.UNIQID AS entity ORDER BY rankingScore DESC
        RETURN entity LIMIT 1
        """ 

    result = run_query(query, parameters={
        "uniprot_accession": uniprot_accession
    })

    if result:
        return result[0][0]
    else:
        return None


def exists_annotations(uniprot_accession: str) -> bool:
    """
    Returns True if there is a KB annotation for an accession, else returns False
    """

    query = """
    MATCH (u:UniProt {ACCESSION:$uniprot_accession})-[:HAS_UNP_RESIDUE]->(ur:UNPResidue)<-[:MAP_TO_UNIPROT_RESIDUE]-(pdbRes:PDBResidue)<-[:FUNPDBE_ANNOTATION_FOR]-(resGroup:FunPDBeResidueGroup)
    RETURN 1 LIMIT 1
    """

    result = run_query(query, parameters={
        "uniprot_accession": uniprot_accession
    })

    if result:
        return True
    else:
        return False
