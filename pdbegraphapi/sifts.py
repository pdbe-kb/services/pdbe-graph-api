#!/usr/bin/env python

import bottle

import re
from pdbegraphapi.amino_acid_codes import amino_acid_codes
from pdbegraphapi.neo4j_model import run_query
import more_itertools as mit
from operator import itemgetter, attrgetter

from pdbegraphapi import request_handler
from pdbegraphapi.config import rePDBid, reSlashOrNot

print('[SIFTS] Starting')

app = bottle.Bottle()

app.install(request_handler)

print('[SIFTS] Loaded')


"""
@api {get} mappings/:pdbId Get annotations for a PDB Entry ID
@apiName GetPDBAnnotations
@apiGroup SIFTS
@apiDescription Get mappings (as assigned by the SIFTS process) from PDB structures to UniProt, Pfam, InterPro, CATH, SCOP, IntEnz, GO, Ensembl and HMMER accessions (and vice versa).
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID

@apiUse GenericResidueTerms
@apiUse CATHTerms
@apiUse SCOPTerms
@apiUse EnsemblTerms
@apiSuccess {String} category The GO category.
@apiSuccess {Integer} unp_start Index of first residue in Uniprot sequence mapped to this PDB entity.
@apiSuccess {Integer} unp_end Index of last residue in Uniprot sequence mapped to this PDB entity.

@apiExample {json=./examples/success/sifts_mappings.json} apiSuccessExample Example success response JSON
"""
@app.get('/'+rePDBid+reSlashOrNot)
def get_mappings_api(pdbid):

    pdbid = pdbid.strip("'")

    unp_result, unp_status = get_uniprot(pdbid)
    pfam_result, pfam_status = get_pfam(pdbid)
    cath_result, cath_status = get_cath(pdbid)
    interpro_result, interpro_status = get_interpro(pdbid)
    scop_result, scop_status = get_scop(pdbid)
    go_result, go_status = get_go(pdbid)
    ec_result, ec_status = get_ec(pdbid)
    ensembl_result, ensembl_status = get_ensembl(pdbid)

    all_status = [unp_status, pfam_status, cath_status, interpro_status,
                  scop_status, go_status, ec_status, ensembl_status]

    api_result = {
        pdbid: {}
    }

    if(all_status == [404 for x in range(0, 8)]):
        bottle.response.status = 404
        return api_result

    api_result[pdbid]["UniProt"] = unp_result
    api_result[pdbid]["Pfam"] = pfam_result
    api_result[pdbid]["CATH"] = cath_result
    api_result[pdbid]["SCOP"] = scop_result
    api_result[pdbid]["InterPro"] = interpro_result
    api_result[pdbid]["GO"] = go_result
    api_result[pdbid]["EC"] = ec_result
    api_result[pdbid]["Ensembl"] = ensembl_result

    bottle.response.status = 200
    return api_result


"""
@api {get} mappings/uniprot/:pdbId Get Uniprot mappings for a PDB Entry ID
@apiName GetPDBUNPMapping
@apiGroup SIFTS
@apiDescription Get mappings (as assigned by the SIFTS process) from PDB structures to UniProt.
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID

@apiUse GenericResidueTerms

@apiExample {json=./examples/success/uniprot_mapping.json} apiSuccessExample Example success response JSON
"""
@app.get('/uniprot/'+rePDBid+reSlashOrNot)
def get_uniprot_api(pdbid):
    
    pdbid = pdbid.strip("'")

    result, status = get_uniprot(pdbid)
    
    api_result = {
        pdbid: {
            "UniProt": {}
        }
    }

    if status == 404:
        bottle.response.status = 404
        return api_result

    api_result[pdbid]["UniProt"] = result

    bottle.response.status = 200
    return api_result


def get_uniprot(pdbid):
    """
    Get mappings (as assigned by the SIFTS process) from PDB structures to UniProt.
    """

    query = """
    MATCH (entry:Entry {ID:$pdbid})-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[r:MAP_TO_UNIPROT_RESIDUE]->
    (unp_res:UNPResidue)<-[:HAS_UNP_RESIDUE]-(unp:UniProt),
    (pdb_res)-[chain_rel:IS_IN_CHAIN]->(chain:Chain)
    RETURN toInteger(entity.ID) as entityId, unp.ACCESSION, unp.NAME, unp.DESCR, toInteger(pdb_res.ID) as pdbResId, toInteger(unp_res.ID) as unpResId, chain.AUTH_ASYM_ID, chain.STRUCT_ASYM_ID,
    toInteger(chain_rel.AUTH_SEQ_ID) as auth_seq_id, chain_rel.PDB_INS_CODE order by toInteger(pdb_res.ID)
    """

    mappings = run_query(query, {"pdbid":pdbid})

    api_result = {}

    if(len(mappings) == 0):
        return api_result, 404

    list_of_accessions = []
    dict_unp_master = {}
    dict_residues = {}
    dict_chains = {}
    dict_entity = {}
    dict_residues_mappings = {}
    dict_struct_asym_id = {}

    for mapping in mappings:

        (entity_id, accession, name, desc, pdb_res_id, unp_res_id,
         auth_asym_id, struct_asym_id, auth_seq_id, pdb_ins_code) = mapping

        if(accession not in list_of_accessions):
            list_of_accessions.append(accession)
            dict_unp_master[accession] = (name, desc)

        if(dict_entity.get(accession) is None):
            dict_entity[accession] = set([entity_id])
        else:
            dict_entity[accession].add(entity_id)

        if(dict_chains.get((accession, entity_id)) is None):
            dict_chains[(accession, entity_id)] = set([auth_asym_id])
        else:
            dict_chains[(accession, entity_id)].add(auth_asym_id)

        if(dict_struct_asym_id.get((accession, entity_id, auth_asym_id)) is None):
            dict_struct_asym_id[(accession, entity_id, auth_asym_id)] = struct_asym_id

        if(dict_residues.get((accession, entity_id, auth_asym_id)) is None):
            dict_residues[(accession, entity_id, auth_asym_id)] = set([pdb_res_id])
        else:
            dict_residues[(accession, entity_id, auth_asym_id)].add(pdb_res_id)

        dict_residues_mappings[(entity_id, auth_asym_id, pdb_res_id)] = (
            unp_res_id, auth_seq_id, pdb_ins_code)

    for accession in list_of_accessions:

        temp_mappings = []
        for entity_id in dict_entity[accession]:
            for auth_asym_id in dict_chains[(accession, entity_id)]:
                # using set to remove duplicates, since PDB residues will be duplicated in case of Chromophores which needs to be ignored
                for group in mit.consecutive_groups(dict_residues[(accession, entity_id, auth_asym_id)]):
                    group = list(group)

                    start_auth_seq_id = dict_residues_mappings[(
                        entity_id, auth_asym_id, group[0])][1]
                    end_auth_seq_id = dict_residues_mappings[(
                        entity_id, auth_asym_id, group[-1])][1]
                    start_pdb_ins_code = dict_residues_mappings[(
                        entity_id, auth_asym_id, group[0])][2]
                    end_pdb_ins_code = dict_residues_mappings[(
                        entity_id, auth_asym_id, group[-1])][2]

                    element = {
                        "entity_id": int(entity_id),
                        "chain_id": auth_asym_id,
                        "struct_asym_id": dict_struct_asym_id[(accession, entity_id, auth_asym_id)],
                        "start": {
                            "author_residue_number": None if start_auth_seq_id is None else int(start_auth_seq_id),
                            "author_insertion_code": "" if not start_pdb_ins_code else start_pdb_ins_code,
                            "residue_number": int(group[0])
                        },
                        "end": {
                            "author_residue_number": None if end_auth_seq_id is None else int(end_auth_seq_id),
                            "author_insertion_code": "" if not end_pdb_ins_code else end_pdb_ins_code,
                            "residue_number": int(group[-1])
                        },
                        "unp_start": int(dict_residues_mappings[(entity_id, auth_asym_id, group[0])][0]),
                        "unp_end": int(dict_residues_mappings[(entity_id, auth_asym_id, group[-1])][0]),
                        "pdb_start": int(group[0]),
                        "pdb_end": int(group[-1])
                    }
                    temp_mappings.append(element)

        api_result[accession] = {
            "name": dict_unp_master[accession][0],
            "identifier": dict_unp_master[accession][0],
            "mappings": sorted(temp_mappings, key=itemgetter('chain_id')),
            "description": dict_unp_master[accession][1]
        }
        
        del temp_mappings

    return api_result, 200


"""
@api {get} mappings/uniprot_segments/:pdbId Get Uniprot segment mappings for a PDB Entry ID
@apiName GetPDBUNPSegmentMapping
@apiGroup SIFTS
@apiDescription Get mappings (as assigned by the SIFTS process) from PDB structures to UniProt. The segments are calculated using the same rules as with the standard call but 
with the UniProt sequence as reference. The outcome is a set of segments that reflects discontinuities in the UniProt sequence.
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID

@apiUse GenericResidueTerms

@apiExample {json=./examples/success/uniprot_segments.json} apiSuccessExample Example success response JSON
"""
@app.get('/uniprot_segments/'+rePDBid+reSlashOrNot)
def get_uniprot_segments_api(pdbid):

    pdbid = pdbid.strip("'")

    # query = """
    # MATCH (entry:Entry {ID:$pdbid})-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[r:MAP_TO_UNIPROT_RESIDUE]->
    # (unp_res:UNPResidue)<-[:HAS_UNP_RESIDUE]-(unp:UniProt),
    # (pdb_res)-[chain_rel:IS_IN_CHAIN]->(chain:Chain)
    # RETURN toInteger(entity.ID) as entityId, unp.ACCESSION, unp.NAME, toInteger(pdb_res.ID) as pdbResId, toInteger(unp_res.ID) as unpResId, chain.AUTH_ASYM_ID, chain.STRUCT_ASYM_ID,
    # toInteger(chain_rel.AUTH_SEQ_ID) as auth_seq_id, chain_rel.PDB_INS_CODE order by toInteger(pdb_res.ID)
    # """

    query = """
    MATCH (entry:Entry {ID:$pdbid})-[:HAS_ENTITY]->(entity:Entity)-[rel:HAS_UNIPROT_SEGMENT]->(uniprot:UniProt) WHERE NOT uniprot.ACCESSION CONTAINS '-'
    MATCH (entity)-[:HAS_PDB_RESIDUE]->(pdbRes:PDBResidue)-[chainRel:IS_IN_CHAIN]->(chain:Chain)
        WHERE pdbRes.ID IN [rel.PDB_START, rel.PDB_END] AND rel.STRUCT_ASYM_ID=chain.STRUCT_ASYM_ID
    RETURN
        DISTINCT uniprot.ACCESSION AS accession, uniprot.NAME AS unpName, toInteger(entity.ID) AS entityId, 
        toInteger(rel.PDB_START) AS pdbStart, toInteger(rel.PDB_END) AS pdbEnd, toInteger(rel.UNP_START) AS unpStart,
        toInteger(rel.UNP_END) AS unpEnd, rel.AUTH_ASYM_ID AS authAsymId, rel.STRUCT_ASYM_ID AS structAsymId,
        toInteger(chainRel.AUTH_SEQ_ID) AS authSeqId, COALESCE(chainRel.PDB_INS_CODE, '') AS pdbInsCode ORDER BY toInteger(chainRel.AUTH_SEQ_ID)
    """

    mappings = run_query(query, {"pdbid":pdbid})

    api_result = {
        pdbid: {
            "UniProt": {}
        }
    }

    if(len(mappings) == 0):
        bottle.response.status = 404
        return api_result

    dict_residues = {}

    for mapping in mappings:
        (accession, unp_name, entity_id, pdb_start, pdb_end, unp_start, unp_end, auth_asym_id, struct_asym_id, auth_seq_id, pdb_ins_code) = mapping

        seq_key = (accession, struct_asym_id, pdb_start, pdb_end)
        if not dict_residues.get(seq_key):
            dict_residues[seq_key] = {
                "entity_id": entity_id,
                "chain_id": auth_asym_id,
                "struct_asym_id": struct_asym_id,
                "start": {
                    "author_residue_number": auth_seq_id,
                    "author_insertion_code": pdb_ins_code,
                    "residue_number": pdb_start
                },
                "unp_start": unp_start,
                "unp_end": unp_end
            }
        else:
            dict_residues[seq_key].update({
                "end": {
                    "author_residue_number": auth_seq_id,
                    "author_insertion_code": pdb_ins_code,
                    "residue_number": pdb_end
                }
            })

        if not api_result[pdbid]['UniProt'].get(accession):
            api_result[pdbid]['UniProt'][accession] = {
                "name": unp_name,
                "identifier": unp_name,
                "mappings": []
            }

    for key in dict_residues.keys():
        (accession, struct_asym_id, pdb_start, pdb_end) = key
        api_result[pdbid]['UniProt'][accession]["mappings"].append(dict_residues[key])

    # list_of_accessions = []
    # dict_unp_master = {}
    # dict_residues = {}
    # dict_chains = {}
    # dict_entity = {}
    # dict_residues_mappings = {}
    # dict_struct_asym_id = {}

    # for mapping in mappings:

    #     (entity_id, accession, name, pdb_res_id, unp_res_id,
    #      auth_asym_id, struct_asym_id, auth_seq_id, pdb_ins_code) = mapping

    #     if(accession not in list_of_accessions):
    #         list_of_accessions.append(accession)
    #         dict_unp_master[accession] = name

    #     if(dict_entity.get(accession) is None):
    #         dict_entity[accession] = set([entity_id])
    #     else:
    #         dict_entity[accession].add(entity_id)

    #     if(dict_chains.get((accession, entity_id)) is None):
    #         dict_chains[(accession, entity_id)] = set([auth_asym_id])
    #     else:
    #         dict_chains[(accession, entity_id)].add(auth_asym_id)

    #     if(dict_struct_asym_id.get((accession, entity_id, auth_asym_id)) is None):
    #         dict_struct_asym_id[(accession, entity_id, auth_asym_id)] = struct_asym_id

    #     if(dict_residues.get((accession, entity_id, auth_asym_id)) is None):
    #         dict_residues[(accession, entity_id, auth_asym_id)] = set([unp_res_id])
    #     else:
    #         dict_residues[(accession, entity_id, auth_asym_id)].add(unp_res_id)

    #     dict_residues_mappings[(entity_id, auth_asym_id, unp_res_id)] = (
    #         pdb_res_id, auth_seq_id, pdb_ins_code)

    # for accession in list_of_accessions:

    #     temp_mappings = []
    #     for entity_id in dict_entity[accession]:
    #         for auth_asym_id in dict_chains[(accession, entity_id)]:
    #             # using set to remove duplicates, since PDB residues will be duplicated in case of Chromophores which needs to be ignored
    #             for group in mit.consecutive_groups(dict_residues[(accession, entity_id, auth_asym_id)]):
    #                 group = list(group)

    #                 start_auth_seq_id = dict_residues_mappings[(
    #                     entity_id, auth_asym_id, group[0])][1]
    #                 end_auth_seq_id = dict_residues_mappings[(
    #                     entity_id, auth_asym_id, group[-1])][1]
    #                 start_pdb_ins_code = dict_residues_mappings[(
    #                     entity_id, auth_asym_id, group[0])][2]
    #                 end_pdb_ins_code = dict_residues_mappings[(
    #                     entity_id, auth_asym_id, group[-1])][2]

    #                 element = {
    #                     "entity_id": int(entity_id),
    #                     "chain_id": auth_asym_id,
    #                     "struct_asym_id": dict_struct_asym_id[(accession, entity_id, auth_asym_id)],
    #                     "start": {
    #                         "author_residue_number": None if start_auth_seq_id is None else int(start_auth_seq_id),
    #                         "author_insertion_code": "" if not start_pdb_ins_code else start_pdb_ins_code,
    #                         "residue_number": int(dict_residues_mappings[(entity_id, auth_asym_id, group[0])][0])
    #                     },
    #                     "end": {
    #                         "author_residue_number": None if end_auth_seq_id is None else int(end_auth_seq_id),
    #                         "author_insertion_code": "" if not end_pdb_ins_code else end_pdb_ins_code,
    #                         "residue_number": int(dict_residues_mappings[(entity_id, auth_asym_id, group[-1])][0])
    #                     },
    #                     "unp_start": int(group[0]),
    #                     "unp_end": int(group[-1])
    #                     # "pdb_start": int(dict_residues_mappings[(entity_id, auth_asym_id, group[0])][0]),
    #                     # "pdb_end": int(dict_residues_mappings[(entity_id, auth_asym_id, group[-1])][0])
    #                 }
    #                 temp_mappings.append(element)

    #     api_result[pdbid]["UniProt"][accession] = {
    #         "name": dict_unp_master[accession],
    #         "identifier": dict_unp_master[accession],
    #         "mappings": sorted(temp_mappings, key = lambda x: (x["chain_id"], x["start"]["residue_number"], x["end"]["residue_number"]))
    #     }
        
    #     del temp_mappings

    bottle.response.status = 200
    return api_result


"""
@api {get} mappings/pfam/:pdbId Get Pfam mappings for a PDB Entry ID
@apiName GetPDBPfamMapping
@apiGroup SIFTS
@apiDescription Get mappings (as assigned by the SIFTS process) from PDB structures to Pfam.
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID

@apiUse GenericResidueTerms

@apiExample {json=./examples/success/pfam_mapping.json} apiSuccessExample Example success response JSON
"""
@app.get('/pfam/'+rePDBid+reSlashOrNot)
def get_pfam_api(pdbid):

    pdbid = pdbid.strip("'")

    result, status = get_pfam(pdbid)

    api_result = {
        pdbid: {
            "Pfam": {}
        }
    }

    if status == 404:
        bottle.response.status = 404
        return api_result

    api_result[pdbid]["Pfam"] = result

    bottle.response.status = 200
    return api_result


def get_pfam(pdbid):
    """
    Get mappings (as assigned by the SIFTS process) from PDB structures to Pfam.
    """

    query = """
    MATCH(entry:Entry {ID:$pdbid})-[:HAS_ENTITY]->(entity:Entity)-[pfamRel:HAS_PFAM]->(pfam:Pfam)
    MATCH (entity)-[:HAS_PDB_RESIDUE]->(pdbRes:PDBResidue)
    MATCH (pdbRes)-[chainRel:IS_IN_CHAIN]->(chain:Chain)
        WHERE pdbRes.ID IN [pfamRel.START, pfamRel.END]
    WITH
        pfam.PFAM_ACCESSION AS pfamAccession, pfam.DESCRIPTION AS pfamDesc, toInteger(entity.ID) AS entityId,
        chain.AUTH_ASYM_ID AS authAsymId, chain.STRUCT_ASYM_ID AS structAsymId, pfamRel.COVERAGE AS coverage,
        toInteger(pdbRes.ID) AS pdbResId, toInteger(chainRel.AUTH_SEQ_ID) AS authSeqId, chainRel.PDB_INS_CODE AS pdbInsCode
    ORDER BY authAsymId, pdbResId
    RETURN pfamAccession, pfamDesc, entityId, authAsymId, structAsymId, coverage, pdbResId, authSeqId, pdbInsCode
    """
    mappings = run_query(query, {"pdbid":pdbid})

    if not mappings:
        return {}, 404

    api_result = {}
    dict_segment_flag = {}

    for row in mappings:
        (pfam_accession, pfam_desc, entity_id, auth_asym_id, struct_asym_id, coverage, pdb_res_id, auth_seq_id, pdb_ins_code) = row

        if not api_result.get(pfam_accession):
            api_result[pfam_accession] = {
                "identifier": pfam_desc,
                "description": pfam_desc,
                "mappings": []
            }
        if not dict_segment_flag.get(pfam_accession):
            dict_segment_flag[pfam_accession] = {
                "entity_id": entity_id,
                "chain_id": auth_asym_id,
                "struct_asym_id": struct_asym_id,
                "start": {
                    "author_residue_number": auth_seq_id,
                    "author_insertion_code": "" if not pdb_ins_code else pdb_ins_code,
                    "residue_number": pdb_res_id
                },
                "coverage": None if coverage is None else float("%.3f" % float(coverage))
            }
        else:
            dict_segment_flag[pfam_accession].update({
                "end": {
                    "author_residue_number": auth_seq_id,
                    "author_insertion_code": "" if not pdb_ins_code else pdb_ins_code,
                    "residue_number": pdb_res_id
                }
            })
            api_result[pfam_accession]["mappings"].append(dict_segment_flag[pfam_accession])
            del dict_segment_flag[pfam_accession]

    return api_result, 200


"""
@api {get} mappings/interpro/:pdbId Get Interpro mappings for a PDB Entry ID
@apiName GetPDBInterproMapping
@apiGroup SIFTS
@apiDescription Get mappings (as assigned by the SIFTS process) from PDB structures to InterPro.
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID

@apiUse GenericResidueTerms

@apiExample {json=./examples/success/interpro_mapping.json} apiSuccessExample Example success response JSON
"""
@app.get('/interpro/'+rePDBid+reSlashOrNot)
def get_interpro_api(pdbid):

    pdbid = pdbid.strip("'")

    api_result = {
        pdbid: {
            "InterPro": {}
        }
    }

    result, status = get_interpro(pdbid)

    if status == 404:
        bottle.response.status = 404
        return api_result

    api_result[pdbid]["InterPro"] = result

    bottle.response.status = 200
    return api_result

def get_interpro(pdbid):
    """
    Get mappings (as assigned by the SIFTS process) from PDB structures to InterPro.
    """

    query = """
    MATCH (entry:Entry {ID:$pdbid})-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[rel:IS_IN_INTERPRO]->(interpro:Interpro),
    (pdb_res)-[chain_rel:IS_IN_CHAIN]->(chain:Chain) WHERE chain.AUTH_ASYM_ID = rel.AUTH_ASYM_ID
    RETURN toInteger(entity.ID), toInteger(pdb_res.ID), toInteger(chain_rel.AUTH_SEQ_ID), chain.AUTH_ASYM_ID, chain.STRUCT_ASYM_ID, rel.GROUP_ID, 
    interpro.INTERPRO_ACCESSION, interpro.NAME, chain_rel.PDB_INS_CODE ORDER BY chain.AUTH_ASYM_ID, toInteger(pdb_res.ID)
    """

    mappings = run_query(query, {"pdbid":pdbid})

    api_result = {}
    dict_residues = {}

    if(len(mappings) == 0):
        return api_result, 404

    set_accessions = set()

    for mapping in mappings:
        (entity_id, res_id, auth_seq_id, auth_asym_id, struct_asym_id, group_id, interpro_accession, interpro_name, pdb_ins_code) = mapping

        set_accessions.add(interpro_accession)

        key = (interpro_accession, entity_id, auth_asym_id, struct_asym_id, group_id)

        if api_result.get(interpro_accession) is None:
            api_result[interpro_accession] = {
                "identifier": interpro_name,
                "name": interpro_name,
                "mappings": []
            }
        
        if dict_residues.get(key) is None:
            dict_residues[key] = [(res_id, auth_seq_id, pdb_ins_code)]
        else:
            dict_residues[key].append((res_id, auth_seq_id, pdb_ins_code))

    for key in dict_residues.keys():
        (interpro_accession, entity_id, auth_asym_id, struct_asym_id, group_id) = key

        start_auth_ins_code = dict_residues[key][0][2]
        end_auth_ins_code = dict_residues[key][-1][2]

        api_result[interpro_accession]["mappings"].append({
            "entity_id": entity_id,
            "end": {
                "author_residue_number": dict_residues[key][-1][1],
                "author_insertion_code": "" if end_auth_ins_code is None else end_auth_ins_code,
                "residue_number": dict_residues[key][-1][0]
            },
            "start": {
                "author_residue_number": dict_residues[key][0][1],
                "author_insertion_code": "" if start_auth_ins_code is None else start_auth_ins_code,
                "residue_number": dict_residues[key][0][0]
            },
            "chain_id": auth_asym_id,
            "struct_asym_id": struct_asym_id
        })

    # sort the list by chain_id, then by start residue number and then by end residue number
    for interpro_accession in set_accessions:
        api_result[interpro_accession]["mappings"] = sorted(api_result[interpro_accession]["mappings"], key = lambda x: (x['chain_id'], x['start']['residue_number'], x['end']['residue_number']))

    return api_result, 200
    
"""
@api {get} mappings/cath/:pdbId Get CATH mappings for a PDB Entry ID
@apiName GetPDBCATHMapping
@apiGroup SIFTS
@apiDescription Get mappings (as assigned by the SIFTS process) from PDB structures to CATH.
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID

@apiUse GenericResidueTerms
@apiUse CATHTerms

@apiExample {json=./examples/success/cath_mapping.json} apiSuccessExample Example success response JSON
"""
@app.get('/cath/'+rePDBid+reSlashOrNot)
def get_cath_api(pdbid):

    pdbid = pdbid.strip("'")

    api_result = {
        pdbid: {
            "CATH": {}
        }
    }

    result, status = get_cath(pdbid)

    if status == 404:
        bottle.response.status = 404
        return api_result

    api_result[pdbid]["CATH"] = result

    bottle.response.status = 200
    return api_result


def get_cath(pdbid):
    """
    Get mappings (as assigned by the SIFTS process) from PDB structures to CATH.
    """

    query = """
    MATCH(entry:Entry {ID:$pdbid})-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[rel:IS_IN_CATH_DOMAIN]->(cath:CATH),
    (pdb_res)-[chain_rel:IS_IN_CHAIN]->(chain:Chain)
    RETURN DISTINCT entity.ID, toInteger(pdb_res.ID), cath.CATHCODE, cath.DOMAIN, cath.HOMOL, cath.CLASS, cath.ARCH, cath.TOPOL, cath.NAME, rel.AUTH_ASYM_ID, 
    rel.STRUCT_ASYM_ID, rel.SEGMENT, chain_rel.PDB_INS_CODE, chain_rel.AUTH_SEQ_ID ORDER by rel.AUTH_ASYM_ID, toInteger(pdb_res.ID), cath.DOMAIN
    """

    mappings = run_query(query, {"pdbid":pdbid})

    api_result = {}
    dict_residues = {}

    if(len(mappings) == 0):
        return api_result, 404

    for mapping in mappings:
        (entity_id, res_id, cathcode, cath_domain, cath_homology, cath_class, cath_arch, cath_topology, cath_name, auth_asym_id, 
        struct_asym_id, segment, auth_ins_code, auth_seq_id) = mapping

        if api_result.get(cathcode) is None:
            api_result[cathcode] = {
                "homology": cath_homology,
                "name": cath_name,
                "class": cath_class,
                "architecture": cath_arch,
                "identifier": cath_topology,
                "topology": cath_topology,
                "mappings": []
            }

        key = (entity_id, cathcode, cath_domain, auth_asym_id, struct_asym_id, segment)

        if dict_residues.get(key) is None:
            dict_residues[key] = [(res_id, auth_seq_id, auth_ins_code)]
        else:
            dict_residues[key].append((res_id, auth_seq_id, auth_ins_code))

    
    for key in dict_residues.keys():
        (entity_id, cathcode, cath_domain, auth_asym_id, struct_asym_id, segment) = key

        api_result[cathcode]["mappings"].append({
            "domain": cath_domain,
            "end": {
                "author_residue_number": None if dict_residues[key][-1][1] is None else int(dict_residues[key][-1][1]),
                "author_insertion_code": "" if dict_residues[key][-1][2] is None else dict_residues[key][-1][2],
                "residue_number": int(dict_residues[key][-1][0])
            },
            "segment_id": int(segment),
            "entity_id": int(entity_id),
            "chain_id": auth_asym_id,
            "start": {
                "author_residue_number": None if dict_residues[key][0][1] is None else int(dict_residues[key][0][1]),
                "author_insertion_code": "" if dict_residues[key][0][2] is None else dict_residues[key][0][2],
                "residue_number": int(dict_residues[key][0][0])
            },
            "struct_asym_id": struct_asym_id
        })

    return api_result, 200

"""
@api {get} mappings/scop/:pdbId Get SCOP mappings for a PDB Entry ID
@apiName GetPDBSCOPMapping
@apiGroup SIFTS
@apiDescription Get mappings (as assigned by the SIFTS process) from PDB structures to SCOP.
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID

@apiUse GenericResidueTerms
@apiUse SCOPTerms

@apiExample {json=./examples/success/scop_mapping.json} apiSuccessExample Example success response JSON
"""
@app.route('/scop/'+rePDBid+reSlashOrNot)
def get_scop_api(pdbid):

    pdbid = pdbid.strip("'")

    api_result = {
        pdbid: {
            "SCOP": {}
        }
    }

    result, status = get_scop(pdbid)

    if status == 404:
        bottle.response.status = 404
        return api_result

    api_result[pdbid]["SCOP"] = result

    bottle.response.status = 200
    return api_result


def get_scop(pdbid):
    """
    Get mappings (as assigned by the SIFTS process) from PDB structures to SCOP.
    """

    query = """
    MATCH (entry:Entry {ID:$pdbid})-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[relation:IS_IN_SCOP_DOMAIN]->(scop:SCOP)
    OPTIONAL MATCH (superfamily:SCOP {SUNID: relation.SUPERFAMILY_ID})
    OPTIONAL MATCH (fold:SCOP {SUNID: relation.FOLD_ID})
    OPTIONAL MATCH (class:SCOP {SUNID: relation.CLASS_ID})
    RETURN scop.SUNID as sunid, scop.DESCRIPTION as desc, superfamily.SUNID as super_sunid, superfamily.DESCRIPTION as super_desc, 
    fold.SUNID as fold_sunid, fold.DESCRIPTION as fold_desc, class.SUNID as class_sunid, class.DESCRIPTION as class_desc, scop.SCCS as sccs, 
    relation.SCOP_ID as scop_id, pdb_res.ID, entity.ID, relation.AUTH_ASYM_ID, relation.STRUCT_ASYM_ID, relation.SEGMENT_ID, relation.AUTH_START, 
    relation.AUTH_END ORDER BY relation.AUTH_ASYM_ID, toInteger(pdb_res.ID)
    """

    api_result = {}

    list_sunid = []
    dict_scop = {}
    mappings = run_query(query, {"pdbid":pdbid})

    if(len(mappings) == 0):
        return api_result, 404

    dict_scop_auth = {}
    dict_scop_sunid = {}

    for mapping in mappings:
        (sunid, desc, super_sunid, super_desc, fold_sunid, fold_desc, class_sunid, class_desc, sccs, scop_id, res_id, entity_id, auth_asym_id, struct_asym_id,
         segment_id, auth_start, auth_end) = mapping

        if(sunid not in list_sunid):

            api_result[sunid] = {
                "superfamily": {
                    "sunid": int(super_sunid),
                    "description": super_desc
                },
                "sccs": sccs,
                "fold": {
                    "sunid": int(fold_sunid),
                    "description": fold_desc
                },
                "identifier": desc,
                "class": {
                    "sunid": int(class_sunid),
                    "description": class_desc
                },
                "description": desc,
                "mappings": []
            }

            list_sunid.append(sunid)

        key = (entity_id, auth_asym_id, struct_asym_id, scop_id, segment_id)

        if(dict_scop.get(key) is None):
            dict_scop[key] = [res_id]
        else:
            dict_scop[key].append(res_id)

        if(dict_scop_sunid.get(scop_id) is None):
            dict_scop_sunid[scop_id] = sunid

        # keep auth_start and auth_end for a domain, this is not to be stored in main dictionary
        if(dict_scop_auth.get(scop_id) is None):
            dict_scop_auth[scop_id] = (auth_start, auth_end)

    for key in dict_scop.keys():
        (entity_id, auth_asym_id, struct_asym_id, scop_id, segment_id) = key

        (auth_start, auth_end) = dict_scop_auth[scop_id]

        temp_segment = {
            "entity_id": int(entity_id),
            "end": {
                "author_residue_number": None if auth_end is None else int(auth_end),
                "author_insertion_code": "",
                "residue_number": int(dict_scop[key][-1])
            },
            "segment_id": int(segment_id),
            "chain_id": auth_asym_id,
            "scop_id": scop_id,
            "start": {
                "author_residue_number": None if auth_start is None else int(auth_start),
                "author_insertion_code": "",
                "residue_number": int(dict_scop[key][0])
            },
            "struct_asym_id": struct_asym_id
        }

        api_result[dict_scop_sunid[scop_id]]["mappings"].append(temp_segment)

    return api_result, 200

"""
@api {get} mappings/go/:pdbId Get GO mappings for a PDB Entry ID
@apiName GetPDBGOMapping
@apiGroup SIFTS
@apiDescription Get mappings (as assigned by the SIFTS process) from PDB structures to GO.
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID

@apiUse GenericResidueTerms
@apiSuccess {String} category The GO category.

@apiExample {json=./examples/success/go_mapping.json} apiSuccessExample Example success response JSON
"""
@app.get('/go/'+rePDBid+reSlashOrNot)
def get_go_api(pdbid):

    pdbid = pdbid.strip("'")

    api_result = {
        pdbid: {
            "GO": {}
        }
    }

    result, status = get_go(pdbid)

    if status == 404:
        bottle.response.status = 404
        return api_result

    api_result[pdbid]["GO"] = result

    bottle.response.status = 200
    return api_result


def get_go(pdbid):
    """
    Get mappings (as assigned by the SIFTS process) from PDB structures to GO.
    """

    bio_go_query = """
    MATCH (entry:Entry {ID:$pdbid})-[:HAS_ENTITY]->(entity:Entity {POLYMER_TYPE:'P'})-[:CONTAINS_CHAIN]->(chain:Chain)
    MATCH (entity)-[:HAS_GO]->(bio_go:GOBiologicalProcess)
    RETURN entity.ID, chain.AUTH_ASYM_ID, chain.STRUCT_ASYM_ID, bio_go.GO_ID, bio_go.DEFINITION, bio_go.NAME
    ORDER BY chain.AUTH_ASYM_ID, entity.ID
    """

    bio_go_mappings = run_query(bio_go_query, {"pdbid":pdbid})

    cell_go_query = """
    MATCH (entry:Entry {ID:$pdbid})-[:HAS_ENTITY]->(entity:Entity {POLYMER_TYPE:'P'})-[:CONTAINS_CHAIN]->(chain:Chain)
    MATCH (entity)-[:HAS_GO]->(cell_go:GOCellularComponent)
    RETURN entity.ID, chain.AUTH_ASYM_ID, chain.STRUCT_ASYM_ID, cell_go.GO_ID, cell_go.DEFINITION, cell_go.NAME
    ORDER BY chain.AUTH_ASYM_ID, entity.ID
    """

    cell_go_mappings = run_query(cell_go_query, {"pdbid":pdbid})

    mol_go_query = """
    MATCH (entry:Entry {ID:$pdbid})-[:HAS_ENTITY]->(entity:Entity {POLYMER_TYPE:'P'})-[:CONTAINS_CHAIN]->(chain:Chain)
    MATCH (entity)-[:HAS_GO]->(mol_go:GOMolecularFunction)
    RETURN entity.ID, chain.AUTH_ASYM_ID, chain.STRUCT_ASYM_ID, mol_go.GO_ID, mol_go.DEFINITION, mol_go.NAME
    ORDER BY chain.AUTH_ASYM_ID, entity.ID
    """

    mol_go_mappings = run_query(mol_go_query, {"pdbid":pdbid})

    go_dict = {}

    api_result = {}

    if(len(bio_go_mappings) == 0 and len(cell_go_mappings) == 0 and len(mol_go_mappings) == 0):
        return api_result, 404

    for mapping in bio_go_mappings:
        (entity_id, auth_asym_id, struct_asym_id, go_id, go_def, go_name) = mapping

        if(go_dict.get(go_id) is None):
            go_dict[go_id] = {
                "category": "Biological_process",
                "definition": go_def,
                "identifier": go_name,
                "name": go_name,
                "mappings": [{
                    "entity_id": int(entity_id),
                    "chain_id": auth_asym_id,
                    "struct_asym_id": struct_asym_id
                }]
            }
        else:
            go_dict[go_id]["mappings"].append({
                "entity_id": int(entity_id),
                "chain_id": auth_asym_id,
                "struct_asym_id": struct_asym_id
            })

    for mapping in cell_go_mappings:
        (entity_id, auth_asym_id, struct_asym_id, go_id, go_def, go_name) = mapping

        if(go_dict.get(go_id) is None):
            go_dict[go_id] = {
                "category": "Cellular_component",
                "definition": go_def,
                "identifier": go_name,
                "name": go_name,
                "mappings": [{
                    "entity_id": int(entity_id),
                    "chain_id": auth_asym_id,
                    "struct_asym_id": struct_asym_id
                }]
            }
        else:
            go_dict[go_id]["mappings"].append({
                "entity_id": int(entity_id),
                "chain_id": auth_asym_id,
                "struct_asym_id": struct_asym_id
            })

    for mapping in mol_go_mappings:
        (entity_id, auth_asym_id, struct_asym_id, go_id, go_def, go_name) = mapping

        if(go_dict.get(go_id) is None):
            go_dict[go_id] = {
                "category": "Molecular_function",
                "definition": go_def,
                "identifier": go_name,
                "name": go_name,
                "mappings": [{
                    "entity_id": int(entity_id),
                    "chain_id": auth_asym_id,
                    "struct_asym_id": struct_asym_id
                }]
            }
        else:
            go_dict[go_id]["mappings"].append({
                "entity_id": int(entity_id),
                "chain_id": auth_asym_id,
                "struct_asym_id": struct_asym_id
            })

    for go_id in go_dict.keys():
        api_result[go_id] = go_dict[go_id]

    return api_result, 200

"""
@api {get} mappings/ec/:pdbId Get EC mappings for a PDB Entry ID
@apiName GetPDBECMapping
@apiGroup SIFTS
@apiDescription Get mappings (as assigned by the SIFTS process) from PDB structures to EC.
@apiVersion 2.0.0

@apiParam {String} pdbId=3d12 PDB Entry ID

@apiUse GenericResidueTerms

@apiExample {json=./examples/success/ec_mapping.json} apiSuccessExample Example success response JSON
"""
@app.get('/ec/'+rePDBid+reSlashOrNot)
def get_ec_api(pdbid):

    pdbid = pdbid.strip("'")

    api_result = {
        pdbid: {
            "EC": {}
        }
    }

    result, status = get_ec(pdbid)

    if status == 404:
        bottle.response.status = 404
        return api_result

    api_result[pdbid]["EC"] = result

    bottle.response.status = 200
    return api_result


def get_ec(pdbid):
    """
    Get mappings (as assigned by the SIFTS process) from PDB structures to EC.
    """

    query = """
    MATCH (entry:Entry {ID:$pdbid})-[:HAS_ENTITY]->(entity:Entity)-[:CONTAINS_CHAIN]->(chain:Chain), (entity)-[:HAS_EC]->(ec:EC)
    RETURN entity.ID, chain.AUTH_ASYM_ID, chain.STRUCT_ASYM_ID, ec.EC_NUM, ec.ACCEPTED_NAME, ec.SYSTEMATIC_NAME, ec.REACTION, ec.SYNONYMS
    ORDER BY chain.AUTH_ASYM_ID, entity.ID
    """

    regex = re.compile('\'|^\s')

    dict_ec = {}
    mappings = run_query(query, {"pdbid":pdbid})

    api_result = {}

    if(len(mappings) == 0):
        return api_result, 404

    for mapping in mappings:

        (entity_id, auth_asym_id, struct_asym_id, ec_num,
         accepted_name, systematic_name, reaction, synonyms) = mapping

        # synonyms returns list as string, so converting them as list
        synonyms = synonyms.translate({ord(c): '' for c in "[]"}).split('\',')
        synonyms = [regex.sub('', synonym) for synonym in synonyms]
        synonyms = sorted(list(set(synonyms)))
        
        # remove EC from ec_num
        ec_num = ec_num.replace("EC ", "")

        if(dict_ec.get(ec_num) is None):
            dict_ec[ec_num] = {
                "reaction": reaction,
                "mappings": [{
                    "entity_id": int(entity_id),
                    "chain_id": auth_asym_id,
                    "struct_asym_id": struct_asym_id
                }],
                "systematic_name": systematic_name,
                "synonyms": synonyms,
                "identifier": accepted_name,
                "accepted_name": accepted_name
            }
        else:
            dict_ec[ec_num]["mappings"].append({
                "entity_id": int(entity_id),
                "chain_id": auth_asym_id,
                "struct_asym_id": struct_asym_id
            })

    for key in dict_ec.keys():
        api_result[key] = dict_ec[key]

    return api_result, 200


"""
@api {get} mappings/best_structures/:accession Get Best Structures for a UniProt accession
@apiName GetBestStructures
@apiGroup SIFTS
@apiDescription Get the list of PDB structures mapping to a UniProt accession sorted by coverage of the protein and, if the same, resolution.
@apiVersion 2.0.0

@apiParam {String} accession=P07550 UniProt accession

@apiSuccess {Integer} end mmcif-style end residue.
@apiSuccess {String} chain_id PDB chain id.
@apiSuccess {String} pdb_id The PDB id.
@apiSuccess {Integer} start mmcif-style start residue.
@apiSuccess {Integer} unp_end Index of last residue in UniProt sequence mapped to this PDB entity.
@apiSuccess {Integer} unp_start Index of first residue in UniProt sequence mapped to this PDB entity.
@apiSuccess {Float} coverage The ratio of coverage of the UniProt protein [0-1].
@apiSuccess {Float} resolution The higher limit of resolution of crystallographic data as reported by depositors. Null if not available.
@apiSuccess {String} experimental_method The experimental method.
@apiSuccess {Integer} tax_id The taxonomy id.

@apiExample {json=./examples/success/best_structures.json} apiSuccessExample Example success response JSON
"""
@app.get('/best_structures/<accession>'+reSlashOrNot)
def get_best_structures_api(accession):

    accession = accession.strip("'")

    api_result = {
        accession: []
    }

    result, resp_status = get_best_structures(accession)

    api_result[accession] = result
    bottle.response.status = resp_status

    return api_result


def get_best_structures(accession):

    old_query = """
    MATCH(uniprot:UniProt {ACCESSION:$accession})<-[en_unp_rel:HAS_UNIPROT]->(entity:Entity)<-[:HAS_ENTITY]-(entry:Entry)-[:EXPERIMENT]->(method:Method), (entity)-[:HAS_TAXONOMY]->(tax:Taxonomy), (entity)-[en_unp_segment_rel:HAS_UNIPROT_SEGMENT]->(uniprot)
    WITH entry.ID AS entry_id, entity.ID AS entity_id, en_unp_segment_rel.STRUCT_ASYM_ID AS chain_id, round(1000 * toFloat(en_unp_rel.COVERAGE))/1000 AS coverage, method.METHOD AS method, tax.TAX_ID AS tax_id, entry.RESOLUTION AS resolution, en_unp_segment_rel.PDB_START AS pdb_start, en_unp_segment_rel.PDB_END AS pdb_end, en_unp_segment_rel.UNP_START AS unp_start, en_unp_segment_rel.UNP_END AS unp_end ORDER BY toInteger(pdb_start)
    RETURN entry_id, entity_id, chain_id, coverage, method, tax_id, resolution, COLLECT(pdb_start)[0] AS pdb_start, COLLECT(pdb_end)[-1] AS pdb_end, COLLECT(unp_start)[0] AS unp_start, COLLECT(unp_end)[-1] AS unp_end ORDER BY coverage DESC, toFloat(resolution)
    """

    # JIRA PDBE-2286, added preferred_assembly_id to query
    query = """
    MATCH (unp:UniProt {ACCESSION:$accession})<-[unp_rel:HAS_UNIPROT_SEGMENT]-(entity:Entity)<-[:HAS_ENTITY]-(entry:Entry), (entry)-[:EXPERIMENT]->(method:Method), (unp)-[:HAS_TAXONOMY]->(tax:Taxonomy), (entity)-[rel:HAS_UNIPROT]->(unp), (entity)-[:IS_PART_OF_ASSEMBLY]->(assembly:Assembly {PREFERED:'True'})
    WITH entry.ID AS entry_id, entity.ID AS entity_id, unp_rel.STRUCT_ASYM_ID AS chain_id, round(1000 * toFloat(rel.COVERAGE))/1000 AS coverage, method.METHOD AS method, tax.TAX_ID AS tax_id, entry.RESOLUTION AS resolution, unp_rel.PDB_START AS pdb_start, unp_rel.PDB_END AS pdb_end, unp_rel.UNP_START AS unp_start, unp_rel.UNP_END AS unp_end, assembly.ID AS preferred_assembly_id
    RETURN entry_id, entity_id, chain_id, coverage, method, tax_id, resolution, pdb_start, pdb_end, unp_start, unp_end, preferred_assembly_id ORDER BY coverage DESC, toFloat(resolution)
    """

    mappings = run_query(query, {"accession":accession})

    result = []

    if not mappings:
        return [], 404

    for mapping in mappings:
        (entry_id, entity_id, chain_id, coverage, experiment, taxonomy_id, resolution, pdb_start, pdb_end, unp_start, unp_end, preferred_assembly_id) = mapping
        result.append({
            "end": int(pdb_end),
            "entity_id": int(entity_id),
            "chain_id": chain_id,
            "pdb_id": entry_id,
            "start": int(pdb_start),
            "unp_end": int(unp_end),
            "coverage": coverage,
            "unp_start": int(unp_start),
            "resolution": None if resolution is None else float("%.2f" % float(resolution)),
            "experimental_method": experiment,
            "tax_id": int(taxonomy_id),
            "preferred_assembly_id": int(preferred_assembly_id)
        })

    return result, 200

"""
@api {get} mappings/homologene/:pdbId/:entityId Get homologene for a PDB entity
@apiName GetHomologene
@apiGroup SIFTS
@apiDescription Get homologene polypeptides for a given PDB entity
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID
@apiParam {String} entityId=1 PDB Entity ID

@apiSuccess {String} name Name of the polypeptide.
@apiSuccess {String} organism_scientific_name Scientific name of the organism.
@apiSuccess {String} homologus_pdb_id Homologus PDB id code.
@apiSuccess {String} homologus_pdb_entity_id Homologus PDB entity id for the PDB id.

@apiExample {json=./examples/success/homologene.json} apiSuccessExample Example success response JSON
"""
@app.get('/homologene/' +rePDBid +reSlashOrNot +'<entity_id>' +reSlashOrNot)
def get_homologene_api(pdbid, entity_id):

    query = """
    MATCH (src_entry:Entry {ID:$pdbid})-[:HAS_ENTITY]->(src_entity:Entity {ID:$entity_id})-[:HAS_HOMOLOGOUS_UNIPROT]->(uniprot:UniProt)-[:IS_HOMOLOGOUS]->(homologene:Homologene)<-[:IS_HOMOLOGOUS]-(uniprot2:UniProt)
    WITH homologene as homologene, [uniprot.ACCESSION, uniprot2.ACCESSION] as accessions
    MATCH (entity)-[entity_unp_rel:HAS_UNIPROT]->(uniprot)
    MATCH (homologene)<-[:IS_HOMOLOGOUS]-(uniprot:UniProt)<-[:HAS_HOMOLOGOUS_UNIPROT]-(entity:Entity)<-[:HAS_ENTITY]-(entry:Entry), (entity)-[:HAS_TAXONOMY]->(taxonomy:Taxonomy) WHERE uniprot.ACCESSION IN accessions
    RETURN DISTINCT homologene.HOMOLOGENE_ID, uniprot.ACCESSION, entity.ID, entity_unp_rel.BEST_NAME, entry.ID, taxonomy.NCBI_SCIENTIFIC
    ORDER BY entry.ID, toInteger(entity.ID)
    """

    pdbid_in = pdbid.strip("'")
    entity_id_in = entity_id.strip("'")

    mappings = run_query(query, {"pdbid":pdbid_in, "entity_id":entity_id_in})

    if(len(mappings) == 0):
        bottle.response.status = 404
        return {}

    api_result = {
        "{}_{}".format(pdbid_in, entity_id_in): {
            "Homologene": {}
        }
    }

    dict_homologene = {}

    for mapping in mappings:
        (homologene_id, accession, entity_id, entity_best_name, entry_id, organism_name) = mapping

        if dict_homologene.get(homologene_id) is None:
            dict_homologene[homologene_id] = {
                "identifier": int(homologene_id),
                "mappings": []
            }

        dict_homologene[homologene_id]["mappings"].append({
            "homologus_pdb_id": entry_id,
            "homologus_pdb_entity_id": int(entity_id),
            "name": entity_best_name,
            "organism_scientific_name": organism_name,
            "accession": accession
        })

    for homologene_id in dict_homologene.keys():

        api_result["{}_{}".format(
            pdbid_in, entity_id_in)]["Homologene"][homologene_id] = dict_homologene.get(homologene_id)

    bottle.response.status = 200
    return api_result

"""
@api {get} mappings/ensembl/:pdbId Get ensembl mappings for a PDB entry ID
@apiName GetEnsembl
@apiGroup SIFTS
@apiDescription Get mappings (as assigned by the SIFTS process) from PDB structures to Ensembl
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID

@apiUse GenericResidueTerms
@apiUse EnsemblTerms
@apiSuccess {Integer} unp_start Index of first residue in Uniprot sequence mapped to this PDB entity.
@apiSuccess {Integer} unp_end Index of last residue in Uniprot sequence mapped to this PDB entity.

@apiExample {json=./examples/success/ensembl.json} apiSuccessExample Example success response JSON
"""
@app.get('/ensembl/'+rePDBid+reSlashOrNot)
def get_ensembl_api(pdbid):

    pdbid = pdbid.strip("'")

    api_result = {
        pdbid: {
            "Ensembl": {}
        }
    }

    result, status = get_ensembl(pdbid)

    if status == 404:
        bottle.response.status = 404
        return api_result

    api_result[pdbid]["Ensembl"] = result

    bottle.response.status = 200
    return api_result


def get_ensembl(pdbid):
    """
    Get mappings (as assigned by the SIFTS process) from PDB structures to Ensembl
    """

    query = """
    MATCH (entry:Entry {ID:$pdbid})-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[ensembl_rel:HAS_ENSEMBL]->(ensembl:EnsemblGene),
    (pdb_res)-[:MAP_TO_UNIPROT_RESIDUE]->(unp_res:UNPResidue)<-[:HAS_UNP_RESIDUE]-(unp:UniProt),
    (pdb_res)-[chain_rel:IS_IN_CHAIN]->(chain:Chain)
    RETURN ensembl.GENE_ID, toInteger(entity.ID), chain.AUTH_ASYM_ID, chain.STRUCT_ASYM_ID, toInteger(ensembl_rel.ORDINAL), ensembl_rel.COVERAGE, ensembl_rel.EXON_ID, 
    ensembl_rel.TRANSCRIPT_ID, ensembl_rel.TRANSLATION_ID, toInteger(ensembl_rel.GENOME_START), toInteger(ensembl_rel.GENOME_END), unp.ACCESSION, toInteger(pdb_res.ID) 
    AS pdb_res_id, toInteger(unp_res.ID) AS unp_res_id, toInteger(chain_rel.AUTH_SEQ_ID) AS auth_seq_id, chain_rel.PDB_INS_CODE as auth_ins_code 
    ORDER BY chain.AUTH_ASYM_ID, toInteger(entity.ID), toInteger(pdb_res.ID)
    """

    mappings = run_query(query, {"pdbid":pdbid})

    if(len(mappings) == 0):
        return {}, 404

    api_result = {}
    dict_residues = {}

    for mapping in mappings:
        (gene_id, entity_id, auth_asym_id, struct_asym_id, ordinal, coverage, exon_id, transcript_id, translation_id, genome_start, genome_end, accession, pdb_res_id,
        unp_res_id, auth_seq_id, auth_ins_code) = mapping

        if api_result.get(gene_id) is None:
            api_result[gene_id] = {
                "identifier": gene_id,
                "mappings": []
            }

        key = (gene_id, entity_id, auth_asym_id, struct_asym_id, ordinal, exon_id, transcript_id, translation_id, genome_start, genome_end, accession, coverage)

        if dict_residues.get(key) is None:
            dict_residues[key] = [(pdb_res_id, unp_res_id, auth_seq_id, auth_ins_code)]
        else:
            dict_residues[key].append((pdb_res_id, unp_res_id, auth_seq_id, auth_ins_code))

    for key in dict_residues.keys():
        (gene_id, entity_id, auth_asym_id, struct_asym_id, ordinal, exon_id, transcript_id, translation_id, genome_start, genome_end, accession, coverage) = key

        api_result[gene_id]["mappings"].append({
            "entity_id": int(entity_id),
            "accession": accession,
            "translation_id": translation_id,
            "end": {
                "author_residue_number": dict_residues[key][-1][2],
                "author_insertion_code": "" if dict_residues[key][-1][3] is None else dict_residues[key][-1][3],
                "residue_number": dict_residues[key][-1][0]
            },
            "chain_id": auth_asym_id,
            "start": {
                "author_residue_number": dict_residues[key][0][2],
                "author_insertion_code": "" if dict_residues[key][0][3] is None else dict_residues[key][0][3],
                "residue_number": dict_residues[key][0][0]
            },
            "ordinal": ordinal,
            "genome_end": genome_end,
            "unp_end": dict_residues[key][-1][1],
            "transcript_id": transcript_id,
            "coverage": float("%.3f" % float(coverage)),
            "exon_id": exon_id,
            "genome_start": genome_start,
            "unp_start": dict_residues[key][0][1],
            "struct_asym_id": struct_asym_id
        })


    return api_result, 200


"""
@api {get} mappings/uniprot_to_pfam/:accession Get Pfam mappings for a UniProt accession
@apiName GetUNPtoPfam
@apiGroup SIFTS
@apiDescription Get mappings (as assigned by the SIFTS process) from a UniProt accession to a Pfam accession with details of the Pfam protein family
@apiVersion 2.0.0

@apiParam {String} accession=P07550 UniProt accession

@apiSuccess {Integer} unp_start Index of first residue in Uniprot sequence mapped to this PDB entity.
@apiSuccess {Integer} unp_end Index of last residue in Uniprot sequence mapped to this PDB entity.

@apiExample {json=./examples/success/uniprot_to_pfam.json} apiSuccessExample Example success response JSON
"""
@app.route('/uniprot_to_pfam/<accession>'+reSlashOrNot)
def get_uniprot_to_pfam_api(accession):

    accession = accession.strip("'")

    query = """
    MATCH (unp:UniProt {ACCESSION:$accession})-[:HAS_UNP_RESIDUE]->(unp_res:UNPResidue)-[:IS_IN_PFAM]->(pfam:Pfam)
    RETURN pfam.PFAM_ACCESSION, pfam.NAME, pfam.DESCRIPTION, toInteger(unp_res.ID) ORDER BY toInteger(unp_res.ID)
    """

    dict_pfam = {}
    api_result = {
        accession: {
            "Pfam": {}
        }
    }

    mappings = run_query(query, {"accession":accession})

    if(len(mappings) == 0):
        bottle.response.status = 404
        return api_result

    for mapping in mappings:
        (pfam_accession, pfam_name, desc, res_id) = mapping

        if(dict_pfam.get(pfam_accession) is None):

            api_result[accession]["Pfam"][pfam_accession] = {
                "description": desc,
                "identifier": desc,
                "name": pfam_name,
                "mappings": []
            }
            dict_pfam[pfam_accession] = [res_id]

        else:
            dict_pfam[pfam_accession].append(res_id)

    for pfam_accession in dict_pfam.keys():

        for group in mit.consecutive_groups(dict_pfam[pfam_accession]):
            group = list(group)
            api_result[accession]["Pfam"][pfam_accession]["mappings"].append({
                "unp_start": group[0],
                "unp_end": group[-1]
            })

    bottle.response.status = 200
    return api_result

"""
@api {get} mappings/sequence_domains/:pdbId Get sequence domains for a PDB entry ID
@apiName GetSeqDomains
@apiGroup SIFTS
@apiDescription Get mappings from protein chains to both Pfam and InterPro as assigned by the SIFTS process.
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID

@apiUse GenericResidueTerms

@apiExample {json=./examples/success/sequence_domains.json} apiSuccessExample Example success response JSON
"""
@app.get('/sequence_domains/'+rePDBid+reSlashOrNot)
def get_sequence_domains_api(pdbid):

    pdbid = pdbid.strip("'")

    interpro_response, interpro_response_status = get_interpro(pdbid)
    pfam_response, pfam_response_status = get_pfam(pdbid)

    if(interpro_response_status != 200 and pfam_response_status != 200):
        response_status = interpro_response_status
    else:
        response_status = 200

    api_result = {
        pdbid: {
            "InterPro": {},
            "Pfam": {}
        }
    }

    if response_status == 404:
        bottle.response.status = 404
        return {}

    api_result = {
        pdbid: {
            "InterPro": interpro_response,
            "Pfam": pfam_response
        }
    }

    bottle.response.status = 200
    return api_result

"""
@api {get} mappings/structural_domains/:pdbId Get structural domains for a PDB entry ID
@apiName GetStructDomains
@apiGroup SIFTS
@apiDescription Get mappings from protein chains to both SCOP and CATH as assigned by the SIFTS process.
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID

@apiUse GenericResidueTerms

@apiExample {json=./examples/success/structural_domains.json} apiSuccessExample Example success response JSON
"""
@app.get('/structural_domains/'+rePDBid+reSlashOrNot)
def get_structural_domains_api(pdbid):

    pdbid = pdbid.strip("'")

    api_result = {
        pdbid: {
            "CATH": {},
            "SCOP": {}
        }
    }

    cath_response, cath_response_status = get_cath(pdbid)
    scop_response, scop_response_status = get_scop(pdbid)

    if(cath_response_status != 200 and scop_response_status != 200):
        response_status = cath_response_status
    else:
        response_status = 200

    if response_status == 404:
        bottle.response.status = 404
        return {}

    api_result = {
        pdbid: {
            "CATH": cath_response,
            "SCOP": scop_response
        }
    }

    bottle.response.status = 200
    return api_result

"""
@api {get} mappings/isoforms/:pdbId Get best isoform for a PDB entry ID
@apiName GetBestIsoform
@apiGroup SIFTS
@apiDescription Get mappings (as assigned by the SIFTS process) from PDB structures to UniProt. It returns the best isoform found in UniProt.
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID

@apiUse GenericResidueTerms
@apiSuccess {Integer} unp_end Index of last residue in Uniprot sequence mapped to this PDB entity.
@apiSuccess {Integer} unp_start Index of first residue in Uniprot sequence mapped to this PDB entity.
@apiSuccess {Float} identity Identity of the alignment (from 0 to 1).

@apiExample {json=./examples/success/best_isoforms.json} apiSuccessExample Example success response JSON
"""
@app.get('/isoforms/'+rePDBid+reSlashOrNot)
def get_best_isoforms_api(pdbid):
    """
    
    """

    pdbid = pdbid.strip("'")

    result, status = get_isoforms(pdbid, 'B')

    api_result = {
        pdbid: {
            "UniProt": {}
        }
    }

    if status == 404:
        bottle.response.status = 404
        return api_result

    api_result[pdbid] = result

    bottle.response.status = 200
    return api_result


"""
@api {get} mappings/all_isoforms/:pdbId Get all isoforms for a PDB entry ID
@apiName GetAllIsoforms
@apiGroup SIFTS
@apiDescription Get mappings (as assigned by the SIFTS process) from PDB structures to UniProt. It returns the all isoforms found in UniProt.
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID

@apiUse GenericResidueTerms
@apiSuccess {Integer} unp_end Index of last residue in Uniprot sequence mapped to this PDB entity.
@apiSuccess {Integer} unp_start Index of first residue in Uniprot sequence mapped to this PDB entity.
@apiSuccess {Float} identity Identity of the alignment (from 0 to 1).

@apiExample {json=./examples/success/all_isoforms.json} apiSuccessExample Example success response JSON
"""
@app.get('/all_isoforms/'+rePDBid+reSlashOrNot)
def get_all_isoforms_api(pdbid):

    pdbid = pdbid.strip("'")

    result, status = get_isoforms(pdbid, 'A')

    api_result = {
        pdbid: {
            "UniProt": {}
        }
    }

    if status == 404:
        bottle.response.status = 404
        return api_result

    api_result[pdbid] = result

    bottle.response.status = 200
    return api_result

def get_isoforms(entry_id, mapping_type):


    best_query = """
    MATCH (entry:Entry {ID:$entry_id})-[:HAS_ENTITY]->(entity:Entity)-[unp_rel:HAS_UNIPROT {BEST_MAPPING:'1'}]->(unp:UniProt)-[:HAS_UNP_RESIDUE]->(unp_res:UNPResidue)<-[:MAP_TO_UNIPROT_RESIDUE]-(pdb_res:PDBResidue)<-[:HAS_PDB_RESIDUE]-(entity)-[:CONTAINS_CHAIN]->(chain:Chain)<-[chain_rel:IS_IN_CHAIN]-(pdb_res)
    RETURN toInteger(entity.ID) as entityId, unp.ACCESSION, unp.NAME, toInteger(pdb_res.ID) as pdbResId, toInteger(unp_res.ID) as unpResId, chain.AUTH_ASYM_ID, chain.STRUCT_ASYM_ID,
    toInteger(chain_rel.AUTH_SEQ_ID) as auth_seq_id, chain_rel.PDB_INS_CODE, unp_rel.IDENTITY order by toInteger(pdb_res.ID)
    """

    all_query = """
    MATCH (entry:Entry {ID:$entry_id})-[:HAS_ENTITY]->(entity:Entity)-[unp_rel:HAS_UNIPROT]->(unp:UniProt)-[:HAS_UNP_RESIDUE]->(unp_res:UNPResidue)<-[:MAP_TO_UNIPROT_RESIDUE]-(pdb_res:PDBResidue)<-[:HAS_PDB_RESIDUE]-(entity)-[:CONTAINS_CHAIN]->(chain:Chain)<-[chain_rel:IS_IN_CHAIN]-(pdb_res)
    RETURN toInteger(entity.ID) as entityId, unp.ACCESSION, unp.NAME, toInteger(pdb_res.ID) as pdbResId, toInteger(unp_res.ID) as unpResId, chain.AUTH_ASYM_ID, chain.STRUCT_ASYM_ID,
    toInteger(chain_rel.AUTH_SEQ_ID) as auth_seq_id, chain_rel.PDB_INS_CODE, unp_rel.IDENTITY order by toInteger(pdb_res.ID)
    """

    if(mapping_type == 'B'):
        mappings = run_query(best_query, {"entry_id":entry_id})
    elif(mapping_type == 'A'):
        mappings = run_query(all_query, {"entry_id":entry_id})

    api_result = {
        "UniProt": {}
    }

    if(len(mappings) == 0):
        return {}, 404

    list_of_accessions = []
    dict_unp_master = {}
    dict_residues = {}
    dict_chains = {}
    dict_entity = {}
    dict_residues_mappings = {}
    dict_struct_asym_id = {}

    for mapping in mappings:

        (entity_id, accession, name, pdb_res_id, unp_res_id,
         auth_asym_id, struct_asym_id, auth_seq_id, pdb_ins_code, identity) = mapping

        if(accession not in list_of_accessions):
            list_of_accessions.append(accession)
            dict_unp_master[accession] = name

        if(dict_entity.get(accession) is None):
            dict_entity[accession] = set([entity_id])
        else:
            dict_entity[accession].add(entity_id)

        if(dict_chains.get((accession, entity_id)) is None):
            dict_chains[(accession, entity_id)] = set([auth_asym_id])
        else:
            dict_chains[(accession, entity_id)].add(auth_asym_id)

        if(dict_struct_asym_id.get((accession, entity_id, auth_asym_id)) is None):
            dict_struct_asym_id[(accession, entity_id, auth_asym_id)] = struct_asym_id

        if(dict_residues.get((accession, entity_id, auth_asym_id)) is None):
            dict_residues[(accession, entity_id, auth_asym_id)] = set([pdb_res_id])
        else:
            dict_residues[(accession, entity_id, auth_asym_id)].add(pdb_res_id)

        dict_residues_mappings[(entity_id, auth_asym_id, pdb_res_id)] = (
            unp_res_id, auth_seq_id, pdb_ins_code, identity)

    for accession in list_of_accessions:

        temp_mappings = []
        for entity_id in dict_entity[accession]:
            for auth_asym_id in dict_chains[(accession, entity_id)]:
                # using set to remove duplicates, since PDB residues will be duplicated in case of Chromophores which needs to be ignored
                for group in mit.consecutive_groups(dict_residues[(accession, entity_id, auth_asym_id)]):
                    group = list(group)
                    start_auth_seq_id = dict_residues_mappings[(
                        entity_id, auth_asym_id, group[0])][1]
                    end_auth_seq_id = dict_residues_mappings[(
                        entity_id, auth_asym_id, group[-1])][1]
                    start_pdb_ins_code = dict_residues_mappings[(
                        entity_id, auth_asym_id, group[0])][2]
                    end_pdb_ins_code = dict_residues_mappings[(
                        entity_id, auth_asym_id, group[-1])][2]
                    identity = dict_residues_mappings[(
                        entity_id, auth_asym_id, group[0])][3]
                    element = {
                        "entity_id": int(entity_id),
                        "chain_id": auth_asym_id,
                        "struct_asym_id": dict_struct_asym_id[(accession, entity_id, auth_asym_id)],
                        "start": {
                            "author_residue_number": None if start_auth_seq_id is None else int(start_auth_seq_id),
                            "author_insertion_code": "" if not start_pdb_ins_code else start_pdb_ins_code,
                            "residue_number": int(group[0])
                        },
                        "end": {
                            "author_residue_number": None if end_auth_seq_id is None else int(end_auth_seq_id),
                            "author_insertion_code": "" if not end_pdb_ins_code else end_pdb_ins_code,
                            "residue_number": int(group[-1])
                        },
                        "unp_start": int(dict_residues_mappings[(entity_id, auth_asym_id, group[0])][0]),
                        "unp_end": int(dict_residues_mappings[(entity_id, auth_asym_id, group[-1])][0]),
                        "pdb_start": int(group[0]),
                        "pdb_end": int(group[-1]),
                        "identity": float("%.3f" % float(identity))
                    }
                    temp_mappings.append(element)

        api_result["UniProt"][accession] = {
            "name": dict_unp_master[accession],
            "identifier": dict_unp_master[accession],
            "mappings": sorted(temp_mappings, key=itemgetter('chain_id'))
        }
        
        del temp_mappings

    return api_result, 200


"""
@api {get} mappings/uniref90/:pdbId Get uniref90 mappings for a PDB entry ID
@apiName GetUniRef90
@apiGroup SIFTS
@apiDescription Get mappings (as assigned by the SIFTS process) from PDB structures to UniProt. It returns mappings to all the UniRef90 members of the UniProt accession.
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID

@apiUse GenericResidueTerms
@apiSuccess {Integer} unp_end Index of last residue in Uniprot sequence mapped to this PDB entity.
@apiSuccess {Integer} unp_start Index of first residue in Uniprot sequence mapped to this PDB entity.
@apiSuccess {Float} identity Identity of the alignment (from 0 to 1).
@apiSuccess {Integer} tax_id The taxonomy id.
@apiSuccess {String} organism_scientific_name The scientific name of the organism.

@apiExample {json=./examples/success/uniref90.json} apiSuccessExample Example success response JSON
"""
@app.get('/uniref90/'+rePDBid+reSlashOrNot)
def get_uniref90_api(pdbid):

    pdbid = pdbid.strip("'")

    result, status = get_uniref90(pdbid, 'A')

    api_result = {
        pdbid: {
            "UniProt": {}
        }
    }

    if status == 404:
        bottle.response.status = 404
        return api_result

    api_result[pdbid]["UniProt"] = result
    bottle.response.status = 200

    return api_result


"""
@api {get} mappings/homologene_uniref90/:pdbId Get uniref90 mappings (Homologene) for a PDB entry ID
@apiName GetUniRef90Homologene
@apiGroup SIFTS
@apiDescription Get mappings (as assigned by the SIFTS process) from PDB structures to UniProt. It returns mappings to all the UniRef90 members of the UniProt accession which also belong 
to the same Homologene clusters.
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID

@apiUse GenericResidueTerms
@apiSuccess {Integer} unp_end Index of last residue in Uniprot sequence mapped to this PDB entity.
@apiSuccess {Integer} unp_start Index of first residue in Uniprot sequence mapped to this PDB entity.
@apiSuccess {Float} identity Identity of the alignment (from 0 to 1).
@apiSuccess {Integer} tax_id The taxonomy id.
@apiSuccess {String} organism_scientific_name The scientific name of the organism.

@apiExample {json=./examples/success/homologene_uniref90.json} apiSuccessExample Example success response JSON
"""
@app.get('/homologene_uniref90/'+rePDBid+reSlashOrNot)
def get_homologene_uniref90_api(pdbid):

    pdbid = pdbid.strip("'")

    result, status = get_uniref90(pdbid, 'H')

    api_result = {
        pdbid: {
            "UniProt": {}
        }
    }

    if status == 404:
        bottle.response.status = 404
        return api_result

    api_result[pdbid]["UniProt"] = result
    bottle.response.status = 200

    return api_result


def get_uniref90(entry_id, type_of_query):

    homologene_unf_query = """
    MATCH(entry:Entry {ID:$entry_id})-[:HAS_ENTITY]->(entity:Entity)-[en_unf_rel:HAS_UNIREF90_SEGMENT]->(unf:UniProt)-[:HAS_TAXONOMY]->(tax:Taxonomy), (unf)-[:IS_HOMOLOGOUS]->(homologene:Homologene)<-[:IS_HOMOLOGOUS]-(unp:UniProt)<-[:HAS_UNIPROT]-(entity)
    RETURN entity.ID, en_unf_rel.AUTH_ASYM_ID, en_unf_rel.STRUCT_ASYM_ID, unf.ACCESSION, unf.NAME, tax.TAX_ID, tax.SPTR_SCIENTIFIC, tax.SPTR_COMMON, en_unf_rel.PDB_START, en_unf_rel.PDB_END, en_unf_rel.UNP_START, en_unf_rel.UNP_END, en_unf_rel.AUTH_START, en_unf_rel.AUTH_END, en_unf_rel.AUTH_START_ICODE, en_unf_rel.AUTH_END_ICODE, en_unf_rel.IDENTITY ORDER BY en_unf_rel.AUTH_ASYM_ID, toInteger(en_unf_rel.PDB_START)
    """

    all_unf_query = """
    MATCH(entry:Entry {ID:$entry_id})-[:HAS_ENTITY]->(entity:Entity)-[en_unf_rel:HAS_UNIREF90_SEGMENT]->(unf:UniProt)-[:HAS_TAXONOMY]->(tax:Taxonomy)
    RETURN entity.ID, en_unf_rel.AUTH_ASYM_ID, en_unf_rel.STRUCT_ASYM_ID, unf.ACCESSION, unf.NAME, tax.TAX_ID, tax.SPTR_SCIENTIFIC, tax.SPTR_COMMON, en_unf_rel.PDB_START, en_unf_rel.PDB_END, en_unf_rel.UNP_START, en_unf_rel.UNP_END, en_unf_rel.AUTH_START, en_unf_rel.AUTH_END, en_unf_rel.AUTH_START_ICODE, en_unf_rel.AUTH_END_ICODE, en_unf_rel.IDENTITY ORDER BY en_unf_rel.AUTH_ASYM_ID, toInteger(en_unf_rel.PDB_START)
    """

    all_unp_query = """
    MATCH(entry:Entry {ID:$entry_id})-[:HAS_ENTITY]->(entity:Entity)-[en_unp_rel:HAS_UNIPROT]->(unp:UniProt)-[:HAS_TAXONOMY]->(tax:Taxonomy), (entity)-[en_unp_segment_rel:HAS_UNIPROT_SEGMENT]->(unp) WHERE toFloat(en_unp_rel.COVERAGE) >= 0.7
    RETURN entity.ID, en_unp_segment_rel.AUTH_ASYM_ID, en_unp_segment_rel.STRUCT_ASYM_ID, unp.ACCESSION, unp.NAME, tax.TAX_ID, tax.SPTR_SCIENTIFIC, tax.SPTR_COMMON, en_unp_segment_rel.PDB_START, en_unp_segment_rel.PDB_END, en_unp_segment_rel.UNP_START, en_unp_segment_rel.UNP_END, en_unp_segment_rel.AUTH_START, en_unp_segment_rel.AUTH_END, en_unp_segment_rel.AUTH_START_ICODE, en_unp_segment_rel.AUTH_END_ICODE, en_unp_segment_rel.IDENTITY ORDER BY en_unp_segment_rel.AUTH_ASYM_ID, toInteger(en_unp_segment_rel.PDB_START)
    """

    homologene_unp_query = """
    MATCH(entry:Entry {ID:$entry_id})-[:HAS_ENTITY]->(entity:Entity)-[en_unp_rel:HAS_UNIPROT]->(unp:UniProt)-[:HAS_TAXONOMY]->(tax:Taxonomy), (unp)-[:IS_HOMOLOGOUS]->(homologene:Homologene), (entity)-[en_unp_segment_rel:HAS_UNIPROT_SEGMENT]->(unp) WHERE toFloat(en_unp_rel.COVERAGE) >= 0.7
    RETURN entity.ID, en_unp_segment_rel.AUTH_ASYM_ID, en_unp_segment_rel.STRUCT_ASYM_ID, unp.ACCESSION, unp.NAME, tax.TAX_ID, tax.SPTR_SCIENTIFIC, tax.SPTR_COMMON, en_unp_segment_rel.PDB_START, en_unp_segment_rel.PDB_END, en_unp_segment_rel.UNP_START, en_unp_segment_rel.UNP_END, en_unp_segment_rel.AUTH_START, en_unp_segment_rel.AUTH_END, en_unp_segment_rel.AUTH_START_ICODE, en_unp_segment_rel.AUTH_END_ICODE, en_unp_segment_rel.IDENTITY ORDER BY en_unp_segment_rel.AUTH_ASYM_ID, toInteger(en_unp_segment_rel.PDB_START)
    """

    if type_of_query == 'A':
        unf_mappings = run_query(all_unf_query, {"entry_id":entry_id})
        unp_mappings = run_query(all_unp_query, {"entry_id":entry_id})
    else:
        unf_mappings = run_query(homologene_unf_query, {"entry_id":entry_id})
        unp_mappings = run_query(homologene_unp_query, {"entry_id":entry_id})

    api_result = {}

    if(len(unf_mappings) == 0 and len(unp_mappings) == 0):
        return api_result, 404

    mappings = unp_mappings + unf_mappings

    for mapping in mappings:

        (entity_id, auth_asym_id, struct_asym_id, unf_accession, unf_name, tax_id, scientific_name, common_name, 
         pdb_start, pdb_end, unf_start, unf_end, auth_start, auth_end, auth_start_icode, auth_end_icode, identity) = mapping

        if api_result.get(unf_accession) is None:
            api_result[unf_accession] = {
                "identifier": unf_name,
                "name": unf_name,
                "organism_scientific_name": "{} ({})".format(scientific_name, common_name) if common_name is not None else scientific_name,
                "tax_id": int(tax_id),
                "mappings": []
            }

        api_result[unf_accession]["mappings"].append({
            "entity_id": int(entity_id),
            "end": {
                "author_residue_number": None if auth_end is None else int(auth_end),
                "author_insertion_code": "" if auth_end_icode is None else auth_end_icode,
                "residue_number": int(pdb_end)
            },
            "chain_id": auth_asym_id,
            "pdb_start": int(pdb_start),
            "start": {
                "author_residue_number": None if auth_start is None else int(auth_start),
                "author_insertion_code": "" if auth_start_icode is None else auth_start_icode,
                "residue_number": int(pdb_start)
            },
            "unp_end": int(unf_end),
            "pdb_end": int(pdb_end),
            "struct_asym_id": struct_asym_id,
            "unp_start": int(unf_start),
            "identity": float("%.3f" % float(identity))
        })

    return api_result, 200

