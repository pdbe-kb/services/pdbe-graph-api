import gzip
import json
import logging
import os
import re
from urllib.parse import urlsplit

import bottle
import simplejson

from pdbegraphapi.config import JSON_GEN_CONFIG

logging.basicConfig(level=logging.INFO)

# One or more PDB ids, comma separated, with single quotes around them
re_list_PDB = re.compile(r"('\d\w{3}')(,'\d\w{3}')*")

# parent_dir_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

json_out_path = os.environ.get("OUTPUT_JSON_PATH")
path_prefix = os.environ.get("PATH_PREFIX")

# Returns the value of the field.
# None if it's not present
def get_field(field):
    if field in bottle.request.query:
        return bottle.request.query.getall(field)[0]
    else:
        return None

# Checks if the field is equal to true
def active_field(field):
    ret = get_field(field)
    return ret is not None and ret.lower() == "true"


def request_handler(fn):
    def wrap(*args, **kwargs):
        
        # Get the first parameter for the method that
        # is going to be called next
        key = fn.__code__.co_varnames[0]

        bottle.response.content_type = 'application/json'

        if bottle.request.method == 'POST':
            qval = bottle.request.body.read()
            
            if isinstance(qval, bytes):
                qval = qval.decode("utf-8")

            if bottle.request.content_type == "application/json":
                try:
                    kwargs[key] = json.loads(qval)
                except Exception as e:
                    bottle.response.status = 400
                    return {
                        "error": "Error in JSON request"
                    }
            else:
                if qval in [None, '']:
                    bottle.response.status = 400
                    return simplejson.dumps({}, indent=4)
                
                if ',' in qval:
                    qval = qval.replace(" ", "")
                    kwargs[key] = qval.replace(",", "','")
                elif ' ' in qval:
                    # Remove whitespaces from front and back (strip) +
                    # + replace any \s sequence with ','
                    kwargs[key] = re.sub(r"\s+", "','", qval.strip())
                else:
                    kwargs[key] = qval

                # More than 1000 items in the query
                if kwargs[key].count(',') > 999:
                    bottle.response.status = 400
                    return simplejson.dumps({}, indent=4)

        elif bottle.request.method == 'GET':
            
            # convert cases for parameters
            for key in kwargs:
                if key == "uniprot_accession":
                    kwargs["uniprot_accession"] = kwargs["uniprot_accession"].upper()
                elif key == "pdbid":
                    kwargs["pdbid"] = kwargs["pdbid"].lower()

        # else:
            
            # Check if it's a string and starts with '
            # if isinstance(kwargs[key], str) and len(kwargs[key]) and kwargs[key][0] != "'":
            #     kwargs[key] = "'%s'" % kwargs[key]
        
            # If it matches the re convert to lowercase
        #     if key != "mc" and re_list_PDB.match(kwargs[key]):
        #         kwargs[key] = kwargs[key].lower()

        # mc = None
        output = None

        # Check if call needs to check for static files
        if bottle.request.method == 'GET' and JSON_GEN_CONFIG.get(fn.__name__):
            cache_url = urlsplit(bottle.request.url).path

            try:
                with gzip.open(f"{json_out_path}/{cache_url}", "rt") as static_file:
                    output = json.load(static_file)

                    # if response is blank, make actual call. There could be issue with static JSON generation
                    if output == {}:
                        output = None
                        pass
                    logging.info(f"Served {bottle.request.url} from FS Cache")
                    bottle.response.status = 200
            except Exception as e:
                logging.info(f"Cache miss for {bottle.request.url}")
                # print("Error serving static json: {}".format(e))
                pass

        # If the call requests caching
        # if bottle.request.method == 'GET' and kwargs.get("mc") is not None:
        #     mc = kwargs["mc"]
            
        #     cache_url = urlsplit(bottle.request.url).path

        #     if cache_url[-1] != "/":
        #         cache_url += "/"

        #     try:
        #         output = mc.get(cache_url)
        #         bottle.response.status = mc.get(cache_url + "_status")
        #     except Exception as e:
        #         pass


        # Caching is not active
        if not output:
            output = fn(*args, **kwargs)
            
            # if bottle.request.method == 'GET' and mc:
            #     try:
            #         # to overcome key-size < 250 issue, can't set it in ctor :-o
            #         mc.set(cache_url, output)
            #         # save the status code in the cache
            #         mc.set(cache_url + "_status", bottle.response.status)
            #     except Exception as e:
            #         pass
        
        if len(output) == 0:
            bottle.response.status = 404


        if active_field("pretty"):
            output = simplejson.dumps(output, indent=4)
        else:
            output = simplejson.dumps(output, separators=(',', ':'))

        return output
    
    return wrap

