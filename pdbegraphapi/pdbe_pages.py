#!/usr/bin/env python

import itertools
import bottle
import re
from pdbegraphapi.amino_acid_codes import amino_acid_codes, amino_acid_codes_one_to_three
from pdbegraphapi import request_handler
from operator import itemgetter
from pdbegraphapi import util_common
from pdbegraphapi.neo4j_model import run_query
from pdbegraphapi.config import rePDBid, reSlashOrNot, reResidue, rePDBComplex
from functools import reduce
from pdbegraphapi.util_common import split_nonconsecutive_residues_with_code, get_amino_three_to_one, split_nonconsecutive_two_residues_with_code
from pdbegraphapi.common_params import RSRZ_OUTLIER_CUTOFF
from pdbegraphapi.common_params import validation_switch
from protvista_adapter.adapter import convert_to_protvista
from pdbegraphapi.uniprot import get_uniprot_variation_api, get_residue_modifications, get_residue_mutations

print('[PDBE PAGES] Starting')

app = bottle.Bottle()

# Installs the request handler plugin
app.install(request_handler)

list_re = r'[\s\']'

print('[PDBE PAGES] Loaded')



"""
@api {get} pdbe_pages/uniprot_mapping/:pdbId/:entityId Get UniProt mapping for an entity
@apiName GetPDBePageUniProtMapping
@apiGroup PDB
@apiDescription Get UniProt mapping for an entity
@apiVersion 2.0.0

@apiParam {String} pdbId=1wpg PDB Entry
@apiParam {String} entityId=1 PDB Entity

@apiUse GenericFields
@apiSuccess {Integer} unpStartIndex Start residue number of UniProt.
@apiSuccess {Integer} unpEndIndex End residue number of UniProt.

@apiExample {json=./examples/success/pdbe_pages_uniprot_mapping.json} apiSuccessExample Example success response JSON
"""
@app.get('/uniprot_mapping/'+rePDBid+reSlashOrNot+'/<entity_id>'+reSlashOrNot)
def get_pdbe_pages_uniprot_mapping_api(pdbid, entity_id):

    pdbid = pdbid.strip("'")
    api_result = {
        pdbid: {}
    }

    sequence = get_pdb_sequence(pdbid, entity_id)

    if not sequence:
        bottle.response.status = 404
        return {}

    api_result[pdbid]["sequence"] = sequence["sequence"]
    api_result[pdbid]["length"] = sequence["length"]
    api_result[pdbid]["dataType"] = "UNIPROT MAPPING"
    api_result[pdbid]["data"] = []

    data = get_uniprot_mapping(pdbid, entity_id)

    if not data:
        bottle.response.status = 404
        return {}
    
    api_result[pdbid]["data"] = data

    bottle.response.status = 200
    return api_result


@app.get('/protvista/uniprot_mapping/'+rePDBid+reSlashOrNot+'/<entity_id>'+reSlashOrNot)
def get_pdbe_pages_protvista_uniprot_mapping_api(pdbid, entity_id):

    generic_response = get_pdbe_pages_uniprot_mapping_api(pdbid, entity_id)

    if not generic_response:
        bottle.response.status = 404
        return {}

    bottle.response.status = 200
    return convert_to_protvista("entry", "uniprot", generic_response)


"""
@api {get} pdbe_pages/secondary_structure/:pdbId/:entityId Get secondary structures for an entity
@apiName GetPDBePageSecStructures
@apiGroup PDB
@apiDescription Get secondary structures for an entity
@apiVersion 2.0.0

@apiParam {String} pdbId=5bp8 PDB Entry
@apiParam {String} entityId=1 PDB Entity

@apiUse GenericFields

@apiExample {json=./examples/success/pdbe_pages_secondary_structure.json} apiSuccessExample Example success response JSON
"""
@app.get('/secondary_structure/'+rePDBid+reSlashOrNot+'/<entity_id>'+reSlashOrNot)
def get_pdbe_pages_secondary_structure_api(pdbid, entity_id):

    pdbid = pdbid.strip("'")
    api_result = {
        pdbid: {}
    }

    sequence = get_pdb_sequence(pdbid, entity_id)

    if not sequence:
        bottle.response.status = 404
        return {}

    api_result[pdbid]["sequence"] = sequence["sequence"]
    api_result[pdbid]["length"] = sequence["length"]
    api_result[pdbid]["dataType"] = "SECONDARY STRUCTURES"
    api_result[pdbid]["data"] = []
    
    data = get_secondary_structure(pdbid, entity_id)
    
    # get IDP data
    idp_data = get_idp_mapping(pdbid, entity_id)
    
    # get WEBnma, KnotProt data
    flex_data = get_flex_predictions(pdbid, entity_id)

    # return 404 only if IDP, KnotProt and secondary structures are empty
    if not data and not idp_data and not flex_data:
        bottle.response.status = 404
        return {}
    
    api_result[pdbid]["data"].extend(data)
    api_result[pdbid]["data"].extend(idp_data)
    api_result[pdbid]["data"].extend(flex_data)

    bottle.response.status = 200
    return api_result


@app.get('/protvista/secondary_structure/'+rePDBid+reSlashOrNot+'/<entity_id>'+reSlashOrNot)
def get_pdbe_pages_protvista_secondary_structure_api(pdbid, entity_id):
    
    generic_response = get_pdbe_pages_secondary_structure_api(pdbid, entity_id)

    if not generic_response:
        bottle.response.status = 404
        return {}

    bottle.response.status = 200
    return convert_to_protvista("entry", "secstructures", generic_response)


"""
@api {get} pdbe_pages/binding_sites/:pdbId/:entityId Get binding sites for an entity
@apiName GetPDBePageBindingSites
@apiGroup PDB
@apiDescription Get binding sites for an entity
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry
@apiParam {String} entityId=1 PDB Entity

@apiUse GenericFields
@apiSuccess {String} scaffoldId SMILES representation of the scaffold.
@apiSuccess {String} cofactorId Unique identifier for the cofactor.
@apiSuccess {Integer} residueCount Number of residues binding to the ligand.
@apiSuccess {String} boundMoleculeId Unique identifier for the bound molecule.

@apiExample {json=./examples/success/pdbe_pages_binding_sites.json} apiSuccessExample Example success response JSON
"""
@app.get('/binding_sites/'+rePDBid+reSlashOrNot+'/<entity_id>'+reSlashOrNot)
def get_pdbe_pages_binding_sites_api(pdbid, entity_id):

    pdbid = pdbid.strip("'")
    api_result = {
        pdbid: {}
    }

    sequence = get_pdb_sequence(pdbid, entity_id)

    if not sequence:
        bottle.response.status = 404
        return {}

    api_result[pdbid]["sequence"] = sequence["sequence"]
    api_result[pdbid]["length"] = sequence["length"]
    api_result[pdbid]["dataType"] = "LIGAND BINDING SITES"
    api_result[pdbid]["data"] = []
    
    data = get_binding_sites(pdbid, entity_id)

    if not data:
        bottle.response.status = 404
        return {}
        
    api_result[pdbid]["data"] = data
    bottle.response.status = 200

    return api_result

@app.get('/protvista/binding_sites/'+rePDBid+reSlashOrNot+'/<entity_id>'+reSlashOrNot)
def get_pdbe_pages_protvista_binding_sites_api(pdbid, entity_id):
    
    generic_response = get_pdbe_pages_binding_sites_api(pdbid, entity_id)

    if not generic_response:
        bottle.response.status = 404
        return {}

    bottle.response.status = 200
    return convert_to_protvista("entry", "ligandsites", generic_response)


"""
@api {get} pdbe_pages/interfaces/:pdbId/:entityId Get interface residues for an entity
@apiName GetPDBePageInterfaceResidues
@apiGroup PDB
@apiDescription Get interface residues for an entity
@apiVersion 2.0.0

@apiParam {String} pdbId=2etx PDB Entry
@apiParam {String} entityId=1 PDB Entity

@apiUse GenericFields

@apiExample {json=./examples/success/pdbe_pages_interfaces.json} apiSuccessExample Example success response JSON
"""
@app.get('/interfaces/'+rePDBid+reSlashOrNot+'/<entity_id>'+reSlashOrNot)
def get_pdbe_pages_interfaces_api(pdbid, entity_id):

    pdbid = pdbid.strip("'")
    api_result = {
        pdbid: {}
    }

    sequence = get_pdb_sequence(pdbid, entity_id)

    if not sequence:
        bottle.response.status = 404
        return {}

    api_result[pdbid]["sequence"] = sequence["sequence"]
    api_result[pdbid]["length"] = sequence["length"]
    api_result[pdbid]["dataType"] = "INTERFACE RESIDUES"
    api_result[pdbid]["data"] = []
    
    data = get_interfaces(pdbid, entity_id)

    if not data:
        bottle.response.status = 404
        return {}
        
    api_result[pdbid]["data"] = data

    bottle.response.status = 200
    return api_result


@app.get('/protvista/interfaces/'+rePDBid+reSlashOrNot+'/<entity_id>'+reSlashOrNot)
def get_pdbe_pages_protvista_interfaces_api(pdbid, entity_id):
    
    generic_response = get_pdbe_pages_interfaces_api(pdbid, entity_id)

    if not generic_response:
        bottle.response.status = 404
        return {}

    bottle.response.status = 200
    return convert_to_protvista("entry", "interface", generic_response)


"""
@api {get} pdbe_pages/annotations/:pdbId/:entityId Get FunPDBe annotations for an entity
@apiName GetPDBePageAnnotations
@apiGroup PDB
@apiDescription Get FunPDBe annotations for an entity
@apiVersion 2.0.0

@apiParam {String} pdbId=1a08 PDB Entry
@apiParam {String} entityId=1 PDB Entity

@apiUse GenericFields
@apiSuccess {String} resourceUrl A URL where details on the resource can be seen.

@apiExample {json=./examples/success/pdbe_pages_annotations.json} apiSuccessExample Example success response JSON
"""
@app.get('/annotations/'+rePDBid+reSlashOrNot+'/<entity_id>'+reSlashOrNot)
def get_pdbe_pages_annotations_api(pdbid, entity_id):

    pdbid = pdbid.strip("'")
    api_result = {
        pdbid: {}
    }

    sequence = get_pdb_sequence(pdbid, entity_id)

    if not sequence:
        bottle.response.status = 404
        return {}

    api_result[pdbid]["sequence"] = sequence["sequence"]
    api_result[pdbid]["length"] = sequence["length"]
    api_result[pdbid]["dataType"] = "ANNOTATIONS"
    api_result[pdbid]["data"] = []
    
    data = get_annotations(pdbid, entity_id)

    if not data:
        bottle.response.status = 404
        return {}

    api_result[pdbid]["data"] = data

    bottle.response.status = 200
    return api_result

@app.get('/protvista/annotations/'+rePDBid+reSlashOrNot+'/<entity_id>'+reSlashOrNot)
def get_pdbe_pages_protvista_annotations_api(pdbid, entity_id):
    
    generic_response = get_pdbe_pages_annotations_api(pdbid, entity_id)

    if not generic_response:
        bottle.response.status = 404
        return {}

    bottle.response.status = 200
    return convert_to_protvista("entry", "annotations", generic_response)


"""
@api {get} pdbe_pages/domains/:pdbId/:entityId Get sequence and structural domains for an entity
@apiName GetPDBePageDomains
@apiGroup PDB
@apiDescription Get sequence and structural domains for an entity
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry
@apiParam {String} entityId=1 PDB Entity

@apiUse GenericFields

@apiExample {json=./examples/success/pdbe_pages_domains.json} apiSuccessExample Example success response JSON
"""
@app.get('/domains/'+rePDBid+reSlashOrNot+'/<entity_id>'+reSlashOrNot)
def get_pdbe_pages_domains_api(pdbid, entity_id):

    pdbid = pdbid.strip("'")
    api_result = {
        pdbid: {}
    }

    sequence = get_pdb_sequence(pdbid, entity_id)

    if not sequence:
        bottle.response.status = 404
        return {}

    api_result[pdbid]["sequence"] = sequence["sequence"]
    api_result[pdbid]["length"] = sequence["length"]
    api_result[pdbid]["dataType"] = "DOMAINS"
    api_result[pdbid]["data"] = []
    
    data = get_domains(pdbid, entity_id)
    
    if not data:
        bottle.response.status = 404
        return {}

    api_result[pdbid]["data"] = data

    bottle.response.status = 200
    return api_result


@app.get('/protvista/domains/'+rePDBid+reSlashOrNot+'/<entity_id>'+reSlashOrNot)
def get_pdbe_pages_protvista_domains_api(pdbid, entity_id):
    
    generic_response = get_pdbe_pages_domains_api(pdbid, entity_id)

    if not generic_response:
        bottle.response.status = 404
        return {}

    bottle.response.status = 200
    return convert_to_protvista("entry", "domains", generic_response)

"""
@api {get} pdbe_pages/chains/:pdbId/:entityId Get chain and quality information for an entity
@apiName GetPDBePageChains
@apiGroup PDB
@apiDescription Get chain and quality information for an entity
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry
@apiParam {String} entityId=1 PDB Entity
"""
@app.get('/chains/'+rePDBid+reSlashOrNot+'/<entity_id>'+reSlashOrNot)
def get_pdbe_pages_chains_api(pdbid, entity_id):

    pdbid = pdbid.strip("'")
    api_result = {
        pdbid: {}
    }

    sequence = get_pdb_sequence(pdbid, entity_id)

    if not sequence:
        bottle.response.status = 404
        return {}

    api_result[pdbid]["sequence"] = sequence["sequence"]
    api_result[pdbid]["length"] = sequence["length"]
    api_result[pdbid]["dataType"] = "CHAINS"
    api_result[pdbid]["data"] = []

    data = get_chains(pdbid, entity_id)

    if not data:
        bottle.response.status = 404
        return {}

    api_result[pdbid]["data"] = data
    
    bottle.response.status = 200
    return api_result


@app.get('/protvista/chains/'+rePDBid+reSlashOrNot+'/<entity_id>'+reSlashOrNot)
def get_pdbe_pages_protvista_chains_api(pdbid, entity_id):
    
    generic_response = get_pdbe_pages_chains_api(pdbid, entity_id)

    if not generic_response:
        bottle.response.status = 404
        return {}

    bottle.response.status = 200
    return convert_to_protvista("entry", "chains", generic_response)

"""
@api {get} pdbe_pages/rfam/:pdbId/:entityId Get Rfam domains for an entity
@apiName GetPDBePageRfamDomains
@apiGroup PDB
@apiDescription Get Rfam domains for an entity
@apiVersion 2.0.0

@apiParam {String} pdbId=1jj2 PDB Entry
@apiParam {String} entityId=1 PDB Entity

@apiUse GenericFields
@apiSuccess {String} domainName Identifier for the accession.
@apiSuccess {String} domainId The mapping accession.

@apiExample {json=./examples/success/pdbe_pages_rfam.json} apiSuccessExample Example success response JSON
"""
@app.get('/rfam/'+rePDBid+reSlashOrNot+'/<entity_id>'+reSlashOrNot)
def get_pdbe_pages_rfam_api(pdbid, entity_id):

    pdbid = pdbid.strip("'")
    api_result = {
        pdbid: {}
    }

    sequence = get_pdb_sequence(pdbid, entity_id)

    if not sequence:
        bottle.response.status = 404
        return {}

    api_result[pdbid]["sequence"] = sequence["sequence"]
    api_result[pdbid]["length"] = sequence["length"]
    api_result[pdbid]["dataType"] = "RFAM"
    api_result[pdbid]["data"] = []

    data = get_rfam(pdbid, entity_id)

    if not data:
        bottle.response.status = 404
        return {}

    api_result[pdbid]["data"] = data
    
    bottle.response.status = 200
    return api_result


@app.get('/protvista/rfam/'+rePDBid+reSlashOrNot+'/<entity_id>'+reSlashOrNot)
def get_pdbe_pages_protvista_rfam_api(pdbid, entity_id):
    
    generic_response = get_pdbe_pages_rfam_api(pdbid, entity_id)

    if not generic_response:
        bottle.response.status = 404
        return {}

    bottle.response.status = 200
    return convert_to_protvista("entry", "rfam", generic_response)


def get_uniprot_mapping(pdb_id, entity_id):

    query = """
    MATCH (entry:Entry {ID:$pdb_id})-[:HAS_ENTITY]->(entity:Entity {ID:$entity_id})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[rel:MAP_TO_UNIPROT_RESIDUE]->(unp_res:UNPResidue)<-[:HAS_UNP_RESIDUE]-(unp:UniProt) WHERE NOT unp.ACCESSION CONTAINS '-'
    RETURN unp.ACCESSION AS accession, unp.NAME AS unp_name, pdb_res.ID, pdb_res.CHEM_COMP_ID, unp_res.ID, unp_res.ONE_LETTER_CODE ORDER BY toInteger(pdb_res.ID)
    """
    
    mappings = run_query(query, parameters={
        "pdb_id": str(pdb_id),
        "entity_id": str(entity_id)
    })

    if not mappings:
        return []

    dict_accession = {}
    dict_residues = {}
    dict_conflicts = {}
    result_list = []
    best_chain_id = get_best_chain_id(pdb_id, entity_id)
    polyprotein_segments = {}

    for mapping in mappings:
        (accession, unp_name, pdb_res_id, pdb_res_code, unp_res_id, unp_res_code) = mapping

        if dict_conflicts.get(accession) is None:
            dict_conflicts[accession] = []

        unp_three_code = util_common.get_amino_one_to_three(unp_res_code)

        if unp_three_code != pdb_res_code:
            dict_conflicts[accession].append((pdb_res_id, unp_res_id, pdb_res_code, unp_three_code))
        
        if dict_accession.get(accession) is None:
            dict_accession[accession] = {
                "dataType": "UniProt",
                "accession": accession,
                "name": unp_name,
                "residues": [],
                "additionalData": {
                    "bestChainId": best_chain_id,
                    "entityId": int(entity_id)
                }
            }

            # keep residues to find segments
            dict_residues[accession] = {
                "pdb_residues": [pdb_res_id],
                "pdb_residue_codes": [pdb_res_code],
                "unp_residues": [unp_res_id]
            }
        else:
            dict_residues[accession]["pdb_residues"].append(pdb_res_id)
            dict_residues[accession]["pdb_residue_codes"].append(pdb_res_code)
            dict_residues[accession]["unp_residues"].append(unp_res_id)
    
    # iterate over the accessions, keeping processed proteins at last
    for accession in sorted(dict_accession.keys(), key=lambda x: x.startswith("PRO_")):
        # keeps the segments first and then conflicts
        for residues in util_common.split_nonconsecutive_two_residues_with_code([dict_residues[accession]["pdb_residues"], dict_residues[accession]["unp_residues"], dict_residues[accession]["pdb_residue_codes"]]):
            unp_start_id = int(residues[0][1])
            unp_end_id = int(residues[-1][1])
            
            # keep the polyprotein segments
            if not accession.startswith("PRO_"):
                polyprotein_segments[accession] = [f"{unp_start_id}:{unp_end_id}"] if polyprotein_segments.get(accession) is None else polyprotein_segments[accession] + [f"{unp_start_id}:{unp_end_id}"]
            else:
                # check if the segment is already in the polyprotein
                if f"{unp_start_id}:{unp_end_id}" in itertools.chain(*polyprotein_segments.values()):
                    continue
            
            dict_accession[accession]["residues"].append({
                "startIndex": int(residues[0][0]),
                "endIndex": int(residues[-1][0]),
                "startCode": residues[0][2],
                "endCode": residues[-1][2],
                "unpStartIndex": unp_start_id,
                "unpEndIndex": unp_end_id,
                "indexType": "PDB"
            })

        residue_modifications = get_residue_modifications(pdb_id, entity_id, accession)
        if residue_modifications:
            for res_modification in residue_modifications:
                (pdb_res_id, chem_comp_id, unp_res_id, unp_res_code) = res_modification

                residue_fragment = {
                    "unpStartIndex": int(unp_res_id),
                    "unpEndIndex": int(unp_res_id),
                    "startIndex": int(pdb_res_id),
                    "endIndex": int(pdb_res_id),
                    "startCode": util_common.get_amino_one_to_three(unp_res_code),
                    "endCode": util_common.get_amino_one_to_three(unp_res_code),
                    "indexType": "UNIPROT",
                    "pdbCode": chem_comp_id,
                    "modification": True
                }

                dict_accession[accession]["residues"].append(residue_fragment)
        
        residue_mutations = get_residue_mutations(pdb_id, entity_id, accession)
        if residue_mutations:
            for res_mutation in residue_mutations:
                items = res_mutation.split("|")

                if len(items) != 7:
                    continue

                [mutation_type, pdb_res_id, unp_res_id, pdb_res_code, unp_res_code, auth_asym_id, struct_asym_id] = items
                
                if struct_asym_id != best_chain_id:
                    continue
                
                residue_fragment = {
                    "unpStartIndex": int(unp_res_id),
                    "unpEndIndex": int(unp_res_id),
                    "startIndex": int(pdb_res_id),
                    "endIndex": int(pdb_res_id),
                    "startCode": util_common.get_amino_one_to_three(unp_res_code),
                    "endCode": util_common.get_amino_one_to_three(unp_res_code),
                    "indexType": "UNIPROT",
                    "pdbCode": pdb_res_code
                }

                # check if it's a non standard amino acid
                if util_common.get_amino_three_to_one(pdb_res_code) == "*":
                    residue_fragment.update({
                        "modification": True
                    })
                else:
                    residue_fragment.update({
                        "mutation": True,
                        "mutationType": mutation_type
                    })
                
                dict_accession[accession]["residues"].append(residue_fragment)

        # only add the proteins if they have residues
        if dict_accession[accession]["residues"]:
            result_list.append(dict_accession[accession])

    return result_list


def get_secondary_structure(pdb_id, entity_id):

    query = """
    MATCH (n:Entry {ID:$pdb_id})-[:HAS_ENTITY]->(entity:Entity {ID:$entity_id})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[rel:IS_IN_CHAIN]->(chain:Chain {STRUCT_ASYM_ID:entity.BEST_CHAIN_ID})
	WHERE rel.OBSERVED='Y' AND (rel.IS_IN_HELIX='Y' OR exists(rel.SHEETS))
    WITH entity.ID AS entity_id, chain.STRUCT_ASYM_ID AS struct_asym_id,
    CASE rel.HELIX_SEGMENT
        WHEN null
            THEN "0"
        ELSE
            rel.HELIX_SEGMENT
    END AS helix_segment,
    rel.SHEETS AS sheets, toInteger(pdb_res.ID) AS pdb_res_id, pdb_res.CHEM_COMP_ID AS pdb_res_code
    RETURN struct_asym_id, helix_segment, sheets, pdb_res_id, pdb_res_code ORDER BY pdb_res_id
    """

    mappings = run_query(query, parameters={
        "pdb_id": str(pdb_id),
        "entity_id": str(entity_id)
    })

    if not mappings:
        return []

    dict_sheets = {}
    dict_helices = {}
    best_chain_id = None
    result_list = []
    best_chain_id = get_best_chain_id(pdb_id, entity_id)

    for mapping in mappings:
        (chain_id, helices, sheets, pdb_res_id, pdb_res_code) = mapping

        # not that great, any better way???
        best_chain_id = chain_id

        # process helices
        if helices != '0':
            helix_segments = [re.sub(list_re, "", a) for a in helices.strip('[]').split(',')]

            for helix_segment in helix_segments:
                if dict_helices.get(helix_segment) is None:
                    dict_helices[helix_segment] = [(pdb_res_id, pdb_res_code)]
                else:
                    dict_helices[helix_segment].append((pdb_res_id, pdb_res_code))

        # process sheets
        if sheets is not None:
            sheets = [re.sub(list_re, "", a) for a in sheets.strip('[]').split(',')]

            for sheet in sheets:
                if dict_sheets.get(sheet) is None:
                    dict_sheets[sheet] = [(pdb_res_id, pdb_res_code)]
                else:
                    dict_sheets[sheet].append((pdb_res_id, pdb_res_code))

    del mappings

    helix_fragment = {
        "dataType": "Helix",
        "accession": "Helix",
        "name": "Helix",
        "residues": [],
        "additionalData": {
            "bestChainId": best_chain_id,
            "entityId": int(entity_id)
        }
    }

    sheet_fragment = {
        "dataType": "Strand",
        "accession": "Strand",
        "name": "Strand",
        "residues": [],
        "additionalData": {
            "bestChainId": best_chain_id,
            "entityId": int(entity_id)
        }
    }

    for helix in dict_helices.keys():
        for residues in util_common.split_nonconsecutive_residues_with_code([list(map(lambda x: x[0], dict_helices[helix])), list(map(lambda x: x[1], dict_helices[helix]))]):
            helix_fragment["residues"].append({
                "startIndex": int(residues[0][0]),
                "endIndex": int(residues[-1][0]),
                "startCode": residues[0][1],
                "endCode": residues[-1][1],
                "indexType": "PDB"
            })
    
    helix_fragment["residues"] = sorted(helix_fragment["residues"], key=itemgetter("startIndex"))
    result_list.append(helix_fragment)

    for sheet in dict_sheets.keys():
        for residues in util_common.split_nonconsecutive_residues_with_code([list(map(lambda x: x[0], dict_sheets[sheet])), list(map(lambda x: x[1], dict_sheets[sheet]))]):
            sheet_fragment["residues"].append({
                "startIndex": int(residues[0][0]),
                "endIndex": int(residues[-1][0]),
                "startCode": residues[0][1],
                "endCode": residues[-1][1],
                "indexType": "PDB"
            })
    
    sheet_fragment["residues"] = sorted(sheet_fragment["residues"], key=itemgetter("startIndex"))
    result_list.append(sheet_fragment)

    return result_list


def get_binding_sites(pdb_id, entity_id):

    query = """
    MATCH (entry:Entry {ID:$pdb_id})-[:HAS_ENTITY]->(entity:Entity {ID:$entity_id})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[:HAS_ARP_CONTACT]-(bound_ligand:BoundLigand)<-[:IS_AN_INSTANCE_OF]-(ligand_entity:Entity)-[:IS_A]->(chem_Comp:ChemicalComponent), (bound_ligand)-[:IS_PART_OF]->(bound_molecule:BoundMolecule)
    RETURN DISTINCT bound_ligand.CHEM_COMP_ID AS het_code, bound_molecule.ID AS bound_molecule_id, pdb_res.ID AS pdb_res_id, pdb_res.CHEM_COMP_ID AS pdb_res_code ORDER BY toInteger(pdb_res_id)
    """

    mappings = run_query(query, parameters={
        "pdb_id": str(pdb_id),
        "entity_id": str(entity_id)
    })

    result_list = []
    dict_hetcode = {}
    dict_hetcode_residues = {}
    best_chain_id = get_best_chain_id(pdb_id, entity_id)

    for mapping in mappings:
        (hetcode, bound_mol_id, pdb_res_id, pdb_res_code) = mapping

        if dict_hetcode.get(hetcode) is None:
            dict_hetcode[hetcode] = {
                "dataType": "Ligand",
                "accession": hetcode,
                "additionalData": {
                    "bestChainId": best_chain_id,
                    "entityId": int(entity_id),
                    "boundMolecules": [bound_mol_id]
                },
                "residues": []
            }
        else:
            dict_hetcode[hetcode]["additionalData"]["boundMolecules"].append(bound_mol_id)

        if dict_hetcode_residues.get((hetcode, bound_mol_id)) is None:
            dict_hetcode_residues[(hetcode, bound_mol_id)] = [(pdb_res_id, pdb_res_code)]
        else:
            dict_hetcode_residues[(hetcode, bound_mol_id)].append((pdb_res_id, pdb_res_code))

    del mappings

    # get details including scaffold and co factor for all hetcodes
    hetcodes = dict_hetcode.keys()
    
    query = """
    MATCH (chem_comp:ChemicalComponent) WHERE chem_comp.ID IN $hetcodes
    OPTIONAL MATCH (chem_comp)-[:ACTS_AS_COFACTOR]->(co_factor:COFactorClass)
    OPTIONAL MATCH (chem_comp)-[:CONTAINS_SCAFFOLD]->(scaffold:Scaffold)
    RETURN DISTINCT chem_comp.ID AS hetcode, chem_comp.NAME AS name, scaffold.UNIQID AS scaffold_id, co_factor.COFACTOR_ID AS co_factor_id
    """

    mappings = run_query(query, parameters={
        "hetcodes": list(hetcodes)
    })

    for mapping in mappings:
        (hetcode, name, scaffold_id, cofactor_id) = mapping

        dict_hetcode[hetcode]["additionalData"]["scaffoldId"] = "" if scaffold_id is None else scaffold_id
        dict_hetcode[hetcode]["additionalData"]["cofactorId"] = "" if cofactor_id is None else cofactor_id
        dict_hetcode[hetcode]["name"] = name

    del mappings
    
    for hetcode in dict_hetcode.keys():
        dict_hetcode[hetcode]["additionalData"]["residueCount"] = 0
        
        for bound_molecule in set(dict_hetcode[hetcode]["additionalData"]["boundMolecules"]):        
            dict_hetcode[hetcode]["additionalData"]["residueCount"] += len(dict_hetcode_residues[(hetcode, bound_molecule)])

            for residues in util_common.split_nonconsecutive_residues_with_code([list(map(lambda x: x[0], dict_hetcode_residues[(hetcode, bound_molecule)])), list(map(lambda x: x[1], dict_hetcode_residues[(hetcode, bound_molecule)]))]):
                residue_fragment = {
                    "startIndex": int(residues[0][0]),
                    "endIndex": int(residues[0][0]),
                    "startCode": residues[0][1],
                    "endCode": residues[0][1],
                    "indexType": "PDB",
                    "additionalData": {
                        "boundMoleculeId": bound_molecule
                    }
                }
                if len(residues) != 1:
                    residue_fragment["endIndex"] = int(residues[-1][0])
                    residue_fragment["endCode"] = residues[-1][1]
                
                dict_hetcode[hetcode]["residues"].append(residue_fragment)

            dict_hetcode[hetcode]["residues"] = sorted(dict_hetcode[hetcode]["residues"], key=itemgetter("startIndex"))

        result_list.append(dict_hetcode[hetcode])
        del dict_hetcode[hetcode]["additionalData"]["boundMolecules"]

    return result_list


def get_interfaces(pdb_id, entity_id):

    query = """
    MATCH (entry:Entry {ID:$pdb_id})-[:HAS_ENTITY]->(entity:Entity {ID:$entity_id})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[:HAS_PISA_BOND]-(interacting_pdb_res:PDBResidue)<-[:HAS_PDB_RESIDUE]-(partner_entity:Entity)-[:HAS_UNIPROT]->(uniprot:UniProt)
    WHERE NOT uniprot.ACCESSION CONTAINS '-'
    WITH DISTINCT uniprot.ACCESSION AS accession, uniprot.DESCR AS description, pdb_res.ID AS pdb_res_id, pdb_res.CHEM_COMP_ID AS pdb_res_code ORDER BY toInteger(pdb_res_id)
    RETURN accession, description, COLLECT(pdb_res_id) AS pdb_residues, COLLECT(pdb_res_code) AS pdb_residue_codes
    """

    mappings = run_query(query, parameters={
        "pdb_id": str(pdb_id),
        "entity_id": str(entity_id)
    })

    best_chain_id = get_best_chain_id(pdb_id, entity_id)
    result_list = []

    if not mappings:
        return []

    for mapping in mappings:
        (accession, description, pdb_residues, pdb_residue_codes) = mapping

        partner_fragment = {
            "dataType": "UniProt",
            "accession": accession,
            "name": description,
            "additionalData": {
                "bestChainId": best_chain_id,
                "entityId": int(entity_id)
            },
            "residues": []
        }

        for residues in util_common.split_nonconsecutive_residues_with_code([pdb_residues, pdb_residue_codes]):
            residue_fragment = {
                "startIndex": int(residues[0][0]),
                "endIndex": int(residues[0][0]),
                "startCode": residues[0][1],
                "endCode": residues[0][1],
                "indexType": "PDB"
            }
            if len(residues) != 1:
                residue_fragment["endIndex"] = int(residues[-1][0])
                residue_fragment["endCode"] = residues[-1][1]
            
            partner_fragment["residues"].append(residue_fragment)

        result_list.append(partner_fragment)

    return result_list


def get_annotations(pdb_id, entity_id):

    result_list = []
    best_chain_id = get_best_chain_id(pdb_id, entity_id)

    query = """
    MATCH (entry:Entry {ID:$pdb_id})-[:HAS_ENTITY]->(entity:Entity {ID:$entity_id})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)<-[res_rel:FUNPDBE_ANNOTATION_FOR {CHAIN_LABEL:entity.BEST_CHAIN_ID}]-(fun_group:FunPDBeResidueGroup)-[:FUNPDBE_RESIDUE_GROUP_OF]->(fun_entry:FunPDBeEntry)
    WHERE
        (NOT fun_entry.DATA_RESOURCE IN ["FoldX","Missense3D","KnotProt","Kincore","WEBnma","Frustratometer","SKEMPI","FireProt DB","dynamine","nextprot", "PDBTM"]) AND
        (NOT (fun_entry.DATA_RESOURCE="3Dcomplex" AND fun_group.LABEL IN ["Interior_residue", "Interface_residue", "Surface_residue"] ))
    WITH DISTINCT fun_entry.DATA_RESOURCE AS data_resource, fun_group.ORDINAL_ID AS ordinal_id, fun_group.LABEL AS group_label, fun_entry.RESOURCE_ENTRY_URL AS resource_url, toInteger(pdb_res.ID) AS pdb_res_id, pdb_res.CHEM_COMP_ID AS pdb_res_code, res_rel.CONFIDENCE_CLASSIFICATION AS conf_level, res_rel.CONFIDENCE_SCORE AS conf_score, res_rel.RAW_SCORE AS raw_score ORDER BY toInteger(pdb_res_id)
    RETURN DISTINCT data_resource, ordinal_id, group_label, resource_url, pdb_res_id AS pdb_residue, pdb_res_code AS pdb_residue_code, conf_level, conf_score, raw_score ORDER BY pdb_res_id
    """

    mappings = run_query(query, parameters={
        "pdb_id": str(pdb_id),
        "entity_id": str(entity_id)
    })

    if not mappings:
        return []

    dict_resource = {}

    for mapping in mappings:
        (data_resource, ordinal_id, group_label, resource_url, pdb_res, pdb_res_code, conf_level, conf_score, raw_score) = mapping

        # PDBE-2832 - Skip dynamine sidechain
        if data_resource == 'dynamine' and group_label == 'sidechain':
            continue
        # PDBE-2574 - remove akid and not high confidence cases
        elif data_resource == 'akid' and conf_level != 'high':
            continue
        # PDBE-4663 - Handle POPScomp data
        elif data_resource == "POPScomp_PDBML":
            data_resource = "Total SASA"

            if group_label.startswith("hydrophobic"):
                data_resource = "Hydrophobic SASA"
            elif group_label.startswith("hydrophilic"):
                data_resource = "Hydrophilic SASA"
        # PDBE-4664
        elif data_resource == "3Dcomplex":
            if group_label == "ASA_BiologicalUnit":
                data_resource = "ASA in assembly"
            elif group_label == "ASA_alone":
                data_resource = "ASA alone"
                
        # PDBE-4718
        elif data_resource == "EMVS":
            if group_label == "local_resolution_mapq":
                data_resource = "Mapq"
            elif group_label == "local_resolution_deepres":
                data_resource = "Deepres"
            elif group_label == "local_resolution_monores":
                data_resource = "Monores"
            elif group_label == "local_resolution_blocres":
                data_resource = "Blocres"
            else:
                data_resource = "Fscq"
                

        if dict_resource.get(data_resource) is None:
            dict_resource[data_resource] = {
                "url": resource_url,
                "residues": [pdb_res],
                "residue_codes": [pdb_res_code],
                "raw_scores": [raw_score],
                "conf_scores": [conf_score],
                "conf_levels": [conf_level],
                "group_labels": [group_label],
                "ordinal_ids": [ordinal_id]
            }
        else:
            dict_resource[data_resource]["residues"].append(pdb_res)
            dict_resource[data_resource]["residue_codes"].append(pdb_res_code)
            dict_resource[data_resource]["raw_scores"].append(raw_score)
            dict_resource[data_resource]["conf_scores"].append(conf_score)
            dict_resource[data_resource]["conf_levels"].append(conf_level)
            dict_resource[data_resource]["group_labels"].append(group_label)
            dict_resource[data_resource]["ordinal_ids"].append(ordinal_id)
    
    del mappings

    for data_resource, values in dict_resource.items():
        resource_fragment = {
            "name": data_resource,
            "accession": data_resource,
            "dataType": data_resource,
            "residues": [],
            "additionalData": {
                "bestChainId": best_chain_id,
                "entityId": int(entity_id),
                "shape": "chevron"
            }
        }

        incr = 0
        while incr < len(values["residues"]):
            residue = values["residues"][incr]
            residue_code = values["residue_codes"][incr]
            raw_score = values["raw_scores"][incr]
            conf_score = values["conf_scores"][incr]

            if raw_score == "None":
                raw_score = None
            if conf_score == "None":
                conf_score = None

            resource_fragment["residues"].append({
                "startIndex": int(residue),
                "endIndex": int(residue),
                "indexType": "PDB",
                "startCode": residue_code,
                "endCode": residue_code,
                "additionalData": {
                    "resourceUrl": dict_resource[data_resource]["url"],
                    "rawScore": float(raw_score) if raw_score is not None and raw_score != "" else None,
                    "confidenceScore": float(conf_score) if conf_score is not None and conf_score != "" else None,
                    "confidenceLevel": values["conf_levels"][incr],
                    "groupLabel": values["group_labels"][incr],
                    "ordinalId": int(values["ordinal_ids"][incr])
                }
            })
            incr += 1

        result_list.append(resource_fragment)

    del dict_resource

    return result_list



def get_domains(pdb_id, entity_id):

    result_list = []
    best_chain_id = get_best_chain_id(pdb_id, entity_id)
    
    # PDBE-2556: Add CATH-B only if different from CATH
    cath_temp_list = []

    cath_fragment = {
        "dataType": "CATH",
        "accession": "CATH domains",
        "name": "CATH domains",
        "additionalData": {
            "entityId": int(entity_id),
            "bestChainId": best_chain_id
        },
        "residues": []
    }

    cath_b_fragment = {
        "dataType": "CATH-B",
        "accession": "CATH-B domains",
        "name": "CATH-B domains",
        "additionalData": {
            "entityId": int(entity_id),
            "bestChainId": best_chain_id
        },
        "residues": []
    }

    pfam_fragment = {
        "dataType": "Pfam",
        "accession": "Pfam domains",
        "name": "Pfam domains",
        "additionalData": {
            "entityId": int(entity_id),
            "bestChainId": best_chain_id
        },
        "residues": []
    }

    scop_fragment = {
        "dataType": "SCOP",
        "accession": "SCOP domains",
        "name": "SCOP domains",
        "additionalData": {
            "entityId": int(entity_id),
            "bestChainId": best_chain_id
        },
        "residues": []
    }

    interpro_fragment = {
        "dataType": "InterPro",
        "accession": "InterPro annotations",
        "name": "InterPro annotations",
        "additionalData": {
            "entityId": int(entity_id),
            "bestChainId": best_chain_id
        },
        "residues": []
    }

    # retrieve CATH domains
    cath_query = """
    MATCH (entry:Entry {ID:$pdb_id})-[:HAS_ENTITY]->(entity:Entity {ID:$entity_id})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[rel:IS_IN_CATH_DOMAIN]->(cath:CATH) WHERE rel.STRUCT_ASYM_ID=entity.BEST_CHAIN_ID
    WITH DISTINCT cath.CATHCODE AS cath_code, cath.NAME AS domain_name, cath.DOMAIN AS domain_id, pdb_res.ID AS pdb_res_id, pdb_res.CHEM_COMP_ID AS pdb_res_code ORDER BY toInteger(pdb_res_id)
    RETURN cath_code, domain_name, domain_id, COLLECT(pdb_res_id) AS pdb_residues, COLLECT(pdb_res_code) AS pdb_residue_codes
    """

    cath_mappings = run_query(cath_query, parameters={
        "pdb_id": str(pdb_id),
        "entity_id": str(entity_id)
    })
    
    for mapping in cath_mappings:
        (cath_code, domain_name, domain_id, pdb_residues, pdb_residue_codes) = mapping

        for residues in util_common.split_nonconsecutive_residues_with_code([pdb_residues, pdb_residue_codes]):

            cath_temp_list.append((int(residues[0][0]), int(residues[-1][0]), cath_code))

            cath_fragment["residues"].append({
                "startIndex": int(residues[0][0]),
                "endIndex": int(residues[-1][0]),
                "startCode": residues[0][1],
                "endCode": residues[-1][1],
                "indexType": "PDB",
                "additionalData": {
                    "domainName": domain_name,
                    "domainId": cath_code,
                    "domain": domain_id
                }
            })

    if cath_fragment["residues"]:
        result_list.append(cath_fragment)
    
    del cath_mappings, cath_fragment

    # retrieve CATH-B domains
    cath_b_query = """
    MATCH (entry:Entry {ID:$pdb_id})-[:HAS_ENTITY]->(entity:Entity {ID:$entity_id})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[rel:IS_IN_CATH_B_DOMAIN]->(cath_b:CATHB) WHERE rel.STRUCT_ASYM_ID=entity.BEST_CHAIN_ID
    WITH DISTINCT cath_b.CATHCODE AS cath_b_code, cath_b.NAME AS domain_name, cath_b.DOMAIN AS domain_id, pdb_res.ID AS pdb_res_id, pdb_res.CHEM_COMP_ID AS pdb_res_code ORDER BY toInteger(pdb_res_id)
    RETURN cath_b_code, domain_name, domain_id, COLLECT(pdb_res_id) AS pdb_residues, COLLECT(pdb_res_code) AS pdb_residue_codes
    """

    cath_b_mappings = run_query(cath_b_query, parameters={
        "pdb_id": str(pdb_id),
        "entity_id": str(entity_id)
    })
    
    for mapping in cath_b_mappings:
        (cath_b_code, domain_name, domain_id, pdb_residues, pdb_residue_codes) = mapping

        for residues in util_common.split_nonconsecutive_residues_with_code([pdb_residues, pdb_residue_codes]):
            
            # PDBE-2556: Add CATH-B only if different from CATH
            if ((int(residues[0][0]), int(residues[-1][0]), cath_b_code)) in cath_temp_list:
                continue

            cath_b_fragment["residues"].append({
                "startIndex": int(residues[0][0]),
                "endIndex": int(residues[-1][0]),
                "startCode": residues[0][1],
                "endCode": residues[-1][1],
                "indexType": "PDB",
                "additionalData": {
                    "domainName": domain_name,
                    "domainId": cath_b_code,
                    "domain": domain_id
                }
            })

    if cath_b_fragment["residues"]:
        result_list.append(cath_b_fragment)
    
    del cath_b_mappings, cath_b_fragment

    # retrieve SCOP domains
    scop_query = """
    MATCH (entry:Entry {ID:$pdb_id})-[:HAS_ENTITY]->(entity:Entity {ID:$entity_id})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[rel:IS_IN_SCOP_DOMAIN]->(scop:SCOP) WHERE rel.STRUCT_ASYM_ID=entity.BEST_CHAIN_ID
    WITH DISTINCT rel.SCOP_ID AS scop_id, scop.SUNID AS scop_domain, scop.DESCRIPTION AS domain_name, pdb_res.ID AS pdb_res_id, pdb_res.CHEM_COMP_ID AS pdb_res_code ORDER BY toInteger(pdb_res_id)
    RETURN scop_id, scop_domain, domain_name, COLLECT(pdb_res_id) AS pdb_residues, COLLECT(pdb_res_code) AS pdb_residue_codes
    """
    
    scop_mappings = run_query(scop_query, parameters={
        "pdb_id": str(pdb_id),
        "entity_id": str(entity_id)
    })

    for mapping in scop_mappings:
        (scop_id, scop_domain, domain_name, pdb_residues, pdb_residue_codes) = mapping

        for residues in util_common.split_nonconsecutive_residues_with_code([pdb_residues, pdb_residue_codes]):
            scop_fragment["residues"].append({
                "startIndex": int(residues[0][0]),
                "endIndex": int(residues[-1][0]),
                "startCode": residues[0][1],
                "endCode": residues[-1][1],
                "indexType": "PDB",
                "additionalData": {
                    "domainName": domain_name,
                    "domainId": scop_domain,
                    "domain": scop_id
                }
            })

    if scop_fragment["residues"]:
        result_list.append(scop_fragment)
    
    del scop_mappings, scop_fragment

    # retrieve Pfam domains
    pfam_query = """
    MATCH (entry:Entry {ID:$pdb_id})-[:HAS_ENTITY]->(entity:Entity {ID:$entity_id})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[:MAP_TO_UNIPROT_RESIDUE]->(unp_res:UNPResidue)-[rel:IS_IN_PFAM]->(pfam:Pfam)
    WITH DISTINCT pfam.PFAM_ACCESSION AS pfam_domain, pfam.DESCRIPTION AS domain_name, pdb_res.ID AS pdb_res_id, pdb_res.CHEM_COMP_ID AS pdb_res_code ORDER BY toInteger(pdb_res_id)
    RETURN pfam_domain, domain_name, COLLECT(pdb_res_id) AS pdb_residues, COLLECT(pdb_res_code) AS pdb_residue_codes
    """

    pfam_mappings = run_query(pfam_query, parameters={
        "pdb_id": str(pdb_id),
        "entity_id": str(entity_id)
    })

    for mapping in pfam_mappings:
        (pfam_domain, domain_name, pdb_residues, pdb_residue_codes) = mapping

        for residues in util_common.split_nonconsecutive_residues_with_code([pdb_residues, pdb_residue_codes]):
            pfam_fragment["residues"].append({
                "startIndex": int(residues[0][0]),
                "endIndex": int(residues[-1][0]),
                "startCode": residues[0][1],
                "endCode": residues[-1][1],
                "indexType": "PDB",
                "additionalData": {
                    "domainName": domain_name,
                    "domainId": pfam_domain
                }
            })

    if pfam_fragment["residues"]:
        result_list.append(pfam_fragment)
    
    del pfam_mappings, pfam_fragment

    # retrieve InterPro domains
    interpro_query = """
    MATCH (entry:Entry {ID:$pdb_id})-[:HAS_ENTITY]->(entity:Entity {ID:$entity_id})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[:IS_IN_INTERPRO]->(interpro:Interpro)
    WITH DISTINCT interpro.INTERPRO_ACCESSION AS interpro_accession, interpro.NAME AS interpro_name, pdb_res.ID AS pdb_res_id, pdb_res.CHEM_COMP_ID AS pdb_res_code ORDER BY toInteger(pdb_res_id)
    RETURN interpro_accession, interpro_name, COLLECT(pdb_res_id) AS pdb_residues, COLLECT(pdb_res_code) AS pdb_residue_codes
    """

    interpro_mappings = run_query(interpro_query, parameters={
        "pdb_id": str(pdb_id),
        "entity_id": str(entity_id)
    })

    for mapping in interpro_mappings:
        (interpro_acc, interpro_name, pdb_residues, pdb_residue_codes) = mapping

        for residues in util_common.split_nonconsecutive_residues_with_code([pdb_residues, pdb_residue_codes]):
            interpro_fragment["residues"].append({
                "startIndex": int(residues[0][0]),
                "endIndex": int(residues[-1][0]),
                "startCode": residues[0][1],
                "endCode": residues[-1][1],
                "indexType": "PDB",
                "additionalData": {
                    "domainName": interpro_name,
                    "domainId": interpro_acc
                }
            })

    if interpro_fragment["residues"]:
        result_list.append(interpro_fragment)
    
    del interpro_mappings, interpro_fragment

    return result_list


def get_chains(pdb_id, entity_id):

    result_list = []
    query1 = """
    MATCH(entry:Entry {ID:$pdb_id})-[:HAS_ENTITY]->(entity:Entity {ID:$entity_id})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[chain_rel:IS_IN_CHAIN {OBSERVED:'Y'}]->(chain:Chain)
    OPTIONAL MATCH (pdb_res)-[val_rel:HAS_VALIDATION_DATA]->(val) WHERE chain.STRUCT_ASYM_ID=val_rel.STRUCT_ASYM_ID
    RETURN DISTINCT chain.STRUCT_ASYM_ID AS struct_asym_id, chain.AUTH_ASYM_ID AS auth_asym_id, pdb_res.ID AS pdb_res_id, pdb_res.CHEM_COMP_ID AS pdb_residue_code, labels(val)[0] ORDER BY toInteger(pdb_res_id)
    """
    query2 = """
    MATCH (entry:Entry {ID:$pdb_id})-[:HAS_ENTITY]->(entity:Entity {ID:$entity_id})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[chain_rel:IS_IN_CHAIN]->(chain:Chain)
    RETURN chain.STRUCT_ASYM_ID AS struct_asym_id, chain.AUTH_ASYM_ID AS auth_asym_id, pdb_res.ID AS pdb_res_id, pdb_res.CHEM_COMP_ID AS pdb_residue_code, chain_rel.OBSERVED AS observed, chain_rel.RAMA, chain_rel.ROTA, chain_rel.RSRZ ORDER BY toInteger(pdb_res_id)
    """
    params = {
        "pdb_id": str(pdb_id),
        "entity_id": str(entity_id)
    }
    mappings = run_query(query1, params) + run_query(query2, params)
    dict_chains = {}
    best_chain_id = get_best_chain_id(pdb_id, entity_id)

    for mapping in mappings:
        # in case of clashes, chirals, bonds, planes and symmetry clashes
        if len(mapping) == 5:
            (struct_asym_id, auth_asym_id, pdb_res_id, pdb_res_code, validation) = mapping
            validation = validation_switch.get(validation)
            key = (struct_asym_id, auth_asym_id, pdb_res_id, pdb_res_code)

            if dict_chains.get(key) is None:
                dict_chains[key] = [validation]
            else:
                dict_chains[key].append(validation)
            
        # in case of rama, rota and rsrz outliers
        if len(mapping) == 8:
            (struct_asym_id, auth_asym_id, pdb_res_id, pdb_res_code, observed, rama, rota, rsrz) = mapping
            key = (struct_asym_id, auth_asym_id, pdb_res_id, pdb_res_code)
            temp_validations = []

            if rota == 'OUTLIER':
                temp_validations.append("sidechain_outliers")
            if rama == 'OUTLIER':
                temp_validations.append("ramachandran_outliers")
            if rsrz is not None and float(rsrz) > RSRZ_OUTLIER_CUTOFF:
                temp_validations.append("RSRZ")
            if observed == 'N':
                temp_validations.append("MISSING")
            if dict_chains.get(key) is None:
                dict_chains[key] = temp_validations
            else:
                dict_chains[key].extend(temp_validations)

    del mappings

    chain_fragment = {}
    rsrz_fragment = {}
    
    for key in dict_chains.keys():
        (struct_asym_id, auth_asym_id, pdb_res_id, pdb_res_code) = key
        filtered_validation_list = list(filter(lambda x: x is not None, dict_chains[key]))
        quality_level = "high"

        # take care of rsrz first and then decide the level of quality
        rsrz_present = False

        try:
            rsrz_index = filtered_validation_list.index('RSRZ')
            del filtered_validation_list[rsrz_index]
            rsrz_present = True
        except ValueError as ve:
            pass

        validation_issue = "No validation issue reported"

        if len(filtered_validation_list) == 1:
            quality_level = "good"

            if filtered_validation_list[0] == "MISSING":
                validation_issue = "Missing residue"
                quality_level = "missing"

        elif len(filtered_validation_list) == 2:
            quality_level = "medium"
        elif len(filtered_validation_list) > 2:
            quality_level = "low"

        if len(filtered_validation_list) != 0 and validation_issue != "Missing residue":
            if len(filtered_validation_list) == 1:
                validation_issue = "Validation issue: " +", ".join(filtered_validation_list)
            else:
                validation_issue = "Validation issues: " +", ".join(filtered_validation_list)

        if chain_fragment.get(struct_asym_id) is None:
            chain_fragment[struct_asym_id] = {
                "dataType": "Chain",
                "accession": "Chain {}".format(struct_asym_id),
                "name": "Chain {}".format(struct_asym_id),
                "additionalData": {
                    "entityId": int(entity_id),
                    "bestChainId": best_chain_id,
                    "chainId": struct_asym_id,
                    "authAsymId": auth_asym_id,
                },
                "residues": []
            }

        chain_fragment[struct_asym_id]["residues"].append({
            "startIndex": int(pdb_res_id),
            "endIndex": int(pdb_res_id),
            "startCode": pdb_res_code,
            "endCode": pdb_res_code,
            "additionalData": {
                "validationIssue":validation_issue,
                "qualityLevel": quality_level
            }
        })

        if rsrz_present:
            if rsrz_fragment.get(struct_asym_id) is None:
                rsrz_fragment[struct_asym_id] = {
                    "dataType": "RSRZ",
                    "accession": "RSRZ Outlier Chain {}".format(struct_asym_id),
                    "name": "RSRZ Outlier Chain {}".format(struct_asym_id),
                    "additionalData": {
                        "entityId": int(entity_id),
                        "bestChainId": best_chain_id,
                        "shape": "circle",
                        "chainId": struct_asym_id,
                        "authAsymId": auth_asym_id,
                    },
                    "residues": []
                }

            rsrz_fragment[struct_asym_id]["residues"].append({
                "startIndex": int(pdb_res_id),
                "endIndex": int(pdb_res_id),
                "startCode": pdb_res_code,
                "endCode": pdb_res_code,
                "additionalData": {
                    "validationIssue": "RSRZ",
                    "qualityLevel": "rsrz"
                }
            })


    # sort based on chain_id and residue number
    # rsrz on top for a chain
    for struct_asym_id in sorted(chain_fragment.keys()):
        if rsrz_fragment.get(struct_asym_id):
            rsrz_fragment[struct_asym_id]["residues"] = sorted(rsrz_fragment[struct_asym_id]["residues"], key=itemgetter("startIndex"))
            result_list.append(rsrz_fragment[struct_asym_id])

        chain_fragment[struct_asym_id]["residues"] = sorted(chain_fragment[struct_asym_id]["residues"], key=itemgetter("startIndex"))
        result_list.append(chain_fragment[struct_asym_id])

    return result_list


@app.get('/variation/'+rePDBid+reSlashOrNot+'/<entity_id>'+reSlashOrNot)
def get_pdbe_pages_variation_api(pdbid, entity_id):

    pdbid = pdbid.strip("'")
    api_result = {
        pdbid: {}
    }

    sequence = get_pdb_sequence(pdbid, entity_id)

    if not sequence:
        bottle.response.status = 404
        return {}

    api_result[pdbid]["sequence"] = sequence["sequence"]
    api_result[pdbid]["length"] = sequence["length"]
    api_result[pdbid]["dataType"] = "VARIATION"
    api_result[pdbid]["data"] = []
    
    uniprot_mapping = get_uniprot_mapping(pdbid, entity_id)

    dict_accession_position = dict()
    return_dict = dict()

    for element in uniprot_mapping:
        accession = element["accession"]
        for residue_mapping in element["residues"]:
            pdb_start = residue_mapping["startIndex"]
            pdb_end = residue_mapping["endIndex"]
            unp_start = residue_mapping["unpStartIndex"]
            unp_end = residue_mapping["unpEndIndex"]

            incr = 0
            for index in range(unp_start, unp_end + 1):
                dict_accession_position[f"{accession}_{str(index)}"] = pdb_start + incr
                incr += 1

        variation_data = get_uniprot_variation_api(accession)
        
        # process only if variation data exists for the accssion
        if variation_data.get(accession):
            for feature in variation_data[accession]["features"]:
                index = feature["begin"]
                dict_key = accession +"_" +index

                if dict_accession_position.get(dict_key) is not None:
                    feature.update({
                        "begin": str(dict_accession_position[dict_key]),
                        "end": str(dict_accession_position[dict_key])
                    })

            # add only if there are features
            if variation_data[accession]["features"]:
                return_dict[accession] = {
                    "accession": accession,
                    "features": variation_data[accession]["features"]
                }

    # get KB variations
    # kb_variations = get_kb_variations(pdbid, entity_id)
    
    # if kb_variations:
    #     return_dict[pdbid] = {
    #         "accession": pdbid,
    #         "features": kb_variations
    #     }
    
    data = return_dict

    if not data:
        bottle.response.status = 404
        return {}

    api_result[pdbid]["data"] = data

    bottle.response.status = 200
    return api_result


@app.get('/protvista/variation/'+rePDBid+reSlashOrNot+'/<entity_id>'+reSlashOrNot)
def get_pdbe_pages_protvista_variation_api(pdbid, entity_id):
    
    generic_response = get_pdbe_pages_variation_api(pdbid, entity_id)

    if not generic_response:
        bottle.response.status = 404
        return {}

    bottle.response.status = 200
    return convert_to_protvista("entry", "variation", generic_response)

def get_rfam(pdb_id, entity_id):
    
    result_list = []

    query = """
    MATCH (entry:Entry {ID:$pdb_id})-[:HAS_ENTITY]->(entity:Entity {ID:$entity_id})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[:IS_IN_RFAM_DOMAIN]->(rfam:RfamFamily), (entity)-[:HAS_RFAM]->(rfam)
    WITH rfam.RFAM_ACC AS rfam_accession, rfam.DESCRIPTION AS rfam_desc, toInteger(pdb_res.ID) AS pdb_res_id, pdb_res.CHEM_COMP_ID AS amino_acid_code ORDER BY pdb_res_id
    RETURN rfam_accession, rfam_desc, COLLECT(pdb_res_id), COLLECT(amino_acid_code)
    """

    mappings = run_query(query, parameters={
        "pdb_id": str(pdb_id),
        "entity_id": str(entity_id)
    })
    
    if not mappings:
        return None

    best_chain_id = get_best_chain_id(pdb_id, entity_id)

    rfam_fragment = {
        "dataType": "Rfam",
        "accession": "Rfam domains",
        "name": "Rfam domains",
        "additionalData": {
            "entityId": int(entity_id),
            "bestChainId": best_chain_id
        },
        "residues": []
    }

    for row in mappings:
        (rfam_accession, rfam_desc, pdb_residues, amino_acid_codes) = row

        for residue in util_common.split_nonconsecutive_residues_with_code([pdb_residues, amino_acid_codes]):
            pdb_start = residue[0][0]
            pdb_end = residue[-1][0]
            pdb_start_code = residue[0][1]
            pdb_end_code = residue[-1][1]

            residue_fragment = {
                "startIndex": pdb_start,
                "endIndex": pdb_end,
                "startCode": pdb_start_code,
                "endCode": pdb_end_code,
                "indexType": "PDB",
                "additionalData": {
                    "domainName": rfam_desc,
                    "domainId": rfam_accession
                }
            }

            rfam_fragment["residues"].append(residue_fragment)
    
    if not rfam_fragment["residues"]:
        return None
    
    return [rfam_fragment]

def get_pdb_sequence(pdb_id, entity_id):

    query = """
    MATCH (entry:Entry {ID:$pdb_id})-[:HAS_ENTITY]->(entity:Entity {ID:$entity_id})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)
    WITH pdb_res.ID AS pdb_res_id, pdb_res.CHEM_COMP_ID AS pdb_res_code ORDER BY toInteger(pdb_res_id)
    RETURN COLLECT(pdb_res_code)
    """

    mappings = run_query(query, parameters={
        "pdb_id": str(pdb_id),
        "entity_id": str(entity_id)
    })
    
    if not mappings:
        return None
    
    residues = mappings[0][0]
    
    if not residues:
        return None

    sequence = reduce(lambda x, y: x + y, list(map(lambda x: util_common.get_amino_three_to_one(x), residues)))
    seq_length = len(residues)

    return {
        "sequence": sequence,
        "length": seq_length
    }


def get_best_chain_id(pdb_id, entity_id):

    query = """
    MATCH (entry:Entry {ID:$pdb_id})-[:HAS_ENTITY]->(entity:Entity {ID:$entity_id})
    RETURN entity.BEST_CHAIN_ID AS best_chain_id
    """

    mappings = run_query(query, parameters={
        "pdb_id": str(pdb_id),
        "entity_id": str(entity_id)
    })
    
    if not mappings:
        return None
    
    return mappings[0][0]


def get_idp_mapping(pdb_id, entity_id):

    query = """
    MATCH (entry:Entry)-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[:MAP_TO_UNIPROT_RESIDUE]->(unp_res:UNPResidue)
    WHERE entry.ID=$pdb_id AND entity.ID=$entity_id AND unp_res.IDP_REGION='Y'
    WITH DISTINCT toInteger(pdb_res.ID) AS pdb_res_id, pdb_res.CHEM_COMP_ID AS pdb_res_code ORDER BY pdb_res_id
    RETURN COLLECT(pdb_res_id), COLLECT(pdb_res_code)
    """

    mappings = run_query(query, parameters={
        "pdb_id": pdb_id,
        "entity_id": entity_id
    })
    
    if not mappings:
        return []
    elif not mappings[0][0]:
        return []

    best_chain_id = get_best_chain_id(pdb_id, entity_id)

    idp_fragment = {
        "dataType": "MobiDB",
        "accession": "MobiDB",
        "name": "MobiDB",
        "additionalData": {
            "bestChainId": best_chain_id,
            "entityId": int(entity_id)
        },
        "residues": []
    }

    for residues in util_common.split_nonconsecutive_residues_with_code(mappings[0]):

        start_res = residues[0][0]
        end_res = residues[-1][0]
        start_code = residues[0][1]
        end_code = residues[-1][1]

        idp_fragment["residues"].append({
            "startIndex": start_res,
            "endIndex": end_res,
            "startCode": start_code,
            "endCode": end_code,
            "indexType": "PDB"
        })

    return [idp_fragment]
    

def get_flex_predictions(pdb_id, entity_id):
    
    query = """
    MATCH
        (entry:Entry)-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->(pdbRes:PDBResidue)<-[resRel:FUNPDBE_ANNOTATION_FOR]-(funGroup:FunPDBeResidueGroup)-
        [:FUNPDBE_RESIDUE_GROUP_OF]->(funEntry:FunPDBeEntry)
    WHERE entry.ID=$pdb_id AND entity.ID=$entity_id AND funEntry.DATA_RESOURCE IN ['KnotProt', 'WEBnma', 'Kincore', 'dynamine']
    WITH
        funEntry.DATA_RESOURCE AS dataResource, resRel.CONFIDENCE_SCORE AS confScore, resRel.CONFIDENCE_CLASSIFICATION AS confClass, resRel.RAW_SCORE AS rawScore,
        pdbRes, entity.BEST_CHAIN_ID AS bestChain, resRel.CHAIN_LABEL AS chainLabel, toInteger(funGroup.ORDINAL_ID) AS siteId, funGroup.LABEL AS label,
        funEntry.RESOURCE_ENTRY_URL AS resUrl
    MATCH (pdbRes)-[chainRel:IS_IN_CHAIN]->(chain:Chain) WHERE chain.STRUCT_ASYM_ID=bestChain AND chain.AUTH_ASYM_ID=chainLabel
    RETURN
        dataResource, siteId, label, resUrl, toInteger(chainRel.AUTH_SEQ_ID) AS pdbResNum, pdbRes.CHEM_COMP_ID AS pdbCode,
        confClass, toFloat(confScore) AS confScore, toFloat(rawScore) AS rawScore
    ORDER BY pdbResNum
    """

    mappings = run_query(query, parameters={
        "pdb_id": str(pdb_id),
        "entity_id": str(entity_id)
    })

    if not mappings:
        return []

    dict_resource = {}
    result_list = []

    for mapping in mappings:
        (data_resource, ordinal_id, group_label, resource_url, pdb_res, pdb_res_code, conf_level, conf_score, raw_score) = mapping
        
        # PDBE-2832 - Skip dynamine sidechain
        if data_resource == "dynamine":
            if group_label == "sidechain":
                continue
            elif group_label == "efoldmine":    
                data_resource = group_label
        # PDBE-4733: Skip none labels for Kincore
        elif data_resource == "Kincore":
            group_label = group_label.split(" ")[-1]
            if group_label == "None":
                continue
            data_resource = "KinCore"

        if not dict_resource.get(data_resource):
            dict_resource[data_resource] = {
                "url": resource_url,
                "residues": [pdb_res],
                "residue_codes": [pdb_res_code],
                "raw_scores": [raw_score],
                "conf_scores": [conf_score],
                "conf_levels": [conf_level],
                "group_labels": [group_label],
                "ordinal_ids": [ordinal_id]
            }
        else:
            dict_resource[data_resource]["residues"].append(pdb_res)
            dict_resource[data_resource]["residue_codes"].append(pdb_res_code)
            dict_resource[data_resource]["raw_scores"].append(raw_score)
            dict_resource[data_resource]["conf_scores"].append(conf_score)
            dict_resource[data_resource]["conf_levels"].append(conf_level)
            dict_resource[data_resource]["group_labels"].append(group_label)
            dict_resource[data_resource]["ordinal_ids"].append(ordinal_id)
    
    del mappings

    for resource, data in dict_resource.items():
        resource_fragment = {
            "name": resource,
            "accession": resource,
            "dataType": resource,
            "residues": [],
            "additionalData": {
                "bestChainId": get_best_chain_id(pdb_id, entity_id),
                "entityId": int(entity_id),
                "shape": "chevron"
            }
        }

        incr = 0
        while incr < len(data["residues"]):
            residue = data["residues"][incr]
            residue_code = data["residue_codes"][incr]
            raw_score = data["raw_scores"][incr]
            conf_score = data["conf_scores"][incr]

            if raw_score == "None":
                raw_score = None
            if conf_score == "None":
                conf_score = None

            resource_fragment["residues"].append({
                "startIndex": int(residue),
                "endIndex": int(residue),
                "indexType": "PDB",
                "startCode": residue_code,
                "endCode": residue_code,
                "additionalData": {
                    "resourceUrl": data["url"],
                    "rawScore": float(raw_score) if raw_score is not None and raw_score != "" else None,
                    "confidenceScore": float(conf_score) if conf_score is not None and conf_score != "" else None,
                    "confidenceLevel": data["conf_levels"][incr],
                    "groupLabel": data["group_labels"][incr],
                    "ordinalId": int(data["ordinal_ids"][incr])
                }
            })
            incr += 1

        result_list.append(resource_fragment)

    del dict_resource

    return result_list


def get_kb_variations(entry_id, entity_id):
    query = """
    MATCH
        (entry:Entry)-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->
        (pdbRes:PDBResidue)
        <-[resRel:FUNPDBE_ANNOTATION_FOR]-(funGroup:FunPDBeResidueGroup)-
        [:FUNPDBE_RESIDUE_GROUP_OF]->(funEntry:FunPDBeEntry)
    WHERE entry.ID=$entry_id AND entity.ID=$entity_id AND
    funEntry.DATA_RESOURCE IN ['FireProt DB']
    WITH
        toInteger(pdbRes.ID) AS pdbRes, pdbRes.CHEM_COMP_ID AS pdbResCode,
        resRel.AA_VARIANT AS variant, resRel.AA_VARIANT_CAUSES AS causes,
        SPLIT(funEntry.SQL_ID, '_')[0] AS pdbId, funEntry.RESOURCE_ENTRY_URL AS
        entryUrl, funGroup.LABEL AS label, funEntry.DATA_RESOURCE AS resource,
        resRel.RAW_SCORE AS rawScore
    ORDER BY pdbRes
    RETURN pdbRes, pdbResCode, variant, causes, pdbId, entryUrl, label, resource, rawScore
    """
    
    mappings = run_query(query, parameters={
        "entry_id": str(entry_id),
        "entity_id": str(entity_id)
    })

    if not mappings:
        return []

    result_list = []

    # process KB variations (FireProt DB, FoldX, Missense3D etc.)
    for row in mappings:
        (
            pdb_res_id,
            pdb_one_code,
            variant,
            variant_causes,
            pdb_id,
            entry_url,
            label,
            resource,
            raw_score
        ) = row

        variant_one_code = get_amino_three_to_one(variant)
        genomic_location = f"{resource}_{pdb_id}_{pdb_one_code}>{variant_one_code}"
        clinical_signifance = "fireprotdb"
        
        if resource == "Missense3D":
            clinical_signifance = "missense"
        elif resource == "FoldX":
            clinical_signifance = "foldx"

        result_data = {
            "type": "VARIANT",
            "alternativeSequence": variant_one_code,
            "begin": str(pdb_res_id),
            "end": str(pdb_res_id),
            "association": [],
            "wildType": pdb_one_code,
            "polyphenPrediction": "",
            "polyphenScore": None,
            "clinicalSignificances": clinical_signifance,
            "siftScore": None,
            "consequenceType": variant_causes,
            "genomicLocation": genomic_location,
            "sourceType": "prediction",
            "category": label,
            "resourceUrl": entry_url,
            "rawScore": raw_score,
        }

        result_list.append(result_data)
        
    return result_list
