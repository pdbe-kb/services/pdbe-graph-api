#!/usr/bin/env python
# encoding: utf-8

import os

print("=" * 80)
print("NEO4J_DEST_URL: %s" % os.environ.get("NEO4J_DEST_URL"))
print("OUTPUT_JSON_PATH: %s" % os.environ.get("OUTPUT_JSON_PATH"))
print("PATH_PREFIX: %s" % os.environ.get("PATH_PREFIX"))
print("=" * 80)


# Regular expressions used across application
reCHAINid = r"<pdb_chain_id:re:\w{1}>"  # TODO is any better possible?
rePDBid = r"<pdbid:re:\d{1}\w{3}>"
reHETid = r"<hetcode:re:\w{1,4}>"
reSlashOrNot = r"<:re:/{0,1}>"
reResidue = r"<unp_res:re:\d{1,4}>"
rePDBComplex = r"<pdb_complex:re:PDB-CPX-\d+>"
reSEQid = r"<pdb_seq_id:re:\d+>"
reIdentity = r"<seq_identity:re:\d+>"

JSON_GEN_CONFIG = {
    "get_uniprot_best_non_overlapping_structures_api": "/uniprot/best_non_overlapping_structures",
    "get_uniprot_protvista_domains_api": "/uniprot/protvista/domains",
    "get_uniprot_generic_unipdb_api": "/uniprot/unipdb",
    "get_uniprot_protvista_unipdb_api": "/uniprot/protvista/unipdb",
    "get_uniprot_generic_interface_residues_api": "/uniprot/interface_residues",
    "get_uniprot_protvista_interface_residues_api": "/uniprot/protvista/interface_residues",
    "get_uniprot_interaction_partners_api": "/uniprot/interaction_partners",
    "get_uniprot_generic_ligand_sites_api": "/uniprot/ligand_sites",
    "get_uniprot_protvista_ligand_sites_api": "/uniprot/protvista/ligand_sites",
    "get_uniprot_generic_annotations_api": "/uniprot/annotations",
    "get_uniprot_protvista_annotations_api": "/uniprot/protvista/annotations",
    "get_uniprot_protvista_secondary_structures_api": "/uniprot/protvista/secondary_structures",
    "get_uniprot_ligands_api": "/uniprot/ligands",
    "get_uniprot_summary_stats_api": "/uniprot/summary_stats",
    "get_uniprot_processed_proteins_api": "/uniprot/processed_proteins",
    "get_uniprot_protvista_processed_proteins_api": "/uniprot/protvista/processed_proteins",
    "get_uniprot_annotation_partners_api": "/uniprot/annotation_partners",
}
