import argparse
import os
from datetime import datetime
import gzip
import json
import traceback
import sys
import re

# parent_dir_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# sys.path.insert(0, "{}/pdbegraphapi".format(parent_dir_path))

from pdbegraphapi.config import JSON_GEN_CONFIG

def prepare_directories(out_dir: str, path_prefix: str):
    os.makedirs(out_dir, exist_ok=True)
    print(f"Created directory: {out_dir}")

    for _, dir in JSON_GEN_CONFIG.items():
        to_create = f"{out_dir}/{path_prefix}{dir}"
        os.makedirs(to_create, exist_ok=True)
        print(f"Created directory: {to_create}")

def generate_json(accession, out_dir, url_prefix):
    import pdbegraphapi.uniprot as uniprot_module

    dict_response = {}

    try:
        main_start = datetime.now()

        for api_call in JSON_GEN_CONFIG.keys():
            start = datetime.now()
            print("Processing {} -> {}: Started at {}".format(api_call, accession, start.strftime("%Y-%m-%d %H:%M:%S")))
            api_result = None
            
            # if api_call == "get_uniprot_interaction_partners_api":
            #     print("{} : Using previous generated response for get_uniprot_protvista_interface_residues_api call".format(accession))
            #     api_result = convert_to_interaction_partners_response(accession, dict_response)
            if api_call == "get_uniprot_summary_stats_api":
                print("{} : Summary stats call, using previous generated responses".format(accession))
                api_result = convert_to_summary_stats_response(accession, dict_response)
            else:
                api_result = getattr(uniprot_module, api_call)(accession)

                # set response in dictionary to be used later
                dict_response[api_call] = api_result

            if api_result is not None:
                with gzip.open("{}/{}{}/{}".format(out_dir, url_prefix, JSON_GEN_CONFIG[api_call], accession), "wt") as out_json:
                    json.dump(api_result, out_json)
            
            end = datetime.now()
            print("Processed {} -> {}: Ended at {}, duration: {}".format(api_call, accession, end.strftime("%Y-%m-%d %H:%M:%S"), (end - start)))

        main_end = datetime.now()
        print("Processed {}. Time taken: {}".format(accession, (main_end - main_start)), flush=True)
        
        return True

    except Exception as e:
        print("Error processing {} -> {}".format(accession, e))
        return False
    
    finally:
        del dict_response


def convert_to_interaction_partners_response(in_accession, response_dict):
    
    input_response = response_dict.get("get_uniprot_protvista_interface_residues_api")

    api_result = {
        in_accession: []
    }

    temp_list = []

    # return None if no response, usually 404 cases - this will make sure no JSON file is created
    if input_response is None or not input_response:
        return None

    for data in input_response[in_accession]["tracks"][0]["data"]:
        
        accession = data["accession"]
        name = accession
        search_result = re.search("^(.*) \(([A-Z0-9-]+)\)$", accession)
        
        if search_result:
            accession = search_result.group(2)
            name = search_result.group(1)

        temp_list.append({
            "accession": accession,
            "pdbs": data["pdb_entities"],
            "name": name
        })

    # sort the list based on highest number of PDB entities
    temp_list = sorted(temp_list, key=lambda x: len(x["pdbs"]), reverse=True)

    for record in temp_list:
        api_result[in_accession].append({
            record["accession"]: {
                "pdbs": record["pdbs"],
                "name": record["name"]
            }
        })

    return api_result


def convert_to_summary_stats_response(in_accession, response_dict):
    import pdbegraphapi.uniprot as uniprot_module

    fails = 0
    final_result = {
        in_accession: {
            "pdbs": 0,
            "ligands": 0,
            "interaction_partners": 0,
            "annotations": 0,
            "similar_proteins": 0
        }
    }
    
    try:
        unipdb_response = response_dict.get("get_uniprot_protvista_unipdb_api")

        if unipdb_response:
            final_result[in_accession]["pdbs"] = len(unipdb_response[in_accession]["tracks"][0]["data"])

    except Exception as err:
        fails += 1

    try:
        ligands_response = response_dict.get("get_uniprot_ligands_api")

        if ligands_response:
            final_result[in_accession]["ligands"] = len(ligands_response[in_accession])

    except Exception as err:
        fails += 1

    try:
        interactions_response = response_dict.get("get_uniprot_protvista_interface_residues_api")

        if interactions_response:
            final_result[in_accession]["interaction_partners"] = len(interactions_response[in_accession]["tracks"][0]["data"])

    except Exception as err:
        fails += 1

    try:
        final_result[in_accession]["annotations"] = 0
        annotations_response = getattr(uniprot_module, "exists_annotations")(in_accession)

        if annotations_response:
            final_result[in_accession]["annotations"] = 1


    except Exception as err:
        fails += 1

    try:
        similar_proteins_result = getattr(uniprot_module, "get_similar_proteins")(in_accession)
        final_result[in_accession]["similar_proteins"] = len(similar_proteins_result)

        similar_proteins_result = getattr(uniprot_module, "get_similar_proteins_identical_uniprot_api")(in_accession, "90")
        
        if similar_proteins_result:
            final_result[in_accession]["similar_proteins"] += len(similar_proteins_result[in_accession])
    
    except Exception as err:
        fails += 1

    # return None if none of them have response, usually 404 cases - this will make sure no JSON file is created
    if fails == 5 or (final_result[in_accession]["pdbs"] == 0 and final_result[in_accession]["ligands"] == 0 and final_result[in_accession]["interaction_partners"] == 0 and final_result[in_accession]["annotations"] == 0 and final_result[in_accession]["similar_proteins"] == 0):
        print("{} : No summary stats, no JSON file created".format(in_accession))
        return None

    return final_result

def run_generation():
    parser = argparse.ArgumentParser(description="PDBe Graph API static JSON generator")
    parser.add_argument("-i", "--accession", type=str, help="A UniProt accession.", required=True)
    parser.add_argument("-o", "--out-dir", type=str, help="Out directory", required=True)
    parser.add_argument("-p", "--path-prefix", type=str, help="Path prefix", required=True)
    parser.add_argument("-b", "--bolt-url", type=str, help="Bolt URL", required=True)
    parser.add_argument("-u", "--neo4j-username", type=str, help="Neo4J username", required=True)
    parser.add_argument("-c", "--neo4j-password", type=str, help="Neo4J password", required=True)
    
    args = parser.parse_args()

    os.environ['NEO4J_DEST_URL'] = args.bolt_url
    os.environ['NEO4J_USERNAME'] = args.neo4j_username
    os.environ['NEO4J_PASSWORD'] = args.neo4j_password

    generate_json(args.accession, args.out_dir, args.path_prefix)


def run_prepare():
    parser = argparse.ArgumentParser(description="PDBe Graph API static JSON generator")
    parser.add_argument("-o", "--out-dir", type=str, help="Out directory", required=True)
    parser.add_argument("-p", "--path-prefix", type=str, help="Path prefix", required=True)

    args = parser.parse_args()

    prepare_directories(args.out_dir, args.path_prefix)

if __name__ == "__main__":
    # run_prepare()
    run_generation()
