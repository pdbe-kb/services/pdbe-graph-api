#!/usr/bin/env python

import bottle

from decimal import Decimal
import re

from pdbegraphapi import request_handler

from pdbegraphapi import util_common
from pdbegraphapi.neo4j_model import run_query
from pdbegraphapi.residue import get_mappings_for_residue_binding_site, get_sequence
from pdbegraphapi.common_params import funpdbe_biophysical_resources, funpdbe_conf_color_code, seq_conservation_aa_colors
from pdbegraphapi.util_common import get_amino_one_to_three, get_amino_three_to_one
from pdbegraphapi.amino_acid_codes import amino_acid_codes_one_to_three

from pdbegraphapi.config import rePDBid, reSlashOrNot, rePDBComplex, reCHAINid, reSEQid

print('[PDB] Starting')

BONDLENGTH_QUANTIZE_DECIMAL = Decimal("0.001")
list_re = r'[\s\']'

app = bottle.Bottle()

# Installs the request handler plugin
app.install(request_handler)


print('[PDB] Loaded')

@app.get('/entry/summary/'+rePDBid+reSlashOrNot)
def entry_summary(pdbid):

    bottle.response.status = 200
    return {
        pdbid: {}
    }


"""
@api {get} pdb/binding_sites/:pdbId Get binding sites for a PDB entry
@apiName GetPDBBindingSites
@apiGroup PDB
@apiDescription This call provides details on binding sites in the entry as per STRUCT_SITE records in PDB files (or mmcif equivalent thereof), such as ligand, residues in the site, 
description of the site, etc.
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID
"""
@app.get('/binding_sites/'+rePDBid+reSlashOrNot)
def get_binding_sites_for_entry_api(pdbid):

    pdbid = pdbid.strip("'")

    api_result = {
        pdbid: []
    }

    result, status = get_binding_sites_for_entry(pdbid)

    if status == 404:
        bottle.response.status = 404
        return api_result

    api_result[pdbid] = result

    bottle.response.status = 200
    return api_result


def get_binding_sites_for_entry(entry_id):

    site_dict = {}
    site_ligand_dict = {}
    site_boundligand_dict = {}
    site_residue_dict = {}
    final_result = []
    no_results = 0

    query = """
    MATCH (entry:Entry {ID:$entry_id})-[relation:HAS_BINDING_SITE]->(site:BindingSite)
    OPTIONAL MATCH (site)<-[ligand_site_relation:IS_IN_BINDING_SITE]-(ligand_chain:BoundLigand)<-[ligand_relation:IS_AN_INSTANCE_OF]-(ligand_entity:Entity)-[:IS_A]->(lig_chem_comp:ChemicalComponent)
    OPTIONAL MATCH (site)-[:BOUNDED_BY]->(boundligand_chain:BoundLigand)<-[bound_relation:IS_AN_INSTANCE_OF]-(boundligand_entity:Entity)-[:IS_A]->(boundlig_chem_comp:ChemicalComponent)
    RETURN site.ID, site.DETAILS, site.EVIDENCE_CODE, boundlig_chem_comp.ID, boundlig_chem_comp.NAME, boundlig_chem_comp.FORMULA, boundligand_chain.AUTH_ASYM_ID, boundligand_chain.STRUCT_ASYM_ID, 
    boundligand_chain.AUTH_SEQ_ID, boundligand_entity.ID, boundligand_chain.ID, boundligand_chain.PDB_INS_CODE, lig_chem_comp.ID, lig_chem_comp.NAME, lig_chem_comp.FORMULA, ligand_chain.AUTH_ASYM_ID, ligand_chain.AUTH_SEQ_ID, 
    ligand_chain.STRUCT_ASYM_ID, ligand_site_relation.SYMMETRY_SYMBOL, ligand_entity.ID, ligand_chain.ID ORDER BY site.ID
    """

    mappings = run_query(query, parameters={
        'entry_id': str(entry_id)
    })

    if(len(mappings) == 0):
        no_results += 1

    for mapping in mappings:

        (site_id, site_name, site_evidence, bound_ligand, bound_ligand_name, bound_ligand_formula, bound_auth_asym_id, bound_struct_asym_id, bound_auth_seq_id, bound_entity_id,
         bound_residue_id, bound_pdb_ins_code, ligand, ligand_name, ligand_formula, ligand_auth_asym_id, ligand_auth_seq_id, ligand_struct_asym_id, ligand_sym_symbol, ligand_entity_id,
         ligand_residue_id) = mapping

        boundligand_to_list = (bound_ligand, bound_ligand_name, bound_ligand_formula, bound_auth_asym_id, bound_struct_asym_id,
                               bound_auth_seq_id, bound_entity_id, bound_residue_id, bound_pdb_ins_code)

        ligand_to_list = (ligand, ligand_auth_asym_id, ligand_auth_seq_id,
                          ligand_struct_asym_id, ligand_sym_symbol, ligand_entity_id, ligand_residue_id, None)
        
        if(site_dict.get(site_id) is None):
            site_dict[site_id] = (site_name, site_evidence)

        # usage of optional match may cause bound ligands with null values, so ignore those
        if bound_ligand is not None:
            if(site_boundligand_dict.get(site_id) is None):
                site_boundligand_dict[site_id] = [boundligand_to_list]
            else:
                site_boundligand_dict[site_id].append(boundligand_to_list)

        # usage of optional match may cause ligands with null values, so ignore those
        if(ligand is not None):
            if(site_ligand_dict.get(site_id) is None):
                site_ligand_dict[site_id] = [ligand_to_list]
            else:
                site_ligand_dict[site_id].append(ligand_to_list)

    del mappings

    query = """
    MATCH (entry:Entry {ID:$entry_id})-[relation:HAS_BINDING_SITE]->(site:BindingSite)
    MATCH (entry)-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[res_relation:IS_IN_BINDING_SITE]->(site), (pdb_res)-[chain_rel:IS_IN_CHAIN]->(chain:Chain)
    WITH entity, site, pdb_res, res_relation, chain_rel, chain
    RETURN DISTINCT site.ID, site.DETAILS, site.EVIDENCE_CODE, pdb_res.ID, entity.ID, pdb_res.CHEM_COMP_ID, res_relation.AUTH_ASYM_ID, 
    res_relation.AUTH_SEQ_ID, res_relation.STRUCT_ASYM_ID, res_relation.SYMMETRY_SYMBOL, chain_rel.PDB_INS_CODE ORDER BY site.ID, toInteger(entity.ID), res_relation.AUTH_ASYM_ID, toInteger(pdb_res.ID)
    """

    mappings = run_query(query, parameters={
        'entry_id': str(entry_id)
    })

    if(len(mappings) == 0):
        no_results += 1

    for mapping in mappings:

        (site_id, site_name, site_evidence, pdb_res, pdb_res_entity_id, pdb_res_chem_comp_id, pdb_res_auth_asym_id, pdb_res_auth_seq_id, pdb_res_struct_asym_id,
         pdb_res_sym_symbol, pdb_ins_code) = mapping

        residue_to_list = (pdb_res_chem_comp_id, pdb_res_auth_asym_id, pdb_res_auth_seq_id,
            pdb_res_struct_asym_id, pdb_res_sym_symbol, pdb_res_entity_id, pdb_res, pdb_ins_code)
        
        if(site_ligand_dict.get(site_id) is None):
            site_ligand_dict[site_id] = [residue_to_list]
        else:
            site_ligand_dict[site_id].append(residue_to_list)
    
    for key in site_dict.keys():
        (site_name, evidence) = site_dict[key]
        temp = {
            "site_id": key,
            "evidence_code": evidence,
            "details": site_name,
            "site_residues": [],
            "ligand_residues": []
        }

        if site_boundligand_dict.get(key) is not None:
            for result in list(set(site_boundligand_dict[key])):
                (bound_ligand, bound_ligand_name, bound_ligand_formula, bound_auth_asym_id,
                bound_struct_asym_id, bound_auth_seq_id, bound_entity_id, bound_residue_id, bound_pdb_ins_code) = result

                temp["ligand_residues"].append({
                    "entity_id": None if bound_entity_id is None else int(bound_entity_id),
                    "residue_number": None if bound_residue_id is None else int(bound_residue_id),
                    "author_insertion_code": bound_pdb_ins_code,
                    "chain_id": bound_auth_asym_id,
                    "author_residue_number": None if bound_auth_seq_id is None else int(bound_auth_seq_id),
                    "chem_comp_id": bound_ligand,
                    "struct_asym_id": bound_struct_asym_id
                })
        
        if site_ligand_dict.get(key) is not None:
            for result in list(set(site_ligand_dict[key])):

                (ligand, ligand_auth_asym_id, ligand_auth_seq_id, ligand_struct_asym_id,
                ligand_sym_symbol, ligand_entity_id, ligand_residue_id, ligand_pdb_ins_code) = result

                temp["site_residues"].append({
                    "entity_id": None if ligand_entity_id is None else int(ligand_entity_id),
                    "residue_number": int(ligand_residue_id),
                    "author_insertion_code": ligand_pdb_ins_code,
                    "chain_id": ligand_auth_asym_id,
                    "author_residue_number": None if ligand_auth_seq_id is None else int(ligand_auth_seq_id),
                    "chem_comp_id": ligand,
                    "struct_asym_id": ligand_struct_asym_id,
                    "symmetry_symbol": ligand_sym_symbol
                })

        temp["site_residues"] = sorted(temp["site_residues"], key = lambda x: (x["entity_id"], x["chain_id"], x["residue_number"]))
        temp["ligand_residues"] = sorted(temp["ligand_residues"], key = lambda x: (x["entity_id"], x["chain_id"], x["residue_number"]))
        final_result.append(temp)

    if(no_results == 2):
        return {}, 404

    return final_result, 200

"""
@api {get} pdb/secondary_structure/:pdbId Get secondary structures for a PDB entry
@apiName GetPDBSecondaryStructures
@apiGroup PDB
@apiDescription This call provides details about residue ranges of regular secondary structure (alpha helices and beta strands) found in protein chains of the entry. For strands, 
sheet id can be used to identify a beta sheet.
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID

@apiUse GenericResidueTerms
@apiUse GenericChemCompTerms
@apiSuccess {Object[]} molecules List of molecules (i.e. mmcif entities).
@apiSuccess {Object[]} helices Description of alpha helices.
@apiSuccess {Object[]} strands Description of beta sheets and strands.
@apiSuccess {Integer} sheet_id Sheet identifier helpful in finding ranges that form a single beta sheet.

@apiExample {json=./examples/success/secondary_structures.json} apiSuccessExample Example success response JSON
"""
@app.get('/secondary_structure/'+rePDBid+reSlashOrNot)
def get_secondary_structures_api(pdbid):

    pdbid = pdbid.strip("'")

    api_result = {
        pdbid: {}
    }

    response, response_status = get_secondary_structures(pdbid)

    if response_status == 404:
        bottle.response.status = 404
        return {}

    api_result[pdbid] = response

    bottle.response.status = 200
    return api_result


def get_secondary_structures(entry_id):

    query = """
    MATCH (n:Entry {ID:$entry_id})-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[rel:IS_IN_CHAIN]->(chain:Chain)
        WHERE rel.OBSERVED='Y' AND (rel.IS_IN_HELIX='Y' OR exists(rel.SHEETS))
    WITH entity.ID AS entity_id, chain.AUTH_ASYM_ID AS auth_asym_id, chain.STRUCT_ASYM_ID AS struct_asym_id,
    CASE rel.HELIX_SEGMENT
        WHEN null
            THEN "0"
        ELSE rel.HELIX_SEGMENT
    END AS helix_segment, rel.SHEETS AS sheets, toInteger(pdb_res.ID) AS pdb_res_id, toInteger(rel.AUTH_SEQ_ID) AS auth_seq_id, rel.PDB_INS_CODE AS pdb_ins_code
    RETURN entity_id, auth_asym_id, struct_asym_id, helix_segment, sheets, pdb_res_id, auth_seq_id, pdb_ins_code
    ORDER by toInteger(entity_id), struct_asym_id, pdb_res_id
    """

    mappings = run_query(query, parameters={
        'entry_id': str(entry_id)
    })

    if(len(mappings) == 0):
        return {}, 404

    final_result = {
        "molecules": []
    }

    dict_helices = {}
    dict_sheets = {}
    dict_of_entity = {}
    dict_list_of_helices = {}
    dict_list_of_sheets = {}

    for mapping in mappings:

        (entity_id, auth_asym_id, struct_asym_id, helix_segments, sheets, pdb_res_id, auth_seq_id, pdb_ins_code) = mapping

        if dict_of_entity.get(entity_id) is None:
            dict_of_entity[entity_id] = set()
            dict_of_entity[entity_id].add((auth_asym_id, struct_asym_id))
        else:
            dict_of_entity[entity_id].add((auth_asym_id, struct_asym_id))

        # process helices
        if helix_segments != '0':
            helix_segments = [re.sub(list_re, "", a) for a in helix_segments.strip('[]').split(',')]

            for helix_segment in helix_segments:
                helix_key = (entity_id, auth_asym_id, struct_asym_id, helix_segment)
                
                if dict_helices.get(helix_key) is None:
                    dict_helices[helix_key] = [(pdb_res_id, auth_seq_id, pdb_ins_code)]
                else:
                    dict_helices[helix_key].append((pdb_res_id, auth_seq_id, pdb_ins_code))

                if dict_list_of_helices.get((entity_id, auth_asym_id, struct_asym_id)) is None:
                    dict_list_of_helices[(entity_id, auth_asym_id, struct_asym_id)] = set(helix_segment)
                else:
                    dict_list_of_helices[(entity_id, auth_asym_id, struct_asym_id)].add(helix_segment)

        # process strands
        if sheets is not None:
            sheets = [re.sub(list_re, "", a) for a in sheets.strip('[]').split(',')]

            for sheet in sheets:
                sheet_key = (entity_id, auth_asym_id, struct_asym_id, sheet)

                if dict_sheets.get(sheet_key) is None:
                    dict_sheets[sheet_key] = [(pdb_res_id, auth_seq_id, pdb_ins_code)]
                else:
                    dict_sheets[sheet_key].append((pdb_res_id, auth_seq_id, pdb_ins_code))
                
                if dict_list_of_sheets.get((entity_id, auth_asym_id, struct_asym_id)) is None:
                    dict_list_of_sheets[(entity_id, auth_asym_id, struct_asym_id)] = set(sheet)
                else:
                    dict_list_of_sheets[(entity_id, auth_asym_id, struct_asym_id)].add(sheet)

    for entity_id in sorted(dict_of_entity.keys()):
        entity_element = {
            "entity_id": int(entity_id),
            "chains": []
        }
        for (auth_asym_id, struct_asym_id) in sorted(dict_of_entity[entity_id]):
            chain_element = {
                "chain_id": auth_asym_id,
                "struct_asym_id": struct_asym_id,
                "secondary_structure": {
                    "helices": [],
                    "strands": []
                }
            }
            helix_list = dict_list_of_helices.get((entity_id, auth_asym_id, struct_asym_id))

            if helix_list is not None:
                for helix_segment in sorted(helix_list, key=int):
                    values = dict_helices.get((entity_id, auth_asym_id, struct_asym_id, helix_segment))
                    if values is not None:
                        helix = {
                            "start": {
                                "author_residue_number": values[0][1],
                                "author_insertion_code": values[0][2],
                                "residue_number": values[0][0]
                            },
                            "end": {
                                "author_residue_number": values[-1][1],
                                "author_insertion_code": values[-1][2],
                                "residue_number": values[-1][0]
                            }
                        }
                        chain_element["secondary_structure"]["helices"].append(helix)
            
            sheets_list = dict_list_of_sheets.get((entity_id, auth_asym_id, struct_asym_id))

            if sheets_list is not None:
                for sheet in sorted(sheets_list, key=int):
                    values = dict_sheets.get((entity_id, auth_asym_id, struct_asym_id, sheet))
                    if values is not None:
                        
                        for group in util_common.split_nonconsecutive_residues(values):

                            sheet_element = {
                                "start": {
                                    "author_residue_number": group[0][1],
                                    "author_insertion_code": group[0][2],
                                    "residue_number": group[0][0]
                                },
                                "end": {
                                    "author_residue_number": group[-1][1],
                                    "author_insertion_code": group[-1][2],
                                    "residue_number": group[-1][0]
                                },
                                "sheet_id": int(sheet)
                            }
                            chain_element["secondary_structure"]["strands"].append(sheet_element)

            chain_element["secondary_structure"]["strands"] = sorted(chain_element["secondary_structure"]["strands"], key = lambda x: (x["start"]["residue_number"]))
            chain_element["secondary_structure"]["helices"] = sorted(chain_element["secondary_structure"]["helices"], key = lambda x: (x["start"]["residue_number"]))

            if not chain_element["secondary_structure"]["strands"]:
                del chain_element["secondary_structure"]["strands"]
            if not chain_element["secondary_structure"]["helices"]:
                del chain_element["secondary_structure"]["helices"]

            entity_element["chains"].append(chain_element)
        final_result["molecules"].append(entity_element)
        final_result["molecules"] = sorted(final_result["molecules"], key = lambda x: x["entity_id"])

    return final_result, 200


"""
@api {get} pdb/funpdbe/:pdbId Get FunPDBe resources for a PDB entry
@apiName GetPDBFunPDBeResources
@apiGroup PDB
@apiDescription This call provides details of all resources which have data for a PDB entry.
@apiVersion 2.0.0

@apiParam {String} pdbId=1a08 PDB Entry ID
"""
@app.get('/funpdbe/'+rePDBid+reSlashOrNot)
def get_funpdbe_for_entry_api(pdbid):

    pdbid = pdbid.strip("'")

    api_result = {
        pdbid: []
    }

    result, status = get_funpdbe_for_entry(pdbid)

    if status == 404:
        bottle.response.status = 404
        return api_result

    api_result[pdbid] = result

    bottle.response.status = 200
    return api_result


def get_funpdbe_for_entry(entry_id):

    # sample entry: 1a08
    query = """
    MATCH (entry:Entry {ID:$entry_id})<-[:FUNPDBE_DATA_RELATED_TO]-(funpdb_entry:FunPDBeEntry)-[:HAS_EVIDENCE_CODE]->(evidence_code:EvidenceCodeOntology), 
    (funpdb_entry)<-[:FUNPDBE_RESIDUE_GROUP_OF]-(funpdbe_group:FunPDBeResidueGroup)
    RETURN funpdb_entry.DATA_RESOURCE, funpdb_entry.RELEASE_DATE, funpdb_entry.RESOURCE_ENTRY_URL, COLLECT(DISTINCT evidence_code.ECO_CODE), COLLECT(DISTINCT funpdbe_group.LABEL)
    """

    mappings = run_query(query, parameters={
        'entry_id': str(entry_id)
    })

    if(len(mappings) == 0):
        return {}, 404

    final_result = []

    for row in mappings:
        (origin, release_date, resource_url, evidence_codes, labels) = row

        final_result.append({
            "origin": origin,
            "labels": [x for x in labels],
            "release_date": release_date,
            "url": resource_url,
            "evidence_codes": [x for x in evidence_codes]
        })

    # sort list based on value of "origin" field
    final_result = sorted(final_result, key=lambda x: x["origin"])


    return final_result, 200


"""
@api {get} pdb/funpdbe_annotation/:pdbId Get all FunPDBe annotations for a PDB entry
@apiName GetPDBFunPDBeAnnotations
@apiGroup PDB
@apiDescription This call provides details of all FunPDBe annotations for a PDB entry.
@apiVersion 2.0.0

@apiParam {String} pdbId=1a08 PDB Entry ID
"""
@app.get('/funpdbe_annotation/'+rePDBid+reSlashOrNot)
def get_funpdbe_annotation_for_entry_api(pdbid):

    pdbid = pdbid.strip("'")

    api_result = {
        pdbid: []
    }

    result, status = get_funpdbe_annotation_for_entry(pdbid)

    if status == 404:
        bottle.response.status = 404
        return api_result

    api_result[pdbid] = result

    bottle.response.status = 200
    return api_result


def get_funpdbe_annotation_for_entry(entry_id):

    # sample entry: 1a08

    query = """
    MATCH (entry:Entry {ID:$entry_id})<-[:FUNPDBE_DATA_RELATED_TO]-(funpdbe_entry:FunPDBeEntry)<-[:FUNPDBE_RESIDUE_GROUP_OF]-(funpdbe_group:FunPDBeResidueGroup)-[residue_rel:FUNPDBE_ANNOTATION_FOR]->(pdb_res:PDBResidue)-[chain_rel:IS_IN_CHAIN]->(chain:Chain)<-[:CONTAINS_CHAIN]-(entity:Entity),
    (funpdbe_entry)-[:HAS_EVIDENCE_CODE]->(evidence_code:EvidenceCodeOntology)
        WHERE chain.AUTH_ASYM_ID=residue_rel.CHAIN_LABEL
    RETURN funpdbe_entry.DATA_RESOURCE, funpdbe_entry.RELEASE_DATE, funpdbe_entry.RESOURCE_ENTRY_URL, COLLECT(evidence_code.ECO_CODE), funpdbe_group.LABEL, funpdbe_group.ORDINAL_ID, entity.ID, chain.AUTH_ASYM_ID, pdb_res.ID, chain_rel.AUTH_SEQ_ID, pdb_res.CHEM_COMP_ID, chain_rel.PDB_INS_CODE, residue_rel.RAW_SCORE, residue_rel.CONFIDENCE_SCORE, residue_rel.CONFIDENCE_CLASSIFICATION
    ORDER BY toInteger(entity.ID), chain.AUTH_ASYM_ID, toInteger(pdb_res.ID)
    """
    mappings = run_query(query, parameters={
        'entry_id': str(entry_id)
    })

    if(len(mappings) == 0):
        return {}, 404

    final_result = []
    dict_resource = {}
    dict_site_residues = {}

    for row in mappings:
        (origin, release_date, resource_url, evidence_codes, label, ordinal_id, entity_id, chain_id, pdb_res_id, auth_seq_id, chem_comp_id, pdb_ins_code, raw_score, conf_score, conf_class) = row
        
        # optional match returns NULL rows, ignore them - NOT REQUIRED
        # if site_id is None: continue

        if dict_resource.get(origin) is None:
            dict_resource[origin] = {
                "origin": origin,
                "release_date": release_date,
                "url": resource_url,
                "evidence_codes": [],
                "annotations": []
            }

        dict_resource[origin]["evidence_codes"] = evidence_codes

        if dict_site_residues.get((origin, label, ordinal_id)) is None:
            dict_site_residues[(origin, label, ordinal_id)] = {
                "site_id": int(ordinal_id),
                "label": label,
                "site_residues": []
            }
            
        dict_site_residues[(origin, label, ordinal_id)]["site_residues"].append({
            "entity_id": int(entity_id),
            "chain_id": chain_id,
            "residue_number": int(pdb_res_id),
            "author_residue_number": "" if auth_seq_id is None else int(auth_seq_id),
            "chem_comp_id": chem_comp_id,
            "author_insertion_code": "" if pdb_ins_code is None else pdb_ins_code,
            "raw_score": None if not raw_score else float(raw_score),
            "confidence_score": None if not conf_score else float(conf_score),
            "confidence_classification": conf_class
        })

            
    for (origin, label, ordinal_id) in dict_site_residues.keys():
        dict_resource[origin]["annotations"].append(dict_site_residues[(origin, label, ordinal_id)])

    for key in dict_resource.keys():
        dict_resource[key]["evidence_codes"] = list(dict_resource[key]["evidence_codes"])
        dict_resource[key]["annotations"] = sorted(dict_resource[key]["annotations"], key=lambda x: x["site_id"])
        final_result.append(dict_resource[key])

    return final_result, 200



"""
@api {get} pdb/funpdbe_annotation/:origin/:pdbId Get all FunPDBe annotations for a PDB entry from a specific resource
@apiName GetPDBFunPDBeAnnotationsForResource
@apiGroup PDB
@apiDescription This call provides details of all FunPDBe annotations for a PDB entry from a specific resource.
@apiVersion 2.0.0

@apiParam {String="cath-funsites","14-3-3-pred","3Dcomplex","akid","3dligandsite","camkinet","canSAR","ChannelsDB","depth","dynamine","FoldX","MetalPDB","M-CSA","p2rank","Missense3D","POPScomp_PDBML","ProKinO"} origin=dynamine Origin/Resource
@apiParam {String} pdbId=1a08 PDB Entry ID
"""
@app.get('/funpdbe_annotation/<origin>/'+rePDBid+reSlashOrNot)
def get_specific_funpdbe_annotation_for_entry_api(origin, pdbid):

    pdbid = pdbid.strip("'")
    origin = origin.strip("'")

    api_result = {
        pdbid: []
    }

    result, status = get_specific_funpdbe_annotation_for_entry(origin, pdbid)

    if status == 404:
        bottle.response.status = 404
        return api_result

    api_result[pdbid] = result

    bottle.response.status = 200
    return api_result


def get_specific_funpdbe_annotation_for_entry(origin, entry_id):

    # sample entry: 1a08
    # query = """
    # MATCH (entry:Entry {ID:$entry_id})<-[:FUNPDBE_DATA_RELATED_TO]-(funpdbe_entry:FunPDBeEntry {DATA_RESOURCE:$origin})-[:HAS_EVIDENCE_CODE]->(evidence_code:EvidenceCodeOntology), (funpdbe_entry)<-[:FUNPDBE_RESIDUE_GROUP_OF]-(funpdbe_group:FunPDBeResidueGroup)
    # OPTIONAL MATCH (entry)-[:HAS_BINDING_SITE]->(site:Binding_Site)<-[:IS_IN_BINDING_SITE]-(pdb_res:PDB_Residue)<-[:HAS_PDB_RESIDUE]-(entity:Entity), (funpdbe_group)-[residue_rel:FUNPDBE_ANNOTATION_FOR]->(pdb_res)-[chain_rel:IS_IN_CHAIN]->(chain:Chain)
    # RETURN funpdbe_entry.DATA_RESOURCE, funpdbe_entry.RELEASE_DATE, funpdbe_entry.RESOURCE_ENTRY_URL, evidence_code.ECO_CODE, site.ID, entity.ID, chain.AUTH_ASYM_ID, pdb_res.ID, chain_rel.AUTH_SEQ_ID, chain_rel.PDB_INS_CODE, residue_rel.RAW_SCORE, residue_rel.CONFIDENCE_SCORE, residue_rel.CONFIDENCE_CLASSIFICATION ORDER BY toInteger(entity.ID), chain.AUTH_ASYM_ID, toInteger(pdb_res.ID)
    # """

    query = """
    MATCH (entry:Entry {ID:$entry_id})<-[:FUNPDBE_DATA_RELATED_TO]-(funpdbe_entry:FunPDBeEntry {DATA_RESOURCE:$origin})<-[:FUNPDBE_RESIDUE_GROUP_OF]-(funpdbe_group:FunPDBeResidueGroup)-[residue_rel:FUNPDBE_ANNOTATION_FOR]->(pdb_res)-[chain_rel:IS_IN_CHAIN]->(chain:Chain)<-[:CONTAINS_CHAIN]-(entity:Entity),
    (funpdbe_entry)-[:HAS_EVIDENCE_CODE]->(evidence_code:EvidenceCodeOntology)
        WHERE chain.AUTH_ASYM_ID=residue_rel.CHAIN_LABEL
    RETURN funpdbe_entry.DATA_RESOURCE, funpdbe_entry.RELEASE_DATE, funpdbe_entry.RESOURCE_ENTRY_URL, COLLECT(evidence_code.ECO_CODE), funpdbe_group.LABEL, funpdbe_group.ORDINAL_ID, entity.ID, chain.AUTH_ASYM_ID, pdb_res.ID, chain_rel.AUTH_SEQ_ID, pdb_res.CHEM_COMP_ID, chain_rel.PDB_INS_CODE, residue_rel.RAW_SCORE, residue_rel.CONFIDENCE_SCORE, residue_rel.CONFIDENCE_CLASSIFICATION
    ORDER BY toInteger(entity.ID), chain.AUTH_ASYM_ID, toInteger(pdb_res.ID)
    """

    mappings = run_query(query, parameters={
        'entry_id': str(entry_id), "origin": str(origin)
    })

    if(len(mappings) == 0):
        return {}, 404

    final_result = []
    dict_resource = {}
    dict_site_residues = {}

    for row in mappings:
        (origin, release_date, resource_url, evidence_codes, label, ordinal_id, entity_id, chain_id, pdb_res_id, auth_seq_id, chem_comp_id, pdb_ins_code, raw_score, conf_score, conf_class) = row
        
        # optional match returns NULL rows, ignore them - NOT REQUIRED
        # if site_id is None: continue

        if dict_resource.get(origin) is None:
            dict_resource[origin] = {
                "origin": origin,
                "release_date": release_date,
                "url": resource_url,
                "evidence_codes": [],
                "annotations": []
            }

        dict_resource[origin]["evidence_codes"] = evidence_codes

        if dict_site_residues.get((origin, label, ordinal_id)) is None:
            dict_site_residues[(origin, label, ordinal_id)] = {
                "site_id": int(ordinal_id),
                "label": label,
                "site_residues": []
            }
            
        dict_site_residues[(origin, label, ordinal_id)]["site_residues"].append({
            "entity_id": int(entity_id),
            "chain_id": chain_id,
            "residue_number": int(pdb_res_id),
            "author_residue_number": "" if auth_seq_id is None else int(auth_seq_id),
            "chem_comp_id": chem_comp_id,
            "author_insertion_code": "" if pdb_ins_code is None else pdb_ins_code,
            "raw_score": None if not raw_score else float(raw_score),
            "confidence_score": None if not conf_score else float(conf_score),
            "confidence_classification": conf_class
        })

            
    for (origin, label, ordinal_id) in dict_site_residues.keys():
        dict_resource[origin]["annotations"].append(dict_site_residues[(origin, label, ordinal_id)])

    for key in dict_resource.keys():
        dict_resource[key]["evidence_codes"] = list(dict_resource[key]["evidence_codes"])
        dict_resource[key]["annotations"] = sorted(dict_resource[key]["annotations"], key=lambda x: x["site_id"])
        final_result.append(dict_resource[key])

    return final_result, 200


"""
@api {get} pdb/complex/:pdbComplexId Get PDB Complex details
@apiName GetPDBComplexDetails
@apiGroup PDB
@apiDescription Get list of participants and subcomplexes (if any) for any PDB Complex ID.
@apiVersion 2.0.0

@apiParam {String} pdbComplexId=PDB-CPX-3815 PDB Complex ID (eg. PDB-CPX-3815)

@apiUse ComplexTerms

@apiExample {json=./examples/success/complex.json} apiSuccessExample Example success response JSON
"""
@app.get('/complex/'+rePDBComplex+reSlashOrNot)
def get_complex_api(pdb_complex):

    pdb_complex = pdb_complex.strip("'")

    api_result = {
        pdb_complex: {
            "participants": [],
            "subcomplexes": []
        }
    }

    participant_result, participant_result_status = get_complex(pdb_complex)
    subcomplex_result, subcomplex_result_status = get_subcomplex(pdb_complex)

    api_result[pdb_complex]["participants"] = participant_result
    api_result[pdb_complex]["subcomplexes"] = subcomplex_result

    if participant_result_status == 404 and subcomplex_result_status == 404:
        bottle.response.status = 404
        return api_result

    bottle.response.status = 200
    return api_result


def get_complex(pdb_complex):

    query = """
    MATCH (src_complex:PDBComplex {COMPLEX_ID:$pdb_complex})<-[rel:IS_PART_OF_PDB_COMPLEX]-(unp:UniProt)-[:HAS_TAXONOMY]->(tax:Taxonomy)
    RETURN unp.ACCESSION, rel.STOICHIOMETRY, tax.TAX_ID ORDER BY unp.ACCESSION
    """

    final_result = []

    mappings = run_query(query, parameters={
        "pdb_complex": str(pdb_complex)
    })

    if(len(mappings) == 0):
        return [], 404

    for mapping in mappings:
        (accession, stoichiometry, tax_id) = mapping
        final_result.append({
            "accession": accession,
            "stoichiometry": int(stoichiometry),
            "taxonomy_id": int(tax_id)
        })

    return final_result, 200



def get_subcomplex(pdb_complex):

    query = """
    MATCH (src_complex:PDBComplex {COMPLEX_ID:$pdb_complex})<-[:IS_SUB_COMPLEX_OF]-(sub_complex:PDBComplex)
    RETURN sub_complex.COMPLEX_ID ORDER BY sub_complex.COMPLEX_ID
    """

    mappings = run_query(query, parameters={
        "pdb_complex": str(pdb_complex)
    })

    if(len(mappings) == 0):
        return [], 404

    return [x[0] for x in mappings], 200


@app.get('/protvista/'+rePDBid+'/'+reCHAINid+'/<data_resource>'+reSlashOrNot)
def get_funpdbe_old_protvista_api(pdbid, pdb_chain_id, data_resource):

    entry_id = pdbid.strip("'")
    chain_id = pdb_chain_id.strip("'")
    data_resource = data_resource.strip("'")

    result, resp_status = get_funpdbe_old_protvista(entry_id, chain_id, data_resource)
    
    api_result = result

    bottle.response.status = resp_status
    return api_result


def get_funpdbe_old_protvista(entry_id, chain_id, data_resource):

    final_result = {}
    
    sequence, seq_resp_status = get_sequence(entry_id, chain_id)
    final_result["sequence"] = sequence
    final_result["features"] = []

    
    query = """
    MATCH (entry:Entry {ID:$entry_id})<-[:FUNPDBE_DATA_RELATED_TO]-(fun_entry:FunPDBeEntry {DATA_RESOURCE:$data_resource})<-[:FUNPDBE_RESIDUE_GROUP_OF]-(fun_group:FunPDBeResidueGroup)-[anno_rel:FUNPDBE_ANNOTATION_FOR]->(pdb_res:PDBResidue)-[:IS_IN_CHAIN]->(chain:Chain {STRUCT_ASYM_ID:$chain_id}), (fun_entry)-[:HAS_EVIDENCE_CODE]->(evidence_code:EvidenceCodeOntology)
    WHERE anno_rel.CHAIN_LABEL=chain.STRUCT_ASYM_ID
    RETURN fun_group.LABEL, fun_group.ORDINAL_ID, anno_rel.CONFIDENCE_CLASSIFICATION, pdb_res.ID, evidence_code.ECO_CODE, anno_rel.RAW_SCORE, fun_entry.RESOURCE_ENTRY_URL
    """

    mappings = run_query(query, parameters={
        "entry_id": str(entry_id), "chain_id": str(chain_id), "data_resource": str(data_resource)
    })

    if(len(mappings) == 0 and seq_resp_status == 404):
        return [], 404

    residue_dict = {}
    eco_code_dict = {}

    for row in mappings:
        (label, ordinal_id, conf_classification, pdb_res_id, eco_code, raw_score, resource_url) = row

        if residue_dict.get((label, pdb_res_id)) is None:
            eco_code_dict[(label, pdb_res_id)] = set([eco_code])
            raw_score = float(raw_score) if raw_score else None
            color_code = None

            # control coloring for only Biophysical parameter resources depending upon raw_score
            if data_resource in funpdbe_biophysical_resources:
                if raw_score:
                    if raw_score < 0.333:
                        color_code = funpdbe_conf_color_code.get("low")
                    elif 0.333 <= raw_score < 0.666:
                        color_code = funpdbe_conf_color_code.get("medium")
                    elif 0.666 <= raw_score <= 1:
                        color_code = funpdbe_conf_color_code.get("high")
                else:
                    color_code = funpdbe_conf_color_code.get("null")
            else:
                color_code = funpdbe_conf_color_code.get(conf_classification)

            residue_dict[(label, pdb_res_id)] = {
                "type": label +" (group id: {})".format(ordinal_id),
                "description": "Confidence: {}".format(conf_classification),
                "category": data_resource,
                "begin": pdb_res_id,
                "end": pdb_res_id,
                "color": color_code,
                "evidences": [{
                    "code": "",
                    "source": {
                        "name": "Prediction score",
                        "id": str(raw_score) if raw_score else None,
                        "url": resource_url
                    }
                }]
            }
        else:
            eco_code_dict[(label, pdb_res_id)].add(eco_code)

    for key in sorted(residue_dict.keys(), key=lambda x: int(x[1])):

        residue_dict[key]["evidences"][0]["code"] = ",".join(eco_code_dict[key])
        final_result["features"].append(residue_dict[key])

    return final_result, 200
    

@app.get('/protvista/'+rePDBid+'/'+reCHAINid+reSlashOrNot)
def get_funpdbe_all_old_protvista_api(pdbid, pdb_chain_id):

    entry_id = pdbid.strip("'")
    chain_id = pdb_chain_id.strip("'")

    result, resp_status = get_funpdbe_all_old_protvista(entry_id, chain_id)
    
    api_result = result

    bottle.response.status = resp_status
    return api_result


def get_funpdbe_all_old_protvista(entry_id, chain_id):

    final_result = {}

    sequence, seq_resp_status = get_sequence(entry_id, chain_id)
    final_result["sequence"] = sequence
    final_result["features"] = []

    category_dict = {
        "cansar": "Binding sites",
        "3dligandsite": "Binding sites",
        "cath-funsites": "Conserved residues",
        "ProKinO": "PTM sites",
        "akid": "PTM sites",
        "nod": "PTM sites",
        "14-3-3-pred": "PTM sites",
        "dynamine": "Biophysical parameters",
        "depth": "Biophysical parameters"
    }
    
    query = """
    MATCH (entry:Entry {ID:$entry_id})<-[:FUNPDBE_DATA_RELATED_TO]-(fun_entry:FunPDBeEntry)<-[:FUNPDBE_RESIDUE_GROUP_OF]-(fun_group:FunPDBeResidueGroup)-[anno_rel:FUNPDBE_ANNOTATION_FOR]->(pdb_res:PDBResidue)-[:IS_IN_CHAIN]->(chain:Chain {STRUCT_ASYM_ID:$chain_id}), (fun_entry)-[:HAS_EVIDENCE_CODE]->(evidence_code:EvidenceCodeOntology)
    WHERE anno_rel.CHAIN_LABEL=chain.STRUCT_ASYM_ID
    RETURN fun_entry.DATA_RESOURCE, fun_group.LABEL, fun_group.ORDINAL_ID, anno_rel.CONFIDENCE_CLASSIFICATION, pdb_res.ID, evidence_code.ECO_CODE, anno_rel.RAW_SCORE, fun_entry.RESOURCE_ENTRY_URL
    """

    mappings = run_query(query, parameters={
        "entry_id": str(entry_id), "chain_id": str(chain_id)
    })

    if(len(mappings) == 0 and seq_resp_status == 404):
        return [], 404

    residue_dict = {}
    eco_code_dict = {}

    for row in mappings:
        (data_resource, label, ordinal_id, conf_classification, pdb_res_id, eco_code, raw_score, resource_url) = row

        if residue_dict.get((data_resource, label, pdb_res_id)) is None:
            eco_code_dict[(data_resource, label, pdb_res_id)] = set([eco_code])

            raw_score = float(raw_score) if raw_score else None
            color_code = None
            
            # control coloring for only Biophysical parameter resources depending upon raw_score
            if data_resource in funpdbe_biophysical_resources:
                if raw_score:
                    if raw_score < 0.333:
                        color_code = funpdbe_conf_color_code.get("low")
                    elif 0.333 <= raw_score < 0.666:
                        color_code = funpdbe_conf_color_code.get("medium")
                    elif 0.666 <= raw_score <= 1:
                        color_code = funpdbe_conf_color_code.get("high")
                else:
                    color_code = funpdbe_conf_color_code.get("null")
            else:
                color_code = funpdbe_conf_color_code.get(conf_classification)

            residue_dict[(data_resource, label, pdb_res_id)] = {
                "type": data_resource,
                "description": "{}, Confidence: {}".format(label, conf_classification),
                "category": category_dict.get(data_resource),
                "begin": pdb_res_id,
                "end": pdb_res_id,
                "color": color_code,
                "evidences": [{
                    "code": "",
                    "source": {
                        "name": "Prediction score",
                        "id": str(raw_score) if raw_score else None,
                        "url": resource_url
                    }
                }]
            }
        else:
            eco_code_dict[(data_resource, label, pdb_res_id)].add(eco_code)

    for key in sorted(residue_dict.keys(), key=lambda x: int(x[2])):

        residue_dict[key]["evidences"][0]["code"] = ",".join(eco_code_dict[key])
        final_result["features"].append(residue_dict[key])

    return final_result, 200


"""
@api {get} pdb/bound_molecules/:pdbId/ Get bound molecules
@apiName GetBoundMolecules
@apiGroup PDB
@apiDescription Get definitions and internal connectivity of all bound molecules found in a given entry.
@apiVersion 2.0.0

@apiParam {String} pdbId=3d12 PDB Entry ID

@apiUse GenericResidueTerms
@apiUse GenericBoundMoleculeTerms

@apiExample {json=./examples/success/bound_molecules.json} apiSuccessExample Example success response JSON
"""
@app.get('/bound_molecules/' + rePDBid + reSlashOrNot)
def get_bound_molecules_api(pdbid):
    pdb_id = pdbid.strip("'")

    bm_result, response_status = get_bound_molecules(pdb_id)
    api_result = {pdb_id: bm_result}

    bottle.response.status = response_status
    return api_result


"""
@api {get} pdb/carbohydrate-polymer/:pdbId/ Get carbohydrate polymers
@apiName GetCarbohydratePolymer
@apiGroup PDB
@apiDescription Get definitions and internal connectivity of all carbohydrate polymers found in a given entry.
@apiVersion 2.0.0

@apiParam {String} pdbId=3d12 PDB Entry ID
"""
@app.get('/carbohydrate-polymer/' + rePDBid + reSlashOrNot)
def get_carbohydrate_polymers_api(pdbid):
    pdb_id = pdbid.strip("'")

    bm_result, response_status = get_bound_molecules(pdb_id, branched_only=True)
    api_result = {pdb_id: bm_result}

    bottle.response.status = response_status
    return api_result


"""
@api {get} pdb/bound_excluding_branched/:pdbId/ Get bound molecules excluding carbohydrate polymers
@apiName BoundExcludingBranched
@apiGroup PDB
@apiDescription Get definitions and internal connectivity of all non-carbohydrate-polymer bound molecules found in a given entry.
@apiVersion 2.0.0

@apiParam {String} pdbId=3d12 PDB Entry ID
"""
@app.get('/bound_excluding_branched/' + rePDBid + reSlashOrNot)
def get_bound_excluding_branched_molecules_api(pdbid):
    pdb_id = pdbid.strip("'")

    bm_result, response_status = get_bound_molecules(pdb_id, exclude_branched=True)
    api_result = {pdb_id: bm_result}

    bottle.response.status = response_status
    return api_result


def get_bound_molecules(pdb_id, bm_id=None, branched_only=False, exclude_branched=False, entity_id=None):
    query = """
    MATCH (e:Entry {ID:$pdb_id})-[:HAS_BOUND_MOLECULE]->(bm:BoundMolecule###1)<-[r:IS_PART_OF]-(bl1:BoundLigand)<-[:IS_AN_INSTANCE_OF]-(entity1:Entity{###2###4})
    OPTIONAL MATCH (bl1)-[:IS_CONNECTED_TO]-(bl2:BoundLigand)<-[:IS_AN_INSTANCE_OF]-(entity2:Entity{###3###4})
    OPTIONAL MATCH (bl1)-[y:HAS_ARP_CONTACT]->(x)
    RETURN DISTINCT bm.AU_FALLBACK, bm.ID, entity1.ID, entity1.POLYMER_TYPE, bl1.CHEM_COMP_ID, bl1.AUTH_ASYM_ID, toInteger(bl1.AUTH_SEQ_ID), bl1.PDB_INS_CODE, COALESCE(r.SYM_OPERATOR,''),
    bl1.AUTH_ASYM_ID+COALESCE(r.SYM_OPERATOR,'')+bl1.AUTH_SEQ_ID AS bl1name,
    entity2.ID, entity2.POLYMER_TYPE, bl2.CHEM_COMP_ID, bl2.AUTH_ASYM_ID, bl2.AUTH_SEQ_ID, bl2.PDB_INS_CODE,
    bl2.AUTH_ASYM_ID+COALESCE(r.SYM_OPERATOR,'')+bl2.AUTH_SEQ_ID AS bl2name
    ORDER BY bm.ID
    """

    bm_result = {}

    query = query.replace('###1', '{ID:$bm_id}') if bm_id else query.replace('###1', '')
    query = query.replace('###4', f', ID: \'{entity_id}\'') if entity_id else query.replace('###4', '')
    if branched_only:
        query = query.replace('###2', ' POLYMER_TYPE:\'S\'') 
        query = query.replace('###3', ' POLYMER_TYPE:\'S\'') 
    elif exclude_branched:
        query = query.replace('###2', ' POLYMER_TYPE:\'B\'') 
        query = query.replace('###3', ' POLYMER_TYPE:\'B\'') 
    else: 
        query = query.replace('###2', '')
        query = query.replace('###3', '')

    query_result = run_query(query, {"pdb_id":pdb_id, "bm_id":bm_id}) if bm_id else run_query(query, {"pdb_id":pdb_id})

    if not query_result:
        return {}, 404

    res_pool = {}
    contacts_pool = {}
    for result in query_result:
        (au_fallback, bm_id, entity1, entity1_type, bl1_chem_comp, bl1_auth_asym, bl1_auth_seq, bl1_ins, sym_op, bl1_name, entity2, entity2_type, bl2_chem_comp,
         bl2_auth_asym, bl2_auth_seq, bl2_ins, bl2_name) = result

        if bm_id not in res_pool:
            res_pool[bm_id] = []
        if bm_id not in contacts_pool:
            contacts_pool[bm_id] = []

        residues = [
            build_bm_residue_with_entity(au_fallback, entity1, entity1_type, bl1_name, bl1_chem_comp, bl1_auth_asym, bl1_auth_seq, sym_op, bl1_ins),
            build_bm_residue_with_entity(au_fallback, entity2, entity2_type, bl2_name, bl2_chem_comp, bl2_auth_asym, bl2_auth_seq, sym_op, bl2_ins)
        ]

        # init bound molecule
        if bm_id not in bm_result:
            bm_result[bm_id] = {
                "bm_id": bm_id,
                "composition": {
                    "ligands": [],
                    "connections": []
                }
            }

        for i in range(0, 2):
            if not residues[i]:
                continue  # we dont want to process empty residue

            res_hash_suffix = residues[i]['author_residue_number'] if (residues[i]['author_residue_number']) else ''
            res_hash = f"{residues[i]['chain_id']}{residues[i]['author_residue_number']}{res_hash_suffix}"

            if res_hash not in res_pool[bm_id]:
                bm_result[bm_id]['composition']['ligands'].append(residues[i])
                res_pool[bm_id].append(res_hash)

        if bl1_name and bl2_name:
            contacts = sorted([bl1_name, bl2_name])
            contacts_hash = f'{contacts[0]}{contacts[1]}'
            if contacts_hash not in contacts_pool[bm_id]:
                bm_result[bm_id]['composition']['connections'].append(contacts)
                contacts_pool[bm_id].append(contacts_hash)

    return list(bm_result.values()), 200


"""
@api {get} pdb/bound_molecule_interactions/:pdbId/:bmid Get bound molecule interactions
@apiName GetBoundMoleculeInteractions
@apiGroup PDB
@apiDescription Get composition and high-level interactions of the bound molecule in the given entry.
@apiVersion 2.0.0

@apiParam {String} pdbId=3d12 PDB Entry ID
@apiParam {String} bmid=bm1 Bound molecule ID (can be obtained from the output of bound_molecules call).

@apiUse GenericResidueTerms
@apiUse GenericBoundMoleculeTerms

@apiExample {json=./examples/success/bound_molecule_interactions.json} apiSuccessExample Example success response JSON
"""
@app.get('/bound_molecule_interactions/' + rePDBid + reSlashOrNot + '<bm_id>' + reSlashOrNot)
def get_bound_molecule_interactions_api(pdbid, bm_id):

    pdb_id = pdbid.strip("'")
    bm_id = bm_id.strip("'")

    api_result, response_status = get_bound_molecule_interactions(pdb_id, bm_id)

    bottle.response.status = response_status
    return api_result

"""
@api {get} pdb/carbohydrate_polymer_interactions/:pdbId/:bmid/:entityId Get carbohydrate polymer interactions
@apiName GetCarbohydratePolymereInteractions
@apiGroup PDB
@apiDescription Get composition and high-level interactions of the carbohydrate polymer in the given entry.
@apiVersion 2.0.0

@apiParam {String} pdbId=5e98 PDB Entry ID
@apiParam {String} bmid=bm1 Bound molecule ID (can be obtained from the output of bound_molecules call).
@apiParam {String} entityId=3 Entity ID (can be obtained from the output of bound_molecules call).

@apiUse GenericResidueTerms
@apiUse GenericBoundMoleculeTerms
"""
@app.get('/carbohydrate_polymer_interactions/' + rePDBid + reSlashOrNot + '<bm_id>' + reSlashOrNot + '<entity_id>' + reSlashOrNot)
def get_carbohydrate_polymer_interactions_api(pdbid, bm_id, entity_id):

    pdb_id = pdbid.strip("'")
    bm_id = bm_id.strip("'")

    api_result, response_status = get_bound_molecule_interactions(pdb_id, bm_id, branched_only=True, entity_id=entity_id)

    bottle.response.status = response_status
    return api_result


def get_bound_molecule_interactions(pdb_id, bm_id, branched_only=False, entity_id=None):

    api_result = {
        pdb_id: [
            {
                'interactions': []
            }
        ]
    }

    bm_details, status = get_bound_molecules(pdb_id, bm_id, branched_only, entity_id=entity_id)

    if status != 200:
        return {}, 404

    for k, v in bm_details[0].items():
        api_result[pdb_id][0][k] = v

    # Now fetch high-level interactions
    api_result[pdb_id][0]["interactions"] = fetch_high_level_interactions(pdb_id, bm_id, entity_id)

    return api_result, 200


def fetch_high_level_interactions(pdb_id, bm_id, entity_id=None):
    query = """
    MATCH (e:Entry {ID:$pdb_id})-[:HAS_BOUND_MOLECULE]->(bm:BoundMolecule {ID:$bm_id})<-[r:IS_PART_OF]-(bl1:BoundLigand)<-[:IS_AN_INSTANCE_OF]-(entity1:Entity###)
    OPTIONAL MATCH (bl1)-[contact:HAS_ARP_CONTACT]-(x)
    WHERE COALESCE(r.SYM_OPERATOR,'--') = COALESCE(contact.SYM_OP_1,'--')
    RETURN bm.AU_FALLBACK, entity1.ID, bl1.CHEM_COMP_ID, contact.AUTH_ASYM_ID_1, bl1.PDB_INS_CODE, contact.AUTH_SEQ_ID_1, COALESCE(contact.SYM_OP_1,''), contact.ATOM_1, x.CHEM_COMP_ID,
    contact.AUTH_SEQ_ID_2, contact.AUTH_ASYM_ID_2, x.PDB_INS_CODE, COALESCE(contact.SYM_OP_2,''), contact.ATOM_2,
    contact.CONTACT_TYPE, contact.INTERACTION_TYPE,
    bl1.AUTH_ASYM_ID+COALESCE(r.SYM_OPERATOR,'')+bl1.AUTH_SEQ_ID AS bl1_name,
    contact.AUTH_ASYM_ID_2+COALESCE(contact.SYM_OP_2,'')+contact.AUTH_SEQ_ID_2 AS bl2_name
    ORDER BY contact.AUTH_SEQ_ID_1, contact.AUTH_SEQ_ID_2
    """

    query = query.replace('###', f'{{ID: \'{entity_id}\'}}') if entity_id else query.replace('###', '')
    interactions = {}
    query_result = run_query(query, {"pdb_id":pdb_id, "bm_id":bm_id})

    for index, result in enumerate(query_result):
        (au_fallback, entity, bl1_chem_comp, bl1_auth_asym, bl1_ins, bl1_auth_seq, sym_op1, atom_1, bl2_chem_comp,
         bl2_auth_seq, bl2_auth_asym, bl2_ins, sym_op2, atom_2, contact_type, interaction_type,
         bl1_name, bl2_name) = result

        interaction_type = interaction_type.replace('-', '_')

        key = sorted([bl1_name, bl2_name])
        key_hash = f'{key[0]}{key[1]}'

        if key_hash not in interactions:
            r1 = build_bm_residue(au_fallback, bl1_name, bl1_chem_comp, bl1_auth_asym, bl1_auth_seq, sym_op1, bl1_ins)
            r2 = build_bm_residue(au_fallback, bl2_name, bl2_chem_comp, bl2_auth_asym, bl2_auth_seq, sym_op1, bl2_ins)

            interactions[key_hash] = {
                "begin": r1,
                "end": r2,
                "interactions": {
                    "atom_atom": set(),
                    "atom_plane": set(),
                    "plane_plane": set(),
                    "group_group": set(),
                    "group_plane": set(),
                }
            }

        interactions_type_eval = eval(contact_type)
        interactions[key_hash]['interactions'][interaction_type].update(interactions_type_eval)

    # set is not JSON serializable so we need to have lists
    for i in interactions.values():
        for k, v in i['interactions'].items():
            i['interactions'][k] = list(v)

    return list(interactions.values())


"""
@api {get} pdb/bound_ligand_interactions/:pdbId/:chain/:seqId Get bound ligand interactions
@apiName GetBoundLigandInteractions
@apiGroup PDB
@apiDescription Get interactions for a bound ligand found in the entry.
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID.
@apiParam {String} chain=A Chain id aka auth_asym_id.
@apiParam {Integer} seqId=200 Residue id id aka auth_seq_id.

@apiSuccess {String} ligand A ligand object.
@apiSuccess {Object[]} interactions A list of interactions.
@apiUse GenericResidueTerms
@apiSuccess {String[]} ligand_atoms A list of atoms.
@apiSuccess {String} interaction_class The class of interaction. It can be atom-atom, atom-plane, plane-plane, group-plane and group-group.
@apiSuccess {String[]} atom_names A list of atom names.
@apiSuccess {String[]} interaction_details A list of interaction types. More info can be found <a target="_blank" href="https://github.com/lpravda/arpeggio">here</a>
@apiSuccess {Float} distance Distance in ångströms between the components that take part in interaction.

@apiExample {json=./examples/success/bound_ligand_interactions.json} apiSuccessExample Example success response JSON
"""
@app.get('/bound_ligand_interactions/' + rePDBid + reSlashOrNot + '<chain_id>' + reSlashOrNot + reSEQid + reSlashOrNot)
def get_molecule_contacts_api(pdbid, chain_id, pdb_seq_id):

    pdbid = pdbid.strip("'")
    pdb_seq_id = int(pdb_seq_id.strip("'"))
    chain_id = chain_id.strip("'")
    sym_op = "_" + chain_id.split('_')[1] if '_' in chain_id else None
    chain_id = chain_id.split('_')[0] if '_' in chain_id else chain_id

    api_result, response_status = get_ligand_interactions(pdbid, chain_id, pdb_seq_id, sym_op)

    if response_status == 404:
        bottle.response.status = 404
        return api_result

    bottle.response.status = 200
    return api_result


def get_ligand_interactions(pdb_id, chain_id, seq_id, sym_op):
    """In case of boundligand - PDB residue interaction, first component will always be boundligand,
    but in case of boundligand-boundligand interactions, the ligand of interest might be first or second component
    """
    query = """
    MATCH (e:Entry  {ID: $pdb_id})-[:HAS_BOUND_MOLECULE]->(bm:BoundMolecule)<-[:IS_PART_OF]-(bl1:BoundLigand {AUTH_ASYM_ID: $chain_id, AUTH_SEQ_ID: $seq_id})-[contact:HAS_ARP_CONTACT]-(x)
    WHERE contact.SYM_OP_1 ###
    RETURN
        CASE
            WHEN bl1.AUTH_SEQ_ID = $seq_id AND bl1.AUTH_ASYM_ID = $chain_id
            THEN bl1.CHEM_COMP_ID
            ELSE x.CHEM_COMP_ID
        END as ligand_name,
        CASE
            WHEN bl1.AUTH_SEQ_ID = $seq_id AND bl1.AUTH_ASYM_ID = $chain_id
            THEN x.CHEM_COMP_ID
            ELSE bl1.CHEM_COMP_ID
        END as other_name,
        CASE
            WHEN bl1.AUTH_SEQ_ID = $seq_id AND bl1.AUTH_ASYM_ID = $chain_id
            THEN bl1.PDB_INS_CODE
            ELSE x.PDB_INS_CODE
        END as ligand_ins_code,
        CASE
            WHEN bl1.AUTH_SEQ_ID = $seq_id AND bl1.AUTH_ASYM_ID = $chain_id
            THEN x.PDB_INS_CODE
            ELSE bl1.PDB_INS_CODE
        END as other_ins_code,
        CASE
            WHEN contact.AUTH_SEQ_ID_1 = $seq_id AND contact.AUTH_ASYM_ID_1 = $chain_id
            THEN toInteger(contact.AUTH_SEQ_ID_1)
            ELSE toInteger(contact.AUTH_SEQ_ID_2)
        END as ligand_seq_id,
        CASE
            WHEN contact.AUTH_SEQ_ID_1 = $seq_id AND contact.AUTH_ASYM_ID_1 = $chain_id
            THEN toInteger(contact.AUTH_SEQ_ID_2)
            ELSE toInteger(contact.AUTH_SEQ_ID_1)
        END as other_seq_id,
        CASE
            WHEN contact.AUTH_SEQ_ID_1 = $seq_id AND contact.AUTH_ASYM_ID_1 = $chain_id
            THEN contact.AUTH_ASYM_ID_1
            ELSE contact.AUTH_ASYM_ID_2
        END as ligand_chain,
        CASE
            WHEN contact.AUTH_SEQ_ID_1 = $seq_id AND contact.AUTH_ASYM_ID_1 = $chain_id
            THEN contact.AUTH_ASYM_ID_2
            ELSE contact.AUTH_ASYM_ID_1
        END as other_chain,
        CASE
            WHEN contact.AUTH_SEQ_ID_1 = $seq_id AND contact.AUTH_ASYM_ID_1 = $chain_id
            THEN contact.SYM_OP_1
            ELSE contact.SYM_OP_2
        END as ligand_sym_op,
        CASE
            WHEN contact.AUTH_SEQ_ID_1 = $seq_id AND contact.AUTH_ASYM_ID_1 = $chain_id
            THEN contact.SYM_OP_2
            ELSE contact.SYM_OP_1
        END as other_sym_op,
        CASE
            WHEN contact.AUTH_SEQ_ID_1 = $seq_id AND contact.AUTH_ASYM_ID_1 = $chain_id
            THEN contact.ATOM_1
            ELSE contact.ATOM_2
        END as ligand_atoms,
        CASE
            WHEN contact.AUTH_SEQ_ID_1 = $seq_id AND contact.AUTH_ASYM_ID_1 = $chain_id
            THEN contact.ATOM_2
            ELSE contact.ATOM_1
        END as other_atoms,
    contact.CONTACT_TYPE, toFloat(contact.DISTANCE), contact.INTERACTION_TYPE
    ORDER BY ligand_seq_id, other_seq_id
    """

    query = query.replace('###','= $sym_op') if sym_op else query.replace('###','is NULL')
    api_result = {
        pdb_id: [
            {
                'ligand': {
                    "author_residue_number": seq_id,
                    'chain_id': f'{chain_id}{sym_op}' if sym_op else chain_id,
                    'chem_comp_id': '',
                    'author_insertion_code': ''
                },
                'interactions': []
            }
        ]
    }

    query_result = run_query(query, {"pdb_id":pdb_id, "chain_id":chain_id, "seq_id":str(seq_id), "sym_op":sym_op})

    if not query_result:
        return {}, 404

    for result in query_result:
        (ligand_name, other_name, ligand_ins_code, other_ins_code, ligand_seq_id, other_seq_id,
         ligand_chain, other_chain, ligand_sym_op, other_sym_op, ligand_atoms, other_atoms,
         interactions, distance, interaction_type) = result

        ligand_r = build_residue_w_atom(ligand_name, ligand_chain, ligand_seq_id, ligand_atoms, ligand_sym_op, ligand_ins_code)
        other_r = build_residue_w_atom(other_name, other_chain, other_seq_id, other_atoms, other_sym_op, other_ins_code)

        api_result[pdb_id][0]['interactions'].append({
            'ligand_atoms': ligand_r['atom_names'],
            'end': other_r,
            'interaction_type': interaction_type,
            'interaction_details': eval(interactions),
            'distance': distance
        })

    api_result[pdb_id][0]['ligand']['chem_comp_id'] = ligand_r['chem_comp_id']
    api_result[pdb_id][0]['ligand']['author_insertion_code'] = " " if not ligand_r['author_insertion_code'] else ligand_r['author_insertion_code']

    return api_result, 200


def build_bm_residue(au_fallback, int_id, name, chain, seq_id, sym_op, ins):
    residue_detail = {}

    if not int_id:
        return residue_detail

    residue_detail["chain_id"] = chain if au_fallback == "true" else f'{chain}{sym_op}'
    residue_detail["author_residue_number"] = int(seq_id)
    residue_detail["chem_comp_id"] = name
    residue_detail["author_insertion_code"] = " " if not ins else ins

    return residue_detail


def build_residue_w_atom(name, chain, seq_id, atom_name, sym_op, ins):
    residue_detail = {}
    if not name:
        return residue_detail

    residue_detail["chain_id"] = f'{chain}{sym_op}' if sym_op else chain
    residue_detail["author_residue_number"] = seq_id
    residue_detail["chem_comp_id"] = name
    residue_detail["atom_names"] = atom_name.split(',')
    residue_detail["author_insertion_code"] = " " if not ins else ins

    return residue_detail


"""
@api {get} pdb/sequence_conservation/:pdbId/:entityId Get sequence conservations for a PDB Entity
@apiName GetPDBEntitySequenceConservation
@apiGroup PDB
@apiDescription Get sequence conservations for a PDB Entity
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry
@apiParam {String} entityId=1 PDB Entity

@apiSuccess {Integer} entity_id Entity id (molecule number in mmcif-speak).
@apiSuccess {Integer} length Length of entities, available for polymeric entities.
@apiSuccess {Object[]} amino List of amino acids.
@apiSuccess {String} letter Amino acid one-letter code.
@apiSuccess {Float} proba The probability score.
@apiSuccess {Integer} start mmcif-style residue index of the starting residue (within entity or struct_asym_id).
@apiSuccess {Integer} end mmcif-style residue index of the ending residue (within entity or struct_asym_id).
@apiSuccess {String} seqId An MD5 hash representation of the sequence.

@apiExample {json=./examples/success/sequence_conservation_pdb_entity.json} apiSuccessExample Example success response JSON
"""
@app.get('/sequence_conservation/' + rePDBid + '/<entity_id>' + reSlashOrNot)
def get_sequence_conservation_pdb_entity_api(pdbid, entity_id):

    pdbid = pdbid.strip("'")
    entity_id = entity_id.strip("'")

    query = """
    MATCH
        (entry:Entry {ID:$pdbid})-[:HAS_ENTITY]->(entity:Entity {ID:$entity_id})-[:HAS_PDB_RESIDUE]->
        (pdbRes:PDBResidue)-[hr:HAS_SEQID]->(seq:PDBSequence)
    WHERE
        NOT hr.CONSERVED_SCORE IN ['m', 'i']
    WITH seq.UNIQID AS seqId, toInteger(pdbRes.ID) AS residueId, hr ORDER BY residueId
    RETURN
        seqId, COLLECT(residueId) AS residues, COLLECT(toFloat(hr.CONSERVED_SCORE)) AS scores,
        COLLECT(toFloat(hr.P_SCORE_A)) AS scoreA, COLLECT(toFloat(hr.P_SCORE_C)) AS scoreC, COLLECT(toFloat(hr.P_SCORE_D)) AS scoreD,
        COLLECT(toFloat(hr.P_SCORE_E)) AS scoreE, COLLECT(toFloat(hr.P_SCORE_F)) AS scoreF, COLLECT(toFloat(hr.P_SCORE_G)) AS scoreG,
        COLLECT(toFloat(hr.P_SCORE_H)) AS scoreH, COLLECT(toFloat(hr.P_SCORE_I)) AS scoreI, COLLECT(toFloat(hr.P_SCORE_K)) AS scoreK, 
        COLLECT(toFloat(hr.P_SCORE_L)) AS scoreL, COLLECT(toFloat(hr.P_SCORE_M)) AS scoreM, COLLECT(toFloat(hr.P_SCORE_N)) AS scoreN,
        COLLECT(toFloat(hr.P_SCORE_P)) AS scoreP, COLLECT(toFloat(hr.P_SCORE_Q)) AS scoreQ, COLLECT(toFloat(hr.P_SCORE_R)) AS scoreR,
        COLLECT(toFloat(hr.P_SCORE_S)) AS scoreS, COLLECT(toFloat(hr.P_SCORE_T)) AS scoreT, COLLECT(toFloat(hr.P_SCORE_V)) AS scoreV,
        COLLECT(toFloat(hr.P_SCORE_W)) AS scoreW, COLLECT(toFloat(hr.P_SCORE_Y)) AS scoreY
    """

    result = run_query(query, parameters={
        'pdbid': str(pdbid), 'entity_id': str(entity_id)
    })

    if(len(result) == 0):
        bottle.response.status = 404
        return {}

    final_result = {
        "identifier": pdbid,
        "main_track_color": "#808080",
        "sub_track_color": "#d3d3d3",
        "data": {}
    }

    result = result[0]

    final_result["length"] = len(result["residues"])
    final_result["seq_id"] = result["seqId"]
    residues = result["residues"]

    final_result["data"] = {
        "index": residues,
        "conservation_score": result["scores"],
        "probability_A": result["scoreA"],
        "probability_C": result["scoreC"],
        "probability_D": result["scoreD"],
        "probability_E": result["scoreE"],
        "probability_F": result["scoreF"],
        "probability_G": result["scoreG"],
        "probability_H": result["scoreH"],
        "probability_I": result["scoreI"],
        "probability_K": result["scoreK"],
        "probability_L": result["scoreL"],
        "probability_M": result["scoreM"],
        "probability_N": result["scoreN"],
        "probability_P": result["scoreP"],
        "probability_Q": result["scoreQ"],
        "probability_R": result["scoreR"],
        "probability_S": result["scoreS"],
        "probability_T": result["scoreT"],
        "probability_V": result["scoreV"],
        "probability_W": result["scoreW"],
        "probability_Y": result["scoreY"],
    }

    bottle.response.status = 200
    return final_result



"""
@api {get} pdb/ligand_monomers/:pdbId Get list of modelled instances of ligands
@apiName GetPDBLigandMonomers
@apiGroup PDB
@apiDescription This call provides a list of modelled instances of ligands, i.e. 'bound' molecules that are not waters.
@apiVersion 2.0.0

@apiParam {String} pdbId=3d12 PDB Entry

@apiUse GenericResidueTerms
@apiUse GenericChemCompTerms

@apiExample {json=./examples/success/ligand_monomers_pdb.json} apiSuccessExample Example success response JSON
"""
@app.get('/ligand_monomers/' + rePDBid + reSlashOrNot)
def get_ligand_monomers_pdb_api(pdbid):

    pdbid = pdbid.strip("'")

    query = """
    MATCH (entry:Entry {ID:$pdbid})-[:HAS_ENTITY]->(entity:Entity {POLYMER_TYPE:'B'})-[:IS_AN_INSTANCE_OF]->(bound_ligand:BoundLigand), (entity)-[:IS_A]->(chem_comp:ChemicalComponent)
    RETURN chem_comp.ID, chem_comp.NAME, entity.ID, bound_ligand.AUTH_ASYM_ID, bound_ligand.STRUCT_ASYM_ID, bound_ligand.RESIDUE_ID, bound_ligand.AUTH_SEQ_ID, bound_ligand.PDB_INS_CODE, 
        bound_ligand.ALT_CONF_NUM ORDER BY toInteger(entity.ID)
    """

    results = run_query(query, parameters={
        'pdbid': str(pdbid)
    })

    final_result = []

    if not results:
        bottle.response.status = 404
        return {}

    for result in results:
        (chem_comp_id, chem_comp_name, entity_id, auth_asym_id, struct_asym_id, residue_id, auth_seq_id, pdb_ins_code, alt_conf_num) = result

        final_result.append({
            "chem_comp_name": chem_comp_name,
            "entity_id": int(entity_id),
            "residue_number": int(residue_id) if residue_id is not None else "",
            "author_residue_number": int(auth_seq_id) if auth_seq_id is not None else "",
            "chain_id": auth_asym_id,
            "alternate_conformers": int(alt_conf_num) if alt_conf_num is not None else 0,
            "author_insertion_code": pdb_ins_code if pdb_ins_code is not None else "",
            "chem_comp_id": chem_comp_id,
            "struct_asym_id": struct_asym_id
        })

    bottle.response.status = 200
    return {
        pdbid: final_result
    }


"""
@api {get} pdb/modified_AA_or_NA/:pdbId Get modified residues for PDB entry
@apiName GetPDBModifiedResidues
@apiGroup PDB
@apiDescription This call provides a list of modelled instances of modified amino acids or nucleotides in protein, DNA or RNA chains.
@apiVersion 2.0.0

@apiParam {String} pdbId=4v5j PDB Entry

@apiUse GenericChemCompTerms
@apiUse GenericResidueTerms

@apiExample {json=./examples/success/modified_aa_or_na_pdb.json} apiSuccessExample Example success response JSON
"""
@app.get('/modified_AA_or_NA/' + rePDBid + reSlashOrNot)
def get_modified_aa_or_na_pdb_api(pdbid):

    pdbid = pdbid.strip("'")

    query = """
    MATCH (chem_comp:ChemicalComponent) WHERE chem_comp.TYPE IN ['P','D','R'] AND chem_comp.MON_NSTD_PARENT_CHEM_COMP_ID IS NOT null
    WITH COLLECT(chem_comp.ID) AS chem_comps
    MATCH (entry:Entry {ID:$pdbid})-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue), (entity)-[:CONTAINS_CHAIN]->(chain:Chain) 
        WHERE entity.POLYMER_TYPE IN ['P','D','R','D/R'] AND pdb_res.CHEM_COMP_ID IN chem_comps
    MATCH (pdb_res)-[chain_rel:IS_IN_CHAIN]->(chain)
    MATCH (chem_comp:ChemicalComponent)
        WHERE pdb_res.CHEM_COMP_ID=chem_comp.ID
    RETURN toInteger(entity.ID), chem_comp.NAME, chain.AUTH_ASYM_ID, chain.STRUCT_ASYM_ID, toInteger(chain_rel.AUTH_SEQ_ID), toInteger(pdb_res.ID), pdb_res.CHEM_COMP_ID, COALESCE(chain_rel.PDB_INS_CODE, ""), 
        COALESCE(toInteger(chain_rel.ALT_CONF_NUM), 0)
            ORDER BY toInteger(entity.ID), toInteger(pdb_res.ID)
    """

    results = run_query(query, parameters={
        'pdbid': str(pdbid)
    })

    final_result = []

    if not results:
        bottle.response.status = 404
        return {}

    for result in results:
        (entity_id, chem_comp_name, auth_asym_id, struct_asym_id, auth_seq_id, residue_id, chem_comp_id, pdb_ins_code, alt_conf_num) = result

        final_result.append({
            "chem_comp_name": chem_comp_name,
            "entity_id": entity_id,
            "residue_number": residue_id,
            "author_residue_number": auth_seq_id,
            "chain_id": auth_asym_id,
            "alternate_conformers": alt_conf_num,
            "author_insertion_code": pdb_ins_code,
            "chem_comp_id": chem_comp_id,
            "struct_asym_id": struct_asym_id
        })

    bottle.response.status = 200
    return {
        pdbid: final_result
    }

"""
@api {get} pdb/mutated_AA_or_NA/:pdbId Get mutated residues for PDB entry
@apiName GetPDBMutatedResidues
@apiGroup PDB
@apiDescription This call provides a list of modelled instances of mutated amino acids in proteins in an entry. (Note that at present it does not provide information about mutated nucleotides in RNA or DNA chains, but it would do so in near future.)
@apiVersion 2.0.0

@apiParam {String} pdbId=1bgj PDB Entry

@apiUse GenericChemCompTerms
@apiUse GenericResidueTerms
@apiSuccess {String} mutation_details An object of mutation details.
@apiSuccess {String} type The type of mutation.
@apiSuccess {String} from One-letter-code of original residue.
@apiSuccess {String} to One-letter-code of residue that the original one was mutated to.

@apiExample {json=./examples/success/mutated_aa_or_na_pdb.json} apiSuccessExample Example success response JSON
"""
@app.get('/mutated_AA_or_NA/' + rePDBid + reSlashOrNot)
def get_mutated_aa_or_na_pdb_api(pdbid):

    pdbid = pdbid.strip("'")

    query = """
    MATCH (entry:Entry {ID:$pdbid})-[:HAS_ENTITY]->(entity:Entity)-[:HAS_RESIDUE_CONFLICT]->(conf:ResidueConflict) WHERE entity.POLYMER_TYPE IN ['P','D','R','D/R']
    RETURN toInteger(entity.ID), conf.DETAILS
    """

    results = run_query(query, parameters={
        'pdbid': str(pdbid)
    })

    final_result = []

    if not results:
        bottle.response.status = 404
        return {}

    for result in results:
        (entity_id, details) = result

        for detail in details:
            [conflict_type, pdb_seq_id, unp_seq_id, chem_comp_id, unp_code, auth_asym_id, struct_asym_id] = detail.split("|")

            final_result.append({
                "entity_id": entity_id,
                "residue_number": int(pdb_seq_id),
                "author_residue_number": "",
                "chain_id": auth_asym_id,
                "author_insertion_code": "",
                "mutation_details": {
                    "to": get_amino_three_to_one(chem_comp_id),
                    "from": unp_code,
                    "type": conflict_type
                },
                "chem_comp_id": chem_comp_id,
                "struct_asym_id": struct_asym_id
            })

    bottle.response.status = 200
    return {
        pdbid: final_result
    }


def get_pdb_sequence(entry_id, entity_id):

    query = """
    MATCH (entry:Entry {ID:$entry_id})-[:HAS_ENTITY]->(entity:Entity {ID:$entity_id})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)
    WITH pdb_res.CHEM_COMP_ID AS chem_comp_id ORDER BY toInt(pdb_res.ID)
    WITH COLLECT(chem_comp_id) AS chem_comp_ids
    RETURN chem_comp_ids, SIZE(chem_comp_ids) AS seq_length
    """

    mapping = run_query(query, parameters={
        "entry_id": str(entry_id), "entity_id": str(entity_id)
    })

    (chem_comp_ids, seq_length) = mapping[0]

    if not chem_comp_ids:
        return {}, 404

    return mapping[0], 200


def build_bm_residue_with_entity(au_fallback, entity, entity_type, int_id, name, chain, seq_id, sym_op, ins):
    residue_detail = {}

    if not int_id:
        return residue_detail

    residue_detail["chain_id"] = chain if au_fallback == "true" else f'{chain}{sym_op}'
    residue_detail["author_residue_number"] = int(seq_id)
    residue_detail["chem_comp_id"] = name
    residue_detail["author_insertion_code"] = " " if not ins else ins
    residue_detail["entity"] = entity
    residue_detail["molecule_type"] = "Carbohydrate-polymer" if entity_type == 'S' else "Bound"

    return residue_detail
