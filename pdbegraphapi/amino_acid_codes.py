#!/usr/bin/env python

"""
amino_acid_codes.py: This have dictionary of amino acid three letter codes and respective one letter code and name
"""

amino_acid_codes = {

    'ALA': ('A', 'Alanine'),
    'ARG':	('R', 'Arginine'),
    'ASN':	('N', 'Asparagine'),
    'ASP':	('D', 'Aspartic acid'),
    'CYS': ('C', 'Cysteine'),
    'GLN': ('Q', 'Glutamine'),
    'GLU': ('E', 'Glutamic acid'),
    'GLY': ('G', 'Glycine'),
    'HIS': ('H', 'Histidine'),
    'ILE': ('I', 'Isoleucine'),
    'LEU': ('L', 'Leucine'),
    'LYS': ('K', 'Lysine'),
    'MET': ('M', 'Methionine'),
    'PHE': ('F', 'Phenylalanine'),
    'PRO': ('P', 'Proline'),
    'PYL': ('O', 'Pyrrolysine'),
    'SER': ('S', 'Serine'),
    'SEC': ('U', 'Selenocysteine'),
    'THR': ('T', 'Threonine'),
    'TRP': ('W', 'Tryptophan'),
    'TYR': ('Y', 'Tyrosine'),
    'VAL': ('V', 'Valine'),
    'ASX': ('B', 'Aspartic acid or Asparagine'),
    'GLX': ('Z', 'Glutamic acid or Glutamine'),
    'XAA': ('X', 'Any amino acid'),
    'XLE': ('J', 'Leucine or Isoleucine'),
    'TERM':	('', 'termination codon'),
    'CCS': ('CCS', 'CCS'),
    'CME': ('CME', 'CME'),
    'MLZ': ('MLZ', 'MLZ'),
    'CSO': ('CSO', 'CSO'),
    'TPO': ('TPO', 'TPO'),
    'SCH': ('SCH', 'SCH'),
    'MLY': ('MLY', 'MLY'),
    'A': ('A', 'Adenine'),
    'C': ('C', 'Cytosine'),
    'G': ('G', 'Guanine'),
    'T': ('T', 'Thymine'),
    'U': ('U', 'Uracil'),
    'DA': ('A', 'Adenine'),
    'DT': ('T', 'Thymine'),
    'DG': ('G', 'Guanine'),
    'DC': ('C', 'Cytosine'),
    'DU': ('U', 'Uracil')
}

amino_acid_codes_one_to_three = {
    'A': 'ALA',
    'R': 'ARG',
    'N': 'ASN',
    'D': 'ASP',
    'C': 'CYS',
    'Q': 'GLN',
    'E': 'GLU',
    'G': 'GLY',
    'H': 'HIS',
    'I': 'ILE',
    'L': 'LEU',
    'K': 'LYS',
    'M': 'MET',
    'F': 'PHE',
    'P': 'PRO',
    'O': 'PYL',
    'S': 'SER',
    'U': 'SEC',
    'T': 'THR',
    'W': 'TRP',
    'Y': 'TYR',
    'V': 'VAL',
    'B': 'ASX',
    'Z': 'GLX',
    'X': 'XAA',
    'J': 'XLE'
}