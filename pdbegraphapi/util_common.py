#!/usr/bin/env python

import bottle
from pdbegraphapi.amino_acid_codes import amino_acid_codes, amino_acid_codes_one_to_three
from functools import reduce

PEPVEP_RECORD_LIMIT = 10
PEPVEP_POSITION_LIMIT = 20


# def create_memcache_plugin():
#     return bottle_memcache.MemcachePlugin(
#         servers=config.MEMCACHE_SERVERS,
#         server_max_value_length=1024 * 1024 * 128
#     )


def split_nonconsecutive_residues(list_of_tuples):

    final_list = []
    temp_list = []
    prev_value = None

    for incr in range(0, len(list_of_tuples)):
        each_tuple = list_of_tuples[incr]

        current_value = each_tuple[0]
        
        # first element
        if prev_value is None:
            temp_list.append(each_tuple)
            prev_value = current_value
            continue
        
        if current_value == prev_value + 1:
            temp_list.append(each_tuple)
            prev_value = current_value

            # last element, add temp list to final list, else will lose the last item
            if incr == len(list_of_tuples) - 1:
                final_list.append(temp_list)

        else:
            final_list.append(temp_list.copy())
            temp_list.clear()
            temp_list.append(each_tuple)
            prev_value = current_value
        
    
    return final_list

def split_nonconsecutive_two_residues(list_of_lists):

    final_list = []
    temp_list = []
    residues_1, residues_2 = list_of_lists
    prev_value = None

    # first list is residue numbers
    for incr in range(0, len(residues_1)):
        current_value = int(residues_1[incr])

        # first element
        if prev_value is None:
            temp_list.append((residues_1[incr], residues_2[incr]))
            prev_value = current_value

            # if there is only one element, add temp list to final list, else will lose the last item
            if incr == len(residues_1) - 1:
                final_list.append(temp_list)

            continue

        if current_value == prev_value + 1:
            temp_list.append((residues_1[incr], residues_2[incr]))
            prev_value = current_value

            # last element, add temp list to final list, else will lose the last item
            if incr == len(residues_1) - 1:
                final_list.append(temp_list)

        else:
            final_list.append(temp_list.copy())
            temp_list.clear()
            temp_list.append((residues_1[incr], residues_2[incr]))
            prev_value = current_value

            # last element, add temp list to final list, else will lose the last item
            if incr == len(residues_1) - 1:
                final_list.append(temp_list)

    return final_list

def split_nonconsecutive_residues_with_code(list_of_lists):

    final_list = []
    temp_list = []
    residues, amino_acids = list_of_lists
    prev_value = None

    # first list is residue numbers
    for incr in range(0, len(residues)):
        current_value = int(residues[incr])

        # first element
        if prev_value is None:
            temp_list.append((residues[incr], amino_acids[incr]))
            prev_value = current_value

            # if there is only one element, add temp list to final list, else will lose the last item
            if incr == len(residues) - 1:
                final_list.append(temp_list)

            continue

        if current_value == prev_value + 1:
            temp_list.append((residues[incr], amino_acids[incr]))
            prev_value = current_value

            # last element, add temp list to final list, else will lose the last item
            if incr == len(residues) - 1:
                final_list.append(temp_list)

        else:
            final_list.append(temp_list.copy())
            temp_list.clear()
            temp_list.append((residues[incr], amino_acids[incr]))
            prev_value = current_value

            # last element, add temp list to final list, else will lose the last item
            if incr == len(residues) - 1:
                final_list.append(temp_list)

    return final_list


def split_nonconsecutive_two_residues_with_code(list_of_lists):
    
    final_list = []
    temp_list = []
    residues_1, residues_2, amino_acids = list_of_lists
    prev_value = None

    # first list is residue numbers
    for incr in range(0, len(residues_1)):
        current_value = int(residues_1[incr])

        # first element
        if prev_value is None:
            temp_list.append((residues_1[incr], residues_2[incr], amino_acids[incr]))
            prev_value = current_value

            # if there is only one element, add temp list to final list, else will lose the last item
            if incr == len(residues_1) - 1:
                final_list.append(temp_list)

            continue

        if current_value == prev_value + 1:
            temp_list.append((residues_1[incr], residues_2[incr], amino_acids[incr]))
            prev_value = current_value

            # last element, add temp list to final list, else will lose the last item
            if incr == len(residues_1) - 1:
                final_list.append(temp_list)

        else:
            final_list.append(temp_list.copy())
            temp_list.clear()
            temp_list.append((residues_1[incr], residues_2[incr], amino_acids[incr]))
            prev_value = current_value

            # last element, add temp list to final list, else will lose the last item
            if incr == len(residues_1) - 1:
                final_list.append(temp_list)

    return final_list


def get_amino_three_to_one(three_code):

    one_code = amino_acid_codes.get(three_code.upper())
    
    if one_code is None:
        return '*'

    return one_code[0]


def get_amino_one_to_three(one_code):

    three_code = amino_acid_codes_one_to_three.get(one_code)

    if three_code is None:
        return '*'
    
    return three_code


def count_residues(list_of_tuples):
    
    # PDBE-2654 - coverage was going wrong since the segments were not sorted, so sort the residues first and then count
    list_of_tuples = sorted(list_of_tuples, key=lambda x: x[0])
    
    return_count = 0
    prev_end = None
    
    for input_tuple in list_of_tuples:
        
        # take care of overlap, in some cases there would be duplicate residues in different segments which makes residue count greater than expected which creates coverage > 1 in some cases.
        # eg. PDB ID 262l and UniProt P00720
        
        # the very first tuple
        if prev_end is None: 
            return_count += (input_tuple[1] - input_tuple[0] + 1)
            prev_end = input_tuple[1]
            continue
        
        if input_tuple[0] < prev_end:
            return_count += (input_tuple[1] - prev_end)
        # PDBE-2667: issue in calculating residues resulted in 0 coverage for valid cases
        else:
            return_count += (input_tuple[1] - input_tuple[0] + 1)


        prev_end = input_tuple[1]

    return return_count

def handle_pepvep_param_error(query_params, mandatory_params):

    if not isinstance(query_params, list):
        bottle.response.status = 400
        return {
            "error": "Error in JSON request"
        }

    # check for max limit of records
    if len(query_params) > PEPVEP_RECORD_LIMIT:
        bottle.response.status = 400
        return {
            "error": "Maximum {} records allowed".format(PEPVEP_RECORD_LIMIT)
        }

    errors = []
    index = 0
    
    # check for max limit of positions
    for record in query_params:
        if record.get("positions"):
            if len(record.get("positions")) > PEPVEP_POSITION_LIMIT:
                errors.append("Maximum {} records allowed in position at index {}".format(PEPVEP_POSITION_LIMIT, index))

        index += 1

    for param in mandatory_params:
        index = 0
        for record in query_params:
            if record.get(param) is None:
                errors.append("Field {} is mandatory at index {}".format(param, index))

            index += 1

    if errors:
        bottle.response.status = 400
        return {
            "error": errors
        }

def generate_data_fragment(list_of_values):
    """
    Summary: 
    Args:
        list_of_values - This is a list of values. Each item should be a tuple. The first element in the tuple is another tuple of values name, accession and dataType. The other elements
            should be entryId, entityId, chain(s), unpResidueId, pdbResidueId and unpResidueCode.
            For eg: [(("Helix", "Helix", "Helix"), "1cbs", "1", "A", 100, 23, "G"), (("Helix", "Helix", "Helix"), "3unn", "1", "A", 22, 10, "T")]
    Returns:
    """

    master_dict = dict()
    residue_dict = dict()
    fragments = []
    
    for value in list_of_values:
        (name, accession, data_type), entry_id, entity_id, chains, unp_res_id, pdb_res_id, unp_res_code = value

        dict_key = (accession, unp_res_id, unp_res_code, pdb_res_id)

        if residue_dict.get(accession) is None:
            residue_dict[accession] = {
                "fragment": {
                    "name": name,
                    "accession": accession,
                    "dataType": data_type,
                    "residues": []
                },
                "res_id": [unp_res_id],
                "res_code": [unp_res_code],
                "pdb_res_id": [pdb_res_id]
            }
        else:
            residue_dict[accession]["res_id"].append(unp_res_id)
            residue_dict[accession]["res_code"].append(unp_res_code)
            residue_dict[accession]["pdb_res_id"].append(pdb_res_id)

        if master_dict.get(dict_key) is None:
            master_dict[dict_key] = [(entry_id, entity_id, chains)]
        else:
            master_dict[dict_key].append((entry_id, entity_id, chains))

    for accession, values in residue_dict.items():
        for residues in split_nonconsecutive_two_residues_with_code([values["res_id"], values["pdb_res_id"], values["res_code"]]):
            unp_start = residues[0][0]
            unp_end = residues[-1][0]
            pdb_start = residues[0][1]
            pdb_end = residues[-1][1]
            unp_start_code = get_amino_one_to_three(residues[0][2])
            unp_end_code = get_amino_one_to_three(residues[-1][2])

            residue_fragment = {
                "startIndex": unp_start,
                "endIndex": unp_end,
                "pdbStartIndex": pdb_start,
                "pdbEndIndex": pdb_end,
                "startCode": unp_start_code,
                "endCode": unp_end_code,
                "indexType": "UNIPROT",
                "pdbEntries": []
            }
            pdb_entries = []

            for entry in master_dict[(accession, unp_start, residues[0][2], pdb_start)]:
                pdb_entries.append(entry)

            for entry in master_dict[(accession, unp_end, residues[-1][2], pdb_end)]:
                pdb_entries.append(entry)

            for entry in list(set(pdb_entries)):
                chains = entry[2]

                if isinstance(chains, str):
                    chains = [chains]

                residue_fragment["pdbEntries"].append({
                    "pdbId": entry[0],
                    "entityId": entry[1],
                    "chainIds": chains
                })
            
            residue_dict[accession]["fragment"]["residues"].append(residue_fragment)

    for accession, values in residue_dict.items():
        # sort the list based on startIndex
        values["fragment"]["residues"] = sorted(values["fragment"]["residues"], key=lambda x: x["startIndex"])
        fragments.append(values["fragment"])

    return fragments

    
def get_pdb_sequence(pdb_id, entity_id, graph):

    query = """
    MATCH (entry:Entry {ID:$pdb_id})-[:HAS_ENTITY]->(entity:Entity {ID:$entity_id})-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)
    WITH pdb_res.ID AS pdb_res_id, pdb_res.CHEM_COMP_ID AS pdb_res_code ORDER BY toInteger(pdb_res_id)
    RETURN COLLECT(pdb_res_code)
    """

    mappings = list(graph.run(query, parameters={
        "pdb_id": str(pdb_id),
        "entity_id": str(entity_id)
    }))
    
    if not mappings:
        return None
    
    residues = mappings[0][0]
    
    if not residues:
        return None

    sequence = reduce(lambda x, y: x + y, list(map(lambda x: get_amino_three_to_one(x), residues)))
    seq_length = len(residues)

    return {
        "sequence": sequence,
        "length": seq_length
    }


def parse_number(value, opt_rounding=3):
    """Parses value to either int or float.

    Args:
        value (str): Value to be parsed

    Raises:
        ValueError: If the value cannot be parsed.

    Returns:
        int or float:
    """
    try:
        return int(value)
    except ValueError:
        try:
            return round(float(value), opt_rounding)
        except ValueError:
            raise ValueError('value is not a parsable string')
