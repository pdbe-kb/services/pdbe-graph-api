#!/usr/bin/env python

import bottle
from pdbegraphapi import request_handler
from pdbegraphapi.neo4j_model import run_query
from pdbegraphapi.config import rePDBid, reSlashOrNot, rePDBComplex, reCHAINid
from operator import itemgetter
from pdbegraphapi.util_common import handle_pepvep_param_error, get_amino_one_to_three
from concurrent.futures import ThreadPoolExecutor
from protvista_adapter.common_params import funpdbe_resource_dict
from collections import OrderedDict
from pdbegraphapi.uniprot import get_uniprot_sequence

print('[PEPVEP] Starting')

app = bottle.Bottle()

# Installs the request handler plugin
app.install(request_handler)


BEST_STRUCTURES_LIMIT = 30
POOL_MAX_WORKERS = 10

print('[PEPVEP] Loaded')


@app.post('/structures'+reSlashOrNot)
def get_pepvep_structures_api(query_params):

    errors = handle_pepvep_param_error(query_params, ["accession", "positions"])
    
    if errors:
        bottle.response.status = 400
        return errors

    result_list = []
    
    with ThreadPoolExecutor(max_workers = POOL_MAX_WORKERS) as executor:
        results = executor.map(get_pepvep_structures_api_helper, query_params)

    for result in results:
        result_list.append(result)

    bottle.response.status = 200
    return result_list


def get_pepvep_structures_api_helper(record):

    uniprot_accession = record.get("accession")
    positions = record.get("positions")
    
    api_result = {
        uniprot_accession: {
            "positions": [],
            "description": "Listing out the top {} best structures".format(BEST_STRUCTURES_LIMIT)
        }
    }

    results, resp_status = get_pepvep_structures(uniprot_accession, positions)
    
    for row in results:
        (unp_res_id, unp_res_code, entries, no_of_structures) = row

        api_result[uniprot_accession]["positions"].append({
            "position": int(unp_res_id),
            "position_code": get_amino_one_to_three(unp_res_code),
            "best_structures": entries,
            "count_best_structures": no_of_structures
        })

    # sort the list based on positions
    api_result[uniprot_accession]["positions"] = sorted(api_result[uniprot_accession]["positions"], key=itemgetter("position"))

    return api_result


def get_pepvep_structures(uniprot_accession, residues):
    
    query = """
    WITH $positions AS positions, $accession AS accession
    MATCH (unp:UniProt)-[:HAS_UNP_RESIDUE]->(unp_res:UNPResidue) WHERE unp.ACCESSION=accession AND unp_res.ID IN positions
    WITH unp, toInteger(unp_res.ID) as position, unp_res.ONE_LETTER_CODE AS position_code
    MATCH (unp)<-[rel:HAS_UNIPROT_SEGMENT]-(entity:Entity) WHERE position IN RANGE(toInteger(rel.UNP_START), toInteger(rel.UNP_END))
    WITH DISTINCT position, position_code, SPLIT(entity.UNIQID, '_')[0] AS entry_id, rel.IDENTITY AS coverage
    MATCH (entry:Entry) WHERE entry.ID=entry_id
    WITH position, position_code, entry AS entry, coverage ORDER BY toFloat(coverage) DESC, toFloat(entry.RESOLUTION)
    WITH position, position_code, COLLECT(DISTINCT entry.ID)[..$best_structures_limit] AS entries
    RETURN position, position_code, entries, SIZE(entries)
    """

    query_results = run_query(query, parameters={
        "accession": str(uniprot_accession),
        "positions": residues,
        "best_structures_limit": BEST_STRUCTURES_LIMIT
    })

    if not query_results:
        return [], 404
    
    return query_results, 200


@app.post('/summary_stats'+reSlashOrNot)
def get_pepvep_summary_stats_api(query_params):

    errors = handle_pepvep_param_error(query_params, ["accession", "positions"])
    
    if errors:
        bottle.response.status = 400
        return errors
    
    result_list = []

    with ThreadPoolExecutor(max_workers = POOL_MAX_WORKERS) as executor:
        results = executor.map(get_pepvep_summary_stats_api_helper, query_params)
    
    for result in results:
        result_list.append(result)
    
    failed_count = 0

    for item in result_list:
        for accession in item.keys():
            if item[accession]["length"] is None:
                failed_count += 1

    if len(result_list) == failed_count:
        bottle.response.status = 404
        return {}

    bottle.response.status = 200
    return result_list


def get_pepvep_summary_stats_api_helper(record):

    uniprot_accession = record.get("accession")
    positions = record.get("positions")
    
    # remove duplicate residues
    if not positions:
        positions = list(set(positions))

    api_result = {
        uniprot_accession: {
            "structures": {},
            "ligands": [],
            "interactions": [],
            "annotations": [],
            "all_structures": []
        }
    }

    # get uniprot length
    uniprot_sequence_result = get_uniprot_sequence(uniprot_accession)

    if uniprot_sequence_result[0]:
        api_result[uniprot_accession]["length"] = get_uniprot_sequence(uniprot_accession)[0][1]
    else:
        api_result[uniprot_accession]["length"] = None
        return api_result

    # get all structures
    api_result[uniprot_accession]["all_structures"] = get_pepvep_all_structures(uniprot_accession)

    # get best structures
    api_result[uniprot_accession]["structures"] = get_pepvep_structures_api_helper(record)[uniprot_accession]

    # get ligands
    api_result[uniprot_accession]["ligands"] = get_pepvep_ligands_api_helper(record)[uniprot_accession]

    # get interaction partners
    api_result[uniprot_accession]["interactions"] = get_pepvep_interactions_api_helper(record)[uniprot_accession]

    # get annotations
    api_result[uniprot_accession]["annotations"] = get_pepvep_annotations_api_helper(record)[uniprot_accession]

    return api_result


def get_pepvep_all_structures(uniprot_accession):

    all_query = """
    MATCH (unp:UniProt)<-[rel:HAS_UNIPROT_SEGMENT]-(entity:Entity) 
    WHERE unp.ACCESSION=$accession
    WITH SPLIT(entity.UNIQID, '_')[0] AS entry_id, entity.ID AS entity_id, rel.AUTH_ASYM_ID AS chain_id, rel.UNP_START AS unp_start, rel.UNP_END AS unp_end ORDER BY toInteger(unp_start)
    WITH entry_id, entity_id, chain_id +','+unp_start +'-' +unp_end AS segment
    WITH entry_id, entity_id, COLLECT(segment) AS segments
    MATCH (entry:Entry) WHERE entry.ID=entry_id
    RETURN entry.ID AS entry_id, entity_id, segments ORDER BY toFloat(entry.RESOLUTION)
    """

    all_query_results = run_query(all_query, parameters={
        "accession": str(uniprot_accession)
    })

    if not all_query_results:
        return {}
    
    final_result = OrderedDict()

    for record in all_query_results:
        (entry_id, entity_id, segments) = record

        if final_result.get(entry_id) is None:
            final_result[entry_id] = []

        for segment in segments:
            [chain_id, residue_range] = segment.split(",")

            final_result[entry_id].append({
                "entity_id": entity_id,
                "chain_id": chain_id,
                "residue_range": residue_range
            })

    return final_result


def get_pepvep_ligands_api_helper(record):

    uniprot_accession = record.get("accession")
    positions = record.get("positions")
    
    api_result = {
        uniprot_accession: {
            "positions": []
        }
    }

    results, resp_status = get_pepvep_ligands(uniprot_accession, positions)
    dict_position = dict()

    for row in results:
        (unp_res_id, unp_res_code, ligand, polymer_entities) = row
        structures = set()

        dict_key = (unp_res_id, unp_res_code)

        if dict_position.get(dict_key) is None:
            dict_position[dict_key] = {
                "position": int(unp_res_id),
                "position_code": get_amino_one_to_three(unp_res_code),
                "ligands": [],
                "count_ligands": 0
            }

        for polymer_entity in polymer_entities:
            structures.add(polymer_entity.split("_")[0])

        [ligand_id, ligand_name, formula, inChi] = ligand.split('|')

        dict_position[dict_key]["ligands"].append({
            "ligand_id": ligand_id,
            "ligand_name": ligand_name,
            "formula": formula,
            "InChi": inChi,
            "structures": list(structures)
        })

        dict_position[dict_key]["count_ligands"] += 1

    for value in dict_position.values():
        api_result[uniprot_accession]["positions"].append(value)

    # sort the list based on positions
    api_result[uniprot_accession]["positions"] = sorted(api_result[uniprot_accession]["positions"], key=itemgetter("position"))

    return api_result


def get_pepvep_ligands(uniprot_accession, residues):
    
    query = """
    WITH $positions AS positions, $accession AS accession
    MATCH (unp:UniProt)-[:HAS_UNP_RESIDUE]->(unp_res:UNPResidue)-[rel:BOUNDED_BY]->(chem_comp:ChemicalComponent) WHERE unp.ACCESSION=accession AND unp_res.ID IN positions
    MATCH (chem_comp)-[:DESCRIBED_BY]->(chem_comp_desc:ChemicalComponentDesc) WHERE chem_comp_desc.PROGRAM='InChI' AND chem_comp_desc.TYPE='InChI'
    WITH unp_res.ID AS position, unp_res.ONE_LETTER_CODE AS position_code, chem_comp, chem_comp_desc, rel
    WITH DISTINCT position, position_code, chem_comp.ID +'|' +chem_comp.NAME +'|' +chem_comp.FORMULA +'|' +chem_comp_desc.DESCRIPTOR AS ligand, rel.INTERACTING_POLYMER_ENTITIES AS polymer_entities ORDER BY ligand
    RETURN position, position_code, ligand, polymer_entities
    """
   
    query_results = run_query(query, parameters={
        "accession": str(uniprot_accession),
        "positions": residues
    })

    if not query_results:
        return [], 404
    
    return query_results, 200

def get_pepvep_interactions_api_helper(record):

    uniprot_accession = record.get("accession")
    positions = record.get("positions")
    
    api_result = {
        uniprot_accession: {
            "positions": []
        }
    }

    results, resp_status = get_pepvep_interactions(uniprot_accession, positions)
    dict_position = dict()
    
    for row in results:
        (unp_res_id, unp_res_code, partner, polymer_entities) = row
        dict_key = (unp_res_id, unp_res_code)

        if dict_position.get(dict_key) is None:
            dict_position[dict_key] = {
                "position": int(unp_res_id),
                "position_code": get_amino_one_to_three(unp_res_code),
                "partners": [],
                "count_partners": 0
            }

        # handle UniProt accession cases
        if "::" in partner:
            [accession, desc] = partner.split("::")
        else:
            accession = desc = partner

        structures = set()
        for list_polymer in polymer_entities:
            for polymer in list_polymer:
                structures.add(polymer.split("_")[0])

        dict_position[dict_key]["partners"].append({
            "partner_name": desc,
            "partner_accession": accession,
            "structures": list(structures)
        })

        dict_position[dict_key]["count_partners"] += 1
    
    for value in dict_position.values():
        api_result[uniprot_accession]["positions"].append(value)

    # sort the list based on positions
    api_result[uniprot_accession]["positions"] = sorted(api_result[uniprot_accession]["positions"], key=itemgetter("position"))

    return api_result


def get_pepvep_interactions(uniprot_accession, residues):
    
    query = """
    WITH $positions AS positions, $accession AS accession
    MATCH (unp:UniProt)-[:HAS_UNP_RESIDUE]->(unp_res:UNPResidue)-[rel:INTERACTS_WITH]->(partner) WHERE unp.ACCESSION=accession AND unp_res.ID IN positions
    WITH unp_res.ID AS position, unp_res.ONE_LETTER_CODE AS position_code, partner, rel
    WITH
        position,
        position_code,
        CASE partner.ACCESSION
            WHEN null
                THEN
                    CASE partner.POLYMER_TYPE
                        WHEN 'D'
                            THEN 'DNA'
                        WHEN 'R'
                            THEN 'RNA'
                        WHEN 'D/R'
                            THEN 'DNA/RNA'
                        WHEN 'P'
                            THEN 'Other'
                    END
            ELSE partner.ACCESSION +'::' +partner.DESCR
        END AS partner,
        rel.INTERACTING_POLYMER_ENTITIES AS polymer_entities
    RETURN position, position_code, partner, COLLECT(polymer_entities)
    """
   
    query_results = run_query(query, parameters={
        "accession": str(uniprot_accession),
        "positions": residues
    })

    if not query_results:
        return [], 404
    
    return query_results, 200


def get_pepvep_annotations_api_helper(record):

    uniprot_accession = record.get("accession")
    positions = record.get("positions")
    
    api_result = {
        uniprot_accession: {
            "positions": []
        }
    }

    results, resp_status = get_pepvep_annotations(uniprot_accession, positions)
    
    for row in results:
        (unp_res_id, unp_res_code, data_resources, count_data_resource) = row

        api_result[uniprot_accession]["positions"].append({
            "position": int(unp_res_id),
            "position_code": get_amino_one_to_three(unp_res_code),
            "data_resource": data_resources,
            "count_data_resource": count_data_resource
        })

    # provide details on the annotations
    api_result[uniprot_accession]["details"] = {}

    # add details of each data resource if atleast one annotation exists
    if results:
        for annotation in funpdbe_resource_dict.keys():

            api_result[uniprot_accession]["details"][annotation] = {
                "type": funpdbe_resource_dict[annotation]["label"],
                "name": funpdbe_resource_dict[annotation]["displayName"],
                "description": funpdbe_resource_dict[annotation]["desc"],
                "link": funpdbe_resource_dict[annotation]["link"]
            }

    # sort the list based on positions
    api_result[uniprot_accession]["positions"] = sorted(api_result[uniprot_accession]["positions"], key=itemgetter("position"))

    return api_result


def get_pepvep_annotations(uniprot_accession, residues):
    
    query = """
    WITH $positions AS positions, $accession AS accession
    MATCH (unp:UniProt)-[:HAS_UNP_RESIDUE]->(unp_res:UNPResidue) WHERE unp.ACCESSION=accession AND unp_res.ID IN positions
    WITH unp, toInteger(unp_res.ID) as position, unp_res.ONE_LETTER_CODE AS position_code
    MATCH (unp)<-[rel:HAS_UNIPROT_SEGMENT]-(entity:Entity) WHERE position IN RANGE(toInteger(rel.UNP_START), toInteger(rel.UNP_END))
    WITH DISTINCT position, position_code, SPLIT(entity.UNIQID, '_')[0] AS entry_id, (toInteger(rel.PDB_END) - (toInteger(rel.UNP_END) - position)) AS pdb_res_position
    MATCH (entry:Entry) WHERE entry.ID=entry_id
    WITH position, position_code, entry AS entry, pdb_res_position ORDER BY toFloat(entry.RESOLUTION)
    WITH position, position_code, COLLECT(entry)[..$best_structures_limit] AS entries, pdb_res_position
    UNWIND entries AS entry
    MATCH (entry)-[:HAS_ENTITY]->(entity)-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)<-[res_rel:FUNPDBE_ANNOTATION_FOR]-(fun_group:FunPDBeResidueGroup)-[:FUNPDBE_RESIDUE_GROUP_OF]->(fun_entry:FunPDBeEntry)
    WHERE NOT fun_entry.DATA_RESOURCE IN ["dynamine", "depth"] AND toInteger(pdb_res.ID)=pdb_res_position
    WITH DISTINCT position, position_code, fun_entry.DATA_RESOURCE AS data_resource ORDER BY data_resource
    RETURN position, position_code, COLLECT(data_resource) AS data_resources, SIZE(COLLECT(data_resource)) AS count_data_resources
    """
   
    query_results = run_query(query, parameters={
        "accession": str(uniprot_accession),
        "positions": residues,
        "best_structures_limit": BEST_STRUCTURES_LIMIT
    })

    if not query_results:
        return [], 404

    return query_results, 200
