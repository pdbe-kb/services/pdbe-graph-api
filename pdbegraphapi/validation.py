#!/usr/bin/env python

import bottle
from decimal import Decimal

from pdbegraphapi import request_handler
from pdbegraphapi.neo4j_model import run_query
from pdbegraphapi.config import rePDBid, reSlashOrNot
from pdbegraphapi.common_params import RSRZ_OUTLIER_CUTOFF, RNA_suite_not_nonRotameric

print('[VALIDATION] Starting')

list_re = r'[\s\']'

app = bottle.Bottle()

# Installs the request handler plugin
app.install(request_handler)


print('[VALIDATION] Loaded')


"""
@api {get} validation/protein-ramachandran-sidechain-outliers/entry/:pdbId Get SIFTS backbone and sidechain outliers for a PDB Entry ID
@apiName GetBackSidePDB
@apiGroup Validation
@apiDescription This call returns backbone and sidechain outliers in protein chains, as calculated by Molprobity as part of wwPDB validation pipeline.
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID

@apiUse ValidationTerms
@apiSuccess {Object[]} ramachandran_outliers Protein backbone outliers.
@apiSuccess {Object[]} sidechain_outliers Protein sidechain outliers.

@apiExample {json=./examples/success/protein_ramachandran_sidechain_outliers.json} apiSuccessExample Example success response JSON
"""
@app.get('/protein-ramachandran-sidechain-outliers/entry/'+rePDBid+reSlashOrNot)
def get_validation_protein_ramachandran_sidechain_outliers_api(pdbid):

    pdbid = pdbid.strip("'")

    api_result = {
        pdbid: {}
    }

    response, response_status = get_validation_protein_ramachandran_sidechain_outliers(pdbid)

    if response_status == 404:
        bottle.response.status = 404
        return api_result

    api_result[pdbid] = response

    bottle.response.status = 200
    return api_result


def get_validation_protein_ramachandran_sidechain_outliers(entry_id):

    query = """
    MATCH (entry:Entry {ID:$entry_id})-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[rel:IS_IN_CHAIN {OBSERVED:'Y'}]->(chain:Chain)
    WHERE rel.RAMA CONTAINS "OUTLIER" OR rel.ROTA CONTAINS "OUTLIER"
    RETURN entity.ID, rel.RAMA, rel.ROTA, rel.MODEL, chain.AUTH_ASYM_ID, chain.STRUCT_ASYM_ID, pdb_res.ID, rel.AUTH_SEQ_ID, rel.PDB_INS_CODE, rel.ALTCODE
    """

    list_rama = []
    list_rota = []
    mappings = run_query(query, {"entry_id":entry_id})

    if(len(mappings) == 0):
        return {
            "ramachandran_outliers": [],
            "sidechain_outliers": []
        }, 404

    for mapping in mappings:

        (entity_id, rama, rota, model, auth_asym_id, struct_asym_id, res_id, auth_seq_id, pdb_ins_code, alt_code) = mapping

        # rama = [re.sub(list_re, "", a) for a in rama.strip('[]').split(',')]
        # rota = [re.sub(list_re, "", a) for a in rota.strip('[]').split(',')]
        # model = [re.sub(list_re, "", a) for a in model.strip('[]').split(',')]
        # alt_code = [re.sub(list_re, "", a) for a in alt_code.strip('[]').split(',')]
        
        # for incr in range(0, len(model)):

        if(rama == "OUTLIER"):
            list_rama.append({
                "model_id": int(model),
                "entity_id": int(entity_id),
                "residue_number": int(res_id),
                "author_residue_number": int(auth_seq_id),
                "chain_id": auth_asym_id,
                "author_insertion_code": "" if pdb_ins_code is None else pdb_ins_code,
                "alt_code": "" if alt_code is None else alt_code,
                "struct_asym_id": struct_asym_id
            })

        if(rota == "OUTLIER"):
            list_rota.append({
                "model_id": int(model),
                "entity_id": int(entity_id),
                "residue_number": int(res_id),
                "author_residue_number": int(auth_seq_id),
                "chain_id": auth_asym_id,
                "author_insertion_code": "" if pdb_ins_code is None else pdb_ins_code,
                "alt_code": "" if alt_code is None else alt_code,
                "struct_asym_id": struct_asym_id
            })
    
    return {
        "ramachandran_outliers": sorted(list_rama, key = lambda x: (x["model_id"], x["chain_id"], x["residue_number"])),
        "sidechain_outliers": sorted(list_rota, key = lambda x: (x["model_id"], x["chain_id"], x["residue_number"]))
    }, 200


"""
@api {get} validation/rama_sidechain_listing/entry/:pdbId Get Ramachandran status for a PDB Entry ID
@apiName GetRamaPDB
@apiGroup Validation
@apiDescription This call returns Ramachandran status (favoured, outlier, etc.), phi-psi values, sidechain status (rotamer name or outlier) as reported by Molprobity component of the 
wwPDB validation pipeline.
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID

@apiUse ValidationTerms
@apiSuccess {Float} psi Protein backbone dihedral angle psi.
@apiSuccess {Float} phi Protein backbone dihedral angle phi.
@apiSuccess {String} cis_peptide Cis isomerization of the peptide bond.as
@apiSuccess {String} rama Sidechain status (OUTLIER or rotamer name) of the residue.
@apiSuccess {String} rota Ramachandran status (Allowed, Favored, OUTLIER etc.) of the residue.

@apiExample {json=./examples/success/rama_sidechain_listing.json} apiSuccessExample Example success response JSON
"""
@app.get('/rama_sidechain_listing/entry/'+rePDBid+reSlashOrNot)
def get_validation_rama_sidechain_listing_api(pdbid):

    pdbid = pdbid.strip("'")

    api_result = {
        pdbid: {}
    }

    response, response_status = get_validation_rama_sidechain_listing(pdbid)

    if response_status == 404:
        bottle.response.status = 404
        return {}

    api_result[pdbid] = response

    bottle.response.status = 200
    return api_result


def get_validation_rama_sidechain_listing(entry_id):
    
    query = """
    MATCH (entry:Entry {ID:$entry_id})-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[rel:IS_IN_CHAIN {OBSERVED:'Y'}]->(chain:Chain) WHERE rel.MODEL IS NOT null
    RETURN entity.ID, chain.AUTH_ASYM_ID, chain.STRUCT_ASYM_ID, rel.MODEL, rel.PSI, rel.CIS_PEPTIDE, pdb_res.ID, rel.AUTH_SEQ_ID, rel.PDB_INS_CODE, rel.RAMA, rel.PHI, pdb_res.CHEM_COMP_ID, rel.ROTA ORDER BY chain.AUTH_ASYM_ID, toInteger(pdb_res.ID)
    """

    dict_models = {}
    dict_chains = {}
    list_entities = []
    dict_residues = {}
    mappings = run_query(query, {"entry_id":entry_id})

    if(len(mappings) == 0):
        return {}, 404

    all_count = 0
    all_null_count = 0

    for mapping in mappings:
        (entity_id, auth_asym_id, struct_asym_id, model, psi, cis_peptide, res_id, auth_seq_id, pdb_ins_code, rama, phi, chem_comp_id, rota) = mapping
        # rama = [re.sub(list_re, "", a) for a in rama.strip('[]').split(',')]
        # rota = [re.sub(list_re, "", a) for a in rota.strip('[]').split(',')]
        # model = [re.sub(list_re, "", a) for a in model.strip('[]').split(',')]
        # psi = [re.sub(list_re, "", a) for a in psi.strip('[]').split(',')]
        # phi = [re.sub(list_re, "", a) for a in phi.strip('[]').split(',')]
        # cis_peptide = [re.sub(list_re, "", a) for a in cis_peptide.strip('[]').split(',')]

        # for incr in range(0, len(model)):
            
        all_count += 1

        if((psi, rama, phi, rota) == tuple('None' for x in range(0, 4))):
            all_null_count += 1
            continue

        key = (entity_id, auth_asym_id, struct_asym_id, model)

        if(entity_id not in list_entities):
            list_entities.append(entity_id)

        if(dict_chains.get(entity_id) is None):
            dict_chains[entity_id] = [(auth_asym_id, struct_asym_id)]
        elif((auth_asym_id, struct_asym_id) not in dict_chains[entity_id]):
            dict_chains[entity_id].append((auth_asym_id, struct_asym_id))

        model_key = (entity_id, auth_asym_id)

        if(dict_models.get(model_key) is None):
            dict_models[model_key] = [model]
        elif(model not in dict_models[model_key]):
            dict_models[model_key].append(model)

        if(dict_residues.get(key) is None):
            dict_residues[key] = [(psi, cis_peptide, res_id, auth_seq_id, pdb_ins_code, rama, phi, chem_comp_id, rota)]
        else:
            dict_residues[key].append((psi, cis_peptide, res_id, auth_seq_id, pdb_ins_code, rama, phi, chem_comp_id, rota))

    
    if(all_null_count == all_count):
        return {}, 404

    api_result = {
        "molecules": []
    }

    for entity in list_entities:

        entity_element = {
            "entity_id": int(entity),
            "chains": []
        }

        for (auth_asym_id, struct_asym_id) in dict_chains[entity]:
            
            chain_element = {
                "chain_id": auth_asym_id,
                "struct_asym_id": struct_asym_id,
                "models": []
            }

            for model in dict_models[(entity, auth_asym_id)]:

                model_element = {
                    "model_id": int(model),
                    "residues": []
                }

                for (psi, cis_peptide, res_id, auth_seq_id, pdb_ins_code, rama, phi, chem_comp_id, rota) in dict_residues[(entity, auth_asym_id, struct_asym_id, model)]:

                    residue_element = {
                        "psi": None if not psi else float("%.1f" % float(psi)),
                        "cis_peptide": None if not cis_peptide else cis_peptide,
                        "residue_number": int(res_id),
                        "author_residue_number": int(auth_seq_id),
                        "rama": None if not rama else rama,
                        "phi": None if not phi else float("%.1f" % float(phi)),
                        "author_insertion_code": "" if pdb_ins_code is None else pdb_ins_code,
                        "residue_name": chem_comp_id,
                        "rota": None if not rota else rota
                    }

                    model_element["residues"].append(residue_element)

                chain_element["models"].append(model_element)

            entity_element["chains"].append(chain_element)

        api_result["molecules"].append(entity_element)
    
    return api_result, 200


"""
@api {get} validation/RNA_pucker_suite_outliers/entry/:pdbId Get Suite and pucker outliers in RNA chains
@apiName GetRNAPucker
@apiGroup Validation
@apiDescription This call returns RNA backbone outliers, i.e. non-rotameric suites and unusual puckers, as calculated by Molprobity as part 
of wwPDB validation pipeline.
@apiVersion 2.0.0

@apiParam {String} pdbId=3j8g PDB Entry ID

@apiUse ValidationTerms
@apiSuccess {Object[]} pucker_outliers RNA sugar pucker outliers.

@apiExample {json=./examples/success/rna_pucker_suite_outliers.json} apiSuccessExample Example success response JSON
"""
@app.get('/RNA_pucker_suite_outliers/entry/'+rePDBid+reSlashOrNot)
def get_validation_rna_pucker_suite_outliers_api(pdbid):

    pdbid = pdbid.strip("'")

    response, response_status = get_validation_rna_pucker_suite_outliers(pdbid)

    api_result = {
        pdbid: response
    }

    if response_status == 404:
        bottle.response.status = 404
        return api_result

    api_result[pdbid] = response

    bottle.response.status = 200
    return api_result


def get_validation_rna_pucker_suite_outliers(entry_id):

    # sample entry : 3j8g
    query = """
    MATCH (entry:Entry {ID:$entry_id})-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[rel:IS_IN_CHAIN]->(chain:Chain)
    WHERE rel.RNA_SUITE CONTAINS 'NonRotameric' OR rel.RNA_SUITE CONTAINS 'Triaged/NotBinned' OR rel.RNA_PUCKER CONTAINS 'outlier'
    RETURN toInteger(entity.ID), chain.AUTH_ASYM_ID, chain.STRUCT_ASYM_ID, toInteger(pdb_res.ID), rel.MODEL, rel.RNA_SUITE, rel.RNA_PUCKER, toInteger(rel.AUTH_SEQ_ID), rel.PDB_INS_CODE, rel.ALTCODE
    ORDER BY chain.AUTH_ASYM_ID, toInteger(pdb_res.ID)
    """

    list_rna_pucker = []
    list_rna_suite = []
    mappings = run_query(query, {"entry_id":entry_id})

    if(len(mappings) == 0):
        return {
            "pucker_outliers": [],
            "suite_outliers": []
        }, 404

    for mapping in mappings:
        (entity_id, auth_asym_id, struct_asym_id, res_id, model, rna_suite, rna_pucker, auth_seq_id, pdb_ins_code, alt_code) = mapping

        # model = [re.sub(list_re, "", a) for a in model.strip('[]').split(',')]
        # rna_suite = [re.sub(list_re, "", a) for a in rna_suite.strip('[]').split(',')]
        # rna_pucker = [re.sub(list_re, "", a) for a in rna_pucker.strip('[]').split(',')]
        # alt_code = [re.sub(list_re, "", a) for a in alt_code.strip('[]').split(',')]

        # for incr in range(0, len(model)):

        if(rna_suite in ['NonRotameric','Triaged/NotBinned']):
            list_rna_suite.append({
                "model_id": int(model),
                "entity_id": entity_id,
                "residue_number": res_id,
                "author_residue_number": auth_seq_id,
                "chain_id": auth_asym_id,
                "author_insertion_code": "" if pdb_ins_code is None else pdb_ins_code,
                "alt_code": alt_code,
                "struct_asym_id": struct_asym_id
            })

        if(rna_pucker == 'outlier'):
            list_rna_pucker.append({
                "model_id": int(model),
                "entity_id": entity_id,
                "residue_number": res_id,
                "author_residue_number": auth_seq_id,
                "chain_id": auth_asym_id,
                "author_insertion_code": "" if pdb_ins_code is None else pdb_ins_code,
                "alt_code": alt_code,
                "struct_asym_id": struct_asym_id
            })

    return {
        "pucker_outliers": list_rna_pucker,
        "suite_outliers": list_rna_suite
    }, 200


"""
@api {get} validation/global-percentiles/entry/:pdbId Get Entry-wide validation metrics
@apiName GetEntryWideValMetrics
@apiGroup Validation
@apiDescription Metrics here are the ones recommended by validation task force. Global is against whole PDB archive and relative is against 
entries of comparable resolution.
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID

@apiSuccess {Object} percent-RSRZ-outliers Percentile based on percent RSRZ outliers (calculated on standard amino acid residues or nucleotides in protein, DNA, RNA chains).
@apiSuccess {Float} relative This percentile is based on entries in the PDB archive that are comparable to the entry, e.g. similar resolution for X-ray entries.
@apiSuccess {Float} rawvalue The raw value of the metric.
@apiSuccess {Float} absolute This percentile is based on all possible entries in the PDB archive.
@apiSuccess {Object} clashscore Percentile based on clash score calculated by Molpropbity component of the wwPDB validation pipeline.
@apiSuccess {Object} percent-rota-outliers Percentile based on percent sidechain outliers calculated by Molpropbity component of the wwPDB validation pipeline.
@apiSuccess {Object} percent-rama-outliers Percentile based on percent Ramachandran outliers calculated by Molpropbity component of the wwPDB validation pipeline.
@apiSuccess {Object} DCC_Rfree Percentile based on Rfree calculated by DCC package running with EDS component of the wwPDB validation pipeline.

@apiExample {json=./examples/success/global_percentiles.json} apiSuccessExample Example success response JSON
"""
@app.get('/global-percentiles/entry/'+rePDBid+reSlashOrNot)
def get_validation_global_percentiles_api(pdbid):

    pdbid = pdbid.strip("'")

    api_result = {
        pdbid: {}
    }

    val_result, response_status = get_validation_global_percentiles(pdbid)

    if response_status == 404:
        bottle.response.status = 404
        return api_result

    api_result[pdbid] = val_result

    bottle.response.status = 200
    return api_result


def get_validation_global_percentiles(entry_id):

    query = """
    MATCH (entry:Entry {ID:$entry_id})
    RETURN entry.ABS_PERCENTILE_PERCENT_RSRZ,entry.REL_PERCENTILE_PERCENT_RSRZ,entry.PERCENT_RSRZ_OUTLIERS, entry.REL_PERCENTILE_PERCENT_ROTA,entry.ABS_PERCENTILE_PERCENT_ROTA,
    entry.PERCENT_ROTA_OUTLIERS,entry.REL_PERCENTILE_PERCENT_RAMA,entry.ABS_PERCENTILE_PERCENT_RAMA,entry.PERCENT_RAMA_OUTLIERS,entry.REL_PERCENTILE_CLASHSCORE,
    entry.ABS_PERCENTILE_CLASHSCORE,entry.CLASHSCORE,entry.REL_PERCENTILE_DCC_RFREE,entry.ABS_PERCENTILE_DCC_RFREE,entry.DCC_RFREE,entry.ABS_PERCENTILE_RNA_SUITENESS,
    entry.REL_PERCENTILE_RNA_SUITENESS,entry.RNA_SUITENESS
    """

    mappings = run_query(query, {"entry_id":entry_id})

    if(len(mappings) == 0):
        return {}, 404

    api_result = {}

    for mapping in mappings:
        (abs_rsrz,rel_rsrz,raw_rsrz,rel_rota,abs_rota,raw_rota,rel_rama,abs_rama,raw_rama,rel_clash,abs_clash,raw_clash,rel_dcc,abs_dcc,raw_dcc,abs_rna,rel_rna,raw_rna) = mapping

        if(rel_rsrz is not None):
            api_result["percent-RSRZ-outliers"] = {
                "relative": float("%.2f" % float(rel_rsrz)),
                "rawvalue": float("%.2f" % float(raw_rsrz)),
                "absolute": float("%.2f" % float(abs_rsrz))
            }
        if(rel_clash is not None):
            api_result["clashscore"] = {
                "relative": float("%.2f" % float(rel_clash)),
                "rawvalue": float("%.2f" % float(raw_clash)),
                "absolute": float("%.2f" % float(abs_clash))
            }
        if(rel_rota is not None):
            api_result["percent-rota-outliers"] = {
                "relative": float("%.2f" % float(rel_rota)),
                "rawvalue": float("%.2f" % float(raw_rota)),
                "absolute": float("%.2f" % float(abs_rota))
            }
        if(rel_rama is not None):
            api_result["percent-rama-outliers"] = {
                "relative": float("%.2f" % float(rel_rama)),
                "rawvalue": float("%.2f" % float(raw_rama)),
                "absolute": float("%.2f" % float(abs_rama))
            }
        if(rel_dcc is not None):
            api_result["DCC_Rfree"] = {
                "relative": float("%.2f" % float(rel_dcc)),
                "rawvalue": float("%.2f" % float(raw_dcc)),
                "absolute": float("%.2f" % float(abs_dcc))
            }
        if(rel_rna is not None):
            api_result["RNAsuiteness"] = {
                "relative": float("%.2f" % float(rel_rna)),
                "rawvalue": float("%.2f" % float(raw_rna)),
                "absolute": float("%.2f" % float(abs_rna))
            }

        # only 1 record is returned
        return api_result, 200


"""
@api {get} validation/summary_quality_scores/entry/:pdbId Get summary of global absolute percentiles
@apiName GetGlobalAbsPercentageSummary
@apiGroup Validation
@apiDescription These scores are harmonic means of absolute percentiles of geometric metrics (e.g. ramachandran, clashscore, sidechains), reflections-based metrics (Rfree, RSRZ) and both these 
kinds of metrics taken together. Wherever a constitutent percentile is 0, the harmonic mean is defined to be 0. When constituent percentiles are all unavailable, the harmonic mean 
is null.
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID

@apiSuccess {Float} overall_quality Harmonic mean of all absolute validation percentile metrics.
@apiSuccess {Float} geometry_quality Harmonic mean of absolute percentiles related to model geometry.
@apiSuccess {Float} data_quality Harmonic mean of absolute percentiles related to experimental data and its fit to the model.
@apiSuccess {Boolean} experiment_data_available Sometimes data quality is absent due to unavailability of experimental data itself. This flag tells whether the data are available.

@apiExample {json=./examples/success/summary_quality_scores.json} apiSuccessExample Example success response JSON
"""
@app.get('/summary_quality_scores/entry/'+rePDBid+reSlashOrNot)
def get_validation_summary_quality_scores_api(pdbid):

    pdbid = pdbid.strip("'")

    api_result = {
        pdbid: {}
    }

    val_result, response_status = get_validation_summary_quality_scores(pdbid)

    if response_status == 404:
        bottle.response.status = 404
        return api_result

    api_result[pdbid] = val_result

    bottle.response.status = 200
    return api_result


def get_validation_summary_quality_scores(entry_id):

    query = """
    MATCH (entry:Entry {ID:$entry_id})-[:EXPERIMENT]->(method:Method)
    RETURN entry.DATA_QUALITY,entry.OVERALL_QUALITY,entry.GEOMETRY_QUALITY,method.METHOD,method.METHOD_CLASS
    """

    mappings = run_query(query, {"entry_id":entry_id})

    if(len(mappings) == 0):
        return {}, 404

    for mapping in mappings:

        (data_quality, overall_quality, geo_quality, method, method_class) = mapping
        
        # only 1 record is returned
        return {
            "overall_quality": None if(overall_quality is None) else float("%.2f" % float(overall_quality)),
            "geometry_quality": None if(geo_quality is None) else float("%.2f" % float(geo_quality)),
            "experiment_data_available": True if (method == 'X-ray diffraction' and method_class == 'x-ray') else "unknown",
            "data_quality": None if(data_quality is None) else float("%.2f" % float(data_quality))
        }, 200


"""
@api {get} validation/key_validation_stats/entry/:pdbId Get key validation stats
@apiName GetKeyValStats
@apiGroup Validation
@apiDescription This is still a very high level summary, but covers metrics of interest not included in percentiles, or a little more 
detail than just percentile.
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID

@apiSuccess {Object} protein_ramachandran Information about Ramachandran outliers.
@apiSuccess {Object} bonds Information about bond length outliers in protein, RNA and DNA chains.
@apiSuccess {Object} RSRZ Information about outlier residues according to the RSRZ criterion which measures the fit between model and electron density.
@apiSuccess {Object} rna_pucker Information about RNA sugar pucker outliers.
@apiSuccess {Object} angles Information about bond angle outliers in protein, RNA, DNA chains.
@apiSuccess {Object} protein_sidechains Information about protein sidechain outliers.
@apiSuccess {Object} rna_suite Information about RNA backbone rotamericity outliers.
@apiSuccess {Integer} num_outliers Number of outliers.
@apiSuccess {Integer} num_checked Number of residues checked for being outliers.
@apiSuccess {Float} percent_outliers Percentage of outliers with respect to number of residues checked.

@apiExample {json=./examples/success/key_validation_stats.json} apiSuccessExample Example success response JSON
"""
@app.get('/key_validation_stats/entry/'+rePDBid+reSlashOrNot)
def get_validation_key_validation_stats_api(pdbid):

    pdbid = pdbid.strip("'")

    api_result = {
        pdbid: {}
    }

    val_result, response_status = get_validation_key_validation_stats(pdbid)

    if response_status == 404:
        bottle.response.status = 404
        return api_result

    api_result[pdbid] = val_result

    bottle.response.status = 200
    return api_result


def get_validation_key_validation_stats(entry_id):

    query = """
    MATCH (entry:Entry {ID:$entry_id})-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)
    OPTIONAL MATCH (pdb_res)-[:HAS_VALIDATION_DATA]->(val_bond:ValBondOutlier)
    OPTIONAL MATCH (pdb_res)-[:HAS_VALIDATION_DATA]->(val_angle:ValAngleOutlier)
    RETURN entry.ANGLES_RMSZ,entry.NUM_ANGLES_RMSZ,entry.BONDS_RMSZ,entry.NUM_BONDS_RMSZ, count(val_bond) as num_val_bonds,count(val_angle) as num_val_angle
    """

    mappings = run_query(query, {"entry_id":entry_id})

    if(len(mappings) == 0):
        return {}, 404

    angle_rmsz, num_angle_rmsz, bonds_rmsz, num_bonds_rmsz, num_bond_outliers, num_angle_outliers = mappings[0]
    percent_bond_outliers = float((Decimal(num_bond_outliers) * Decimal(100.0) / Decimal(num_bonds_rmsz)))
    percent_angle_outliers = float((Decimal(num_angle_outliers) * Decimal(100.0) / Decimal(num_angle_rmsz)))

    api_result = {
        "bonds": {
            "rmsz":  float("%.2f" % float(bonds_rmsz)),
            "num_checked": int(num_bonds_rmsz),
            "percent_outliers": float("%.2f" % float(percent_bond_outliers)),
            "num_outliers": int(num_bond_outliers)
        },
        "angles": {
            "rmsz": float("%.2f" % float(angle_rmsz)),
            "num_checked": int(num_angle_rmsz),
            "percent_outliers": float("%.2f" % float(percent_angle_outliers)),
            "num_outliers": int(num_angle_outliers)
        }
    }

    query = """
    MATCH (entry:Entry {ID:$entry_id})-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[chain_rel:IS_IN_CHAIN {OBSERVED:'Y'}]->(chain:Chain)
    RETURN chain_rel.RAMA AS rama,chain_rel.ROTA AS rota,chain_rel.RSRZ AS rsrz,chain_rel.RNA_SUITE AS rna_suite,chain_rel.RNA_PUCKER AS rna_pucker
    """

    mappings = run_query(query, {"entry_id":entry_id})
    keys = ["rama", "rota", "rsrz", "rna_suite", "rna_pucker"]

    for key in keys:
        if(api_result.get(key) is None):
            api_result[key] = {
                "num_checked": 0,
                "num_outliers": 0
            }

    for mapping in mappings:
        
        for key in keys:
            
            # values returns list as string, so converting them as list
            # values = [a.strip() for a in mapping.get(key).strip('[]').split(',')]
            value = mapping.get(key)
            
            # for value in values:
            #     value = value.strip('\'')
                
            if value is not None:
                api_result[key]["num_checked"] += 1

                if(key in ["rama", "rota"] and value == "OUTLIER"):
                    api_result[key]["num_outliers"] += 1
                elif(key == "rsrz" and float(value) > RSRZ_OUTLIER_CUTOFF):
                    api_result[key]["num_outliers"] += 1
                elif(key == "rna_suite" and value not in RNA_suite_not_nonRotameric):
                    api_result[key]["num_outliers"] += 1
                elif(key == "rna_pucker" and value == "outlier"):
                    api_result[key]["num_outliers"] += 1
                    
    # correcting rna_pucker
    api_result["rna_pucker"]["num_checked"] = api_result["rna_suite"]["num_checked"]

    for key in keys:
        num_checked = Decimal(api_result[key]["num_checked"])
        num_outliers = Decimal(api_result[key]["num_outliers"])
        percent_outliers = None
        
        if(num_checked != 0):
            percent_outliers = (num_outliers * Decimal(100.0) / num_checked)

        api_result[key]["percent_outliers"] = None if percent_outliers is None else str(float("%.2f" % float(percent_outliers)))

    # renaming keys
    api_result["protein_ramachandran"] = api_result["rama"]
    del api_result["rama"]

    api_result["RSRZ"] = api_result["rsrz"]
    del api_result["rsrz"]

    api_result["protein_sidechains"] = api_result["rota"]
    del api_result["rota"]


    return api_result, 200

"""
@api {get} validation/xray_refine_data_stats/entry/:pdbId Get X-Ray refine data stats
@apiName GetXRayRefineStats
@apiGroup Validation
@apiDescription Get X-Ray refine data stats.
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID

@apiSuccess {String} source Source of this property, e.g. depositor or software.
@apiSuccess {String/Float/Integer} value Value of this property.
@apiSuccess {Object} EDS_R R factor calculated by EDS step of wwPDB validation pipeline.
@apiSuccess {Object} EDS_resolution_low Resolution calculated by EDS, the lower cutoff.
@apiSuccess {Object} IoverSigma Signal to noise ratio for highest resolution shell.
@apiSuccess {Object} acentric_outliers Number of acentric outliers.
@apiSuccess {Object} TransNCS A statement on translational NCS.
@apiSuccess {Object} TwinL L and L2 are metrics calculated by Xtriage as part of L-test for twinning.
@apiSuccess {Object} DCC_Rfree Free R factor recalculated by DCC software.
@apiSuccess {Object} DataCompleteness Completeness of data, determined by EDS step of wwPDB validation pipeline.
@apiSuccess {Object} TwinL2 L and L2 are metrics calculated by Xtriage as part of L-test for twinning.
@apiSuccess {Object} DCC_refinement_program Refinement program used by depositor.
@apiSuccess {Object} bulk_solvent_b Bulk solvent B.
@apiSuccess {Object} bulk_solvent_k Bulk solvent K.
@apiSuccess {Object} precent-free-reflections Percentage of total reflections that are part of the free set, i.e. not used in refinement.
@apiSuccess {Object} Fo_Fc_correlation Correlation between Fo and Fc structure factor amplitudes.
@apiSuccess {Object} centric_outliers Number of centric outliers.
@apiSuccess {Object} DCC_R R factor recalculated by DCC software.
@apiSuccess {Object} WilsonBestimate The Wilson B factor.
@apiSuccess {Object} numMillerIndices Number of structure factors recorded in the dataset used for refinement.
@apiSuccess {Object} EDS_resolution Resolution calculated by EDS, the higher cutoff.

@apiExample {json=./examples/success/xray_refine_data_stats.json} apiSuccessExample Example success response JSON
"""
@app.get('/xray_refine_data_stats/entry/'+rePDBid+reSlashOrNot)
def get_validation_xray_refine_data_stats_api(pdbid):


    pdbid = pdbid.strip("'")

    api_result = {
        pdbid: {}
    }

    response, response_status = get_validation_xray_refine_data_stats(pdbid)

    if response_status == 404:
        bottle.response.status = 404
        return {}

    api_result[pdbid] = response

    bottle.response.status = 200
    return api_result


def get_validation_xray_refine_data_stats(entry_id):

    query = """
    MATCH (entry:Entry {ID:$entry_id})
    RETURN entry.DATA_COMPLETENESS,entry.NUM_FREE_REFLECTIONS,toInteger(entry.NUM_MILLER_INDICES),entry.TRANS_NCS,entry.DCC_RFREE, entry.DCC_REFINEMENT_PROGRAM,toInteger(entry.CENTRIC_OUTLIERS),
    entry.TWIN_L, entry.EDS_R,entry.PERCENT_FREE_REFLECTIONS,toInteger(entry.ACENTRIC_OUTLIERS),entry.DCC_R,entry.EDS_RES_LOW,entry.WILSON_B_ESTIMATE,entry.BULK_SOLVENT_K,entry.EDS_RES,
    entry.FO_FC_CORRELATION, entry.I_OVER_SIGMA,entry.TWIN_L2,entry.BULK_SOLVENT_B
    """

    mappings = run_query(query, {"entry_id":entry_id})
    if(len(mappings) == 0):
        return {}, 404

    (data_completeness,num_free_reflections,num_miller_indices,trans_ncs,dcc_rfree,dcc_refinement_program,centric_outliers,twin_l,eds_r,percent_free_reflections,
    acentric_outliers,dcc_r,eds_res_low,wilson_b_estimate,bulk_solvent_k,eds_res,fo_fc_correlation,i_over_sigma,twin_l2,bulk_solvent_b) = mappings[0]

    if(data_completeness,num_free_reflections,num_miller_indices,trans_ncs,dcc_rfree,dcc_refinement_program,centric_outliers,twin_l,eds_r,percent_free_reflections,
    acentric_outliers,dcc_r,eds_res_low,wilson_b_estimate,bulk_solvent_k,eds_res,fo_fc_correlation,i_over_sigma,twin_l2,bulk_solvent_b) == tuple(None for x in range(0, 20)):
        return {
            "DataCompleteness": None,
            "num-free-reflections": None,
            "numMillerIndices": None,
            "TransNCS": None,
            "DCC_refinement_program": None,
            "centric_outliers": None,
            "TwinL": None,
            "EDS_R": None,
            "DCC_Rfree": None,
            "percent-free-reflections": None,
            "acentric_outliers": None,
            "DCC_R": None,
            "EDS_resolution_low": None,
            "WilsonBestimate": None,
            "bulk_solvent_k": None,
            "EDS_resolution": None,
            "Fo_Fc_correlation": None,
            "IoverSigma": None,
            "TwinL2": None,
            "bulk_solvent_b": None
        }, 200

    return {
        "DataCompleteness": {
            "source": "EDS",
            "value": None if data_completeness is None else float("%.2f" % float(data_completeness))
        },
        "num-free-reflections": {
            "source": "EDS",
            "value": None if num_free_reflections is None else int(num_free_reflections)
        },
        "numMillerIndices": {
            "source": "Xtriage(Phenix)",
            "value": num_miller_indices
        },
        "TransNCS": {
            "source": "Xtriage(Phenix)",
            "value": trans_ncs
        },
        "DCC_refinement_program": {
            "source": "DCC",
            "value": dcc_refinement_program
        },
        "centric_outliers": {
            "source": "Xtriage(Phenix)",
            "value": centric_outliers
        },
        "TwinL": {
            "source": "Xtriage(Phenix)",
            "value": None if twin_l is None else float("%.3f" % float(twin_l))
        },
        "EDS_R": {
            "source": "EDS",
            "value": None if eds_r is None else float("%.2f" % float(eds_r))
        },
        "DCC_Rfree": {
            "source": "DCC",
            "value": None if dcc_rfree is None else float("%.2f" % float(dcc_rfree))
        },
        "percent-free-reflections": {
            "source": "EDS",
            "value": None if percent_free_reflections is None else float("%.2f" % float(percent_free_reflections))
        },
        "acentric_outliers": {
            "source": "Xtriage(Phenix)",
            "value": acentric_outliers
        },
        "DCC_R": {
            "source": "DCC",
            "value": None if dcc_r is None else float("%.2f" % float(dcc_r))
        },
        "EDS_resolution_low": {
            "source": "EDS",
            "value": None if eds_res_low is None else float("%.2f" % float(eds_res_low))
        },
        "WilsonBestimate": {
            "source": "Xtriage(Phenix)",
            "value": None if wilson_b_estimate is None else float("%.3f" % float(wilson_b_estimate))
        },
        "bulk_solvent_k": {
            "source": "EDS",
            "value": None if bulk_solvent_k is None else float("%.3f" % float(bulk_solvent_k))
        },
        "EDS_resolution": {
            "source": "EDS",
            "value": None if eds_res is None else float("%.2f" % float(eds_res))
        },
        "Fo_Fc_correlation": {
            "source": "EDS",
            "value": None if fo_fc_correlation is None else float("%.3f" % float(fo_fc_correlation))
        },
        "IoverSigma": {
            "source": "Xtriage(Phenix)",
            "value": i_over_sigma
        },
        "TwinL2": {
            "source": "Xtriage(Phenix)",
            "value": None if twin_l2 is None else float("%.3f" % float(twin_l2))
        },
        "bulk_solvent_b": {
            "source": "EDS",
            "value": None if bulk_solvent_b is None else float("%.3f" % float(bulk_solvent_b))
        }
    }, 200


"""
@api {get} validation/residuewise_outlier_summary/entry/:pdbId Get a list of outlier types found in residues
@apiName GetResidueWiseOutlierSummary
@apiGroup Validation
@apiDescription A residue can have many types of geometric or experimental-data-based outliers. This call lists all kinds of outliers found in a residue. 
For residues with no recorded outlier, there is no information returned.
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID

@apiUse GenericResidueTerms

@apiExample {json=./examples/success/residuewise_outlier_summary.json} apiSuccessExample Example success response JSON
"""
@app.get('/residuewise_outlier_summary/entry/'+rePDBid+reSlashOrNot)
@app.post('/residuewise_outlier_summary/entry'+reSlashOrNot)
def get_validation_residuewise_outlier_summary_api(pdbid):

    
    pdbids = pdbid.replace("'", "").split(",")

    api_result = {}
    list_response_status = []

    for pdbid in pdbids:

        response, response_status = get_validation_residuewise_outlier_summary(pdbid)
        list_response_status.append(response_status)

        api_result[pdbid] = response

    # fail only if all responses are 404
    if list_response_status == [404 for x in range(1, len(list_response_status)+1)]:
        bottle.response.status = 404
        return {}
    
    bottle.response.status = 200
    return api_result


def get_validation_residuewise_outlier_summary(entry_id):

    query1 = """
    MATCH(entry:Entry {ID:$entry_id})-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[chain_rel:IS_IN_CHAIN {OBSERVED:'Y'}]->(chain:Chain)
    MATCH (pdb_res)-[val_rel:HAS_VALIDATION_DATA]->(val) WHERE chain.STRUCT_ASYM_ID=val_rel.STRUCT_ASYM_ID
    RETURN DISTINCT entity.ID, chain.AUTH_ASYM_ID, chain.STRUCT_ASYM_ID, chain_rel.MODEL, pdb_res.ID, chain_rel.AUTH_SEQ_ID, chain_rel.PDB_INS_CODE, chain_rel.ALTCODE, labels(val)[0]
    """

    query2 = """
    MATCH (entry:Entry {ID:$entry_id})-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[chain_rel:IS_IN_CHAIN]->(chain:Chain) WHERE chain_rel.MODEL <> 'null'
    RETURN entity.ID, chain.AUTH_ASYM_ID, chain.STRUCT_ASYM_ID, chain_rel.MODEL, pdb_res.ID, chain_rel.AUTH_SEQ_ID, chain_rel.PDB_INS_CODE, chain_rel.ALTCODE, chain_rel.RAMA, chain_rel.ROTA, chain_rel.RSRZ
    """

    mappings = run_query(query1, {"entry_id":entry_id}) + run_query(query2, {"entry_id":entry_id})
    
    if(len(mappings) == 0):
        return {}, 404

    api_result = {
        "molecules": []
    }

    dict_entity = {}
    dict_chains = {}
    dict_model = {}
    dict_residues = {}

    entity_id, pdb_res_id, auth_seq_id, pdb_ins_code, alt_code, auth_asym_id, struct_asym_id, model, type_of_outlier, rama, rota, rsrz = (None for x in range(0, 12))

    for mapping in mappings:

        types_of_outlier = []
        
        # in case of clashes, chirals, bonds, planes and symmetry clashes
        if(len(mapping) == 9):

            (entity_id, auth_asym_id, struct_asym_id, model, pdb_res_id, auth_seq_id, pdb_ins_code, alt_code, type_of_outlier) = mapping

            # model = [re.sub(list_re, "", a) for a in model.strip('[]').split(',')]

            if(model is None): continue

            if(type_of_outlier == "ValClash"):
                types_of_outlier.append("clashes")
            elif(type_of_outlier == "ValChiralOutlier"):
                types_of_outlier.append("chirals")
            elif(type_of_outlier == "ValBondOutlier"):
                types_of_outlier.append("bond_lengths")
            elif(type_of_outlier == "ValAngleOutlier"):
                types_of_outlier.append("bond_angles")
            elif(type_of_outlier == "ValPlaneOutlier"):
                types_of_outlier.append("planes")
            elif(type_of_outlier == "ValSymmClash"):
                types_of_outlier.append("symm_clashes")

        # in case of rama, rota and rsrz outliers
        elif(len(mapping) == 11):
            
            (entity_id, auth_asym_id, struct_asym_id, model, pdb_res_id, auth_seq_id, pdb_ins_code, alt_code, rama, rota, rsrz) = mapping

            # model = [re.sub(list_re, "", a) for a in model.strip('[]').split(',')]
            # rama = [re.sub(list_re, "", a) for a in rama.strip('[]').split(',')]
            # rota = [re.sub(list_re, "", a) for a in rota.strip('[]').split(',')]
            # rsrz = [re.sub(list_re, "", a) for a in rsrz.strip('[]').split(',')]
            # alt_code = [re.sub(list_re, "", a) for a in alt_code.strip('[]').split(',')]

            # for incr in range(0, len(model)):
            if(rota == 'OUTLIER'):
                types_of_outlier.append("sidechain_outliers")
            if(rama == 'OUTLIER'):
                types_of_outlier.append("ramachandran_outliers")
            if(rsrz != 'None' and float(rsrz) > RSRZ_OUTLIER_CUTOFF):
                types_of_outlier.append("RSRZ")
    
        # for incr in range(0, len(model)):

        if(dict_entity.get(entity_id) is None):
            dict_entity[entity_id] = [(auth_asym_id, struct_asym_id)]
        else:
            dict_entity[entity_id].append((auth_asym_id, struct_asym_id))

        if(dict_chains.get((entity_id, auth_asym_id)) is None):
            dict_chains[(entity_id, auth_asym_id)] = [model]
        else:
            dict_chains[(entity_id, auth_asym_id)].append(model)

        if(dict_model.get((entity_id, auth_asym_id, model)) is None):
            dict_model[(entity_id, auth_asym_id, model)] = [(pdb_res_id, auth_seq_id, pdb_ins_code)]
        else:
            dict_model[(entity_id, auth_asym_id, model)].append((pdb_res_id, auth_seq_id, pdb_ins_code))

        if(dict_residues.get((entity_id, auth_asym_id, model, pdb_res_id)) is None):
            dict_residues[(entity_id, auth_asym_id, model, pdb_res_id)] = [x for x in types_of_outlier]
        else:
            dict_residues[(entity_id, auth_asym_id, model, pdb_res_id)] += [x for x in types_of_outlier]

    for entity_id in dict_entity.keys():
        entity_segment = {
            "entity_id": int(entity_id),
            "chains": []
        }
        for auth_asym_id, struct_asym_id in set(dict_entity[entity_id]):
            chain_element = {
                "chain_id": auth_asym_id,
                "struct_asym_id": struct_asym_id,
                "models": []
            }
            for model in set(dict_chains[(entity_id, auth_asym_id)]):
                model_element = {
                    "model_id": int(model),
                    "residues": []
                }
                for pdb_res_id, auth_seq_id, pdb_ins_code in set(dict_model[(entity_id, auth_asym_id, model)]):
                    residue_element = {
                        "author_insertion_code": "" if pdb_ins_code is None else pdb_ins_code,
                        "author_residue_number": int(auth_seq_id),
                        "alt_code": "",
                        "outlier_types": dict_residues[(entity_id, auth_asym_id, model, pdb_res_id)],
                        "residue_number": int(pdb_res_id)
                    }
                    if residue_element["outlier_types"]:
                        model_element["residues"].append(residue_element)
                model_element["residues"] = sorted(model_element["residues"], key = lambda x: (x["residue_number"]))
                chain_element["models"].append(model_element)
            chain_element["models"] = sorted(chain_element["models"], key = lambda x: (x["model_id"]))
            entity_segment["chains"].append(chain_element)
        entity_segment["chains"] = sorted(entity_segment["chains"], key = lambda x: (x["chain_id"]))
        api_result["molecules"].append(entity_segment)
    api_result["molecules"] = sorted(api_result["molecules"], key = lambda x: (x["entity_id"]))
        

    return api_result, 200


"""
@api {get} validation/protein-RNA-DNA-geometry-outlier-residues/entry/:pdbId Get residues with geometric outliers in protein, DNA, RNA chains
@apiName GetGeometricOutliers
@apiGroup Validation
@apiDescription Lists residues in protein, DNA, RNA chains that contain various types of geometry outliers.
@apiVersion 2.0.0

@apiParam {String} pdbId=1cbs PDB Entry ID

@apiUse GenericResidueTerms

@apiExample {json=./examples/success/protein_rna_dna_geometry_outlier.json} apiSuccessExample Example success response JSON
"""
@app.get('/protein-RNA-DNA-geometry-outlier-residues/entry/'+rePDBid+reSlashOrNot)
def get_validation_protein_rna_dna_geometry_outlier_residues_api(pdbid):

    pdbid = pdbid.strip("'")

    api_result = {
        pdbid: {}
    }

    response, response_status = get_validation_protein_rna_dna_geometry_outlier_residues(pdbid)

    if response_status == 404:
        bottle.response.status = 404
        return api_result

    api_result[pdbid] = response

    bottle.response.status = 200
    return api_result
    

def get_validation_protein_rna_dna_geometry_outlier_residues(entry_id):

    query = """
    MATCH(entry:Entry {ID:$entry_id})-[:HAS_ENTITY]->(entity:Entity)-[:HAS_PDB_RESIDUE]->(pdb_res:PDBResidue)-[chain_rel:IS_IN_CHAIN {OBSERVED:'Y'}]->(chain:Chain)
    MATCH (pdb_res)-[val_rel:HAS_VALIDATION_DATA]->(val) WHERE chain.STRUCT_ASYM_ID=val_rel.STRUCT_ASYM_ID
    RETURN DISTINCT entity.ID, chain.AUTH_ASYM_ID, chain.STRUCT_ASYM_ID, chain_rel.MODEL, pdb_res.ID, chain_rel.AUTH_SEQ_ID, chain_rel.PDB_INS_CODE, chain_rel.ALTCODE, labels(val)[0]
    """

    mappings = run_query(query, {"entry_id":entry_id})

    if(len(mappings) == 0):
        return {}, 404

    api_result = {
        "molecules": []
    }

    dict_entity = {}
    dict_chains = {}
    dict_model = {}
    dict_residues = {}
    dict_outlier = {}
    list_outlier = ['clashes','chirals','bond_lengths','bond_angles','planes','symm_clashes']

    for mapping in mappings:

        types_of_outlier = []

        (entity_id, auth_asym_id, struct_asym_id, model, pdb_res_id, auth_seq_id, pdb_ins_code, alt_code, type_of_outlier) = mapping

        # model = [re.sub(list_re, "", a) for a in model.strip('[]').split(',')]
        # alt_code = [re.sub(list_re, "", a) for a in alt_code.strip('[]').split(',')]

        # for incr in range(0, len(model)):

        if(model is None): continue

        if(type_of_outlier == "ValClash"):
            types_of_outlier.append("clashes")
            type_of_outlier = "clashes"
        elif(type_of_outlier == "ValChiralOutlier"):
            types_of_outlier.append("chirals")
            type_of_outlier = "chirals"
        elif(type_of_outlier == "ValBondOutlier"):
            types_of_outlier.append("bond_lengths")
            type_of_outlier = "bond_lengths"
        elif(type_of_outlier == "ValAngleOutlier"):
            types_of_outlier.append("bond_angles")
            type_of_outlier = "bond_angles"
        elif(type_of_outlier == "ValPlaneOutlier"):
            types_of_outlier.append("planes")
            type_of_outlier = "planes"
        elif(type_of_outlier == "ValSymmClash"):
            types_of_outlier.append("symm_clashes")
            type_of_outlier = "symm_clashes"

        if(dict_entity.get(entity_id) is None):
            dict_entity[entity_id] = [(auth_asym_id, struct_asym_id)]
        else:
            dict_entity[entity_id].append((auth_asym_id, struct_asym_id))

        if(dict_chains.get((entity_id, auth_asym_id)) is None):
            dict_chains[(entity_id, auth_asym_id)] = [model]
        else:
            dict_chains[(entity_id, auth_asym_id)].append(model)

        if(dict_model.get((entity_id, auth_asym_id, model)) is None):
            dict_model[(entity_id, auth_asym_id, model)] = [(pdb_res_id, auth_seq_id, pdb_ins_code, alt_code)]
        else:
            dict_model[(entity_id, auth_asym_id, model)].append((pdb_res_id, auth_seq_id, pdb_ins_code, alt_code))

        if(dict_outlier.get((entity_id, auth_asym_id, model, type_of_outlier)) is None):
            dict_outlier[(entity_id, auth_asym_id, model, type_of_outlier)] = [(pdb_res_id, auth_seq_id, pdb_ins_code, alt_code)]
        else:
            dict_outlier[(entity_id, auth_asym_id, model, type_of_outlier)].append((pdb_res_id, auth_seq_id, pdb_ins_code, alt_code))

        if(dict_residues.get((entity_id, auth_asym_id, model, pdb_res_id)) is None):
            dict_residues[(entity_id, auth_asym_id, model, pdb_res_id)] = [x for x in types_of_outlier]
        else:
            dict_residues[(entity_id, auth_asym_id, model, pdb_res_id)] += [x for x in types_of_outlier]

    for entity_id in dict_entity.keys():
        entity_segment = {
            "entity_id": int(entity_id),
            "chains": []
        }
        for auth_asym_id, struct_asym_id in set(dict_entity[entity_id]):
            chain_element = {
                "chain_id": auth_asym_id,
                "struct_asym_id": struct_asym_id,
                "models": []
            }
            for model in set(dict_chains[(entity_id, auth_asym_id)]):
                model_element = {
                    "model_id": int(model),
                    "outlier_types": {}
                }
                for outlier in list_outlier:
                    if(dict_outlier.get((entity_id, auth_asym_id, model, outlier)) is not None):
                        outlier_element = []
                        for pdb_res_id, auth_seq_id, pdb_ins_code, alt_code in dict_outlier[(entity_id, auth_asym_id, model, outlier)]:
                            residue_element = {
                                "author_insertion_code": "" if pdb_ins_code is None else pdb_ins_code,
                                "author_residue_number": int(auth_seq_id),
                                "alt_code": alt_code,
                                "residue_number": int(pdb_res_id)
                            }
                            outlier_element.append(residue_element)
                        outlier_element = sorted(outlier_element, key = lambda x: (x["residue_number"]))
                        model_element["outlier_types"][outlier] = outlier_element
                chain_element["models"].append(model_element)
            chain_element["models"] = sorted(chain_element["models"], key = lambda x: (x["model_id"]))
            entity_segment["chains"].append(chain_element)
        entity_segment["chains"] = sorted(entity_segment["chains"], key = lambda x: (x["chain_id"]))
        api_result["molecules"].append(entity_segment)
    api_result["molecules"] = sorted(api_result["molecules"], key = lambda x: (x["entity_id"]))
        

    return api_result, 200
