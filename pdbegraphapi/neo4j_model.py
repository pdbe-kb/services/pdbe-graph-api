#!/usr/bin/env python

"""
neo4j_model.py: Retrieves an instance of Neo4J
"""

from neo4j import GraphDatabase
import os
from functools import lru_cache
import logging


def get_neo4j_instance():pass

logger = logging.getLogger(__name__)

@lru_cache(maxsize=None)
def load_driver():
    try:
        neo4j_driver = GraphDatabase.driver(
            os.environ.get("NEO4J_DEST_URL"),
            auth=(os.environ.get("NEO4J_USERNAME"), os.environ.get("NEO4J_PASSWORD")),
        )
        print(f"Connected to Neo4J instance {neo4j_driver.address}")
    except Exception:
        raise Exception(
            "Can't connect to Neo4J!!! Check connection details and credentials"
        )

    return neo4j_driver


def get_session(db_type="read"):
    """Returns a Neo4J driver session

    Args:
        db_type (str, optional): Type of DB access. Defaults to "read".

    Returns:
        neo4j.Session: A Neo4J driver session.
    """
    return load_driver().session()


def run_query(query: str, parameters: dict = None, desc: str = None) -> list:
    """Runs a Cypher query.

    Args:
        query (str): Cypher query to execute.
        parameters (dict, optional): Dictionary of query parameters. Defaults to None.
        desc (str, optional): Description of query. Defaults to None.

    Raises:
        CypherException: Raised when there is an error in Cypher query.

    Returns:
        list[neo4j.Result]: A list of result objects.
    """
    with get_session() as session:
        try:
            
            logger.debug(f"Executing Cypher: {desc if desc else ''}: STARTED")
            results = list(session.run(query, parameters=parameters))

            logger.debug(
                f"Executing Cypher: {desc if desc else ''} : ENDED"
            )
            return results
        except Exception as e:
            raise Exception("Query failed!!!", e)

