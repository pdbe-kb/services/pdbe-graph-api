from decimal import Decimal

funpdbe_biophysical_resources = ["dynamine", "depth"]

funpdbe_conf_color_code = {
    "high": "#728ef7",
    "medium": "#7fbcf1",
    "low": "#bad9f4",
    "null": "#dcdde2"
}

RSRZ_OUTLIER_CUTOFF = 2
BONDLENGTH_QUANTIZE_DECIMAL = Decimal("0.001")
RNA_suite_not_nonRotameric = ["NotAvailable", "Rotameric", None]

validation_switch = {
    "ValClash": "clashes",
    "ValAngleOutlier": "angle_outliers",
    "ValBondOutlier": "bond_angles",
    "ValChiralOutlier": "chirals",
    "ValPlaneOutlier": "plane_outliers",
    "ValSymmClash": "symmetry_clashes"
}

seq_conservation_aa_colors = {
    'A': '#80a0f0',
    'C': '#f08080',
    'D': '#c048c0',
    'E': '#c048c0',
    'F': '#80a0f0',
    'G': '#f09048',
    'H': '#15a4a4',
    'I': '#80a0f0',
    'K': '#f01505',
    'L': '#80a0f0',
    'M': '#80a0f0',
    'N': '#15c015',
    'P': '#c0c000',
    'Q': '#15c015',
    'R': '#f01505',
    'S': '#15c015',
    'T': '#15c015',
    'V': '#80a0f0',
    'W': '#80a0f0',
    'Y': '#15a4a4'
}
