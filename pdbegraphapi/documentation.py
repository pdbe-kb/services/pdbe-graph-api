import bottle
import os
from bottle import static_file

print('[DOCUMENTATION] Starting')
app = bottle.Bottle()
print('[DOCUMENTATION] Loaded')


# serve docu files for test instance
@app.route('/<filename:re:.*>')
def serve_api_doc_files(filename):
    documentation_path = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        "..",
        "pdbe_doc"
    )

    return static_file(filename, documentation_path)


# Load index.html by default.
# This is the default behavior for Apache but not for the local instance
# of the documentation pages.
@app.route('/')
def root():
    return serve_api_doc_files("index.html")
