import bottle
from pdbegraphapi import request_handler
from pdbegraphapi import pdb
from pdbegraphapi import sifts
from pdbegraphapi import residue
from pdbegraphapi import uniprot
from pdbegraphapi import compound
from pdbegraphapi import validation
from pdbegraphapi import pdbe_pages
from pdbegraphapi import pepvep
import simplejson

app = bottle.Bottle()
app.install(request_handler)

mounts = {
    'pdb': pdb.app,
    'mappings': sifts.app,
    'residue_mapping': residue.app,
    'uniprot': uniprot.app,
    'compound': compound.app,
    'validation': validation.app,
    'pdbe_pages': pdbe_pages.app,
    'pepvep': pepvep.app
}


for mountpoint, mountapp in mounts.items():

    app.mount('/' + mountpoint + '/', mountapp)

reSlashOrNot = '<:re:/{0,1}>'


# Common parameters to be used in documentation
"""
@apiDefine GenericFields

@apiSuccess {String} sequence Sequence of the entity - available for polymeric entities only. Usually there is one character per sequence position, but not if single-letter-code is actually multiple characters - so this string might be longer the length field suggests.
@apiSuccess {Integer} length Length of entities, available for polymeric entities.
@apiSuccess {String} dataType A string denoting the type of data provided in the section.
@apiSuccess {Object[]} data A list of objects which contains the data for the corresponding data type.
@apiSuccess {String} name Name of the resource, annotation, etc.
@apiSuccess {String} accession A unique identifier for the resource, annotation, etc.
@apiSuccess {Object[]} residues A list of residue objects.
@apiSuccess {Integer} startIndex Starting residue number. mmcif-style residue index (within entity or struct_asym_id) in case of PDB.
@apiSuccess {Integer} endIndex Ending residue number. mmcif-style residue index (within entity or struct_asym_id) in case of PDB.
@apiSuccess {String} indexType Type of index, can be PDB or UNP.
@apiSuccess {String} startCode Amino acid three-letter code for the residue in startIndex.
@apiSuccess {String} endCode Amino acid three-letter code for the residue in endIndex.
@apiSuccess {Object} additionalData An object which holds additional data related to the resource/annotation.
@apiSuccess {String} bestChainId The PDB Chain ID (auth_asym_id) of the best chain.
@apiSuccess {Integer} entityId Entity id (molecule number in mmcif-speak).
"""
 
"""
@apiDefine GenericResidueTerms

@apiSuccess {Integer} author_residue_number Residue number (in PDB-style residue addressing scheme).
@apiSuccess {String} author_insertion_code Residue insertion code.
@apiSuccess {Integer} residue_number mmcif-style residue index (within entity or struct_asym_id).
@apiSuccess {String} chain_id PDB chain id.
@apiSuccess {String} struct_asym_id struct_asym_id (chain id in mmcif-speak).
@apiSuccess {Integer} entity_id Entity id (molecule number in mmcif-speak).
@apiSuccess {String} chem_comp_id For entities represented as single molecules, the identifier corresponding to the chemical definition for the molecule.
@apiSuccess {Object} begin Denotes a residue object where a mapping starts.
@apiSuccess {Object} end Denotes a residue object where a mapping ends.
@apiSuccess {Integer} alternate_conformers Number of alternate conformers modelled for this residue.
"""

"""
@apiDefine GenericBoundMoleculeTerms

@apiSuccess {String} bm_id Bound molecule ID (can be obtained from the output of bound_molecules call).
@apiSuccess {Object} interactions An interaction class object and each item is a list of different types of interactions.
@apiSuccess {Object[]} ligands A list of ligands.
@apiSuccess {Object} composition An object which denotes the composition of a bound molecule.
@apiSuccess {String[][]} connections This defines the connection between 2 ligands in a bound molecule. Each ligand is represented as a concatenation of <strong>chain_id</strong> and <strong>author_residue_number</strong>.
"""

"""
@apiDefine GenericChemCompTerms

@apiSuccess {String} chem_comp_id Chemical component identifier.
@apiSuccess {String} chem_comp_name Name for the chemical component.
"""

"""
@apiDefine CATHTerms

@apiSuccess {String} homology CATH homology.
@apiSuccess {String} domain CATH domain.
@apiSuccess {Integer} segment_id The segment id (discontinus domains have same domain and different segment_id)
@apiSuccess {String} name The name for the CATH domain.
@apiSuccess {String} architecture The CATH architecture.
@apiSuccess {String} identifier An identifier for the accession.
@apiSuccess {String} class CATH class.
@apiSuccess {String} topology CATH topology.
"""

"""
@apiDefine SCOPTerms

@apiSuccess {String} superfamily SCOP superfamily.
@apiSuccess {Integer} segment_id The segment id (discontinus domains have same domain and different segment_id)
@apiSuccess {Integer} sunid SCOP sunid.
@apiSuccess {String} scop_id SCOP id.
@apiSuccess {String} fold SCOP fold.
@apiSuccess {String} class SCOP class.
"""

"""
@apiDefine EnsemblTerms

@apiSuccess {String} translation_id Translation id from Ensembl.
@apiSuccess {Integer} ordinal When a mapping is discontinuous (i.e. the mapping to UniProt has a gap) the different produced segments have consecutive ordinal numbers.When a mapping is discontinuous (i.e. the mapping to UniProt has a gap) the different produced segments have consecutive ordinal numbers.
@apiSuccess {Long} genome_end Index of the last base mapped to this PDB entity.
@apiSuccess {Long} genome_start Index of the first base mapped to this PDB entity.
@apiSuccess {String} transcript_id Transcript id from Ensembl.
@apiSuccess {Float} coverage The coverage of the Ensembl id.
@apiSuccess {String} exon_id Exon id from Ensembl.
"""

"""
@apiDefine ComplexTerms

@apiSuccess {Object[]} participants A list of participants that take part in a complex.
@apiSuccess {String} accession A unique identifier for the component.
@apiSuccess {Integer} stoichiometry Relative quantity of the component that takes part in the complex.
@apiSuccess {Integer} taxonomy_id Unique identifier assigned by the NCBI to the source organism.
@apiSuccess {String[]} subcomplexes A list of complex IDs which are the subcomplex of the complex in query.
"""

"""
@apiDefine ValidationTerms

@apiSuccess {Integer} author_residue_number Residue number (in PDB-style residue addressing scheme).
@apiSuccess {String} author_insertion_code Residue insertion code.
@apiSuccess {Integer} residue_number mmcif-style residue index (within entity or struct_asym_id).
@apiSuccess {String} chain_id PDB chain id.
@apiSuccess {String} struct_asym_id struct_asym_id (chain id in mmcif-speak).
@apiSuccess {Integer} entity_id Entity id (molecule number in mmcif-speak).
@apiSuccess {String} alt_code Alternate location indicator of the outlier residue.
"""


class SSLWSGIRefServer(bottle.ServerAdapter):
    '''gratefully copied from http://www.socouldanyone.com/2014/01/bottle-with-ssl.html'''

    def run(self, handler):
        from wsgiref.simple_server import make_server, WSGIRequestHandler
        import ssl
        if self.quiet:
            class QuietHandler(WSGIRequestHandler):
                def log_request(*args, **kw): pass
            self.options['handler_class'] = QuietHandler
        srv = make_server(self.host, self.port, handler, **self.options)
        srv.socket = ssl.wrap_socket(
            srv.socket,
            certfile='/tmp/server.pem',  # path to certificate
            server_side=True)
        print("SERVING ON HTTPS....")
        srv.serve_forever()


@app.get('/health')
def health(*args):
    from pdbegraphapi.neo4j_model import run_query

    try:
        run_query("MATCH (n) RETURN count(n) as count")
    except Exception as e:
        print(e)
        bottle.response.status = 500
        return {"status": "error"}

    bottle.response.status = 200
    return {"status": "ok"}

if __name__ == '__main__':

    print("Local instance")

    import documentation

    # to allow documentation to be shown from local server
    app.mount('/pdbe_doc/', documentation.app)

    @app.route('/doc'+reSlashOrNot)
    def foo(mc=None):
        bottle.redirect("/pdbe_doc/")

    import optparse

    parser = optparse.OptionParser()
    parser.add_option(
        "--host", help='''name of the host to run API instance, default is localhost''', default='127.0.0.1')
    parser.add_option(
        "--port", help='''port number of the host to run API instance on, default is 5000''', default=5000)
    parser.add_option(
        "--https", help='''runs by default on http, set this option to yes if you want https''', default=False)
    (opts, args) = parser.parse_args()

    if opts.https == "yes" or opts.https == "YES" or opts.https == "Yes":
        srv = SSLWSGIRefServer(host=opts.host, port=opts.port)
        bottle.run(app=app, reloader=False, server=srv)
    else:
        # error_handler = {
        #    405: method_not_allowed,
        # }

        #app.error_handler = error_handler

        bottle.run(app, host=opts.host, port=opts.port, reloader=True)
