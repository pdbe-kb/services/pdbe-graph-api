#!/usr/bin/env python

import bottle

from decimal import Decimal
import ast


from pdbegraphapi import request_handler
from pdbegraphapi.neo4j_model import run_query
from pdbegraphapi.util_common import parse_number

print("[COMPOUND] Starting")

BONDLENGTH_QUANTIZE_DECIMAL = Decimal("0.001")


app = bottle.Bottle()

# Installs the request handler plugin
app.install(request_handler)

print("[COMPOUND] Loaded")

reCHAINid = "<pdb_chain_id:re:.*>"  # TODO is any better possible?
rePDBid = "<pdbid:re:\d{1}\w{3}>"
reHETid = "<hetcode:re:\w{1,18}>"
reSlashOrNot = "<:re:/{0,1}>"

"""
@api {get} compound/atoms/:hetcode Get atoms for a compound
@apiName GetCompoundAtoms
@apiGroup Compounds
@apiDescription This set of calls provides information about atoms in a chemical groups defined in the PDB Chemical Component Dictionary. For each atoms, properties such as name, 
element symbol, ideal coordinates, stereochemistry, aromaticity (when applicable), etc. are available.
@apiVersion 2.0.0

@apiParam {String} hetcode=ADP Hetcode for the compound

@apiSuccess {String} stereo This flag indicates R/S stereochemistry for atoms (or E/Z for bonds) when applicable, else false.
@apiSuccess {Boolean} leaving_atom Flag to indicate if this is a leaving atom.
@apiSuccess {String} pdb_name Alternate name of the atom.
@apiSuccess {String} aromatic Indicates whether the atom or bond is within an aromatic substructure.
@apiSuccess {String} element Element type of the atom.
@apiSuccess {String} ideal_x X of calculated idealized coordinates.
@apiSuccess {String} ideal_y Y of calculated idealized coordinates.
@apiSuccess {String} ideal_z Z of calculated idealized coordinates.
@apiSuccess {Float} charge Formal charge on the atom.
@apiSuccess {String} atom_name Atom identifier from chemical components dictionary.

@apiExample {json=./examples/success/compound_atoms.json} apiSuccessExample Example success response JSON
"""


@app.get("/atoms/" + reHETid + reSlashOrNot)
def get_compound_atoms_api(hetcode):
    hetcode = hetcode.strip("'")

    api_result = {hetcode: {}}
    response, response_status = get_compound_atoms(hetcode)

    if response_status == 404:
        bottle.response.status = 404
        return api_result

    api_result[hetcode] = response

    bottle.response.status = 200
    return api_result


def get_compound_atoms(chem_comp_id):
    query = """
    MATCH (ligand:ChemicalComponent {ID:$chem_comp_id})-[:HAS_ATOM]->(atom:Atom)
    RETURN atom.STEREO_CONFIG, atom.LEAVING_ATOM_FLAG, atom.ALT_ATOM_ID, atom.AROMATIC_FLAG, atom.ELEMENT_SYMBOL, atom.MODEL_CARTN_X_IDEAL, atom.MODEL_CARTN_Y_IDEAL, 
    atom.MODEL_CARTN_Z_IDEAL, atom.CHARGE, atom.ID
    """

    mappings = run_query(query, parameters={"chem_comp_id": chem_comp_id})

    if len(mappings) == 0:
        return {}, 404

    result_list = []

    for mapping in mappings:
        (
            stereo_config,
            leaving_atom_flag,
            alt_atom_id,
            aromatic_flag,
            element_symbol,
            x_ideal,
            y_ideal,
            z_ideal,
            charge,
            atom_id,
        ) = mapping

        if leaving_atom_flag == "N":
            leaving_atom_flag = False
        else:
            leaving_atom_flag = True

        if aromatic_flag == "N":
            aromatic_flag = False
        else:
            aromatic_flag = True

        if stereo_config == "N":
            stereo_config = False
        else:
            stereo_config = True

        result_list.append(
            {
                "stereo": stereo_config,
                "leaving_atom": leaving_atom_flag,
                "pdb_name": alt_atom_id,
                "aromatic": aromatic_flag,
                "element": element_symbol,
                "ideal_y": None if y_ideal is None else float("%.3f" % float(y_ideal)),
                "ideal_x": None if x_ideal is None else float("%.3f" % float(x_ideal)),
                "charge": None if charge is None else float(charge),
                "ideal_z": None if z_ideal is None else float("%.3f" % float(z_ideal)),
                "atom_name": atom_id,
            }
        )

    return result_list, 200


"""
@api {get} compound/bonds/:hetcode Get bonds for a compound
@apiName GetCompoundBonds
@apiGroup Compounds
@apiDescription This set of calls provides information about bonds in a chemical groups defined in the PDB Chemical Component Dictionary. For each bond, properties such as 
atom names, bond type, stereochemistry and aromaticity (when applicable) etc. are available.
@apiVersion 2.0.0

@apiParam {String} hetcode=ADP Hetcode for the compound

@apiSuccess {String} stereo This flag indicates R/S stereochemistry for atoms (or E/Z for bonds) when applicable, else false.
@apiSuccess {Boolean} leaving_atom Flag to indicate if this is a leaving atom.
@apiSuccess {String} atom_1 Name of atom 1 in the bond.
@apiSuccess {String} atom_2 Name of atom 2 in the bond.
@apiSuccess {String} bond_type A string describing bond type.
@apiSuccess {Integer} bond_order A number describing bond order.
@apiSuccess {Float} ideal_length Target length of the bond, e.g. for refinement.
@apiSuccess {Boolean} aromatic Indicates whether the atom or bond is within an aromatic substructure.

@apiExample {json=./examples/success/compound_bonds.json} apiSuccessExample Example success response JSON
"""


@app.get("/bonds/" + reHETid + reSlashOrNot)
def get_compound_bonds_api(hetcode):
    hetcode = hetcode.strip("'")

    api_result = {hetcode: {}}

    response, response_status = get_compound_bonds(hetcode)

    if response_status == 404:
        bottle.response.status = 404
        return api_result

    api_result[hetcode] = response

    bottle.response.status = 200
    return api_result


def get_compound_bonds(chem_comp_id):
    query = """
    MATCH (ligand:ChemicalComponent {ID:$chem_comp_id})-[:HAS_ATOM]->(atom1:Atom)-[relation:BONDS]->(atom2:Atom)
    RETURN relation.STEREO_CONFIG, relation.AROMATIC_FLAG, relation.ORDER_NUMBER, relation.VALUE_ORDER, relation.VALUE_DIST_IDEAL, atom1.ID, atom2.ID ORDER BY atom1.ID
    """

    mappings = run_query(query, parameters={"chem_comp_id": chem_comp_id})

    if len(mappings) == 0:
        return {}, 404

    result_list = []

    for mapping in mappings:
        (
            stereo_config,
            aromatic_flag,
            bond_order,
            bond_type,
            ideal_length,
            atom1,
            atom2,
        ) = mapping

        if stereo_config == "N":
            stereo_config = False
        else:
            stereo_config = True

        if aromatic_flag == "N":
            aromatic_flag = False
        else:
            aromatic_flag = True

        result_list.append(
            {
                "stereo": stereo_config,
                "atom_1": atom1,
                "atom_2": atom2,
                "bond_type": bond_type,
                "bond_order": float(bond_order),
                "ideal_length": float("%.3f" % float(ideal_length)),
                "aromatic": aromatic_flag,
            }
        )

    return result_list, 200


"""
@api {get} compound/in_pdb/:hetcode Get PDB entries that contains the compound
@apiName GetEntriesInCompound
@apiGroup Compounds
@apiDescription This set of calls returns a list of PDB entries that contain the compound defined in the PDB Chemical Component Dictionary.
@apiVersion 2.0.0

@apiParam {String} hetcode=ADP Hetcode for the compound

@apiExample {json=./examples/success/compound_in_pdb.json} apiSuccessExample Example success response JSON
"""


@app.get("/in_pdb/" + reHETid + reSlashOrNot)
def get_compound_in_pdb_api(hetcode):
    hetcode = hetcode.strip("'")

    api_result = {hetcode: {}}

    response, response_status = get_compound_in_pdb(hetcode)

    if response_status == 404:
        bottle.response.status = 404
        return {}

    api_result[hetcode] = response

    bottle.response.status = 200
    return api_result


def get_compound_in_pdb(chem_comp_id):
    query = """
    MATCH (chem_comp:ChemicalComponent {ID:$chem_comp_id})<-[:IS_A]-(entity:Entity)
    WITH SPLIT(entity.UNIQID,'_')[0] AS entry_id ORDER BY entry_id
    RETURN COLLECT(DISTINCT entry_id)
    """

    mappings = run_query(query, parameters={"chem_comp_id": chem_comp_id})

    if len(mappings) != 0:
        items = mappings[0][0]

        if len(items) == 0:
            return [], 404

        return items, 200
    else:
        return [], 404


"""
@api {get} compound/cofactors/ Get summary of Cofactors
@apiName GetCofactorSummary
@apiGroup Compounds
@apiDescription This call provides a summary of the cofactor annotation in the PDB.
@apiVersion 2.0.0

@apiSuccess {String[]} EC A list of enzyme classes (ECs), where cofactors of this class play a biological role.
@apiSuccess {String[]} cofactors A list of het codes annotated as members of the cofactor class.

@apiExample {json=./examples/success/compound_co_factors.json} apiSuccessExample Example success response JSON
"""


@app.get("/cofactors" + reSlashOrNot)
def get_compound_co_factors_api():
    response, response_status = get_compound_co_factors()

    api_result = {}

    if response_status == 404:
        bottle.response.status = 404
        return api_result

    api_result = response

    bottle.response.status = 200
    return api_result


def get_compound_co_factors():
    query = """
    MATCH (co:COFactorClass)-[:HAS_EC]->(ec:EC), (co)<-[:ACTS_AS_COFACTOR]-(ligand:ChemicalComponent)
    WITH co.COFACTOR_ID AS cofactor_id, co.NAME AS co_name, ec.EC_NUM AS ec_num, ligand.ID AS ligand_id ORDER BY ec_num, ligand_id
    RETURN cofactor_id, co_name, collect(DISTINCT split(ec_num,' ')[1]) as ec, collect(DISTINCT ligand_id) as cofactors
    """

    api_result = {}

    mappings = run_query(query)

    if len(mappings) == 0:
        return {}, 404

    for mapping in mappings:
        (cofactor_class, cofactor_class_name, list_ec, list_cofactors) = mapping

        api_result[cofactor_class_name] = [{"EC": list_ec, "cofactors": list_cofactors}]

    return api_result, 200


"""
@api {get} compound/cofactors/het/:hetcode Get similar hetcodes
@apiName GetSimilarHetCodes
@apiGroup Compounds
@apiDescription This call provides hetcodes similar to the query parameter with the same functional annotation.
@apiVersion 2.0.0

@apiParam {String} hetcode=TDP Hetcode for the compound

@apiSuccess {String} acts_as Indicates the type if it acts as a cofactor, reactant etc.
@apiSuccess {Object[]} chem_comp_ids A list of Objects including hetcode and it's name.
@apiSuccess {String} chem_comp_id Chemical component identifier, the so-called 3-letter code, but it need not be 3-letter long!
@apiSuccess {String} name A name for the hetcode

@apiExample {json=./examples/success/compound_co_factors_het.json} apiSuccessExample Example success response JSON
"""


@app.get("/cofactors/het/" + reHETid + reSlashOrNot)
def get_compound_co_factors_het_api(hetcode):
    """ """

    hetcode = hetcode.strip("'")

    api_result = {hetcode: []}

    co_factor_result, response_status = get_compound_co_factors_het(hetcode)

    if response_status == 404:
        bottle.response.status = 404
        return api_result

    api_result[hetcode] = [co_factor_result]

    bottle.response.status = 200
    return api_result


def get_compound_co_factors_het(het_code):
    query = """
    MATCH (src_ligand:ChemicalComponent {ID:$het_code})-[:ACTS_AS_COFACTOR]->(co:COFactorClass)
    OPTIONAL MATCH (co)<-[:ACTS_AS_COFACTOR]->(dest_ligand:ChemicalComponent)
    RETURN co.NAME, dest_ligand.ID, dest_ligand.NAME
    """

    api_result = {"acts_as": "cofactor", "chem_comp_ids": []}
    mappings = run_query(query, parameters={"het_code": het_code})

    if len(mappings) == 0:
        return None, 404

    for mapping in mappings:
        (co_name, dest_ligand_id, dest_ligand_name) = mapping

        api_result["class"] = co_name
        api_result["chem_comp_ids"].append(
            {"chem_comp_id": dest_ligand_id, "name": dest_ligand_name}
        )

    return api_result, 200


"""
@api {get} compound/substructures/:hetcode Get substructures for the compound
@apiName GetSubstructures
@apiGroup Compounds
@apiDescription This call provides information about scaffold and fragments found in the structure.
@apiVersion 2.0.0

@apiParam {String} hetcode=ATP Hetcode for the compound

@apiExample {json=./examples/success/compound_substructures.json} apiSuccessExample Example success response JSON
"""


@app.get("/substructures/" + reHETid + reSlashOrNot)
def get_compound_substructures_api(hetcode):
    hetcode = hetcode.strip("'")

    fragments = get_compound_fragments(hetcode)
    scaffold = get_compound_scaffold(hetcode)

    if not fragments and not scaffold:
        bottle.response.status = 404
        return {}

    api_result = {hetcode: [{"fragments": fragments, "scaffold": scaffold}]}

    bottle.response.status = 200
    return api_result


def get_compound_fragments(het_code):
    query = """
    MATCH (chem_comp:ChemicalComponent {ID:$het_code})-[:CONTAINS_FRAGMENT]->(fragment:Fragment)<-[rel:IS_PART_OF_FRAGMENT]-(atom:Atom), (atom)<-[:HAS_ATOM]-(chem_comp)
    return fragment.NAME, atom.ID, toInteger(rel.SUBSTRUCT_ORDINAL) as ordinal order by ordinal
    """

    mappings = run_query(query, parameters={"het_code": het_code})
    api_result = {}

    if not mappings:
        return api_result

    for m in mappings:
        name, atom_id, fragment_id = m

        if name not in api_result:
            api_result[name] = []

        if len(api_result[name]) != fragment_id:
            api_result[name].append([])

        api_result[name][fragment_id - 1].append(atom_id)

    return api_result


def get_compound_scaffold(het_code):
    query = """
    MATCH (ch:ChemicalComponent {ID:$het_code})-[:CONTAINS_SCAFFOLD]->(sc:Scaffold)<-[:IS_PART_OF_SCAFFOLD]-(at:Atom)<-[:HAS_ATOM]-(ch) RETURN sc.UNIQID, at.ID
    """

    mappings = run_query(query, parameters={"het_code": het_code})
    api_result = {}

    if not mappings:
        return api_result

    smiles = mappings[0][0]
    atom_names = [x[1] for x in mappings]

    api_result[smiles] = atom_names

    return api_result


"""
@api {get} compound/summary/:hetcode Get summary information for the compound
@apiName GetCompoundSummary
@apiGroup Compounds
@apiDescription This call provides summary information for a chemical component
@apiVersion 2.0.0

@apiParam {String} hetcode=ATP Hetcode for the compound

@apiSuccess {String} name The name of the chemical component.
@apiSuccess {Boolean} released A flag denoting if the hetcode is released or not.
@apiSuccess {String} superseded_by A hetcode which superseeds the hetcode in query.
@apiSuccess {String} formula The chemical formula of the component.
@apiSuccess {String} inchi The full INCHI of the component.
@apiSuccess {String} inchi_key INCHI key of the component.
@apiSuccess {Object[]} smiles The SMILES representation of the component (could be multiple).
@apiSuccess {Object} ww_pdb_info An info object which provides details of the chemical component from wwPDB.
@apiSuccess {Date} defined_at The date the chemical component was defined in wwPDB.
@apiSuccess {Date} modified The modified date of the chemical component in wwPDB.
@apiSuccess {String} modification_flag Y/N denoting the modification status of the chemical component in wwPDB.
@apiSuccess {String} polymer_type This flag denotes if the chemical component is a polymer or non-polymer in wwPDB.
@apiSuccess {String} standard_parent The standard chemical component defined in wwPDB.
@apiSuccess {Object[]} functional_annotations A list of functional annotations for the chemical component.
@apiSuccess {Object[]} cross_links Cross references for this chemical component from other resources.
@apiSuccess {String} resource The external resource name.
@apiSuccess {String} resource_id The external resource id.
@apiSuccess {Object[]} synonyms A list of synomyms for the chemical component from other sources.
@apiSuccess {String} origin The resource which provides synonym for the chemical component.
@apiSuccess {String} value The synonym provided by the resource.
@apiSuccess {Object} phys_chem_properties An object of physical chemical properties.
@apiSuccess (phys_chem_properties) {Float} crippen_mr Wildman-Crippen molar refractivity is a common descriptor accounting for molecular size and polarizability.
@apiSuccess (phys_chem_properties) {Integer} num_atom_stereo_centers Number of atoms with four attachments different from each other.
@apiSuccess (phys_chem_properties) {Float} crippen_clog_p Octanol/Water partition coeficient predicted using Wildman-Crippen method.
@apiSuccess (phys_chem_properties) {Integer} num_rings Number of rings.
@apiSuccess (phys_chem_properties) {Integer} num_rotatable_bonds Number of single bonds, not part of a ring bound to a nonterminal heavy atom.
@apiSuccess (phys_chem_properties) {Integer} num_heteroatoms Number of non oxygen and non carbon atoms.
@apiSuccess (phys_chem_properties) {Float} fraction_csp3 Fraction of C atoms that are SP3 hybridized.
@apiSuccess (phys_chem_properties) {Integer} num_aromatic_rings Number of aromatic rings for the molecule.
@apiSuccess (phys_chem_properties) {Float} exactmw Total mass of the molecule.
@apiSuccess (phys_chem_properties) {Integer} num_spiro_atoms Atoms shared between rings that share exactly one atom.
@apiSuccess (phys_chem_properties) {Integer} num_heavy_atoms Number of non hydrogen atoms.
@apiSuccess (phys_chem_properties) {Integer} num_aliphatic_rings Number of aliphatic rings.
@apiSuccess (phys_chem_properties) {Integer} num_hbd Number of hydrogen bond donors.
@apiSuccess (phys_chem_properties) {Integer} num_saturated_heterocycles Number of saturated heterocycles.
@apiSuccess (phys_chem_properties) {Float} tpsa Topological surface area.
@apiSuccess (phys_chem_properties) {Integer} num_bridgehead_atoms Number of atoms shared between rings that share at least two bonds.
@apiSuccess (phys_chem_properties) {Integer} num_aromatic_heterocycles Number or aromatic rings with at least two different elements.
@apiSuccess (phys_chem_properties) {Float} labute_asa Accessible surface area accorging to the Labute' definition.
@apiSuccess (phys_chem_properties) {Integer} num_hba Number of hydrogen bond acceptors.
@apiSuccess (phys_chem_properties) {Integer} num_amide_bonds Number of amide bonds.
@apiSuccess (phys_chem_properties) {Integer} num_saturated_rings Number of saturated rings.
@apiSuccess (phys_chem_properties) {Float} lipinski_hba Number of hydrogen bond acceptors according to Lipinsky definition.
@apiSuccess (phys_chem_properties) {Integer} num_unspec_atom_stereo_centers Number of unsuspected stereocenters.
@apiSuccess (phys_chem_properties) {Float} lipinski_hbd Number of hydrogen bond donors according to Lipinsky definition.
@apiSuccess (phys_chem_properties) {Integer} num_heterocycles Number or rings with at least two different elements.
@apiSuccess (phys_chem_properties) {Integer} num_aliphatic_heterocycles Number of aliphatic heterocycles.

@apiExample {json=./examples/success/compound_summary.json} apiSuccessExample Example success response JSON
"""


@app.get("/summary/" + reHETid + reSlashOrNot)
def get_compound_summary_api(hetcode):
    hetcode = hetcode.strip("'")

    summary = get_compound_summary(hetcode)

    if not summary:
        bottle.response.status = 404
        return summary

    api_result = {hetcode: [summary]}

    bottle.response.status = 200

    return api_result


def get_compound_summary(hetcode):
    query = """
    MATCH (x:ChemicalComponent {ID:$het_code})-[:IS_A_TARGET|:HAS_DRUGBANK|:HAS_CHEBI|:DESCRIBED_BY|:HAS_PHYSCHEM_PROPERTIES|:HAS_SYNONYM|:ACTS_AS_COFACTOR]-(y) return *
    """
    mappings = run_query(query, parameters={"het_code": hetcode})

    if not mappings:
        return {}

    het = mappings[0][0]
    properties = {}
    functional_annotations = []

    descriptors = [
        x[1] for x in mappings if str(list(x[1].labels)[0]) == "ChemicalComponentDesc"
    ]
    targets = [x[1] for x in mappings if str(list(x[1].labels)[0]) == "UniProt"]
    cross_links = [
        x.split("_", 1)
        for x in ast.literal_eval(mappings[0][0]["EXTERNAL_RESOURCES"][0])
    ]
    cofactors = [x[1] for x in mappings if str(list(x[1].labels)[0]) == "COFactorClass"]
    drugbanks = [
        x[1]["ID"] for x in mappings if str(list(x[1].labels)[0]) == "DrugBank"
    ]
    chebis = [x[1]["ID"] for x in mappings if str(list(x[1].labels)[0]) == "ChEBI"]

    tmp_properties = [
        x[1]._properties
        for x in mappings
        if str(list(x[1].labels)[0]) == "ChemicalComponentRDKITProperty"
    ]

    if tmp_properties:
        tmp_properties[0].pop("CHEM_COMP_ID")
        properties = {k.lower(): parse_number(v) for k, v in tmp_properties[0].items()}

    synonyms = [
        {"origin": x[1]["PROVENANCE"], "value": x[1]["NAME"]}
        for x in mappings
        if str(x[1].labels) == ":ChemicalComponentSynonym"
    ]

    cross_links = [{"resource": x[0], "resource_id": x[1]} for x in cross_links]

    for did in drugbanks:
        cross_links.append({"resource": "DrugBank", "resource_id": did})

    for cid in chebis:
        cross_links.append({"resource": "ChEBI", "resource_id": cid})

    if cofactors:
        functional_annotations.append(
            {
                "name": "cofactor",
                "description": f'{hetcode} is a cofactor in the cofactor class {cofactors[0]["NAME"]}.',
                "url": "https://doi.org/10.1093/bioinformatics/btz115",
            }
        )

    if targets:
        functional_annotations.append(
            {
                "name": "Pharmacologically active",
                "description": f'{hetcode} has a pharmacological effect on {len(targets)} target{("s" if len(targets) > 1 else "")}.',
                "url": f"https://drugbank.ca/drugs/{drugbanks[0]}",
            }
        )

    inchi = next((x["DESCRIPTOR"] for x in descriptors if x["TYPE"] == "InChI"), "")
    inchikey = next(
        (x["DESCRIPTOR"] for x in descriptors if x["TYPE"] == "InChIKey"), ""
    )
    smiles = next(
        (
            x["DESCRIPTOR"]
            for x in descriptors
            if x["TYPE"] == "SMILES" and x["PROGRAM"] == "OpenEye OEToolkits"
        ),
        "",
    )

    api_result = {
        "name": het["NAME"],
        "released": het["RELEASE_STATUS"] == "REL",
        "superseded_by": het["REPLACED_BY_CHEM_COMP_ID"]
        if "REPLACED_BY_CHEM_COMP_ID" in het
        else None,
        "formula": het["FORMULA"],
        "inchi": inchi,
        "inchi_key": inchikey,
        "smiles": smiles,
        "ww_pdb_info": {
            "defined_at": het["INITIAL_DATE"],
            "modified": het["MODIFIED_DATE"],
            "modification_flag": het["MODIFICATION_FLAG"],
            "polymer_type": het["TYPE_TEXT"],
            "standard_parent": het["MON_NSTD_PARENT_CHEM_COMP_ID"]
            if "MON_NSTD_PARENT_CHEM_COMP_ID" in het
            else None,
        },
        "functional_annotations": functional_annotations,
        "cross_links": cross_links,
        "synonyms": synonyms,
        "phys_chem_properties": properties,
    }

    return api_result


"""
@api {get} compound/similarity/:hetcode Get similar ligands
@apiName GetSimilarLigands
@apiGroup Compounds
@apiDescription This call provides ligands similar to the ligand of interest. Stereoisomers, ligands with the same scaffold and ligands with similarity over 60% defined by PARITY method are returned.
@apiVersion 2.0.0

@apiParam {String} hetcode=ATP Hetcode for the compound

@apiSuccess {Object[]} stereoisomers A list of stereoisomer hetcode objects.
@apiSuccess {Object[]} same_scaffold A list of hetcode objects which is part of the same scaffold.
@apiSuccess {String} chem_comp_id Chemical component identifier, the so-called 3-letter code, but it need not be 3-letter long!
@apiSuccess {String} name A name for the hetcode
@apiSuccess {Object[]} substructure_match A list of atom names in the hetcode that match with the hetcode in the query
@apiSuccess {Float} similarity_score Similarity score in the range of 0 to 1 defined by the PARITY method.

@apiExample {json=./examples/success/similar_ligands.json} apiSuccessExample Example success response JSON
"""


@app.get("/similarity/" + reHETid + reSlashOrNot)
def get_similar_ligands_api(hetcode):
    hetcode = hetcode.strip("'")

    similarites = get_similar_ligands(hetcode)

    if not any(x for x in similarites.values()):
        bottle.response.status = 404
        return similarites

    api_result = {hetcode: [similarites]}

    bottle.response.status = 200

    return api_result


def get_similar_ligands(hetcode):
    response = {"stereoisomers": [], "same_scaffold": [], "similar_ligands": []}
    similarities_q = 'MATCH (c:ChemicalComponent {ID:$het_code})-[s:HAS_SIMILARITY]-(other:ChemicalComponent {RELEASE_STATUS:"REL"}) return *'
    scaffolds_q = """MATCH (ch:ChemicalComponent {ID:$het_code})-[:CONTAINS_SCAFFOLD]->(sc:Scaffold)<-[:IS_PART_OF_SCAFFOLD]-(at:Atom)<-[:HAS_ATOM]-(ch1:ChemicalComponent)
                     WHERE ch1.ID <> $het_code and ch1.RELEASE_STATUS = 'REL' return ch1.ID, ch1.NAME, collect(at.ID)"""
    stereoisomers_q = """MATCH (c:ChemicalComponent {ID:$het_code})-[:IS_PART_OF_STEREO_CLASS]->(y)<-[:IS_PART_OF_STEREO_CLASS]-(x:ChemicalComponent)
                        WHERE x.RELEASE_STATUS = 'REL' return x.ID, x.NAME"""

    temp_similar_ligands = run_query(similarities_q, {"het_code": hetcode})
    similar_ligands = {}
    same_scaffold = run_query(scaffolds_q, {"het_code": hetcode})

    temp_stereoisomers = run_query(stereoisomers_q, {"het_code": hetcode})
    stereoisomers = {x[0]: x[1] for x in temp_stereoisomers}  # MAN: alpha-D-mannose

    for x in temp_similar_ligands:
        score = float(x["s"]["SIMILARITY_SCORE"])
        atom_mapping = {}

        for z in x["s"]["ATOM_MAPPING"].split(";"):
            splitted = z.split(":")
            atom_mapping[splitted[0]] = splitted[1]

        # swap atom mapping as the atom-atom relation ship is stored in HEA, HEM order.
        if x["c"]["ID"] < x["other"]["ID"]:
            atom_mapping = [v for k, v in atom_mapping.items()]
        else:
            atom_mapping = [k for k, v in atom_mapping.items()]

        if x["other"]["id"] not in stereoisomers:
            similar_ligands[x["other"]["ID"]] = {
                "chem_comp_id": x["other"]["ID"],
                "name": x["other"]["NAME"],
                "similarity_score": score,
                "substructure_match": atom_mapping,
            }

    response["stereoisomers"] = [
        {"chem_comp_id": k, "name": v} for k, v in stereoisomers.items()
    ]

    for x in same_scaffold:
        if x[0] not in stereoisomers:
            response["same_scaffold"].append(
                {"chem_comp_id": x[0], "name": x[1], "substructure_match": x[2]}
            )
            response["same_scaffold"][-1]["similarity_score"] = (
                similar_ligands[x[0]]["similarity_score"]
                if x[0] in similar_ligands
                else None
            )

    response["similar_ligands"] = [
        v for k, v in similar_ligands.items() if k not in stereoisomers
    ]

    return response
