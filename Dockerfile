# Use an official Python runtime as a parent image, with a specific version for predictability
FROM python:3.9-slim  as builder

# Set environment variables to reduce Python package issues and turn off buffering
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Set work directory
WORKDIR /var/www/pdbe-graph-api

# Install system dependencies
# We combine RUN commands to reduce layers and use '&&' to ensure that if one command fails, the docker build fails
RUN apt-get update && apt-get install -y --no-install-recommends \
    apache2 \
    apache2-dev \
    libapache2-mod-wsgi-py3 \
    build-essential \
    vim \
    nano \
    curl \
    lsof \
    git \
    nodejs \
    npm \
    && apt-get clean \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/*

# Copy over and install the requirements
COPY ./requirements.txt requirements.txt

RUN pip install -r requirements.txt

# Install apidoc
COPY ./package.json package.json
RUN npm install


# Copy over the apache configuration file and enable the site
COPY ./conf/httpd-pdbe-graph-api-docker.conf /etc/apache2/sites-available/httpd-pdbe-graph-api-docker.conf
COPY ./conf/pdbe-graph-api-docker.wsgi /var/www/pdbe-graph-api/conf/pdbe-graph-api-docker.wsgi
RUN a2ensite httpd-pdbe-graph-api-docker
RUN a2enmod headers

# Copy over the application, configuration and API doc files
COPY ./pdbegraphapi pdbegraphapi/
COPY ./doc_template ./doc_template
COPY ./apidoc.json ./apidoc.json

# Create API doc
RUN ./node_modules/apidoc/bin/apidoc -i pdbegraphapi/ -o pdbe_doc -t doc_template

RUN a2dissite 000-default.conf
RUN a2ensite httpd-pdbe-graph-api-docker.conf

EXPOSE 80

# Use exec format to ensure proper signal handling and apache runs in foreground
CMD ["apache2ctl", "-D", "FOREGROUND"]
