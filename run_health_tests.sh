function kill_app(){
    kill -9 `ps -aef | grep app/app.py | grep -v grep | awk '{print $2}'`
}

if [ -d venv ]; then

    echo 'Sourcing the virtual environment'
    source venv/bin/activate

fi

export HOST=http://127.0.0.1:5000

# kill any existing app
kill_app

# run the application
python app/app.py > /dev/null 2>&1 &

sleep 2

echo 'Running tests'
py.test -v -s -n 4

if [[ $? -ne 0 ]]; then
    echo "Health check failed!!!"
    kill_app
    exit 1
else
    # kill current app
    kill_app
    exit 0
fi
