from deepdiff import DeepDiff
import json
import requests
import random
import sys
import os

PWD = os.path.dirname(os.path.realpath(__file__))
CONFIG_FILE = PWD + "/../conf/endpoint_config.json"
SAMPLE_FILES_PATH = PWD + "/sample_files"


def get_random_input(input_list, no_of_randoms):
    result = []

    for x in random.sample(range(0, len(input_list)), no_of_randoms):
        result.append(input_list[x])
    # return ['3enr','1cbs','4mq2','5nxg','5npw','3j8g']
    # return ['3enr','1cbs','3f5x','4mq2','5nxg','5npw','3j8g']
    # return ['1zua/1']
    # return ['1jw6']
    return result


if __name__ == "__main__":

    args = sys.argv

    if(len(args) != 3):
        print('Usage: python api_compare.py <number of entry/accession/hetcode> <full difference report (Y or N)>')
        exit()

    num_random_data = int(args[1])
    difference_flag = args[2]

    config = json.loads(open(CONFIG_FILE).read())
    entries = open('{}/entry.csv'.format(SAMPLE_FILES_PATH)).read().split("\n")
    accessions = open('{}/accession.csv'.format(SAMPLE_FILES_PATH)).read().split("\n")
    hetcodes = open('{}/hetcode.csv'.format(SAMPLE_FILES_PATH)).read().split("\n")
    entities = open('{}/entity.csv'.format(SAMPLE_FILES_PATH)).read().split("\n")

    print("Loaded configuration and data files")

    print("Running tests for {} random inputs".format(num_random_data))

    for key in config.keys():

        # skip few keys
        if(key not in ["common"] and config[key]["compare_enabled"] == "true"):
            print("Running test for {}".format(key))
            success = 0
            failed = 0
            param_list = []
            failed_data = []

            if(config[key]["param"] == "pdbid"):
                param_list = get_random_input(entries, num_random_data)
            elif(config[key]["param"] == "accession"):
                param_list = get_random_input(accessions, num_random_data)
            elif(config[key]["param"] == "hetcode"):
                param_list = get_random_input(hetcodes, num_random_data)
            elif(config[key]["param"] == "pdbid/entity_id"):
                param_list = get_random_input(entities, num_random_data)
            else:
                param_list = [None]

            for data in param_list:
                endpoint1_url = config["common"]["endpoint1_host"] +config[key]["endpoint1_url"]
                if data is not None:
                    endpoint1_url = endpoint1_url +"/{}".format(data)

                print(endpoint1_url)
                endpoint1_json = requests.get(endpoint1_url).json()
                endpoint2_url = config["common"]["endpoint2_host"] +config[key]["endpoint2_url"]
                if data is not None:
                    endpoint2_url = endpoint2_url +"/{}".format(data)
                print(endpoint2_url)
                
                endpoint2_json = requests.get(endpoint2_url).json()
                difference = DeepDiff(endpoint1_json, endpoint2_json, ignore_order=False, report_repetition=True)
                
                if(len(difference.keys()) == 0):
                    success += 1
                else:
                    failed += 1
                    if difference_flag == 'Y' or difference_flag == 'y':
                        failed_data.append((data, difference))
                    else:
                        failed_data.append(data)

            print("Completed test for {}".format(key))
            print("Success: {}, Failed: {}".format(success,failed))
            if failed_data:
                print("Failed for {}".format(failed_data))
            print('Endpoint1 URL: {}'.format(endpoint1_url))
            print('Endpoint2 URL: {}'.format(endpoint2_url))
            
