import json
import requests
import random
import sys
import numpy as np
import copy
import os
from boom.boom import load, calc_stats

PWD = os.path.dirname(os.path.realpath(__file__))
CONFIG_FILE = PWD + "/../conf/endpoint_config.json"
SAMPLE_FILES_PATH = PWD + "/sample_files"

def get_random_input(input_list, no_of_randoms):
    result = []

    for x in random.sample(range(0, len(input_list)), no_of_randoms):
        result.append(input_list[x])
    #return ['4mq2','3enr','1cbs','5nxg','5npw','3j8g']
    return result

def get_average_timings(list_of_timings):
    
    if(len(list_of_timings) >= 2):
        list_avg_timings = []
        for stat in list_of_timings:
            list_avg_timings.append(getattr(stat, 'avg'))        
        return np.mean(list_avg_timings)
    else:
        print("Not enough values to get average")

if __name__ == "__main__":

    args = sys.argv

    if(len(args) != 4):
        print('Usage: python api_benchmark.py <number of random inputs> <number of requests> <number of concurrent requests>')
        exit()

    num_rand_inputs = int(args[1])
    num_conn = int(args[2])
    num_concurrent = int(args[3])

    config = json.loads(open(CONFIG_FILE).read())
    entries = open('{}/entry.csv'.format(SAMPLE_FILES_PATH)).read().split(",")
    accessions = open('{}/accession.csv'.format(SAMPLE_FILES_PATH)).read().split(",")
    hetcodes = open('{}/hetcode.csv'.format(SAMPLE_FILES_PATH)).read().split(",")
    residues = open('{}/residue.csv'.format(SAMPLE_FILES_PATH)).read().split(",")
    dict_timings = {}

    print("Loaded configuration and data files")

    print("Running tests for {} random inputs with {} total requests and {} concurrent requests".format(num_rand_inputs, num_conn, num_concurrent))

    for key in config.keys():
        
        # skip few keys
        if(config[key]["category"] not in ["Additional", "common"]):

            if(config[key]["benchmark_enabled"] == "false"): continue

            dict_timings[key] = ([],[])

            print("Running benchmark for {}".format(key))
            success = 0
            failed = 0
            param_list = []
            failed_data = []

            if(config[key]["param"] == "pdbid"):
                param_list = get_random_input(entries, num_rand_inputs)
            elif(config[key]["param"] == "accession"):
                param_list = get_random_input(accessions, num_rand_inputs)
            elif(config[key]["param"] == "hetcode"):
                param_list = get_random_input(hetcodes, num_rand_inputs)
            else:
                param_list = [None]
        
            for data in param_list:

                endpoint1_url = config["common"]["endpoint1_host"] +config[key]["endpoint1_url"]
                endpoint2_url = config["common"]["endpoint2_host"] +config[key]["endpoint2_url"]
                
                if(data is not None):
                    endpoint1_url+= "/{}".format(data)
                    endpoint2_url+= "/{}".format(data)

                endpoint1_response = requests.get(endpoint1_url)
                endpoint2_response = requests.get(endpoint2_url)

                # check for valid response, also does a warm up
                if(endpoint1_response.status_code == 200):
                    
                    # measure the performance for a single request
                    endpoint1_stats = calc_stats(load(endpoint1_url, 1, 1, 0, 'GET', None, 'application/json', None, quiet=True))
                    print(endpoint1_url)
                    print('A single request')
                    print(endpoint1_stats)

                    # measure performance for concurrent requests
                    endpoint1_stats = calc_stats(load(endpoint1_url, num_conn, num_concurrent, 0, 'GET', None, 'application/json', None, quiet=True))
                    print(endpoint1_url)
                    print('Total {} requests, {} concurrent requests, average timings'.format(num_conn, num_concurrent))
                    print(endpoint1_stats)

                    if(endpoint1_stats is not None):
                        dict_timings[key][0].append(endpoint1_stats)

                else:
                    print("Error in response for {}".format(endpoint1_url))

                # check for valid response, also does a warm up
                if(endpoint2_response.status_code == 200):

                    # measure the performance for a single request
                    endpoint2_stats = calc_stats(load(endpoint2_url, 1, 1, 0, 'GET', None, 'application/json', None, quiet=True))
                    print(endpoint2_url)
                    print('A single request')
                    print(endpoint2_stats)
                    
                    # measure performance for concurrent requests
                    endpoint2_stats = calc_stats(load(endpoint2_url, num_conn, num_concurrent, 0, 'GET', None, 'application/json', None, quiet=True))
                    print(endpoint2_url)
                    print('Total {} requests, {} concurrent requests, average timings'.format(num_conn, num_concurrent))
                    print(endpoint2_stats)

                    if(endpoint2_stats is not None):
                        dict_timings[key][1].append(endpoint2_stats)
                else:
                    print("Error in response for {}".format(endpoint2_url))

                print("\n\n")

        # calls in additional category doesn't have an endpoint1, as they are newly introduced
        elif(config[key]["category"] == "Additional"):
            
            if(config[key]["benchmark_enabled"] == "false"): continue

            print('Running performance test for {}'.format(key))

            param_list = []

            if(config[key]["param"] == "pdbid/entityid/residueid"):
                param_list = get_random_input(residues, 1)
            elif(config[key]["param"] == "accession"):
                param_list = get_random_input(accessions, 1)
            
            data = param_list[0]

            endpoint_url = config["common"]["endpoint2_host"] +config[key]["endpoint_url"]

            if(data is not None):
                endpoint_url += "/{}".format(data)

            endpoint_response = requests.get(endpoint_url)

            if(endpoint_response.status_code == 200):
                
                # measure the performance for a single request
                endpoint_stats = calc_stats(load(endpoint_url, 1, 1, 0, 'GET', None, 'application/json', None, quiet=True))
                print(endpoint_url)
                print('A single request')
                print(endpoint_stats)

                # measure performance for concurrent requests
                endpoint_stats = calc_stats(load(endpoint_url, num_conn, num_concurrent, 0, 'GET', None, 'application/json', None, quiet=True))
                print(endpoint_url)
                print('Total {} requests, {} concurrent requests, average timings'.format(num_conn, num_concurrent))
                print(endpoint_stats)
            else:
                print("Error in response for {}".format(endpoint_url))

    for key in dict_timings.keys():
        
        # endpoint1_average_timings = get_average_timings_list_of_list(dict_timings[key][0])
        # endpoint2_average_timings = get_average_timings_list_of_list(dict_timings[key][1])

        endpoint1_average_timings = get_average_timings(dict_timings[key][0])
        endpoint2_average_timings = get_average_timings(dict_timings[key][1])

        print(key)
        print('Endpoint 1 average timings')
        print(endpoint1_average_timings)
        print('Endpoint 2 average timings')
        print(endpoint2_average_timings)

